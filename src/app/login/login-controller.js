(function () {
    var mod = angular.module('jsTemplateApp.login.controller',
        [ ]);

    mod.config(function config($stateProvider, constants) {

        $stateProvider.state('login', {
            url: constants.navigationUrl.login,
            views: {
                "main": {
                    controller: 'LoginController',
                    templateUrl: 'login/login.tpl.html'
                }
            },
            data: { pageTitle: 'Login PIAF Page' }
        });
    });

    mod.controller('LoginController', function ($scope, $location, $log, $modal, $q, uiUtils, constants) {

        /*****************
         * Initialization
         ******************/

        /*****************
         * scope FUNCTIONS Declaration
         ******************/

        function initScope() {


        }

        $scope.goToHome = function(){
          $location.path('/todo');
        };

        /*****************
         * scope EVENTS, WATCHES
         ******************/

        $q.all($scope.bootPromiseHash).then(function (resultHash) {
            $log.log("bootPromiseHash.then");

            initScope();

        }, function (reason) {
            $log.error("reason");
        });


    });

}());
