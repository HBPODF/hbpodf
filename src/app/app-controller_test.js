

describe('AppCtrl', function () {
    var appCtrl, scope;

    beforeEach( module('ui.router') );
    beforeEach( module('webStorageModule'));
    beforeEach( module('jsTemplateApp.common.services.constants'));
    beforeEach( module('jsTemplateApp' ) );
    beforeEach( module('jsTemplateApp.controller'));

 

    beforeEach(inject(function ($controller, $rootScope, _constants_) {
        scope = $rootScope.$new();
        appCtrl = $controller('AppCtrl', {
            $scope: scope,
            constants: _constants_,
        });
    }));


    it( 'should exist', inject( function() {
        expect( appCtrl ).toBeTruthy();
    }));
});

