﻿(function () {
    var mod = angular.module('jsTemplateApp.admin1.controller',
        [ ]);

    mod.config(function config($stateProvider, constants) {

        $stateProvider.state('admin1', {
            url: constants.navigationUrl.admin1,
            views: {
                "main": {
                    controller: 'Admin1Controller',
                    templateUrl: 'admin/admin1.tpl.html'
                }
            },
            data: { pageTitle: 'Administration' }
        });
    });

    mod.controller('Admin1Controller', function ($scope, $location, $log, $modal, $q, uiUtils, constants, habilitationService) {

        /*****************
        * Initialization
        ******************/

        /*****************
        * scope FUNCTIONS Declaration
        ******************/

        function initScope() {

            initRights();

            if (!$scope.rights.canAccessPage) {
                uiUtils.navigateToPath(constants.navigationUrl.home);
            }
        }

        function initRights() {
            $scope.rights.canAccessPage = habilitationService.hasAutorisation([constants.authItems.viewAdmin, constants.authItems.viewAll], constants.autorizations.can_access_page);
        }


        /*****************
        * scope EVENTS, WATCHES
        ******************/

        $q.all($scope.bootPromiseHash).then(function (resultHash) {
            $log.log("bootPromiseHash.then");

            initScope();

        }, function (reason) {
            $log.error("reason");
        });


    });

}());
