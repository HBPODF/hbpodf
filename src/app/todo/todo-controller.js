(function () {
    var mod = angular.module('jsTemplateApp.todo.controller',
        [ ]);

    mod.config(function config($stateProvider, constants) {

        $stateProvider.state('todo', {
            url: constants.navigationUrl.todo,
            views: {
                "main": {
                    controller: 'TodoController',
                    templateUrl: 'todo/todo.tpl.html'
                }
            },
            data: { pageTitle: 'ToDO TED Page' }
        });
    });

    mod.controller('TodoController', function ($scope, $location, $log, $modal, $q, uiUtils, constants, globalContext) {

        /*****************
         * Initialization
         ******************/

        /*****************
         * scope FUNCTIONS Declaration
         ******************/

        $scope.imageSource = globalContext.imageSource;

        function initScope() {

        }

        $scope.navToHome = function(){
          $location.path('/home');
            globalContext.imageSource = 'assets/images/todo-après.png';
        };

        /*****************
         * scope EVENTS, WATCHES
         ******************/

        $q.all($scope.bootPromiseHash).then(function (resultHash) {
            $log.log("bootPromiseHash.then");

            initScope();

        }, function (reason) {
            $log.error("reason");
        });

        $scope.showNav = false;

        $scope.imgSrcNav = 'assets/images/piaf-pictos_23.png';

        $scope.showNavFunc = function(){
            $scope.showNav = ! $scope.showNav;
            if($scope.showNav === true){
                $scope.imgSrcNav = 'assets/images/piaf-pictos_26.png';
            }
            else{
                $scope.imgSrcNav = 'assets/images/piaf-pictos_23.png';
            }
        };

        $scope.navToTodo = function(){
            $location.path('/todo');
        };

        $scope.navLogin = function(){
            $location.path('/login');
        };


    });

}());
