﻿/*
 * jpe 6/9/22015
 */
(function () {
    var mod = angular.module('jsTemplateApp.accessDenied.controller',
        []);


    mod.config(function config($stateProvider, constants) {

        $stateProvider.state('accessDenied', {
            url: constants.navigationUrl.accessDenied,
            views: {
                "main": {
                    controller: function ($scope) { },
                    templateUrl: 'accessDenied/access-denied.tpl.html'
                }
            },
            data: { pageTitle: 'Access denied' }
        });
    });



}());
