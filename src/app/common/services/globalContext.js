﻿(function () {
    //https://github.com/fredricrylander/angular-webstorage

    var mod = angular.module('jsTemplateApp.common.services.globalContext', []);

    //Global service for global variables
    mod.factory("globalContext", [ '$http',


        function($http, Quartier) {
            var _this = this;

            _this._data = {
                user: window.user,
                authenticated: !! window.user,
                cntChecked:0,

                iris: {
                    "type": "FeatureCollection",
                    "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },

                    "features": [
                        { "type": "Feature", "properties": { "DEPCOM": "44109", "NOM_COM": "Nantes", "IRIS": "0105", "DCOMIRIS": "441090105", "NOM_IRIS": "Bretagne", "TYP_IRIS": "H" }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -1.556885434949962, 47.217061423840128 ], [ -1.557016662325209, 47.217048142296662 ], [ -1.557173357687622, 47.217023036785214 ], [ -1.559183239995733, 47.215752656982765 ], [ -1.560646213607703, 47.215524594553351 ], [ -1.560688612668339, 47.215712296692523 ], [ -1.561651374401082, 47.216295955779358 ], [ -1.562577446876364, 47.217070786994988 ], [ -1.562679201689847, 47.217645715898861 ], [ -1.562630983970054, 47.217701362053425 ], [ -1.562355224272935, 47.217883026149579 ], [ -1.560879483797695, 47.218897055056537 ], [ -1.560268935163904, 47.219344939925392 ], [ -1.560232410400685, 47.219382449553784 ], [ -1.559368687073633, 47.219966549487687 ], [ -1.559204053663737, 47.220054192481804 ], [ -1.559074288866893, 47.220086355821515 ], [ -1.555086119216195, 47.220504853312029 ], [ -1.555041971523647, 47.220452484182935 ], [ -1.554895574993871, 47.220286908002571 ], [ -1.554852189482417, 47.220243526509364 ], [ -1.555648378263386, 47.219720915297778 ], [ -1.556773097596689, 47.219014481747315 ], [ -1.55684615901768, 47.2189395545252 ], [ -1.557062270536839, 47.218678552123315 ], [ -1.557134788412033, 47.218441147003993 ], [ -1.557165444299287, 47.2181783365642 ], [ -1.557091556969463, 47.217775267888683 ], [ -1.557003709583911, 47.217363640784235 ], [ -1.556885434949962, 47.217061423840128 ] ] ] } },
                        { "type": "Feature", "properties": { "DEPCOM": "44109", "NOM_COM": "Nantes", "IRIS": "0106", "DCOMIRIS": "441090106", "NOM_IRIS": "Decré-Cathédrale", "TYP_IRIS": "H" }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -1.546857995933008, 47.216337511290604 ], [ -1.547087833747923, 47.216238307258983 ], [ -1.54670118554149, 47.2157214526217 ], [ -1.54659625502745, 47.21559030912438 ], [ -1.546698742018229, 47.215550213288473 ], [ -1.546774846760424, 47.215511153569203 ], [ -1.553838699849564, 47.21250112777804 ], [ -1.554119777794949, 47.212850778041307 ], [ -1.553582613074114, 47.213071201356684 ], [ -1.55326275223553, 47.213201013559001 ], [ -1.552546928332064, 47.213499698886125 ], [ -1.551718936260286, 47.214036708845704 ], [ -1.551167447386801, 47.214400793852576 ], [ -1.550791505110111, 47.214649943127739 ], [ -1.550691306013967, 47.214717005875741 ], [ -1.550341748693063, 47.214965206439231 ], [ -1.55093740676601, 47.214797507853369 ], [ -1.551377654102523, 47.214681004897557 ], [ -1.552168894487882, 47.21451462743989 ], [ -1.552805576672384, 47.21438137631548 ], [ -1.553404368516652, 47.21425862968772 ], [ -1.553768772241825, 47.214181171340506 ], [ -1.554015718585651, 47.214126368104942 ], [ -1.554932843699199, 47.213855823850068 ], [ -1.555049273484298, 47.213824189305235 ], [ -1.555092707301934, 47.21386819997425 ], [ -1.555150824849192, 47.213929307527685 ], [ -1.556077834260112, 47.215029057401715 ], [ -1.556188457325111, 47.215241128562987 ], [ -1.556424816824163, 47.215843496969789 ], [ -1.556526801206722, 47.216109743612428 ], [ -1.556696861248762, 47.216554476404724 ], [ -1.556869195183904, 47.217025992249162 ], [ -1.556885434949962, 47.217061423840128 ], [ -1.557003709583911, 47.217363640784235 ], [ -1.557091556969463, 47.217775267888683 ], [ -1.557165444299287, 47.2181783365642 ], [ -1.557134788412033, 47.218441147003993 ], [ -1.557062270536839, 47.218678552123315 ], [ -1.55684615901768, 47.2189395545252 ], [ -1.556773097596689, 47.219014481747315 ], [ -1.555648378263386, 47.219720915297778 ], [ -1.554852189482417, 47.220243526509364 ], [ -1.554895574993871, 47.220286908002571 ], [ -1.554832737724664, 47.220326352735306 ], [ -1.552905434176727, 47.222574518703432 ], [ -1.552933857485189, 47.222753754244827 ], [ -1.552945280492597, 47.222888569990957 ], [ -1.552642607562503, 47.222909489787725 ], [ -1.552551019385923, 47.222922108851471 ], [ -1.552338411527134, 47.222912433353947 ], [ -1.552253239443961, 47.222844542738784 ], [ -1.552165834613279, 47.222765918664713 ], [ -1.55209377960003, 47.222696610440927 ], [ -1.551715878631818, 47.222296655743001 ], [ -1.551132632776065, 47.221652277868934 ], [ -1.55095721110692, 47.221451767492503 ], [ -1.550501824643953, 47.220919680528304 ], [ -1.548629420055515, 47.218658501628745 ], [ -1.547861298303153, 47.21771477317764 ], [ -1.547293084920735, 47.216943450814988 ], [ -1.546903660755655, 47.216407949055416 ], [ -1.546857995933008, 47.216337511290604 ] ] ] } },
                        { "type": "Feature", "properties": { "DEPCOM": "44109", "NOM_COM": "Nantes", "IRIS": "0404", "DCOMIRIS": "441090404", "NOM_IRIS": "Talensac-Pont Morand", "TYP_IRIS": "H" }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -1.552945280492597, 47.222888569990957 ], [ -1.552933857485189, 47.222753754244827 ], [ -1.552905434176727, 47.222574518703432 ], [ -1.554832737724664, 47.220326352735306 ], [ -1.554895574993871, 47.220286908002571 ], [ -1.555041971523647, 47.220452484182935 ], [ -1.555086119216195, 47.220504853312029 ], [ -1.559074288866893, 47.220086355821515 ], [ -1.559204053663737, 47.220054192481804 ], [ -1.559438209011113, 47.220161930739927 ], [ -1.559951679522747, 47.220285630390258 ], [ -1.560455781683054, 47.220454875672424 ], [ -1.560770642696764, 47.22057773822187 ], [ -1.561031248847587, 47.220685152861442 ], [ -1.561608246848997, 47.220933402504606 ], [ -1.562559060723346, 47.22184279725267 ], [ -1.56306560789873, 47.222508181434435 ], [ -1.563201132712288, 47.222701230070108 ], [ -1.562334555825895, 47.223096090779919 ], [ -1.559372671954484, 47.224537558897694 ], [ -1.559131145733194, 47.224655191940109 ], [ -1.55913148832251, 47.224971248742243 ], [ -1.55908407633944, 47.225036510066829 ], [ -1.558995551613142, 47.22508526463826 ], [ -1.55443004258293, 47.227609534934778 ], [ -1.553769642007687, 47.228086391840982 ], [ -1.553532695406735, 47.228249010256846 ], [ -1.553484255250226, 47.228296003434956 ], [ -1.553412443554196, 47.228389003905399 ], [ -1.553225104672252, 47.228549670766583 ], [ -1.553090750205221, 47.228510767442536 ], [ -1.552670486061552, 47.228237916081987 ], [ -1.552641805538227, 47.22821190064581 ], [ -1.552703968212987, 47.228164368293243 ], [ -1.553221432700373, 47.227694040488778 ], [ -1.553476621000624, 47.227440528739216 ], [ -1.553680478782703, 47.227206168581205 ], [ -1.553611818191389, 47.226784948957537 ], [ -1.553338106760963, 47.225228083916242 ], [ -1.553044704107708, 47.223751077150688 ], [ -1.55293485474409, 47.222924870279812 ], [ -1.552945280492597, 47.222888569990957 ] ] ] } }
                    ]
                },
                clients:[
                    {
                        "coteb":"038",
                        "comax":"0027659",
                        "siret":"41025895800025",
                        "enseigne":"MC DONALDS",
                        "raison-social":"EURL TIVOLI",
                        "adresse1":"",
                        "adresse2":"12 RUE SAINT-LEONARD",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"7",
                        "risque-coface":"OR",
                        "code-marche":"1",
                        "lib-marche":"ent",
                        "code-naf":"5610C",
                        "date-creation":"199612",
                        "date-relation":"200311",
                        "captial":"3 032 000 €",
                        "respresentant-civilite":"MR",
                        "respresentant-prenom":"Bruno",
                        "respresentant-nom":"FRANDEBOEUF",
                        "tel":"0240335979",
                        "fax":"0240850132",
                        "segment-creation":"+5 ans",
                        "anciente":"18",
                        "mail":"annie.frandeboeuf@wanadoo.fr",
                        "pnb":"2 247 €",
                        "pnb-ress":"2 225 €",
                        "pnb-emp":"402 €",
                        "pnb-comm":"-380 €",
                        "geoloc": {
                            "lat": "47.217018",
                            "lon": "-1.555418"
                        },
                        "credits-en-cours": {
                            "credit": {
                                "nopret":"",
                                "montant-initial":"",
                                "date-debut":"",
                                "capital-restant":"",
                                "date-capital-restant":"",
                                "nb-mensualites":""
                            }
                        }
                    },
                    {
                        "coteb":"038",
                        "comax":"0027661",
                        "siret":"41026337000026",
                        "enseigne":"MC DONALDS",
                        "raison-social":"SAS OCEANE",
                        "adresse1":"",
                        "adresse2":"3 Rue de Gorges",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"9",
                        "risque-coface":"OR",
                        "code-marche":"1",
                        "lib-marche":"ent",
                        "code-naf":"5610C",
                        "date-creation":"199612",
                        "date-relation":"200311",
                        "captial":"4 250 600 €",
                        "respresentant-civilite":"MME",
                        "respresentant-prenom":"Virginie",
                        "respresentant-nom":"ASPE",
                        "tel":"0241349257",
                        "fax":"0251141036",
                        "segment-creation":"+5 ans",
                        "anciente":"18",
                        "mail":"darc2@wanadoo.fr",
                        "pnb":"28 €",
                        "pnb-ress":"2 982 €",
                        "pnb-emp":"164 €",
                        "pnb-comm":"-3 118 €",
                        "geoloc": {
                            "lat": "47.214038",
                            "lon": "-1.558288"
                        }
                    },
                    {
                        "coteb":"038",
                        "comax":"0256551",
                        "siret":"51183903700013",
                        "enseigne":"SUSHI SHOP",
                        "raison-social":"SARL GELAU",
                        "adresse1":"",
                        "adresse2":"11 RUE PARE",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"2",
                        "risque-coface":"OR",
                        "code-marche":"2",
                        "lib-marche":"pro",
                        "code-naf":"5610C",
                        "date-creation":"200904",
                        "date-relation":"200904",
                        "captial":"1 488 200 €",
                        "respresentant-civilite":"MR",
                        "respresentant-prenom":"Gerard",
                        "respresentant-nom":"RATHERY",
                        "tel":"0663318856",
                        "fax":"",
                        "segment-creation":"+5 ans",
                        "anciente":"5",
                        "mail":"gerard.rathery@live.fr",
                        "pnb":"4 323 €",
                        "pnb-ress":"2 016 €",
                        "pnb-emp":"7 €",
                        "pnb-comm":"2 300 €",
                        "geoloc": {
                            "lat": "47.216732",
                            "lon": "-1.559228"
                        }
                    },
                    {
                        "coetb":"038",
                        "comax":"0510672",
                        "siret":"45067420500023",
                        "enseigne":"PIZZA SPRINT",
                        "raison-social":"CARRILLO MICKAEL",
                        "adresse1":"",
                        "adresse2":"5 RUE DE BUDAPEST",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"6",
                        "risque-coface":"VE",
                        "code-marche":"2",
                        "lib-marche":"pro",
                        "code-naf":"5610C",
                        "date-creation":"200310",
                        "date-relation":"201501",
                        "captial":"207 400 €",
                        "respresentant-civilite":"MR",
                        "respresentant-prenom":"Mickael",
                        "respresentant-nom":"CARRILLO",
                        "tel":"0687083571",
                        "fax":"",
                        "segment-creation":"+5 ans",
                        "anciente":"11",
                        "mail":"",
                        "pnb":"288 €",
                        "pnb-ress":"1 €",
                        "pnb-emp":"38 €",
                        "pnb-comm":"249 €",
                        "geoloc": {
                            "lat": "47.216430",
                            "lon": "-1.560318"
                        }
                    },
                    {
                        "coteb":"038",
                        "comax":"0371474",
                        "siret":"53944696300016",
                        "enseigne":"SUBWAY",
                        "raison-social":"SAS M.G.P.S.",
                        "adresse1":"",
                        "adresse2":"3 ALLEE DUQUESNE",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"6",
                        "risque-coface":"RO",
                        "code-marche":"2",
                        "lib-marche":"pro",
                        "code-naf":"5610C",
                        "date-creation":"201202",
                        "date-relation":"201201",
                        "captial":"205 071 €",
                        "respresentant-civilite":"MR",
                        "respresentant-prenom":"Philippe",
                        "respresentant-nom":"SALOME",
                        "tel":"0687253335",
                        "fax":"",
                        "segment-creation":"2-5 ans",
                        "anciente":"3",
                        "mail":"salomeph@wanadoo.fr",
                        "pnb":"1 128 €",
                        "pnb-ress":"-90 €",
                        "pnb-emp":"830 €",
                        "pnb-comm":"389 €",
                        "geoloc": {
                            "lat": "47.217057",
                            "lon": "-1.556492"
                        }
                    },
                    {
                        "coteb":"038",
                        "comax":"3427194",
                        "siret":"85580209600013",
                        "enseigne":"LA TAVERNE DE MAITRE KANTER",
                        "raison-social":"SA CAFE LE CONTINENTAL",
                        "adresse1":"",
                        "adresse2":"1 PLACE ROYALE",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"13",
                        "risque-coface":"OR",
                        "code-marche":"1",
                        "lib-marche":"ent",
                        "code-naf":"5610A",
                        "date-creation":"195501",
                        "date-relation":"199411",
                        "captial":"3 969 078 €",
                        "respresentant-civilite":"",
                        "respresentant-prenom":"",
                        "respresentant-nom":"CHABOT",
                        "tel":"0240357961",
                        "fax":"",
                        "segment-creation":"+5 ans",
                        "anciente":"60",
                        "mail":"maitrekanternantes@wanadoo.fr",
                        "pnb":"1 061 €",
                        "pnb-ress":"134 €",
                        "pnb-emp":"29 €",
                        "pnb-comm":"898 €",
                        "geoloc": {
                            "lat": "47.214599",
                            "lon": "-1.557898"
                        }
                    },
                    {
                        "coteb":"038",
                        "comax":"3635921",
                        "siret":"41061729400047",
                        "enseigne":"",
                        "raison-social":"SARL 2 P",
                        "adresse1":"",
                        "adresse2":"24 RUE CREBILLON",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"3",
                        "risque-coface":"OR",
                        "code-marche":"1",
                        "lib-marche":"ent",
                        "code-naf":"5610C",
                        "date-creation":"199701",
                        "date-relation":"201112",
                        "captial":"1 583 000 €",
                        "respresentant-civilite":"",
                        "respresentant-prenom":"",
                        "respresentant-nom":"",
                        "tel":"0240717405",
                        "fax":"",
                        "segment-creation":"+5 ans",
                        "anciente":"18",
                        "mail":"p.pontais@wanadoo.fr",
                        "pnb":"-3 562 €",
                        "pnb-ress":"1 863 €",
                        "pnb-emp":"0 €",
                        "pnb-comm":"-5 425 €",
                        "geoloc": {
                            "lat": "47.213901",
                            "lon": "-1.561665"
                        }
                    },
                    {
                        "coteb":"038",
                        "comax":"5223403",
                        "siret":"42267933200011",
                        "enseigne":"BELGIAN BEER CAFE",
                        "raison-social":"EURL EPICES ET COMPTOIR",
                        "adresse1":"",
                        "adresse2":"10 ALLEE DUQUESNE",
                        "code-postal":"44000",
                        "ville":"NANTES",
                        "note-mcdono":"3",
                        "risque-coface":"OR",
                        "code-marche":"1",
                        "lib-marche":"ent",
                        "code-naf":"5610A",
                        "date-creation":"199903",
                        "date-relation":"199902",
                        "captial":"1 650 200 €",
                        "respresentant-civilite":"MR",
                        "respresentant-prenom":"Dominique",
                        "respresentant-nom":"JAUD",
                        "tel":"0240200950",
                        "fax":"",
                        "segment-creation":"+5 ans",
                        "anciente":"16",
                        "mail":"Lecoq.nantes@orange.fr",
                        "pnb":"3 049 €",
                        "pnb-ress":"2 745 €",
                        "pnb-emp":"174 €",
                        "pnb-comm":"129 €",
                        "geoloc": {
                            "lat": "47.218654",
                            "lon": "-1.556873"
                        }
                    }
                ],
                population:[
                    {
                        "IRIS": 441090105,
                        "REG": 52,
                        "REG2016": 52,
                        "DEP": 44,
                        "UU2010": 44701,
                        "COM": 44109,
                        "LIBCOM": "Nantes",
                        "TRIRIS": 440551,
                        "GRD_QUART": 4410901,
                        "LIBIRIS": "Bretagne",
                        "TYP_IRIS": "H",
                        "MODIF_IRIS": "00",
                        "LAB_IRIS": 1,
                        "P12_POP": 1992,
                        "P12_POP0002": 75,
                        "P12_POP0305": 51,
                        "P12_POP0610": 37,
                        "P12_POP1117": 111,
                        "P12_POP1824": 427,
                        "P12_POP2539": 652,
                        "P12_POP4054": 249,
                        "P12_POP5564": 215,
                        "P12_POP6579": 112,
                        "P12_POP80P": 64,
                        "P12_POP0014": 215,
                        "P12_POP1529": 761,
                        "P12_POP3044": 442,
                        "P12_POP4559": 293,
                        "P12_POP6074": 175,
                        "P12_POP75P": 107,
                        "P12_POP0019": 365,
                        "P12_POP2064": 1451,
                        "P12_POP65P": 176,
                        "P12_POPH": 1020,
                        "P12_H0014": 118,
                        "P12_H1529": 392,
                        "P12_H3044": 245,
                        "P12_H4559": 144,
                        "P12_H6074": 83,
                        "P12_H75P": 38,
                        "P12_H0019": 213,
                        "P12_H2064": 731,
                        "P12_H65P": 76,
                        "P12_POPF": 972,
                        "P12_F0014": 97,
                        "P12_F1529": 368,
                        "P12_F3044": 197,
                        "P12_F4559": 149,
                        "P12_F6074": 92,
                        "P12_F75P": 69,
                        "P12_F0019": 152,
                        "P12_F2064": 720,
                        "P12_F65P": 100,
                        "C12_POP15P": 1777,
                        "C12_POP15P_CS1": 0,
                        "C12_POP15P_CS2": 52,
                        "C12_POP15P_CS3": 497,
                        "C12_POP15P_CS4": 377,
                        "C12_POP15P_CS5": 177,
                        "C12_POP15P_CS6": 74,
                        "C12_POP15P_CS7": 205,
                        "C12_POP15P_CS8": 395,
                        "C12_H15P": 902,
                        "C12_H15P_CS1": 0,
                        "C12_H15P_CS2": 34,
                        "C12_H15P_CS3": 294,
                        "C12_H15P_CS4": 188,
                        "C12_H15P_CS5": 52,
                        "C12_H15P_CS6": 56,
                        "C12_H15P_CS7": 87,
                        "C12_H15P_CS8": 191,
                        "C12_F15P": 875,
                        "C12_F15P_CS1": 0,
                        "C12_F15P_CS2": 18,
                        "C12_F15P_CS3": 202,
                        "C12_F15P_CS4": 188,
                        "C12_F15P_CS5": 125,
                        "C12_F15P_CS6": 18,
                        "C12_F15P_CS7": 118,
                        "C12_F15P_CS8": 204,
                        "P12_POP_FR": 1906,
                        "P12_POP_ETR": 85,
                        "P12_POP_IMM": 90,
                        "P12_PMEN": 1992,
                        "P12_PHORMEN": 0
                    },
                    {
                        "IRIS": 441090106,
                        "REG": 52,
                        "REG2016": 52,
                        "DEP": 44,
                        "UU2010": 44701,
                        "COM": 44109,
                        "LIBCOM": "Nantes",
                        "TRIRIS": 440551,
                        "GRD_QUART": 4410901,
                        "LIBIRIS": "Decré-Cathédrale",
                        "TYP_IRIS": "H",
                        "MODIF_IRIS": "00",
                        "LAB_IRIS": 1,
                        "P12_POP": 6005,
                        "P12_POP0002": 149,
                        "P12_POP0305": 81,
                        "P12_POP0610": 159,
                        "P12_POP1117": 262,
                        "P12_POP1824": 2058,
                        "P12_POP2539": 1715,
                        "P12_POP4054": 731,
                        "P12_POP5564": 394,
                        "P12_POP6579": 293,
                        "P12_POP80P": 163,
                        "P12_POP0014": 544,
                        "P12_POP1529": 3056,
                        "P12_POP3044": 1067,
                        "P12_POP4559": 695,
                        "P12_POP6074": 403,
                        "P12_POP75P": 241,
                        "P12_POP0019": 1066,
                        "P12_POP2064": 4483,
                        "P12_POP65P": 456,
                        "P12_POPH": 2917,
                        "P12_H0014": 231,
                        "P12_H1529": 1423,
                        "P12_H3044": 618,
                        "P12_H4559": 363,
                        "P12_H6074": 197,
                        "P12_H75P": 84,
                        "P12_H0019": 431,
                        "P12_H2064": 2297,
                        "P12_H65P": 188,
                        "P12_POPF": 3089,
                        "P12_F0014": 313,
                        "P12_F1529": 1633,
                        "P12_F3044": 449,
                        "P12_F4559": 333,
                        "P12_F6074": 205,
                        "P12_F75P": 156,
                        "P12_F0019": 635,
                        "P12_F2064": 2186,
                        "P12_F65P": 268,
                        "C12_POP15P": 5464,
                        "C12_POP15P_CS1": 0,
                        "C12_POP15P_CS2": 251,
                        "C12_POP15P_CS3": 1113,
                        "C12_POP15P_CS4": 841,
                        "C12_POP15P_CS5": 706,
                        "C12_POP15P_CS6": 320,
                        "C12_POP15P_CS7": 530,
                        "C12_POP15P_CS8": 1704,
                        "C12_H15P": 2688,
                        "C12_H15P_CS1": 0,
                        "C12_H15P_CS2": 164,
                        "C12_H15P_CS3": 608,
                        "C12_H15P_CS4": 456,
                        "C12_H15P_CS5": 306,
                        "C12_H15P_CS6": 256,
                        "C12_H15P_CS7": 240,
                        "C12_H15P_CS8": 660,
                        "C12_F15P": 2777,
                        "C12_F15P_CS1": 0,
                        "C12_F15P_CS2": 88,
                        "C12_F15P_CS3": 505,
                        "C12_F15P_CS4": 385,
                        "C12_F15P_CS5": 400,
                        "C12_F15P_CS6": 64,
                        "C12_F15P_CS7": 290,
                        "C12_F15P_CS8": 1045,
                        "P12_POP_FR": 5720,
                        "P12_POP_ETR": 285,
                        "P12_POP_IMM": 337,
                        "P12_PMEN": 5971,
                        "P12_PHORMEN": 34
                    },
                    {
                        "IRIS": 441090404,
                        "REG": 52,
                        "REG2016": 52,
                        "DEP": 44,
                        "UU2010": 44701,
                        "COM": 44109,
                        "LIBCOM": "Nantes",
                        "TRIRIS": 440631,
                        "GRD_QUART": 4410904,
                        "LIBIRIS": "Talensac-Pont Morand",
                        "TYP_IRIS": "H",
                        "MODIF_IRIS": "00",
                        "LAB_IRIS": 1,
                        "P12_POP": 3608,
                        "P12_POP0002": 64,
                        "P12_POP0305": 73,
                        "P12_POP0610": 85,
                        "P12_POP1117": 155,
                        "P12_POP1824": 1051,
                        "P12_POP2539": 1025,
                        "P12_POP4054": 504,
                        "P12_POP5564": 273,
                        "P12_POP6579": 249,
                        "P12_POP80P": 129,
                        "P12_POP0014": 294,
                        "P12_POP1529": 1670,
                        "P12_POP3044": 678,
                        "P12_POP4559": 472,
                        "P12_POP6074": 272,
                        "P12_POP75P": 222,
                        "P12_POP0019": 650,
                        "P12_POP2064": 2580,
                        "P12_POP65P": 377,
                        "P12_POPH": 1702,
                        "P12_H0014": 159,
                        "P12_H1529": 771,
                        "P12_H3044": 378,
                        "P12_H4559": 235,
                        "P12_H6074": 93,
                        "P12_H75P": 65,
                        "P12_H0019": 321,
                        "P12_H2064": 1259,
                        "P12_H65P": 122,
                        "P12_POPF": 1906,
                        "P12_F0014": 134,
                        "P12_F1529": 899,
                        "P12_F3044": 300,
                        "P12_F4559": 237,
                        "P12_F6074": 178,
                        "P12_F75P": 157,
                        "P12_F0019": 329,
                        "P12_F2064": 1321,
                        "P12_F65P": 255,
                        "C12_POP15P": 3307,
                        "C12_POP15P_CS1": 0,
                        "C12_POP15P_CS2": 57,
                        "C12_POP15P_CS3": 713,
                        "C12_POP15P_CS4": 677,
                        "C12_POP15P_CS5": 328,
                        "C12_POP15P_CS6": 165,
                        "C12_POP15P_CS7": 430,
                        "C12_POP15P_CS8": 936,
                        "C12_H15P": 1544,
                        "C12_H15P_CS1": 0,
                        "C12_H15P_CS2": 43,
                        "C12_H15P_CS3": 419,
                        "C12_H15P_CS4": 302,
                        "C12_H15P_CS5": 117,
                        "C12_H15P_CS6": 139,
                        "C12_H15P_CS7": 138,
                        "C12_H15P_CS8": 387,
                        "C12_F15P": 1762,
                        "C12_F15P_CS1": 0,
                        "C12_F15P_CS2": 15,
                        "C12_F15P_CS3": 294,
                        "C12_F15P_CS4": 374,
                        "C12_F15P_CS5": 212,
                        "C12_F15P_CS6": 26,
                        "C12_F15P_CS7": 292,
                        "C12_F15P_CS8": 549,
                        "P12_POP_FR": 3511,
                        "P12_POP_ETR": 97,
                        "P12_POP_IMM": 139,
                        "P12_PMEN": 3562,
                        "P12_PHORMEN": 46
                    }
                ],
                transports:[
                    {
                        "nom":"Abel Durand",
                        "longitude":-1.60339246,
                        "latitude":47.22015730
                    },
                    {
                        "nom":"Avenue Blanche",
                        "longitude":-1.58942059,
                        "latitude":47.22973100
                    },
                    {
                        "nom":"Angle Chaillou",
                        "longitude":-1.57211610,
                        "latitude":47.26976827
                    },
                    {
                        "nom":"Audubon",
                        "longitude":-1.72349280,
                        "latitude":47.21391693
                    },
                    {
                        "nom":"Aimé Delrue",
                        "longitude":-1.55023624,
                        "latitude":47.20908271
                    },
                    {
                        "nom":"Avenue des Pins",
                        "longitude":-1.59766118,
                        "latitude":47.25367844
                    },
                    {
                        "nom":"Aéroclub",
                        "longitude":-1.59825918,
                        "latitude":47.15935541
                    },
                    {
                        "nom":"Aéroport",
                        "longitude":-1.59741579,
                        "latitude":47.15686225
                    },
                    {
                        "nom":"Aérospatiale",
                        "longitude":-1.58793208,
                        "latitude":47.16078877
                    },
                    {
                        "nom":"Anatole France",
                        "longitude":-1.57193969,
                        "latitude":47.22393027
                    },
                    {
                        "nom":"L'Angebert",
                        "longitude":-1.45576884,
                        "latitude":47.14054089
                    },
                    {
                        "nom":"Angevinière",
                        "longitude":-1.61156236,
                        "latitude":47.24505212
                    },
                    {
                        "nom":"Airbus",
                        "longitude":-1.59185083,
                        "latitude":47.15840336
                    },
                    {
                        "nom":"Aiguillon",
                        "longitude":-1.49652294,
                        "latitude":47.25447241
                    },
                    {
                        "nom":"Allende",
                        "longitude":-1.62320432,
                        "latitude":47.17525341
                    },
                    {
                        "nom":"Aulne",
                        "longitude":-1.63893833,
                        "latitude":47.27685201
                    },
                    {
                        "nom":"Armoricaine",
                        "longitude":-1.64747915,
                        "latitude":47.25827307
                    },
                    {
                        "nom":"Américains",
                        "longitude":-1.56820260,
                        "latitude":47.23612591
                    },
                    {
                        "nom":"Ar Mor",
                        "longitude":-1.62367286,
                        "latitude":47.22801889
                    },
                    {
                        "nom":"Ampère",
                        "longitude":-1.55346472,
                        "latitude":47.26076174
                    },
                    {
                        "nom":"Les Anges",
                        "longitude":-1.63640188,
                        "latitude":47.27477771
                    },
                    {
                        "nom":"Antons",
                        "longitude":-1.62256356,
                        "latitude":47.25768995
                    },
                    {
                        "nom":"Apave",
                        "longitude":-1.64769437,
                        "latitude":47.22565984
                    },
                    {
                        "nom":"Ampère",
                        "longitude":-1.50789738,
                        "latitude":47.30227711
                    },
                    {
                        "nom":"Ambroise Paré",
                        "longitude":-1.60854526,
                        "latitude":47.22907805
                    },
                    {
                        "nom":"Aquitaine",
                        "longitude":-1.61478790,
                        "latitude":47.20976939
                    },
                    {
                        "nom":"Arago",
                        "longitude":-1.50480495,
                        "latitude":47.30346079
                    },
                    {
                        "nom":"Arbois",
                        "longitude":-1.58187869,
                        "latitude":47.25853899
                    },
                    {
                        "nom":"Arcades",
                        "longitude":-1.65355356,
                        "latitude":47.21203639
                    },
                    {
                        "nom":"Argonautes",
                        "longitude":-1.48182549,
                        "latitude":47.30242198
                    },
                    {
                        "nom":"Armorique",
                        "longitude":-1.44534382,
                        "latitude":47.27147205
                    },
                    {
                        "nom":"Antarès",
                        "longitude":-1.45970027,
                        "latitude":47.28171763
                    },
                    {
                        "nom":"La Chapelle Aulnay",
                        "longitude":-1.54342688,
                        "latitude":47.30838347
                    },
                    {
                        "nom":"L'Aufrère",
                        "longitude":-1.52507017,
                        "latitude":47.16804829
                    },
                    {
                        "nom":"Augé",
                        "longitude":-1.51736648,
                        "latitude":47.19496578
                    },
                    {
                        "nom":"Aveneaux",
                        "longitude":-1.62990901,
                        "latitude":47.23365905
                    },
                    {
                        "nom":"Barbinière",
                        "longitude":-1.48169027,
                        "latitude":47.15976810
                    },
                    {
                        "nom":"Bagatelle",
                        "longitude":-1.62005467,
                        "latitude":47.24345501
                    },
                    {
                        "nom":"Babinière",
                        "longitude":-1.54620749,
                        "latitude":47.25920497
                    },
                    {
                        "nom":"Bel Air",
                        "longitude":-1.55964729,
                        "latitude":47.22578752
                    },
                    {
                        "nom":"Balinière",
                        "longitude":-1.55440535,
                        "latitude":47.18462388
                    },
                    {
                        "nom":"Bois des Anses",
                        "longitude":-1.49477506,
                        "latitude":47.24669507
                    },
                    {
                        "nom":"La Barre",
                        "longitude":-1.38295034,
                        "latitude":47.30844177
                    },
                    {
                        "nom":"Bastille",
                        "longitude":-1.59693154,
                        "latitude":47.17903610
                    },
                    {
                        "nom":"Batignolles",
                        "longitude":-1.52966934,
                        "latitude":47.25552751
                    },
                    {
                        "nom":"Baugerie",
                        "longitude":-1.51948320,
                        "latitude":47.20417165
                    },
                    {
                        "nom":"Basse Orévière",
                        "longitude":-1.65893741,
                        "latitude":47.22040673
                    },
                    {
                        "nom":"Bd de La Baule",
                        "longitude":-1.60838431,
                        "latitude":47.22151772
                    },
                    {
                        "nom":"Boisbonne",
                        "longitude":-1.50837641,
                        "latitude":47.28686058
                    },
                    {
                        "nom":"Bois Cesbron",
                        "longitude":-1.62453869,
                        "latitude":47.26113484
                    },
                    {
                        "nom":"Basse Chênaie",
                        "longitude":-1.50919095,
                        "latitude":47.24117164
                    },
                    {
                        "nom":"Branchoire",
                        "longitude":-1.60415010,
                        "latitude":47.21787967
                    },
                    {
                        "nom":"Blanchet",
                        "longitude":-1.54329386,
                        "latitude":47.17166881
                    },
                    {
                        "nom":"Bd des Anglais",
                        "longitude":-1.58298444,
                        "latitude":47.22670758
                    },
                    {
                        "nom":"Baudelaire",
                        "longitude":-1.59314693,
                        "latitude":47.23365711
                    },
                    {
                        "nom":"Bodinières",
                        "longitude":-1.64190242,
                        "latitude":47.21000731
                    },
                    {
                        "nom":"Bout des Landes",
                        "longitude":-1.58226680,
                        "latitude":47.26744242
                    },
                    {
                        "nom":"Bourdonnais",
                        "longitude":-1.57998111,
                        "latitude":47.20555361
                    },
                    {
                        "nom":"Bd de Doulon",
                        "longitude":-1.52001726,
                        "latitude":47.22441834
                    },
                    {
                        "nom":"Bout des Pavés",
                        "longitude":-1.57629132,
                        "latitude":47.25269421
                    },
                    {
                        "nom":"Bourderies",
                        "longitude":-1.59592025,
                        "latitude":47.20663194
                    },
                    {
                        "nom":"Bd des Sports",
                        "longitude":-1.46876821,
                        "latitude":47.16559974
                    },
                    {
                        "nom":"Bourgault-Ducoudray",
                        "longitude":-1.54879097,
                        "latitude":47.20246652
                    },
                    {
                        "nom":"Bel Ebat",
                        "longitude":-1.63623963,
                        "latitude":47.27793573
                    },
                    {
                        "nom":"Bel endroit",
                        "longitude":-1.63557761,
                        "latitude":47.15329989
                    },
                    {
                        "nom":"Beauger",
                        "longitude":-1.55299443,
                        "latitude":47.23429829
                    },
                    {
                        "nom":"Beillevaire",
                        "longitude":-1.60741126,
                        "latitude":47.24956252
                    },
                    {
                        "nom":"Dépôt Le Bêle",
                        "longitude":-1.51035666,
                        "latitude":47.26472929
                    },
                    {
                        "nom":"Belges-Montbazon",
                        "longitude":-1.54098400,
                        "latitude":47.23461282
                    },
                    {
                        "nom":"Beaulieu",
                        "longitude":-1.53704358,
                        "latitude":47.20394287
                    },
                    {
                        "nom":"Belloc",
                        "longitude":-1.61469247,
                        "latitude":47.20310746
                    },
                    {
                        "nom":"Bessonneau",
                        "longitude":-1.71349968,
                        "latitude":47.21462890
                    },
                    {
                        "nom":"Belle Etoile",
                        "longitude":-1.45165075,
                        "latitude":47.28018099
                    },
                    {
                        "nom":"Beauvaiserie",
                        "longitude":-1.65444519,
                        "latitude":47.13627173
                    },
                    {
                        "nom":"Bonne Garde",
                        "longitude":-1.53124407,
                        "latitude":47.19684238
                    },
                    {
                        "nom":"Beaugency",
                        "longitude":-1.49024992,
                        "latitude":47.20379533
                    },
                    {
                        "nom":"Basse-Goulaine",
                        "longitude":-1.46850063,
                        "latitude":47.20829825
                    },
                    {
                        "nom":"Bourgonnière",
                        "longitude":-1.65545309,
                        "latitude":47.20737690
                    },
                    {
                        "nom":"Bourgogne",
                        "longitude":-1.58931505,
                        "latitude":47.24630711
                    },
                    {
                        "nom":"Bougrière",
                        "longitude":-1.47183238,
                        "latitude":47.25511025
                    },
                    {
                        "nom":"Bergerie",
                        "longitude":-1.62824352,
                        "latitude":47.23794964
                    },
                    {
                        "nom":"Bergeronnettes",
                        "longitude":-1.60124722,
                        "latitude":47.23950537
                    },
                    {
                        "nom":"Bougainville",
                        "longitude":-1.58359414,
                        "latitude":47.19894579
                    },
                    {
                        "nom":"Bois Hardy",
                        "longitude":-1.60059220,
                        "latitude":47.19827606
                    },
                    {
                        "nom":"Aristide Briand",
                        "longitude":-1.49516251,
                        "latitude":47.29639626
                    },
                    {
                        "nom":"Belle-Ile",
                        "longitude":-1.73275085,
                        "latitude":47.22439959
                    },
                    {
                        "nom":"Bignons",
                        "longitude":-1.49298004,
                        "latitude":47.19947164
                    },
                    {
                        "nom":"Basse Ile",
                        "longitude":-1.56313727,
                        "latitude":47.19567710
                    },
                    {
                        "nom":"Basse Indre",
                        "longitude":-1.67272277,
                        "latitude":47.19795002
                    },
                    {
                        "nom":"Bigeottière",
                        "longitude":-1.61953406,
                        "latitude":47.25608260
                    },
                    {
                        "nom":"Bois Jaulin",
                        "longitude":-1.63685031,
                        "latitude":47.17757559
                    },
                    {
                        "nom":"Beaujoire",
                        "longitude":-1.52593760,
                        "latitude":47.25880479
                    },
                    {
                        "nom":"Balavoine",
                        "longitude":-1.55480112,
                        "latitude":47.30637948
                    },
                    {
                        "nom":"Blancho",
                        "longitude":-1.73467907,
                        "latitude":47.21325202
                    },
                    {
                        "nom":"Boulodrome",
                        "longitude":-1.51540161,
                        "latitude":47.25762607
                    },
                    {
                        "nom":"Belem",
                        "longitude":-1.45300144,
                        "latitude":47.26563709
                    },
                    {
                        "nom":"Berligout",
                        "longitude":-1.67842053,
                        "latitude":47.21576638
                    },
                    {
                        "nom":"Bellestre",
                        "longitude":-1.69406522,
                        "latitude":47.14793376
                    },
                    {
                        "nom":"Bois de La Noé",
                        "longitude":-1.67169414,
                        "latitude":47.14186934
                    },
                    {
                        "nom":"Blordière",
                        "longitude":-1.53398357,
                        "latitude":47.17990799
                    },
                    {
                        "nom":"Bellevue",
                        "longitude":-1.47360365,
                        "latitude":47.23541838
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61140560,
                        "latitude":47.20871441
                    },
                    {
                        "nom":"Berlioz",
                        "longitude":-1.57163115,
                        "latitude":47.23961242
                    },
                    {
                        "nom":"Bernard",
                        "longitude":-1.52515008,
                        "latitude":47.16732508
                    },
                    {
                        "nom":"Blanchetière",
                        "longitude":-1.55609936,
                        "latitude":47.28967380
                    },
                    {
                        "nom":"Bignonnet",
                        "longitude":-1.46511578,
                        "latitude":47.17553725
                    },
                    {
                        "nom":"Bénélux",
                        "longitude":-1.49914288,
                        "latitude":47.25411511
                    },
                    {
                        "nom":"Bernanos",
                        "longitude":-1.61385671,
                        "latitude":47.20610844
                    },
                    {
                        "nom":"Bignon",
                        "longitude":-1.59786501,
                        "latitude":47.24925815
                    },
                    {
                        "nom":"BN",
                        "longitude":-1.49662542,
                        "latitude":47.18394946
                    },
                    {
                        "nom":"Bonneville",
                        "longitude":-1.53650309,
                        "latitude":47.24395007
                    },
                    {
                        "nom":"Centre de Bouguenais",
                        "longitude":-1.62332507,
                        "latitude":47.17867198
                    },
                    {
                        "nom":"Bourdonnières",
                        "longitude":-1.52078997,
                        "latitude":47.18746583
                    },
                    {
                        "nom":"Bouffay",
                        "longitude":-1.55235513,
                        "latitude":47.21459535
                    },
                    {
                        "nom":"Bourgeonnière",
                        "longitude":-1.56136733,
                        "latitude":47.25301947
                    },
                    {
                        "nom":"Bonde",
                        "longitude":-1.55214517,
                        "latitude":47.22261834
                    },
                    {
                        "nom":"Bourrelière",
                        "longitude":-1.49835581,
                        "latitude":47.15831350
                    },
                    {
                        "nom":"Boissière",
                        "longitude":-1.56600465,
                        "latitude":47.25493416
                    },
                    {
                        "nom":"Boutière",
                        "longitude":-1.46819367,
                        "latitude":47.15751304
                    },
                    {
                        "nom":"Bio Ouest Laënnec",
                        "longitude":-1.64187860,
                        "latitude":47.23279622
                    },
                    {
                        "nom":"Bouvre",
                        "longitude":-1.61311926,
                        "latitude":47.17821183
                    },
                    {
                        "nom":"Boulay Paty",
                        "longitude":-1.57627077,
                        "latitude":47.22891717
                    },
                    {
                        "nom":"Bois Raguenet",
                        "longitude":-1.60034915,
                        "latitude":47.26682671
                    },
                    {
                        "nom":"Bois Robillard",
                        "longitude":-1.51973294,
                        "latitude":47.23514550
                    },
                    {
                        "nom":"Bréchetière",
                        "longitude":-1.50345493,
                        "latitude":47.28468285
                    },
                    {
                        "nom":"Briandière",
                        "longitude":-1.69205782,
                        "latitude":47.18970915
                    },
                    {
                        "nom":"Bretonnière",
                        "longitude":-1.46289052,
                        "latitude":47.16651436
                    },
                    {
                        "nom":"Beauregard",
                        "longitude":-1.65649887,
                        "latitude":47.21076342
                    },
                    {
                        "nom":"Brains",
                        "longitude":-1.72303943,
                        "latitude":47.16961500
                    },
                    {
                        "nom":"Brouillard",
                        "longitude":-1.51446158,
                        "latitude":47.27395885
                    },
                    {
                        "nom":"Baronnière",
                        "longitude":-1.60378548,
                        "latitude":47.25959399
                    },
                    {
                        "nom":"Bretagne",
                        "longitude":-1.55835979,
                        "latitude":47.21718463
                    },
                    {
                        "nom":"Bruneau",
                        "longitude":-1.56181551,
                        "latitude":47.22832614
                    },
                    {
                        "nom":"Bobby Sands",
                        "longitude":-1.65277709,
                        "latitude":47.22998763
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58579949,
                        "latitude":47.23805046
                    },
                    {
                        "nom":"Bossis",
                        "longitude":-1.71280909,
                        "latitude":47.21249135
                    },
                    {
                        "nom":"Bois St-Louis",
                        "longitude":-1.58028406,
                        "latitude":47.24580363
                    },
                    {
                        "nom":"Bussonnière",
                        "longitude":-1.44376460,
                        "latitude":47.26972263
                    },
                    {
                        "nom":"Beau Soleil",
                        "longitude":-1.64478281,
                        "latitude":47.17766190
                    },
                    {
                        "nom":"Beau Soleil",
                        "longitude":-1.47502649,
                        "latitude":47.26094875
                    },
                    {
                        "nom":"Bastière",
                        "longitude":-1.45936421,
                        "latitude":47.14843797
                    },
                    {
                        "nom":"Bois St-Lys",
                        "longitude":-1.48194193,
                        "latitude":47.30034674
                    },
                    {
                        "nom":"BTAI",
                        "longitude":-1.66577687,
                        "latitude":47.22170065
                    },
                    {
                        "nom":"Bottière",
                        "longitude":-1.51838071,
                        "latitude":47.24023431
                    },
                    {
                        "nom":"Bouteillerie",
                        "longitude":-1.54360044,
                        "latitude":47.22146527
                    },
                    {
                        "nom":"Bertrand",
                        "longitude":-1.57568980,
                        "latitude":47.25712789
                    },
                    {
                        "nom":"Bugallière",
                        "longitude":-1.63768218,
                        "latitude":47.25996214
                    },
                    {
                        "nom":"Butte des Landes",
                        "longitude":-1.49378067,
                        "latitude":47.16080750
                    },
                    {
                        "nom":"Bougon",
                        "longitude":-1.61057452,
                        "latitude":47.17415582
                    },
                    {
                        "nom":"Buisson",
                        "longitude":-1.48751972,
                        "latitude":47.25990480
                    },
                    {
                        "nom":"Bouvardière",
                        "longitude":-1.59839758,
                        "latitude":47.23852184
                    },
                    {
                        "nom":"Bouvernière",
                        "longitude":-1.61145724,
                        "latitude":47.23807536
                    },
                    {
                        "nom":"Beauvoir",
                        "longitude":-1.63057890,
                        "latitude":47.17851239
                    },
                    {
                        "nom":"Centre de Bouaye",
                        "longitude":-1.69164746,
                        "latitude":47.14387489
                    },
                    {
                        "nom":"Gare de Bouaye",
                        "longitude":-1.68094178,
                        "latitude":47.13825909
                    },
                    {
                        "nom":"Buzardières",
                        "longitude":-1.65967180,
                        "latitude":47.20723065
                    },
                    {
                        "nom":"Cabrol",
                        "longitude":-1.58866916,
                        "latitude":47.20327644
                    },
                    {
                        "nom":"Centre de Carquefou",
                        "longitude":-1.49093632,
                        "latitude":47.29653649
                    },
                    {
                        "nom":"Cadoire",
                        "longitude":-1.48808660,
                        "latitude":47.25673385
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.42885050,
                        "latitude":47.27588403
                    },
                    {
                        "nom":"Californie",
                        "longitude":-1.58426052,
                        "latitude":47.19180767
                    },
                    {
                        "nom":"Canclaux",
                        "longitude":-1.57477787,
                        "latitude":47.21392654
                    },
                    {
                        "nom":"Capellia",
                        "longitude":-1.55497680,
                        "latitude":47.27971448
                    },
                    {
                        "nom":"Carquefou",
                        "longitude":-1.48536253,
                        "latitude":47.30374589
                    },
                    {
                        "nom":"Castorama",
                        "longitude":-1.50836563,
                        "latitude":47.25912172
                    },
                    {
                        "nom":"Cartier",
                        "longitude":-1.45556222,
                        "latitude":47.26816466
                    },
                    {
                        "nom":"Château D'Aux",
                        "longitude":-1.67455481,
                        "latitude":47.18536593
                    },
                    {
                        "nom":"Bac de Couëron",
                        "longitude":-1.75170597,
                        "latitude":47.20598337
                    },
                    {
                        "nom":"Charbonneau",
                        "longitude":-1.49083624,
                        "latitude":47.30068260
                    },
                    {
                        "nom":"Chapeau Berger",
                        "longitude":-1.46875604,
                        "latitude":47.17100393
                    },
                    {
                        "nom":"Corbinerie",
                        "longitude":-1.54228836,
                        "latitude":47.16143502
                    },
                    {
                        "nom":"Chemin Bleu",
                        "longitude":-1.52501183,
                        "latitude":47.17822772
                    },
                    {
                        "nom":"Cambronne",
                        "longitude":-1.54850983,
                        "latitude":47.22535286
                    },
                    {
                        "nom":"Couëron-Bougon",
                        "longitude":-1.73386051,
                        "latitude":47.22508102
                    },
                    {
                        "nom":"Cambronne",
                        "longitude":-1.50631578,
                        "latitude":47.20668305
                    },
                    {
                        "nom":"Choblèterie",
                        "longitude":-1.46140176,
                        "latitude":47.13621221
                    },
                    {
                        "nom":"Cochardières",
                        "longitude":-1.63320361,
                        "latitude":47.22994281
                    },
                    {
                        "nom":"Couchant",
                        "longitude":-1.51378720,
                        "latitude":47.27010866
                    },
                    {
                        "nom":"Cimetière de la Classerie",
                        "longitude":-1.55806356,
                        "latitude":47.17135033
                    },
                    {
                        "nom":"Champ de Courses",
                        "longitude":-1.56197183,
                        "latitude":47.24498315
                    },
                    {
                        "nom":"Chêne Creux",
                        "longitude":-1.54845981,
                        "latitude":47.17005354
                    },
                    {
                        "nom":"Chêne des Anglais",
                        "longitude":-1.57362988,
                        "latitude":47.26152094
                    },
                    {
                        "nom":"Cité Internationale des Congrès",
                        "longitude":-1.54514983,
                        "latitude":47.21366741
                    },
                    {
                        "nom":"Champ de Manoeuvre",
                        "longitude":-1.50074174,
                        "latitude":47.27270482
                    },
                    {
                        "nom":"Le Cellier",
                        "longitude":-1.34786995,
                        "latitude":47.32388492
                    },
                    {
                        "nom":"Champ de Foire",
                        "longitude":-1.61890717,
                        "latitude":47.12424017
                    },
                    {
                        "nom":"Cimetière de la Gaudinière",
                        "longitude":-1.58355651,
                        "latitude":47.24172945
                    },
                    {
                        "nom":"Chêne Gala",
                        "longitude":-1.53923829,
                        "latitude":47.18315380
                    },
                    {
                        "nom":"Chabossière",
                        "longitude":-1.68236264,
                        "latitude":47.21544882
                    },
                    {
                        "nom":"Chambelles",
                        "longitude":-1.53058634,
                        "latitude":47.23162967
                    },
                    {
                        "nom":"Champ Fleuri",
                        "longitude":-1.52477013,
                        "latitude":47.14374039
                    },
                    {
                        "nom":"Châtaigniers",
                        "longitude":-1.60287866,
                        "latitude":47.22215639
                    },
                    {
                        "nom":"Cholière",
                        "longitude":-1.60671900,
                        "latitude":47.25093723
                    },
                    {
                        "nom":"Charmes",
                        "longitude":-1.46120459,
                        "latitude":47.19115701
                    },
                    {
                        "nom":"Charbonnière",
                        "longitude":-1.64206735,
                        "latitude":47.26377405
                    },
                    {
                        "nom":"Chocolaterie",
                        "longitude":-1.52769143,
                        "latitude":47.24280469
                    },
                    {
                        "nom":"Champonnière",
                        "longitude":-1.50588807,
                        "latitude":47.16391658
                    },
                    {
                        "nom":"Chalâtres",
                        "longitude":-1.53787365,
                        "latitude":47.23183531
                    },
                    {
                        "nom":"Chaussée",
                        "longitude":-1.52106736,
                        "latitude":47.17664867
                    },
                    {
                        "nom":"Châtelets",
                        "longitude":-1.54061477,
                        "latitude":47.19686964
                    },
                    {
                        "nom":"Chanzy",
                        "longitude":-1.54497641,
                        "latitude":47.22403084
                    },
                    {
                        "nom":"CIFAM",
                        "longitude":-1.48814027,
                        "latitude":47.26672891
                    },
                    {
                        "nom":"Cimetière de St-Joseph-de-Porterie",
                        "longitude":-1.52068599,
                        "latitude":47.26852726
                    },
                    {
                        "nom":"Clos Ami",
                        "longitude":-1.65714695,
                        "latitude":47.21236227
                    },
                    {
                        "nom":"Clos Bonnet",
                        "longitude":-1.56358856,
                        "latitude":47.18917694
                    },
                    {
                        "nom":"Centre René Gauducheau",
                        "longitude":-1.63820369,
                        "latitude":47.23850738
                    },
                    {
                        "nom":"Clétras",
                        "longitude":-1.51216575,
                        "latitude":47.25494213
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.47229979,
                        "latitude":47.18925961
                    },
                    {
                        "nom":"Cimetière de La Montagne",
                        "longitude":-1.68645864,
                        "latitude":47.18729268
                    },
                    {
                        "nom":"Colonac",
                        "longitude":-1.60316273,
                        "latitude":47.22421827
                    },
                    {
                        "nom":"Clotais",
                        "longitude":-1.71820783,
                        "latitude":47.19302513
                    },
                    {
                        "nom":"Clos Roux",
                        "longitude":-1.74978398,
                        "latitude":47.19812466
                    },
                    {
                        "nom":"Closeaux",
                        "longitude":-1.62759856,
                        "latitude":47.23995337
                    },
                    {
                        "nom":"Châlet",
                        "longitude":-1.54444432,
                        "latitude":47.22945271
                    },
                    {
                        "nom":"Clouet",
                        "longitude":-1.51089539,
                        "latitude":47.16744204
                    },
                    {
                        "nom":"Calvaire",
                        "longitude":-1.51918269,
                        "latitude":47.14680985
                    },
                    {
                        "nom":"Clouzeaux",
                        "longitude":-1.49260107,
                        "latitude":47.17939999
                    },
                    {
                        "nom":"Cirque-Marais",
                        "longitude":-1.55723249,
                        "latitude":47.21804233
                    },
                    {
                        "nom":"Clémencière",
                        "longitude":-1.43568045,
                        "latitude":47.26953696
                    },
                    {
                        "nom":"Charmille",
                        "longitude":-1.60562586,
                        "latitude":47.25763967
                    },
                    {
                        "nom":"Commune 1871",
                        "longitude":-1.50009235,
                        "latitude":47.19914494
                    },
                    {
                        "nom":"Clos du Moulin",
                        "longitude":-1.52117338,
                        "latitude":47.14512195
                    },
                    {
                        "nom":"Camus",
                        "longitude":-1.57264675,
                        "latitude":47.21913271
                    },
                    {
                        "nom":"Chantiers Navals",
                        "longitude":-1.56753441,
                        "latitude":47.20885819
                    },
                    {
                        "nom":"Chenonceau",
                        "longitude":-1.43483996,
                        "latitude":47.27262643
                    },
                    {
                        "nom":"Congo",
                        "longitude":-1.61011890,
                        "latitude":47.23965236
                    },
                    {
                        "nom":"Chinon",
                        "longitude":-1.42912627,
                        "latitude":47.27416393
                    },
                    {
                        "nom":"Centaure",
                        "longitude":-1.62717988,
                        "latitude":47.17007260
                    },
                    {
                        "nom":"Corlay",
                        "longitude":-1.64412854,
                        "latitude":47.20794884
                    },
                    {
                        "nom":"Corbières",
                        "longitude":-1.48747186,
                        "latitude":47.20190618
                    },
                    {
                        "nom":"Cochard",
                        "longitude":-1.55052864,
                        "latitude":47.22402387
                    },
                    {
                        "nom":"Les Couëts",
                        "longitude":-1.58648859,
                        "latitude":47.18074336
                    },
                    {
                        "nom":"Chohonnière",
                        "longitude":-1.48589688,
                        "latitude":47.26311075
                    },
                    {
                        "nom":"Colinière",
                        "longitude":-1.50853300,
                        "latitude":47.23569971
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55637092,
                        "latitude":47.21346902
                    },
                    {
                        "nom":"Corniche",
                        "longitude":-1.59468551,
                        "latitude":47.25278917
                    },
                    {
                        "nom":"Coquelicots",
                        "longitude":-1.72917198,
                        "latitude":47.22254421
                    },
                    {
                        "nom":"Conraie",
                        "longitude":-1.58773135,
                        "latitude":47.26077182
                    },
                    {
                        "nom":"Cousteau",
                        "longitude":-1.51293285,
                        "latitude":47.23987594
                    },
                    {
                        "nom":"Coutancière",
                        "longitude":-1.56008462,
                        "latitude":47.30620126
                    },
                    {
                        "nom":"Coudray",
                        "longitude":-1.54239752,
                        "latitude":47.23222357
                    },
                    {
                        "nom":"Chapelles",
                        "longitude":-1.53965688,
                        "latitude":47.16521640
                    },
                    {
                        "nom":"Chapelle Laënnec",
                        "longitude":-1.63783808,
                        "latitude":47.23536751
                    },
                    {
                        "nom":"Copernic",
                        "longitude":-1.56440687,
                        "latitude":47.21490855
                    },
                    {
                        "nom":"Champagnère",
                        "longitude":-1.46124437,
                        "latitude":47.21033886
                    },
                    {
                        "nom":"Chaplin",
                        "longitude":-1.61625115,
                        "latitude":47.22241909
                    },
                    {
                        "nom":"Chaupières",
                        "longitude":-1.49505175,
                        "latitude":47.24317344
                    },
                    {
                        "nom":"Chat-Qui-Guette",
                        "longitude":-1.69711679,
                        "latitude":47.19196413
                    },
                    {
                        "nom":"Coqueluchon",
                        "longitude":-1.47034749,
                        "latitude":47.18410051
                    },
                    {
                        "nom":"Clairais",
                        "longitude":-1.43833070,
                        "latitude":47.27521402
                    },
                    {
                        "nom":"Corberie",
                        "longitude":-1.52404890,
                        "latitude":47.15205072
                    },
                    {
                        "nom":"Courocerie",
                        "longitude":-1.49914592,
                        "latitude":47.25231376
                    },
                    {
                        "nom":"Château de Rezé",
                        "longitude":-1.55943289,
                        "latitude":47.18472412
                    },
                    {
                        "nom":"Crémetterie",
                        "longitude":-1.60774358,
                        "latitude":47.21820708
                    },
                    {
                        "nom":"Chemin Rouge",
                        "longitude":-1.51761350,
                        "latitude":47.25160798
                    },
                    {
                        "nom":"Criport",
                        "longitude":-1.50350606,
                        "latitude":47.15832194
                    },
                    {
                        "nom":"Place du Cirque",
                        "longitude":-1.55675776,
                        "latitude":47.21696856
                    },
                    {
                        "nom":"Croissant",
                        "longitude":-1.53043801,
                        "latitude":47.24055106
                    },
                    {
                        "nom":"Crétinières",
                        "longitude":-1.55315709,
                        "latitude":47.27653350
                    },
                    {
                        "nom":"Cravate",
                        "longitude":-1.58843772,
                        "latitude":47.24336474
                    },
                    {
                        "nom":"Chassay",
                        "longitude":-1.48462514,
                        "latitude":47.24532068
                    },
                    {
                        "nom":"Cimetière St-Clair",
                        "longitude":-1.59242049,
                        "latitude":47.21656884
                    },
                    {
                        "nom":"Chapelle-sur-Erdre",
                        "longitude":-1.55005779,
                        "latitude":47.29942423
                    },
                    {
                        "nom":"Cassegrain",
                        "longitude":-1.48950839,
                        "latitude":47.18787872
                    },
                    {
                        "nom":"Carrières",
                        "longitude":-1.63807066,
                        "latitude":47.20824804
                    },
                    {
                        "nom":"Clos Siban",
                        "longitude":-1.64641182,
                        "latitude":47.21021181
                    },
                    {
                        "nom":"Casimir Périer",
                        "longitude":-1.56829658,
                        "latitude":47.23197965
                    },
                    {
                        "nom":"Classerie",
                        "longitude":-1.55875707,
                        "latitude":47.16817448
                    },
                    {
                        "nom":"Casterneau",
                        "longitude":-1.53264215,
                        "latitude":47.22714751
                    },
                    {
                        "nom":"Conservatoire",
                        "longitude":-1.53265665,
                        "latitude":47.20724258
                    },
                    {
                        "nom":"Coteaux",
                        "longitude":-1.66932799,
                        "latitude":47.18437689
                    },
                    {
                        "nom":"Caboteurs",
                        "longitude":-1.58743716,
                        "latitude":47.18647549
                    },
                    {
                        "nom":"Châtelier",
                        "longitude":-1.59326213,
                        "latitude":47.18483577
                    },
                    {
                        "nom":"Courteline",
                        "longitude":-1.58773874,
                        "latitude":47.23564270
                    },
                    {
                        "nom":"Carterons",
                        "longitude":-1.73865625,
                        "latitude":47.21860614
                    },
                    {
                        "nom":"Clos Toreau",
                        "longitude":-1.52907208,
                        "latitude":47.19421331
                    },
                    {
                        "nom":"Chantrerie",
                        "longitude":-1.51810700,
                        "latitude":47.28230302
                    },
                    {
                        "nom":"Coty",
                        "longitude":-1.48806168,
                        "latitude":47.24898935
                    },
                    {
                        "nom":"Gare de Couëron",
                        "longitude":-1.72409285,
                        "latitude":47.22182251
                    },
                    {
                        "nom":"Chausserie",
                        "longitude":-1.70421559,
                        "latitude":47.14946944
                    },
                    {
                        "nom":"Curie",
                        "longitude":-1.48692026,
                        "latitude":47.19048644
                    },
                    {
                        "nom":"Cuvier",
                        "longitude":-1.57414588,
                        "latitude":47.21070555
                    },
                    {
                        "nom":"Cheveux Blancs",
                        "longitude":-1.63517944,
                        "latitude":47.26545255
                    },
                    {
                        "nom":"Cimetière de Vieux Doulon",
                        "longitude":-1.50040266,
                        "latitude":47.23849235
                    },
                    {
                        "nom":"Chapeau Verni",
                        "longitude":-1.51073836,
                        "latitude":47.18915299
                    },
                    {
                        "nom":"Chevalerie",
                        "longitude":-1.54517072,
                        "latitude":47.25221480
                    },
                    {
                        "nom":"Chauvinière",
                        "longitude":-1.57210879,
                        "latitude":47.25157511
                    },
                    {
                        "nom":"Cheverny",
                        "longitude":-1.60487161,
                        "latitude":47.24597455
                    },
                    {
                        "nom":"Convention",
                        "longitude":-1.58670450,
                        "latitude":47.21072899
                    },
                    {
                        "nom":"Chézine",
                        "longitude":-1.59897057,
                        "latitude":47.23372865
                    },
                    {
                        "nom":"Danemark",
                        "longitude":-1.49857702,
                        "latitude":47.26097867
                    },
                    {
                        "nom":"Dalby",
                        "longitude":-1.52971650,
                        "latitude":47.22508411
                    },
                    {
                        "nom":"Danube",
                        "longitude":-1.50061050,
                        "latitude":47.30405060
                    },
                    {
                        "nom":"Davum",
                        "longitude":-1.49620153,
                        "latitude":47.26475017
                    },
                    {
                        "nom":"Dayat",
                        "longitude":-1.67426307,
                        "latitude":47.20086884
                    },
                    {
                        "nom":"Duchesse Anne-Château",
                        "longitude":-1.54719152,
                        "latitude":47.21630061
                    },
                    {
                        "nom":"Du Chaffault",
                        "longitude":-1.58184631,
                        "latitude":47.20756176
                    },
                    {
                        "nom":"Desmoulins",
                        "longitude":-1.52217027,
                        "latitude":47.23227195
                    },
                    {
                        "nom":"Dervallières",
                        "longitude":-1.59808602,
                        "latitude":47.22889513
                    },
                    {
                        "nom":"Diane",
                        "longitude":-1.57811502,
                        "latitude":47.23966265
                    },
                    {
                        "nom":"Espace Diderot",
                        "longitude":-1.56292570,
                        "latitude":47.18370524
                    },
                    {
                        "nom":"Delacroix",
                        "longitude":-1.58936784,
                        "latitude":47.22360815
                    },
                    {
                        "nom":"Drillet",
                        "longitude":-1.66623091,
                        "latitude":47.23483550
                    },
                    {
                        "nom":"Delorme",
                        "longitude":-1.56472955,
                        "latitude":47.21570823
                    },
                    {
                        "nom":"Domaine",
                        "longitude":-1.47086745,
                        "latitude":47.17840937
                    },
                    {
                        "nom":"Dumont d'Urville",
                        "longitude":-1.63633918,
                        "latitude":47.22587166
                    },
                    {
                        "nom":"Danais",
                        "longitude":-1.58969749,
                        "latitude":47.21368938
                    },
                    {
                        "nom":"Doucet",
                        "longitude":-1.62960572,
                        "latitude":47.27771363
                    },
                    {
                        "nom":"Dolto",
                        "longitude":-1.63617274,
                        "latitude":47.21650999
                    },
                    {
                        "nom":"Doumer",
                        "longitude":-1.57842860,
                        "latitude":47.22226896
                    },
                    {
                        "nom":"Douettée",
                        "longitude":-1.49899677,
                        "latitude":47.20782755
                    },
                    {
                        "nom":"Déportés",
                        "longitude":-1.49183227,
                        "latitude":47.19635756
                    },
                    {
                        "nom":"Désirade",
                        "longitude":-1.48446172,
                        "latitude":47.27819851
                    },
                    {
                        "nom":"Druides",
                        "longitude":-1.50742048,
                        "latitude":47.23861884
                    },
                    {
                        "nom":"Droitière",
                        "longitude":-1.37313692,
                        "latitude":47.30821718
                    },
                    {
                        "nom":"Desaix",
                        "longitude":-1.54205870,
                        "latitude":47.22755156
                    },
                    {
                        "nom":"Dukas",
                        "longitude":-1.60251920,
                        "latitude":47.23522868
                    },
                    {
                        "nom":"Dunant",
                        "longitude":-1.52129149,
                        "latitude":47.24391964
                    },
                    {
                        "nom":"Embarcadère",
                        "longitude":-1.64653306,
                        "latitude":47.19408468
                    },
                    {
                        "nom":"Ecobuts",
                        "longitude":-1.50764703,
                        "latitude":47.19393956
                    },
                    {
                        "nom":"Echoppes",
                        "longitude":-1.68645373,
                        "latitude":47.14310177
                    },
                    {
                        "nom":"Echalier",
                        "longitude":-1.46184614,
                        "latitude":47.21509229
                    },
                    {
                        "nom":"Ecosse",
                        "longitude":-1.52635200,
                        "latitude":47.21510965
                    },
                    {
                        "nom":"Ecole Centrale-Audencia",
                        "longitude":-1.55115045,
                        "latitude":47.24895114
                    },
                    {
                        "nom":"Enchanterie",
                        "longitude":-1.52375537,
                        "latitude":47.23041759
                    },
                    {
                        "nom":"Edit de Nantes",
                        "longitude":-1.56886391,
                        "latitude":47.21439732
                    },
                    {
                        "nom":"Erdreau",
                        "longitude":-1.50127235,
                        "latitude":47.29114983
                    },
                    {
                        "nom":"Egalité",
                        "longitude":-1.58858996,
                        "latitude":47.20940382
                    },
                    {
                        "nom":"Einstein",
                        "longitude":-1.57089717,
                        "latitude":47.26395527
                    },
                    {
                        "nom":"Embellie",
                        "longitude":-1.51835371,
                        "latitude":47.26554307
                    },
                    {
                        "nom":"Ennerie Corbon",
                        "longitude":-1.70470652,
                        "latitude":47.14197580
                    },
                    {
                        "nom":"Ecoles",
                        "longitude":-1.75912140,
                        "latitude":47.19752281
                    },
                    {
                        "nom":"Epinais",
                        "longitude":-1.61055959,
                        "latitude":47.13632633
                    },
                    {
                        "nom":"Epinay",
                        "longitude":-1.48692971,
                        "latitude":47.29234637
                    },
                    {
                        "nom":"Equipement",
                        "longitude":-1.57550052,
                        "latitude":47.22380946
                    },
                    {
                        "nom":"Eraudière",
                        "longitude":-1.53501460,
                        "latitude":47.24165838
                    },
                    {
                        "nom":"Escall",
                        "longitude":-1.50301835,
                        "latitude":47.19940772
                    },
                    {
                        "nom":"Ecoles",
                        "longitude":-1.52753353,
                        "latitude":47.14544898
                    },
                    {
                        "nom":"Esso",
                        "longitude":-1.62715773,
                        "latitude":47.19475301
                    },
                    {
                        "nom":"Eugène Sue",
                        "longitude":-1.59603562,
                        "latitude":47.23337846
                    },
                    {
                        "nom":"Europe",
                        "longitude":-1.50407499,
                        "latitude":47.26016529
                    },
                    {
                        "nom":"Europe",
                        "longitude":-1.47629459,
                        "latitude":47.17300614
                    },
                    {
                        "nom":"Ecole Vétérinaire",
                        "longitude":-1.52263248,
                        "latitude":47.28998722
                    },
                    {
                        "nom":"Facultés",
                        "longitude":-1.55552578,
                        "latitude":47.24673203
                    },
                    {
                        "nom":"Fardière",
                        "longitude":-1.60682070,
                        "latitude":47.19851337
                    },
                    {
                        "nom":"Fallières",
                        "longitude":-1.57927872,
                        "latitude":47.22485204
                    },
                    {
                        "nom":"Faneurs",
                        "longitude":-1.70205892,
                        "latitude":47.21367931
                    },
                    {
                        "nom":"Fantaisie",
                        "longitude":-1.57555877,
                        "latitude":47.25352966
                    },
                    {
                        "nom":"Fauvelière",
                        "longitude":-1.50531922,
                        "latitude":47.28299968
                    },
                    {
                        "nom":"Fresche Blanc",
                        "longitude":-1.55820038,
                        "latitude":47.25312647
                    },
                    {
                        "nom":"Faraday",
                        "longitude":-1.61643132,
                        "latitude":47.22664620
                    },
                    {
                        "nom":"Fac de Droit",
                        "longitude":-1.55196551,
                        "latitude":47.24378992
                    },
                    {
                        "nom":"Fac de Lettres",
                        "longitude":-1.55183232,
                        "latitude":47.24559572
                    },
                    {
                        "nom":"Ferrière",
                        "longitude":-1.59076630,
                        "latitude":47.24445633
                    },
                    {
                        "nom":"Félix Faure",
                        "longitude":-1.56952508,
                        "latitude":47.22707441
                    },
                    {
                        "nom":"Fief Guérin",
                        "longitude":-1.66076091,
                        "latitude":47.14396105
                    },
                    {
                        "nom":"Fleuriaye",
                        "longitude":-1.49941791,
                        "latitude":47.29850645
                    },
                    {
                        "nom":"Floride",
                        "longitude":-1.60369399,
                        "latitude":47.21348186
                    },
                    {
                        "nom":"Fleurs",
                        "longitude":-1.71809144,
                        "latitude":47.21590864
                    },
                    {
                        "nom":"François Mitterrand",
                        "longitude":-1.63817557,
                        "latitude":47.22211543
                    },
                    {
                        "nom":"Fresnel",
                        "longitude":-1.52334410,
                        "latitude":47.24304037
                    },
                    {
                        "nom":"Foch-Cathédrale",
                        "longitude":-1.55054562,
                        "latitude":47.21879945
                    },
                    {
                        "nom":"Fonderies",
                        "longitude":-1.54396268,
                        "latitude":47.20641203
                    },
                    {
                        "nom":"Forum d'Orvault",
                        "longitude":-1.61517856,
                        "latitude":47.25443054
                    },
                    {
                        "nom":"Fontaine",
                        "longitude":-1.48918540,
                        "latitude":47.20554189
                    },
                    {
                        "nom":"Foulquier",
                        "longitude":-1.54911861,
                        "latitude":47.31017344
                    },
                    {
                        "nom":"Frachon",
                        "longitude":-1.62050229,
                        "latitude":47.21912076
                    },
                    {
                        "nom":"Frébaudière",
                        "longitude":-1.62655239,
                        "latitude":47.27223426
                    },
                    {
                        "nom":"Fraîches",
                        "longitude":-1.48948053,
                        "latitude":47.17455019
                    },
                    {
                        "nom":"Forêt",
                        "longitude":-1.57541602,
                        "latitude":47.24434768
                    },
                    {
                        "nom":"Frégate",
                        "longitude":-1.47610372,
                        "latitude":47.23524567
                    },
                    {
                        "nom":"François II",
                        "longitude":-1.68324467,
                        "latitude":47.21839050
                    },
                    {
                        "nom":"Franklin",
                        "longitude":-1.62038789,
                        "latitude":47.22651044
                    },
                    {
                        "nom":"Frémoire",
                        "longitude":-1.47634919,
                        "latitude":47.15337046
                    },
                    {
                        "nom":"Frênes",
                        "longitude":-1.48770396,
                        "latitude":47.25323412
                    },
                    {
                        "nom":"Frêne Rond",
                        "longitude":-1.49641680,
                        "latitude":47.19026089
                    },
                    {
                        "nom":"Félix Tableau",
                        "longitude":-1.54695065,
                        "latitude":47.18748742
                    },
                    {
                        "nom":"Fructidor",
                        "longitude":-1.56104460,
                        "latitude":47.23952046
                    },
                    {
                        "nom":"Gagnerie",
                        "longitude":-1.50722581,
                        "latitude":47.23223083
                    },
                    {
                        "nom":"Garillère",
                        "longitude":-1.52075715,
                        "latitude":47.19251060
                    },
                    {
                        "nom":"Galheur",
                        "longitude":-1.58118031,
                        "latitude":47.16597257
                    },
                    {
                        "nom":"Gambetta",
                        "longitude":-1.53846192,
                        "latitude":47.22352952
                    },
                    {
                        "nom":"Garotterie",
                        "longitude":-1.62710292,
                        "latitude":47.12620991
                    },
                    {
                        "nom":"Gassendi",
                        "longitude":-1.58009416,
                        "latitude":47.19987545
                    },
                    {
                        "nom":"Gâtine",
                        "longitude":-1.66066224,
                        "latitude":47.21341132
                    },
                    {
                        "nom":"Gautellerie",
                        "longitude":-1.60196482,
                        "latitude":47.17742295
                    },
                    {
                        "nom":"Grand Blottereau",
                        "longitude":-1.51114046,
                        "latitude":47.22966852
                    },
                    {
                        "nom":"Guiblinière",
                        "longitude":-1.52028568,
                        "latitude":47.29231724
                    },
                    {
                        "nom":"Grand Bois",
                        "longitude":-1.59060114,
                        "latitude":47.23320345
                    },
                    {
                        "nom":"Grande-Bretagne",
                        "longitude":-1.50236925,
                        "latitude":47.26220345
                    },
                    {
                        "nom":"Grand Carcouët",
                        "longitude":-1.59286320,
                        "latitude":47.23159521
                    },
                    {
                        "nom":"Grande Censive",
                        "longitude":-1.54992762,
                        "latitude":47.25214465
                    },
                    {
                        "nom":"Gachet",
                        "longitude":-1.52392909,
                        "latitude":47.29327618
                    },
                    {
                        "nom":"Gauchoux",
                        "longitude":-1.60044963,
                        "latitude":47.14604009
                    },
                    {
                        "nom":"Glacisol",
                        "longitude":-1.50465591,
                        "latitude":47.25168005
                    },
                    {
                        "nom":"Gare de Chantenay",
                        "longitude":-1.59355966,
                        "latitude":47.19788570
                    },
                    {
                        "nom":"Goldens",
                        "longitude":-1.50045104,
                        "latitude":47.16175622
                    },
                    {
                        "nom":"Gendronnerie",
                        "longitude":-1.63906358,
                        "latitude":47.12732846
                    },
                    {
                        "nom":"Grande Ouche",
                        "longitude":-1.57979430,
                        "latitude":47.18133148
                    },
                    {
                        "nom":"Grande Pièce",
                        "longitude":-1.51091509,
                        "latitude":47.19896414
                    },
                    {
                        "nom":"Guindré",
                        "longitude":-1.53801165,
                        "latitude":47.23921601
                    },
                    {
                        "nom":"Grande Noëlle",
                        "longitude":-1.50221513,
                        "latitude":47.16241793
                    },
                    {
                        "nom":"Géraudière",
                        "longitude":-1.56639743,
                        "latitude":47.26212616
                    },
                    {
                        "nom":"Gesvrine",
                        "longitude":-1.55213869,
                        "latitude":47.26981296
                    },
                    {
                        "nom":"Gesvres",
                        "longitude":-1.54864554,
                        "latitude":47.26362612
                    },
                    {
                        "nom":"Grignon",
                        "longitude":-1.46643829,
                        "latitude":47.21647189
                    },
                    {
                        "nom":"Gauguin",
                        "longitude":-1.55562893,
                        "latitude":47.16692925
                    },
                    {
                        "nom":"Gilarderie",
                        "longitude":-1.68252224,
                        "latitude":47.18589884
                    },
                    {
                        "nom":"Gibraye",
                        "longitude":-1.51637728,
                        "latitude":47.20508614
                    },
                    {
                        "nom":"Gare d'Indre",
                        "longitude":-1.66227247,
                        "latitude":47.20488859
                    },
                    {
                        "nom":"Gicquelière",
                        "longitude":-1.45894604,
                        "latitude":47.25805673
                    },
                    {
                        "nom":"Grillaud",
                        "longitude":-1.58171923,
                        "latitude":47.22026571
                    },
                    {
                        "nom":"Grande Lande",
                        "longitude":-1.49761927,
                        "latitude":47.19229235
                    },
                    {
                        "nom":"Gilière",
                        "longitude":-1.55335302,
                        "latitude":47.29922316
                    },
                    {
                        "nom":"Galarne",
                        "longitude":-1.52994633,
                        "latitude":47.20814414
                    },
                    {
                        "nom":"Grange au Loup",
                        "longitude":-1.51604758,
                        "latitude":47.26291822
                    },
                    {
                        "nom":"Gare Maritime",
                        "longitude":-1.57308561,
                        "latitude":47.20704874
                    },
                    {
                        "nom":"Guy Mollet",
                        "longitude":-1.55367050,
                        "latitude":47.24904628
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53287597,
                        "latitude":47.19930942
                    },
                    {
                        "nom":"Gobinière",
                        "longitude":-1.58682125,
                        "latitude":47.25017483
                    },
                    {
                        "nom":"Grande Ouche",
                        "longitude":-1.46672720,
                        "latitude":47.21123877
                    },
                    {
                        "nom":"Golf",
                        "longitude":-1.60486096,
                        "latitude":47.23649968
                    },
                    {
                        "nom":"Gournière",
                        "longitude":-1.54938114,
                        "latitude":47.26828472
                    },
                    {
                        "nom":"Grand Portail",
                        "longitude":-1.52304507,
                        "latitude":47.20225108
                    },
                    {
                        "nom":"Gare de Pont-Rousseau",
                        "longitude":-1.54906834,
                        "latitude":47.19300016
                    },
                    {
                        "nom":"Grammoire",
                        "longitude":-1.47308957,
                        "latitude":47.18364959
                    },
                    {
                        "nom":"Gare de l'Etat",
                        "longitude":-1.56017356,
                        "latitude":47.20397352
                    },
                    {
                        "nom":"Grimm",
                        "longitude":-1.64552801,
                        "latitude":47.21609703
                    },
                    {
                        "nom":"Graslin",
                        "longitude":-1.56345851,
                        "latitude":47.21277904
                    },
                    {
                        "nom":"Grimau",
                        "longitude":-1.65446267,
                        "latitude":47.22776755
                    },
                    {
                        "nom":"Gripots",
                        "longitude":-1.49181313,
                        "latitude":47.19239539
                    },
                    {
                        "nom":"Garotterie",
                        "longitude":-1.60678975,
                        "latitude":47.22499493
                    },
                    {
                        "nom":"George Sand",
                        "longitude":-1.54577558,
                        "latitude":47.16582084
                    },
                    {
                        "nom":"Grassinière",
                        "longitude":-1.50800609,
                        "latitude":47.16582731
                    },
                    {
                        "nom":"Gare SNCF Nord",
                        "longitude":-1.54241668,
                        "latitude":47.21790248
                    },
                    {
                        "nom":"Gare SNCF Sud",
                        "longitude":-1.54319643,
                        "latitude":47.21589478
                    },
                    {
                        "nom":"Garettes-Landreau",
                        "longitude":-1.63072434,
                        "latitude":47.27317170
                    },
                    {
                        "nom":"Gaston Veil",
                        "longitude":-1.55560165,
                        "latitude":47.21016254
                    },
                    {
                        "nom":"Guilloterie",
                        "longitude":-1.55735797,
                        "latitude":47.17254507
                    },
                    {
                        "nom":"Gustave Roch",
                        "longitude":-1.55198669,
                        "latitude":47.20406998
                    },
                    {
                        "nom":"Gare de Vertou Nord",
                        "longitude":-1.47834737,
                        "latitude":47.18698803
                    },
                    {
                        "nom":"Gare de Vertou Sud",
                        "longitude":-1.47666976,
                        "latitude":47.18569263
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52203092,
                        "latitude":47.24866828
                    },
                    {
                        "nom":"Harouys",
                        "longitude":-1.56747172,
                        "latitude":47.21705645
                    },
                    {
                        "nom":"Haubans",
                        "longitude":-1.53001825,
                        "latitude":47.21462644
                    },
                    {
                        "nom":"Halvêque",
                        "longitude":-1.52041001,
                        "latitude":47.25736862
                    },
                    {
                        "nom":"Hangar à Bananes",
                        "longitude":-1.57312719,
                        "latitude":47.20038230
                    },
                    {
                        "nom":"Halbarderie",
                        "longitude":-1.43810608,
                        "latitude":47.26828686
                    },
                    {
                        "nom":"Hôpital Bellier",
                        "longitude":-1.52454473,
                        "latitude":47.22120456
                    },
                    {
                        "nom":"Herbray",
                        "longitude":-1.47099504,
                        "latitude":47.15048552
                    },
                    {
                        "nom":"Hauts de Couëron",
                        "longitude":-1.66970210,
                        "latitude":47.24056965
                    },
                    {
                        "nom":"Haudrière",
                        "longitude":-1.46758151,
                        "latitude":47.15447112
                    },
                    {
                        "nom":"Hermeland",
                        "longitude":-1.61680890,
                        "latitude":47.22996584
                    },
                    {
                        "nom":"Hameau Fleuri",
                        "longitude":-1.58938761,
                        "latitude":47.25089809
                    },
                    {
                        "nom":"Haute Forêt",
                        "longitude":-1.55544842,
                        "latitude":47.23349494
                    },
                    {
                        "nom":"Haute Forêt",
                        "longitude":-1.46565950,
                        "latitude":47.16831427
                    },
                    {
                        "nom":"Haute Ile",
                        "longitude":-1.55632975,
                        "latitude":47.19473647
                    },
                    {
                        "nom":"Haute Indre",
                        "longitude":-1.64993520,
                        "latitude":47.19540815
                    },
                    {
                        "nom":"Hippodrome",
                        "longitude":-1.56771648,
                        "latitude":47.24758084
                    },
                    {
                        "nom":"Haute Lande",
                        "longitude":-1.52345288,
                        "latitude":47.15477271
                    },
                    {
                        "nom":"Houmaille",
                        "longitude":-1.58696919,
                        "latitude":47.18910344
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.47244142,
                        "latitude":47.17268334
                    },
                    {
                        "nom":"Hôtel Dieu",
                        "longitude":-1.55334592,
                        "latitude":47.21185991
                    },
                    {
                        "nom":"Hongrie",
                        "longitude":-1.52403029,
                        "latitude":47.21590797
                    },
                    {
                        "nom":"Housseau",
                        "longitude":-1.49842423,
                        "latitude":47.28097750
                    },
                    {
                        "nom":"Hôpital Laënnec",
                        "longitude":-1.64086027,
                        "latitude":47.23688456
                    },
                    {
                        "nom":"Hôtel de Région",
                        "longitude":-1.52625290,
                        "latitude":47.21031340
                    },
                    {
                        "nom":"Hérelle",
                        "longitude":-1.58667861,
                        "latitude":47.20136272
                    },
                    {
                        "nom":"Mairie de Carquefou",
                        "longitude":-1.49031126,
                        "latitude":47.29889879
                    },
                    {
                        "nom":"Hôtel de Ville",
                        "longitude":-1.55430456,
                        "latitude":47.21777194
                    },
                    {
                        "nom":"Helvêtie",
                        "longitude":-1.49798070,
                        "latitude":47.25082145
                    },
                    {
                        "nom":"ICAM",
                        "longitude":-1.50186234,
                        "latitude":47.27356816
                    },
                    {
                        "nom":"Ile de France",
                        "longitude":-1.46798232,
                        "latitude":47.16940836
                    },
                    {
                        "nom":"Ile de Nantes",
                        "longitude":-1.53878178,
                        "latitude":47.20604601
                    },
                    {
                        "nom":"Imprimeurs",
                        "longitude":-1.67008841,
                        "latitude":47.23866470
                    },
                    {
                        "nom":"Internat Laënnec",
                        "longitude":-1.64058805,
                        "latitude":47.23500246
                    },
                    {
                        "nom":"Indre",
                        "longitude":-1.66833797,
                        "latitude":47.20116504
                    },
                    {
                        "nom":"Irlandais",
                        "longitude":-1.60705292,
                        "latitude":47.22318454
                    },
                    {
                        "nom":"Iroise",
                        "longitude":-1.53642616,
                        "latitude":47.24836583
                    },
                    {
                        "nom":"IUT",
                        "longitude":-1.50506472,
                        "latitude":47.29786834
                    },
                    {
                        "nom":"Jalière",
                        "longitude":-1.59698972,
                        "latitude":47.26432935
                    },
                    {
                        "nom":"Jamet",
                        "longitude":-1.60304042,
                        "latitude":47.20999149
                    },
                    {
                        "nom":"Jaunais",
                        "longitude":-1.53382955,
                        "latitude":47.17595024
                    },
                    {
                        "nom":"Japy",
                        "longitude":-1.51156536,
                        "latitude":47.25946532
                    },
                    {
                        "nom":"Jarriais",
                        "longitude":-1.71826229,
                        "latitude":47.21293011
                    },
                    {
                        "nom":"Jean Bart",
                        "longitude":-1.67272277,
                        "latitude":47.19795002
                    },
                    {
                        "nom":"Jeanne Bernard",
                        "longitude":-1.59128052,
                        "latitude":47.23525187
                    },
                    {
                        "nom":"Jacques Cartier",
                        "longitude":-1.63619237,
                        "latitude":47.22389516
                    },
                    {
                        "nom":"Jacques Demy",
                        "longitude":-1.68282901,
                        "latitude":47.15813597
                    },
                    {
                        "nom":"Joncours",
                        "longitude":-1.58672381,
                        "latitude":47.22000538
                    },
                    {
                        "nom":"Jean V",
                        "longitude":-1.56591135,
                        "latitude":47.21197546
                    },
                    {
                        "nom":"Jules Ferry",
                        "longitude":-1.48752402,
                        "latitude":47.25071836
                    },
                    {
                        "nom":"Jean Fraix",
                        "longitude":-1.54196353,
                        "latitude":47.18612433
                    },
                    {
                        "nom":"Jaguère",
                        "longitude":-1.56722515,
                        "latitude":47.17563358
                    },
                    {
                        "nom":"Jahardières",
                        "longitude":-1.64274770,
                        "latitude":47.12539960
                    },
                    {
                        "nom":"Jean Jaurès",
                        "longitude":-1.54979231,
                        "latitude":47.30123444
                    },
                    {
                        "nom":"Jean Jaurès",
                        "longitude":-1.56044252,
                        "latitude":47.21855529
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51938315,
                        "latitude":47.18994470
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51724194,
                        "latitude":47.18956604
                    },
                    {
                        "nom":"Jean Macé",
                        "longitude":-1.58926240,
                        "latitude":47.20055415
                    },
                    {
                        "nom":"Jean Moulin",
                        "longitude":-1.60030396,
                        "latitude":47.20693259
                    },
                    {
                        "nom":"Jean Monnet",
                        "longitude":-1.54992478,
                        "latitude":47.16658153
                    },
                    {
                        "nom":"Jean Monnet",
                        "longitude":-1.46524928,
                        "latitude":47.16625637
                    },
                    {
                        "nom":"Jaunaie",
                        "longitude":-1.72783718,
                        "latitude":47.21889815
                    },
                    {
                        "nom":"Jonelière",
                        "longitude":-1.54076407,
                        "latitude":47.25164252
                    },
                    {
                        "nom":"Jouetterie",
                        "longitude":-1.66249597,
                        "latitude":47.13147945
                    },
                    {
                        "nom":"Jean Perrin",
                        "longitude":-1.56089755,
                        "latitude":47.18125199
                    },
                    {
                        "nom":"Jean Rostand",
                        "longitude":-1.59517970,
                        "latitude":47.24691790
                    },
                    {
                        "nom":"Justice",
                        "longitude":-1.63207268,
                        "latitude":47.20935550
                    },
                    {
                        "nom":"Juiverie",
                        "longitude":-1.62464411,
                        "latitude":47.21078204
                    },
                    {
                        "nom":"Jumelière",
                        "longitude":-1.48773150,
                        "latitude":47.29430114
                    },
                    {
                        "nom":"Jules Vallès",
                        "longitude":-1.57114591,
                        "latitude":47.17324888
                    },
                    {
                        "nom":"Jules Verne",
                        "longitude":-1.51728077,
                        "latitude":47.24882713
                    },
                    {
                        "nom":"Janvraie",
                        "longitude":-1.61234453,
                        "latitude":47.19994542
                    },
                    {
                        "nom":"Keren",
                        "longitude":-1.53362652,
                        "latitude":47.24440692
                    },
                    {
                        "nom":"Koenig",
                        "longitude":-1.61563916,
                        "latitude":47.19803104
                    },
                    {
                        "nom":"Koufra",
                        "longitude":-1.53001925,
                        "latitude":47.24939142
                    },
                    {
                        "nom":"Lac de Beaulieu",
                        "longitude":-1.69243768,
                        "latitude":47.21933051
                    },
                    {
                        "nom":"L'Arche",
                        "longitude":-1.70834000,
                        "latitude":47.21282847
                    },
                    {
                        "nom":"Ladoumègue",
                        "longitude":-1.48653946,
                        "latitude":47.29614184
                    },
                    {
                        "nom":"Largeau",
                        "longitude":-1.50997182,
                        "latitude":47.19503280
                    },
                    {
                        "nom":"Les Ailes",
                        "longitude":-1.57567912,
                        "latitude":47.17021274
                    },
                    {
                        "nom":"Lallié",
                        "longitude":-1.54580044,
                        "latitude":47.22755169
                    },
                    {
                        "nom":"Lamartine",
                        "longitude":-1.57893087,
                        "latitude":47.21648755
                    },
                    {
                        "nom":"Launay",
                        "longitude":-1.63213722,
                        "latitude":47.26546719
                    },
                    {
                        "nom":"Lauriers",
                        "longitude":-1.60490455,
                        "latitude":47.20479377
                    },
                    {
                        "nom":"La Croix",
                        "longitude":-1.48670499,
                        "latitude":47.16527571
                    },
                    {
                        "nom":"Les Bauches",
                        "longitude":-1.67723271,
                        "latitude":47.26336633
                    },
                    {
                        "nom":"Lindbergh",
                        "longitude":-1.59751423,
                        "latitude":47.15460712
                    },
                    {
                        "nom":"L'Ebaupin",
                        "longitude":-1.51000653,
                        "latitude":47.17161474
                    },
                    {
                        "nom":"Liberté",
                        "longitude":-1.58909763,
                        "latitude":47.19830804
                    },
                    {
                        "nom":"Le Cardo",
                        "longitude":-1.58461673,
                        "latitude":47.26159828
                    },
                    {
                        "nom":"Le Corbusier",
                        "longitude":-1.56854374,
                        "latitude":47.19008987
                    },
                    {
                        "nom":"La Carrée",
                        "longitude":-1.54307903,
                        "latitude":47.16870383
                    },
                    {
                        "nom":"Lechat",
                        "longitude":-1.54389596,
                        "latitude":47.17633201
                    },
                    {
                        "nom":"La Coulée",
                        "longitude":-1.57471905,
                        "latitude":47.26013301
                    },
                    {
                        "nom":"Le Chêne",
                        "longitude":-1.48445890,
                        "latitude":47.16526021
                    },
                    {
                        "nom":"La Cogne",
                        "longitude":-1.55734888,
                        "latitude":47.28683964
                    },
                    {
                        "nom":"La Close",
                        "longitude":-1.57277848,
                        "latitude":47.24083446
                    },
                    {
                        "nom":"La Côte",
                        "longitude":-1.56057971,
                        "latitude":47.28213726
                    },
                    {
                        "nom":"Landas",
                        "longitude":-1.51076745,
                        "latitude":47.16933769
                    },
                    {
                        "nom":"Landreau",
                        "longitude":-1.51608955,
                        "latitude":47.23229520
                    },
                    {
                        "nom":"Landas",
                        "longitude":-1.55976130,
                        "latitude":47.17471548
                    },
                    {
                        "nom":"Lechat",
                        "longitude":-1.58115476,
                        "latitude":47.20353220
                    },
                    {
                        "nom":"Leygues",
                        "longitude":-1.54188245,
                        "latitude":47.20323969
                    },
                    {
                        "nom":"Levoyer",
                        "longitude":-1.58101777,
                        "latitude":47.19443993
                    },
                    {
                        "nom":"Les Faulx",
                        "longitude":-1.52258688,
                        "latitude":47.15741367
                    },
                    {
                        "nom":"La ferme",
                        "longitude":-1.68034232,
                        "latitude":47.26424888
                    },
                    {
                        "nom":"Laurier Fleuri",
                        "longitude":-1.48329448,
                        "latitude":47.18781477
                    },
                    {
                        "nom":"La Forêt",
                        "longitude":-1.63839344,
                        "latitude":47.15032034
                    },
                    {
                        "nom":"Lefeuvre",
                        "longitude":-1.49502977,
                        "latitude":47.26686052
                    },
                    {
                        "nom":"La Garde",
                        "longitude":-1.50849013,
                        "latitude":47.25533492
                    },
                    {
                        "nom":"Le Goffic",
                        "longitude":-1.56312702,
                        "latitude":47.22999305
                    },
                    {
                        "nom":"La Grille",
                        "longitude":-1.48119010,
                        "latitude":47.25092822
                    },
                    {
                        "nom":"Lagathu",
                        "longitude":-1.53652635,
                        "latitude":47.18036290
                    },
                    {
                        "nom":"La Hache",
                        "longitude":-1.49893768,
                        "latitude":47.26231761
                    },
                    {
                        "nom":"La Herdrie",
                        "longitude":-1.45269297,
                        "latitude":47.19783168
                    },
                    {
                        "nom":"Le Halleray",
                        "longitude":-1.45660738,
                        "latitude":47.26984144
                    },
                    {
                        "nom":"La Houssais",
                        "longitude":-1.55294998,
                        "latitude":47.17368480
                    },
                    {
                        "nom":"L'Hopitau",
                        "longitude":-1.55165518,
                        "latitude":47.27226101
                    },
                    {
                        "nom":"Libération",
                        "longitude":-1.67908088,
                        "latitude":47.17701141
                    },
                    {
                        "nom":"Lilas",
                        "longitude":-1.46890324,
                        "latitude":47.13776565
                    },
                    {
                        "nom":"Lionnière",
                        "longitude":-1.69595730,
                        "latitude":47.21866696
                    },
                    {
                        "nom":"Lippmann",
                        "longitude":-1.52128541,
                        "latitude":47.23833587
                    },
                    {
                        "nom":"Littré",
                        "longitude":-1.58246463,
                        "latitude":47.21780852
                    },
                    {
                        "nom":"Léon Jost",
                        "longitude":-1.56342095,
                        "latitude":47.23583744
                    },
                    {
                        "nom":"Léo Lagrange",
                        "longitude":-1.55530903,
                        "latitude":47.29699568
                    },
                    {
                        "nom":"Les Lions",
                        "longitude":-1.61549961,
                        "latitude":47.24982601
                    },
                    {
                        "nom":"Le Clémois",
                        "longitude":-1.72257729,
                        "latitude":47.13738336
                    },
                    {
                        "nom":"La Lorie",
                        "longitude":-1.65700442,
                        "latitude":47.23524557
                    },
                    {
                        "nom":"La Marche",
                        "longitude":-1.65914283,
                        "latitude":47.18067739
                    },
                    {
                        "nom":"Lambert",
                        "longitude":-1.53904511,
                        "latitude":47.17136155
                    },
                    {
                        "nom":"Louise Michel",
                        "longitude":-1.61659274,
                        "latitude":47.21988542
                    },
                    {
                        "nom":"Lemoine",
                        "longitude":-1.54740057,
                        "latitude":47.23556768
                    },
                    {
                        "nom":"Lamoricière",
                        "longitude":-1.57243725,
                        "latitude":47.21445630
                    },
                    {
                        "nom":"La Montagne",
                        "longitude":-1.68918890,
                        "latitude":47.18845833
                    },
                    {
                        "nom":"Le Nain",
                        "longitude":-1.60096082,
                        "latitude":47.22663531
                    },
                    {
                        "nom":"La Noue",
                        "longitude":-1.60769065,
                        "latitude":47.14038786
                    },
                    {
                        "nom":"Linot",
                        "longitude":-1.52589009,
                        "latitude":47.26547109
                    },
                    {
                        "nom":"Loire",
                        "longitude":-1.48655882,
                        "latitude":47.24831869
                    },
                    {
                        "nom":"Longchamp",
                        "longitude":-1.58240304,
                        "latitude":47.23501359
                    },
                    {
                        "nom":"Bac du Pellerin",
                        "longitude":-1.75490668,
                        "latitude":47.20289715
                    },
                    {
                        "nom":"Le Pellerin",
                        "longitude":-1.75914657,
                        "latitude":47.20130524
                    },
                    {
                        "nom":"La Plée",
                        "longitude":-1.46219951,
                        "latitude":47.19400619
                    },
                    {
                        "nom":"La Plume",
                        "longitude":-1.50520704,
                        "latitude":47.19492160
                    },
                    {
                        "nom":"La Roche",
                        "longitude":-1.51931543,
                        "latitude":47.21651614
                    },
                    {
                        "nom":"Les Roches",
                        "longitude":-1.58001044,
                        "latitude":47.26373625
                    },
                    {
                        "nom":"Lourneau",
                        "longitude":-1.48803472,
                        "latitude":47.20053655
                    },
                    {
                        "nom":"La Salle",
                        "longitude":-1.68915135,
                        "latitude":47.21431111
                    },
                    {
                        "nom":"Les Salles",
                        "longitude":-1.53603503,
                        "latitude":47.25027033
                    },
                    {
                        "nom":"La Source",
                        "longitude":-1.55169769,
                        "latitude":47.30558341
                    },
                    {
                        "nom":"Léon Say",
                        "longitude":-1.56931311,
                        "latitude":47.22960347
                    },
                    {
                        "nom":"Le Tour",
                        "longitude":-1.68945617,
                        "latitude":47.15160805
                    },
                    {
                        "nom":"Lieu Unique",
                        "longitude":-1.54582722,
                        "latitude":47.21571612
                    },
                    {
                        "nom":"Lycée de Bouaye",
                        "longitude":-1.68144912,
                        "latitude":47.15576111
                    },
                    {
                        "nom":"Lycée Appert",
                        "longitude":-1.61140346,
                        "latitude":47.25167763
                    },
                    {
                        "nom":"Lycée Rieffel",
                        "longitude":-1.64248078,
                        "latitude":47.25546429
                    },
                    {
                        "nom":"Lusançay",
                        "longitude":-1.58241386,
                        "latitude":47.20267876
                    },
                    {
                        "nom":"Maillardière",
                        "longitude":-1.63436175,
                        "latitude":47.27403732
                    },
                    {
                        "nom":"MIN",
                        "longitude":-1.55378787,
                        "latitude":47.20157737
                    },
                    {
                        "nom":"Mandon",
                        "longitude":-1.47064382,
                        "latitude":47.16040413
                    },
                    {
                        "nom":"Mauves-Gare",
                        "longitude":-1.38969773,
                        "latitude":47.29318500
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.55104153,
                        "latitude":47.18924083
                    },
                    {
                        "nom":"Mail",
                        "longitude":-1.61136441,
                        "latitude":47.26906222
                    },
                    {
                        "nom":"Maison d'Arrêt",
                        "longitude":-1.50333251,
                        "latitude":47.26640432
                    },
                    {
                        "nom":"Matière",
                        "longitude":-1.44900684,
                        "latitude":47.27648529
                    },
                    {
                        "nom":"Centre De Mauves",
                        "longitude":-1.39409930,
                        "latitude":47.29745553
                    },
                    {
                        "nom":"Mazères",
                        "longitude":-1.35047871,
                        "latitude":47.31371579
                    },
                    {
                        "nom":"Maison Blanche",
                        "longitude":-1.60469183,
                        "latitude":47.22164405
                    },
                    {
                        "nom":"Mont-Blanc",
                        "longitude":-1.55521489,
                        "latitude":47.17576990
                    },
                    {
                        "nom":"Mairie de Bouguenais",
                        "longitude":-1.61872647,
                        "latitude":47.17901016
                    },
                    {
                        "nom":"Moulin Brûlé",
                        "longitude":-1.65458039,
                        "latitude":47.25946869
                    },
                    {
                        "nom":"Moulin Boisseau",
                        "longitude":-1.48413143,
                        "latitude":47.28838631
                    },
                    {
                        "nom":"Moulin Cassé",
                        "longitude":-1.46913780,
                        "latitude":47.25627992
                    },
                    {
                        "nom":"Marché Commun",
                        "longitude":-1.49993852,
                        "latitude":47.25967252
                    },
                    {
                        "nom":"Mairie de Chantenay",
                        "longitude":-1.58783853,
                        "latitude":47.20636707
                    },
                    {
                        "nom":"Mairie de Couëron",
                        "longitude":-1.72720030,
                        "latitude":47.21225494
                    },
                    {
                        "nom":"Madame Curie",
                        "longitude":-1.55433651,
                        "latitude":47.18912957
                    },
                    {
                        "nom":"Maison des Arts",
                        "longitude":-1.61745893,
                        "latitude":47.21192952
                    },
                    {
                        "nom":"Mandel",
                        "longitude":-1.53707786,
                        "latitude":47.20259073
                    },
                    {
                        "nom":"Moulin des Landes",
                        "longitude":-1.48145788,
                        "latitude":47.26208697
                    },
                    {
                        "nom":"Madeleine",
                        "longitude":-1.48349616,
                        "latitude":47.27579884
                    },
                    {
                        "nom":"Mairie de Doulon",
                        "longitude":-1.51860295,
                        "latitude":47.22680732
                    },
                    {
                        "nom":"Moulin de Porterie",
                        "longitude":-1.51338998,
                        "latitude":47.27561575
                    },
                    {
                        "nom":"Madoire",
                        "longitude":-1.63806295,
                        "latitude":47.26508301
                    },
                    {
                        "nom":"Mondésir",
                        "longitude":-1.56884285,
                        "latitude":47.21773053
                    },
                    {
                        "nom":"Médiathèque",
                        "longitude":-1.56082889,
                        "latitude":47.21115673
                    },
                    {
                        "nom":"Méditerranée",
                        "longitude":-1.48501031,
                        "latitude":47.27106552
                    },
                    {
                        "nom":"Méliès",
                        "longitude":-1.55630487,
                        "latitude":47.18167753
                    },
                    {
                        "nom":"Métairie",
                        "longitude":-1.69836721,
                        "latitude":47.21380863
                    },
                    {
                        "nom":"Marie Galante",
                        "longitude":-1.45104526,
                        "latitude":47.26417032
                    },
                    {
                        "nom":"Monge-Chauvinière",
                        "longitude":-1.56942314,
                        "latitude":47.25283697
                    },
                    {
                        "nom":"Mongendrière",
                        "longitude":-1.55457252,
                        "latitude":47.29233716
                    },
                    {
                        "nom":"Mangin",
                        "longitude":-1.54507404,
                        "latitude":47.20169115
                    },
                    {
                        "nom":"Magnoliers",
                        "longitude":-1.60568888,
                        "latitude":47.22620349
                    },
                    {
                        "nom":"Mignonnerie",
                        "longitude":-1.48178738,
                        "latitude":47.24262277
                    },
                    {
                        "nom":"Mainguais",
                        "longitude":-1.48557518,
                        "latitude":47.28266468
                    },
                    {
                        "nom":"Marcel Hatet",
                        "longitude":-1.53070863,
                        "latitude":47.22234888
                    },
                    {
                        "nom":"Mahaudières",
                        "longitude":-1.56039923,
                        "latitude":47.18892461
                    },
                    {
                        "nom":"Moulin à l'Huile",
                        "longitude":-1.56288910,
                        "latitude":47.17776199
                    },
                    {
                        "nom":"Minais",
                        "longitude":-1.46445709,
                        "latitude":47.25742494
                    },
                    {
                        "nom":"Michelet",
                        "longitude":-1.55724171,
                        "latitude":47.23451518
                    },
                    {
                        "nom":"Mimosas",
                        "longitude":-1.58748790,
                        "latitude":47.24123544
                    },
                    {
                        "nom":"Mairie d'Indre",
                        "longitude":-1.66830435,
                        "latitude":47.20071585
                    },
                    {
                        "nom":"Mitrie",
                        "longitude":-1.52541388,
                        "latitude":47.22775017
                    },
                    {
                        "nom":"Malakoff",
                        "longitude":-1.52113569,
                        "latitude":47.21609496
                    },
                    {
                        "nom":"Magellan",
                        "longitude":-1.46428932,
                        "latitude":47.18808321
                    },
                    {
                        "nom":"Morlachère",
                        "longitude":-1.48370116,
                        "latitude":47.16204305
                    },
                    {
                        "nom":"Maladrie",
                        "longitude":-1.49627525,
                        "latitude":47.18828419
                    },
                    {
                        "nom":"Moulin Hérel",
                        "longitude":-1.63757971,
                        "latitude":47.21943384
                    },
                    {
                        "nom":"Moulin",
                        "longitude":-1.50517039,
                        "latitude":47.18888850
                    },
                    {
                        "nom":"Mellinet",
                        "longitude":-1.57667075,
                        "latitude":47.21089003
                    },
                    {
                        "nom":"Moulinets",
                        "longitude":-1.62921210,
                        "latitude":47.20999442
                    },
                    {
                        "nom":"Mulonnière",
                        "longitude":-1.54997711,
                        "latitude":47.26556269
                    },
                    {
                        "nom":"Malraux",
                        "longitude":-1.51767585,
                        "latitude":47.25430780
                    },
                    {
                        "nom":"Mermoz",
                        "longitude":-1.58496831,
                        "latitude":47.23933968
                    },
                    {
                        "nom":"Mermoz",
                        "longitude":-1.51552255,
                        "latitude":47.19872011
                    },
                    {
                        "nom":"Moulin Neuf",
                        "longitude":-1.51644242,
                        "latitude":47.22985168
                    },
                    {
                        "nom":"Manufacture",
                        "longitude":-1.53732576,
                        "latitude":47.21879424
                    },
                    {
                        "nom":"Moulinier",
                        "longitude":-1.66556362,
                        "latitude":47.13526403
                    },
                    {
                        "nom":"Minoterie",
                        "longitude":-1.72160133,
                        "latitude":47.21686600
                    },
                    {
                        "nom":"Malnoue",
                        "longitude":-1.50514128,
                        "latitude":47.20320971
                    },
                    {
                        "nom":"Ménétrier",
                        "longitude":-1.52387404,
                        "latitude":47.24122134
                    },
                    {
                        "nom":"Maison Neuve",
                        "longitude":-1.46727465,
                        "latitude":47.27471369
                    },
                    {
                        "nom":"Mouettes",
                        "longitude":-1.74227284,
                        "latitude":47.21055137
                    },
                    {
                        "nom":"Moinard",
                        "longitude":-1.72928032,
                        "latitude":47.21353271
                    },
                    {
                        "nom":"Moinet",
                        "longitude":-1.49236094,
                        "latitude":47.26469759
                    },
                    {
                        "nom":"Morrhonnière-Petit Port",
                        "longitude":-1.55731697,
                        "latitude":47.24099740
                    },
                    {
                        "nom":"Motte Rouge",
                        "longitude":-1.55410676,
                        "latitude":47.22777599
                    },
                    {
                        "nom":"Moutonnerie",
                        "longitude":-1.53315129,
                        "latitude":47.21956494
                    },
                    {
                        "nom":"Mairie d'Orvault",
                        "longitude":-1.62292189,
                        "latitude":47.27145824
                    },
                    {
                        "nom":"Monzie",
                        "longitude":-1.53991144,
                        "latitude":47.20339609
                    },
                    {
                        "nom":"Marcel Paul",
                        "longitude":-1.61626953,
                        "latitude":47.24592664
                    },
                    {
                        "nom":"Millepertuis",
                        "longitude":-1.67942003,
                        "latitude":47.22203672
                    },
                    {
                        "nom":"Melpomène",
                        "longitude":-1.50003396,
                        "latitude":47.27759170
                    },
                    {
                        "nom":"Maison Poitard",
                        "longitude":-1.64974194,
                        "latitude":47.14668519
                    },
                    {
                        "nom":"Marquis de Dion",
                        "longitude":-1.48779883,
                        "latitude":47.29709079
                    },
                    {
                        "nom":"Mocquelière",
                        "longitude":-1.61902651,
                        "latitude":47.23313228
                    },
                    {
                        "nom":"Maraîchers",
                        "longitude":-1.51082379,
                        "latitude":47.18482702
                    },
                    {
                        "nom":"Maison Radieuse",
                        "longitude":-1.56694643,
                        "latitude":47.18829761
                    },
                    {
                        "nom":"Mairie de Rezé",
                        "longitude":-1.57022370,
                        "latitude":47.19138392
                    },
                    {
                        "nom":"Morges",
                        "longitude":-1.47202356,
                        "latitude":47.16495172
                    },
                    {
                        "nom":"Maurienne",
                        "longitude":-1.44245510,
                        "latitude":47.27552930
                    },
                    {
                        "nom":"Moulin Rothard",
                        "longitude":-1.74115678,
                        "latitude":47.19590819
                    },
                    {
                        "nom":"Marrière",
                        "longitude":-1.52466349,
                        "latitude":47.23750227
                    },
                    {
                        "nom":"Marterie",
                        "longitude":-1.53057879,
                        "latitude":47.17849125
                    },
                    {
                        "nom":"Mairie de St-Herblain",
                        "longitude":-1.64823952,
                        "latitude":47.21167984
                    },
                    {
                        "nom":"Marseillaise",
                        "longitude":-1.59188602,
                        "latitude":47.20749011
                    },
                    {
                        "nom":"Mairie des Sorinières",
                        "longitude":-1.53229012,
                        "latitude":47.14916207
                    },
                    {
                        "nom":"Mississipi",
                        "longitude":-1.60914177,
                        "latitude":47.23716387
                    },
                    {
                        "nom":"Monteil",
                        "longitude":-1.54953382,
                        "latitude":47.21396999
                    },
                    {
                        "nom":"Métairie",
                        "longitude":-1.51149914,
                        "latitude":47.20524916
                    },
                    {
                        "nom":"Martellière",
                        "longitude":-1.52492749,
                        "latitude":47.19903572
                    },
                    {
                        "nom":"Mortrais",
                        "longitude":-1.61667784,
                        "latitude":47.12800978
                    },
                    {
                        "nom":"Monty",
                        "longitude":-1.43296748,
                        "latitude":47.27421861
                    },
                    {
                        "nom":"Mortier Vannerie",
                        "longitude":-1.49939264,
                        "latitude":47.18016469
                    },
                    {
                        "nom":"Monteverdi",
                        "longitude":-1.44632051,
                        "latitude":47.26657684
                    },
                    {
                        "nom":"Mauvoisins",
                        "longitude":-1.52559735,
                        "latitude":47.19000671
                    },
                    {
                        "nom":"Nantes-Atlantique",
                        "longitude":-1.60091305,
                        "latitude":47.15773346
                    },
                    {
                        "nom":"Nouveau Bêle",
                        "longitude":-1.50190990,
                        "latitude":47.26870322
                    },
                    {
                        "nom":"Noé Blanche",
                        "longitude":-1.50612485,
                        "latitude":47.25739514
                    },
                    {
                        "nom":"Noé Cottée",
                        "longitude":-1.47927437,
                        "latitude":47.19443256
                    },
                    {
                        "nom":"Naudières",
                        "longitude":-1.53603890,
                        "latitude":47.17362430
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59217451,
                        "latitude":47.17721695
                    },
                    {
                        "nom":"Niagara",
                        "longitude":-1.49333602,
                        "latitude":47.30600333
                    },
                    {
                        "nom":"Nil",
                        "longitude":-1.60856303,
                        "latitude":47.24186727
                    },
                    {
                        "nom":"Noé Lambert",
                        "longitude":-1.52849088,
                        "latitude":47.23007878
                    },
                    {
                        "nom":"Noé Allais",
                        "longitude":-1.68868129,
                        "latitude":47.22036251
                    },
                    {
                        "nom":"Noë St-Jean",
                        "longitude":-1.73499847,
                        "latitude":47.21918582
                    },
                    {
                        "nom":"North'House",
                        "longitude":-1.57118445,
                        "latitude":47.19549447
                    },
                    {
                        "nom":"Noé Nozou 1",
                        "longitude":-1.63722119,
                        "latitude":47.13024744
                    },
                    {
                        "nom":"Noëlles",
                        "longitude":-1.60434756,
                        "latitude":47.24030012
                    },
                    {
                        "nom":"Noé Nozou 2",
                        "longitude":-1.64163529,
                        "latitude":47.13210355
                    },
                    {
                        "nom":"Noé Rocard",
                        "longitude":-1.49066067,
                        "latitude":47.16334300
                    },
                    {
                        "nom":"Néruda",
                        "longitude":-1.61365099,
                        "latitude":47.21764447
                    },
                    {
                        "nom":"Ouche Blanche",
                        "longitude":-1.56437046,
                        "latitude":47.17996352
                    },
                    {
                        "nom":"Océane",
                        "longitude":-1.63593842,
                        "latitude":47.22759682
                    },
                    {
                        "nom":"Couëron-Océan",
                        "longitude":-1.73918225,
                        "latitude":47.21336306
                    },
                    {
                        "nom":"Ouche Cormier",
                        "longitude":-1.62400648,
                        "latitude":47.26466582
                    },
                    {
                        "nom":"Ouche des landes",
                        "longitude":-1.48181737,
                        "latitude":47.19488867
                    },
                    {
                        "nom":"Ordronneau",
                        "longitude":-1.57489540,
                        "latitude":47.19383736
                    },
                    {
                        "nom":"Orvault-Grand Val",
                        "longitude":-1.59436976,
                        "latitude":47.26288752
                    },
                    {
                        "nom":"Olivier",
                        "longitude":-1.72468851,
                        "latitude":47.21747786
                    },
                    {
                        "nom":"Oliveraie",
                        "longitude":-1.53117399,
                        "latitude":47.18855867
                    },
                    {
                        "nom":"Olympe de Gouge",
                        "longitude":-1.54466143,
                        "latitude":47.30897242
                    },
                    {
                        "nom":"Orvault-Morlière",
                        "longitude":-1.60325434,
                        "latitude":47.24700256
                    },
                    {
                        "nom":"Onchère",
                        "longitude":-1.46704982,
                        "latitude":47.20276230
                    },
                    {
                        "nom":"Ouche Quinet",
                        "longitude":-1.50462533,
                        "latitude":47.19602177
                    },
                    {
                        "nom":"Orvault",
                        "longitude":-1.64041888,
                        "latitude":47.27896266
                    },
                    {
                        "nom":"Ouessant",
                        "longitude":-1.44670053,
                        "latitude":47.27007669
                    },
                    {
                        "nom":"50 Otages",
                        "longitude":-1.55563519,
                        "latitude":47.21970848
                    },
                    {
                        "nom":"Paix",
                        "longitude":-1.59242516,
                        "latitude":47.18243245
                    },
                    {
                        "nom":"Parc",
                        "longitude":-1.48417618,
                        "latitude":47.27235400
                    },
                    {
                        "nom":"Pablée",
                        "longitude":-1.74015814,
                        "latitude":47.21576063
                    },
                    {
                        "nom":"Parc",
                        "longitude":-1.47936549,
                        "latitude":47.21424333
                    },
                    {
                        "nom":"Petit Anjou",
                        "longitude":-1.50453728,
                        "latitude":47.20584171
                    },
                    {
                        "nom":"Pont Allard",
                        "longitude":-1.66627488,
                        "latitude":47.19655292
                    },
                    {
                        "nom":"Palmiers",
                        "longitude":-1.53300429,
                        "latitude":47.19011839
                    },
                    {
                        "nom":"Pâtis Rondin",
                        "longitude":-1.51693917,
                        "latitude":47.24775778
                    },
                    {
                        "nom":"Parc de la Bouvre 1",
                        "longitude":-1.61248538,
                        "latitude":47.17499101
                    },
                    {
                        "nom":"Parc Bel Air",
                        "longitude":-1.47485280,
                        "latitude":47.17872785
                    },
                    {
                        "nom":"Paul Bert",
                        "longitude":-1.59799118,
                        "latitude":47.21322637
                    },
                    {
                        "nom":"Paimboeuf",
                        "longitude":-1.61907431,
                        "latitude":47.17656629
                    },
                    {
                        "nom":"Pâtis Brûlé",
                        "longitude":-1.50736222,
                        "latitude":47.19917280
                    },
                    {
                        "nom":"Poulbot",
                        "longitude":-1.64835997,
                        "latitude":47.21329698
                    },
                    {
                        "nom":"Petit Breton",
                        "longitude":-1.60011407,
                        "latitude":47.21333400
                    },
                    {
                        "nom":"Port Boyer",
                        "longitude":-1.53828029,
                        "latitude":47.24109835
                    },
                    {
                        "nom":"Petit Carcouët",
                        "longitude":-1.58679594,
                        "latitude":47.22819914
                    },
                    {
                        "nom":"Picabia",
                        "longitude":-1.57682529,
                        "latitude":47.25456749
                    },
                    {
                        "nom":"Picaudière",
                        "longitude":-1.48541452,
                        "latitude":47.28041848
                    },
                    {
                        "nom":"Place Centrale",
                        "longitude":-1.67198475,
                        "latitude":47.26219776
                    },
                    {
                        "nom":"Portechaise",
                        "longitude":-1.52596699,
                        "latitude":47.20062205
                    },
                    {
                        "nom":"Planchonnais",
                        "longitude":-1.47476516,
                        "latitude":47.25357239
                    },
                    {
                        "nom":"Poincaré",
                        "longitude":-1.58520707,
                        "latitude":47.22275905
                    },
                    {
                        "nom":"Petite Censive",
                        "longitude":-1.56111925,
                        "latitude":47.26050332
                    },
                    {
                        "nom":"Petit Chantilly",
                        "longitude":-1.60076891,
                        "latitude":47.25456313
                    },
                    {
                        "nom":"Pontecorvo",
                        "longitude":-1.53256261,
                        "latitude":47.24615385
                    },
                    {
                        "nom":"Pont de Chézine",
                        "longitude":-1.61437263,
                        "latitude":47.23815563
                    },
                    {
                        "nom":"Pas d'Ane",
                        "longitude":-1.46993696,
                        "latitude":47.18204264
                    },
                    {
                        "nom":"Pont du Cens",
                        "longitude":-1.57731488,
                        "latitude":47.24860646
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.58145537,
                        "latitude":47.26539845
                    },
                    {
                        "nom":"Porte Douillard",
                        "longitude":-1.51930528,
                        "latitude":47.28973819
                    },
                    {
                        "nom":"Prairie au Duc",
                        "longitude":-1.56508419,
                        "latitude":47.20425767
                    },
                    {
                        "nom":"Perdriaux",
                        "longitude":-1.48562593,
                        "latitude":47.16125865
                    },
                    {
                        "nom":"Pégers",
                        "longitude":-1.47153044,
                        "latitude":47.13758868
                    },
                    {
                        "nom":"Pelousière",
                        "longitude":-1.64702227,
                        "latitude":47.20775876
                    },
                    {
                        "nom":"Perron",
                        "longitude":-1.60175834,
                        "latitude":47.22489675
                    },
                    {
                        "nom":"Perray",
                        "longitude":-1.51414241,
                        "latitude":47.24749096
                    },
                    {
                        "nom":"Pétrels",
                        "longitude":-1.50737626,
                        "latitude":47.25456149
                    },
                    {
                        "nom":"Purfina",
                        "longitude":-1.63494095,
                        "latitude":47.19457500
                    },
                    {
                        "nom":"Pagerie",
                        "longitude":-1.65318427,
                        "latitude":47.17872222
                    },
                    {
                        "nom":"Plinguetière",
                        "longitude":-1.61374439,
                        "latitude":47.13297444
                    },
                    {
                        "nom":"Péguy",
                        "longitude":-1.61367916,
                        "latitude":47.22160658
                    },
                    {
                        "nom":"Parc Chantrerie",
                        "longitude":-1.52107843,
                        "latitude":47.28679692
                    },
                    {
                        "nom":"Pré Hervé",
                        "longitude":-1.51757136,
                        "latitude":47.24368379
                    },
                    {
                        "nom":"Piano'cktail",
                        "longitude":-1.60652973,
                        "latitude":47.17483482
                    },
                    {
                        "nom":"Pierre Blanche",
                        "longitude":-1.62719445,
                        "latitude":47.17205368
                    },
                    {
                        "nom":"Picasso",
                        "longitude":-1.53692066,
                        "latitude":47.21502510
                    },
                    {
                        "nom":"Pierre Abélard",
                        "longitude":-1.68458074,
                        "latitude":47.15508427
                    },
                    {
                        "nom":"Pipay",
                        "longitude":-1.53690356,
                        "latitude":47.22574347
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54206624,
                        "latitude":47.19665866
                    },
                    {
                        "nom":"Pin Sec",
                        "longitude":-1.51809262,
                        "latitude":47.24357630
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.44402237,
                        "latitude":47.26440701
                    },
                    {
                        "nom":"Placis",
                        "longitude":-1.51994400,
                        "latitude":47.19956291
                    },
                    {
                        "nom":"Port la Blanche",
                        "longitude":-1.51667583,
                        "latitude":47.27163335
                    },
                    {
                        "nom":"Polyclinique",
                        "longitude":-1.61163773,
                        "latitude":47.22798140
                    },
                    {
                        "nom":"Plessis",
                        "longitude":-1.55242073,
                        "latitude":47.30096579
                    },
                    {
                        "nom":"Pouliguen",
                        "longitude":-1.54232443,
                        "latitude":47.24033191
                    },
                    {
                        "nom":"Port launay",
                        "longitude":-1.74630488,
                        "latitude":47.20968794
                    },
                    {
                        "nom":"Ploreau",
                        "longitude":-1.55191079,
                        "latitude":47.30305444
                    },
                    {
                        "nom":"Place des Pêcheurs",
                        "longitude":-1.66871410,
                        "latitude":47.13644248
                    },
                    {
                        "nom":"Plessis",
                        "longitude":-1.66007149,
                        "latitude":47.26035939
                    },
                    {
                        "nom":"Platière",
                        "longitude":-1.56107499,
                        "latitude":47.19457601
                    },
                    {
                        "nom":"Pont Marchand",
                        "longitude":-1.63138111,
                        "latitude":47.26062944
                    },
                    {
                        "nom":"Port aux Meules",
                        "longitude":-1.49305446,
                        "latitude":47.16542495
                    },
                    {
                        "nom":"Pont de la Métairie",
                        "longitude":-1.44602801,
                        "latitude":47.26244371
                    },
                    {
                        "nom":"Prairie de Mauves",
                        "longitude":-1.51666021,
                        "latitude":47.22002740
                    },
                    {
                        "nom":"Prière",
                        "longitude":-1.59681234,
                        "latitude":47.22236358
                    },
                    {
                        "nom":"Pinier",
                        "longitude":-1.54533445,
                        "latitude":47.18159748
                    },
                    {
                        "nom":"Plantiveau",
                        "longitude":-1.51155784,
                        "latitude":47.24649647
                    },
                    {
                        "nom":"Poitou",
                        "longitude":-1.56548394,
                        "latitude":47.22423907
                    },
                    {
                        "nom":"Poulenc",
                        "longitude":-1.58125232,
                        "latitude":47.25540792
                    },
                    {
                        "nom":"Pommeraie",
                        "longitude":-1.69578708,
                        "latitude":47.14625206
                    },
                    {
                        "nom":"Pontereau",
                        "longitude":-1.37870501,
                        "latitude":47.30641716
                    },
                    {
                        "nom":"Potiron",
                        "longitude":-1.51871013,
                        "latitude":47.27048463
                    },
                    {
                        "nom":"Poste",
                        "longitude":-1.55412164,
                        "latitude":47.29523442
                    },
                    {
                        "nom":"Pottier",
                        "longitude":-1.62734333,
                        "latitude":47.17763302
                    },
                    {
                        "nom":"Poyaux",
                        "longitude":-1.53052577,
                        "latitude":47.16678439
                    },
                    {
                        "nom":"Pompidou",
                        "longitude":-1.52823416,
                        "latitude":47.20923373
                    },
                    {
                        "nom":"Petit Port",
                        "longitude":-1.55711745,
                        "latitude":47.24370612
                    },
                    {
                        "nom":"Peupliers",
                        "longitude":-1.49733040,
                        "latitude":47.19932695
                    },
                    {
                        "nom":"Pré Poulain",
                        "longitude":-1.44458990,
                        "latitude":47.27203712
                    },
                    {
                        "nom":"Petit Plessis",
                        "longitude":-1.38607257,
                        "latitude":47.30194725
                    },
                    {
                        "nom":"Papotière",
                        "longitude":-1.49681824,
                        "latitude":47.24014265
                    },
                    {
                        "nom":"Procé",
                        "longitude":-1.58175846,
                        "latitude":47.22260614
                    },
                    {
                        "nom":"Praudière",
                        "longitude":-1.58387156,
                        "latitude":47.25685006
                    },
                    {
                        "nom":"Preux",
                        "longitude":-1.61827480,
                        "latitude":47.22117876
                    },
                    {
                        "nom":"Profondine",
                        "longitude":-1.48851746,
                        "latitude":47.19619748
                    },
                    {
                        "nom":"Portail Rouge",
                        "longitude":-1.50439053,
                        "latitude":47.24430373
                    },
                    {
                        "nom":"Petit Rocher",
                        "longitude":-1.47899478,
                        "latitude":47.24055373
                    },
                    {
                        "nom":"Primevères",
                        "longitude":-1.67288995,
                        "latitude":47.22487620
                    },
                    {
                        "nom":"Pyramide",
                        "longitude":-1.49279982,
                        "latitude":47.20803366
                    },
                    {
                        "nom":"Perrines",
                        "longitude":-1.51548924,
                        "latitude":47.22763193
                    },
                    {
                        "nom":"Pont-Rousseau-Martyrs",
                        "longitude":-1.54757281,
                        "latitude":47.19242012
                    },
                    {
                        "nom":"Perrières",
                        "longitude":-1.55197896,
                        "latitude":47.30944669
                    },
                    {
                        "nom":"Pressoir",
                        "longitude":-1.63892195,
                        "latitude":47.12364032
                    },
                    {
                        "nom":"Portereau",
                        "longitude":-1.50171153,
                        "latitude":47.16828894
                    },
                    {
                        "nom":"Porte de Rezé",
                        "longitude":-1.54079053,
                        "latitude":47.15716224
                    },
                    {
                        "nom":"Porte de Sautron",
                        "longitude":-1.61874600,
                        "latitude":47.25079554
                    },
                    {
                        "nom":"Plaisance",
                        "longitude":-1.59124771,
                        "latitude":47.24020674
                    },
                    {
                        "nom":"Parc de la Sèvre",
                        "longitude":-1.47925036,
                        "latitude":47.16444225
                    },
                    {
                        "nom":"Pasteur",
                        "longitude":-1.67863326,
                        "latitude":47.20098683
                    },
                    {
                        "nom":"Place St Méen",
                        "longitude":-1.34741542,
                        "latitude":47.31921670
                    },
                    {
                        "nom":"Prières",
                        "longitude":-1.52615191,
                        "latitude":47.15008877
                    },
                    {
                        "nom":"Parc des Sports",
                        "longitude":-1.44906847,
                        "latitude":47.26054278
                    },
                    {
                        "nom":"Pôle Sud",
                        "longitude":-1.46895354,
                        "latitude":47.18864971
                    },
                    {
                        "nom":"Petit Bois",
                        "longitude":-1.67838988,
                        "latitude":47.18541245
                    },
                    {
                        "nom":"Patache",
                        "longitude":-1.61959538,
                        "latitude":47.27121209
                    },
                    {
                        "nom":"Polytech'",
                        "longitude":-1.51375897,
                        "latitude":47.28073700
                    },
                    {
                        "nom":"Poitou",
                        "longitude":-1.46926854,
                        "latitude":47.20980394
                    },
                    {
                        "nom":"Plessis Tison",
                        "longitude":-1.53346310,
                        "latitude":47.23666684
                    },
                    {
                        "nom":"Platanes",
                        "longitude":-1.52471966,
                        "latitude":47.24560617
                    },
                    {
                        "nom":"Portillon",
                        "longitude":-1.46906172,
                        "latitude":47.14928857
                    },
                    {
                        "nom":"Printemps",
                        "longitude":-1.50039329,
                        "latitude":47.18859741
                    },
                    {
                        "nom":"Portricq",
                        "longitude":-1.52123154,
                        "latitude":47.26508661
                    },
                    {
                        "nom":"Portereau",
                        "longitude":-1.47733928,
                        "latitude":47.19323576
                    },
                    {
                        "nom":"Pâtures",
                        "longitude":-1.55638074,
                        "latitude":47.30443488
                    },
                    {
                        "nom":"Pâtis Viaud",
                        "longitude":-1.45761689,
                        "latitude":47.13867871
                    },
                    {
                        "nom":"Planty",
                        "longitude":-1.49494731,
                        "latitude":47.17526907
                    },
                    {
                        "nom":"Pluchets",
                        "longitude":-1.65443833,
                        "latitude":47.21858103
                    },
                    {
                        "nom":"Petit Village",
                        "longitude":-1.65048707,
                        "latitude":47.21700643
                    },
                    {
                        "nom":"Pavillon",
                        "longitude":-1.67948603,
                        "latitude":47.14366941
                    },
                    {
                        "nom":"Porte de Vertou",
                        "longitude":-1.50567017,
                        "latitude":47.18112627
                    },
                    {
                        "nom":"Quai des Antilles",
                        "longitude":-1.56954034,
                        "latitude":47.20374641
                    },
                    {
                        "nom":"Quai Brunais",
                        "longitude":-1.66957679,
                        "latitude":47.19652821
                    },
                    {
                        "nom":"Quai surcouf",
                        "longitude":-1.57788061,
                        "latitude":47.19490685
                    },
                    {
                        "nom":"Quintaine",
                        "longitude":-1.46420283,
                        "latitude":47.21663570
                    },
                    {
                        "nom":"Rabine",
                        "longitude":-1.64356226,
                        "latitude":47.26075015
                    },
                    {
                        "nom":"Ragon",
                        "longitude":-1.54244296,
                        "latitude":47.16539277
                    },
                    {
                        "nom":"Ravardière",
                        "longitude":-1.66532092,
                        "latitude":47.18379560
                    },
                    {
                        "nom":"Ranzay",
                        "longitude":-1.53043062,
                        "latitude":47.25325039
                    },
                    {
                        "nom":"Roche Ballue",
                        "longitude":-1.65575479,
                        "latitude":47.17953387
                    },
                    {
                        "nom":"René Bouhier",
                        "longitude":-1.57131140,
                        "latitude":47.20990103
                    },
                    {
                        "nom":"Robinière",
                        "longitude":-1.53558119,
                        "latitude":47.16364228
                    },
                    {
                        "nom":"René Bernier",
                        "longitude":-1.49945517,
                        "latitude":47.19394244
                    },
                    {
                        "nom":"René Cassin",
                        "longitude":-1.57633471,
                        "latitude":47.26413120
                    },
                    {
                        "nom":"Rocher",
                        "longitude":-1.63949256,
                        "latitude":47.21315293
                    },
                    {
                        "nom":"Rocher",
                        "longitude":-1.50522732,
                        "latitude":47.16943260
                    },
                    {
                        "nom":"Route de la Chapelle",
                        "longitude":-1.57618523,
                        "latitude":47.25485941
                    },
                    {
                        "nom":"Rochu",
                        "longitude":-1.66647964,
                        "latitude":47.21519099
                    },
                    {
                        "nom":"Raoul Dufy",
                        "longitude":-1.59961481,
                        "latitude":47.22271825
                    },
                    {
                        "nom":"Radigois",
                        "longitude":-1.51802149,
                        "latitude":47.19305249
                    },
                    {
                        "nom":"Rue de Mauves",
                        "longitude":-1.43264535,
                        "latitude":47.27152739
                    },
                    {
                        "nom":"Rue de Nantes",
                        "longitude":-1.53427678,
                        "latitude":47.15107676
                    },
                    {
                        "nom":"Redras",
                        "longitude":-1.50251433,
                        "latitude":47.16474964
                    },
                    {
                        "nom":"Réfractaires",
                        "longitude":-1.54751948,
                        "latitude":47.30275197
                    },
                    {
                        "nom":"Retas",
                        "longitude":-1.53705860,
                        "latitude":47.15485606
                    },
                    {
                        "nom":"Reynière",
                        "longitude":-1.57865918,
                        "latitude":47.25612645
                    },
                    {
                        "nom":"Reigniers",
                        "longitude":-1.46582359,
                        "latitude":47.13714694
                    },
                    {
                        "nom":"Rue d'Indre",
                        "longitude":-1.66418944,
                        "latitude":47.20401138
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.47585629,
                        "latitude":47.21507998
                    },
                    {
                        "nom":"Rouleaux",
                        "longitude":-1.46621185,
                        "latitude":47.20026822
                    },
                    {
                        "nom":"Rennes-Longchamp",
                        "longitude":-1.57063549,
                        "latitude":47.23865543
                    },
                    {
                        "nom":"Roche Maurice",
                        "longitude":-1.61828778,
                        "latitude":47.19631886
                    },
                    {
                        "nom":"Romanet",
                        "longitude":-1.60898075,
                        "latitude":47.21167971
                    },
                    {
                        "nom":"Renan",
                        "longitude":-1.61355149,
                        "latitude":47.21629683
                    },
                    {
                        "nom":"Rinière",
                        "longitude":-1.63589246,
                        "latitude":47.12905932
                    },
                    {
                        "nom":"Renault",
                        "longitude":-1.49570894,
                        "latitude":47.26341561
                    },
                    {
                        "nom":"Renaudières",
                        "longitude":-1.50796107,
                        "latitude":47.29948308
                    },
                    {
                        "nom":"Reinetière",
                        "longitude":-1.49153714,
                        "latitude":47.25688956
                    },
                    {
                        "nom":"Rocade",
                        "longitude":-1.47441804,
                        "latitude":47.16334141
                    },
                    {
                        "nom":"Rouet",
                        "longitude":-1.52139985,
                        "latitude":47.14826668
                    },
                    {
                        "nom":"Rond-Point de Paris",
                        "longitude":-1.53657884,
                        "latitude":47.23404038
                    },
                    {
                        "nom":"République",
                        "longitude":-1.55448386,
                        "latitude":47.20569695
                    },
                    {
                        "nom":"Repos de Chasse",
                        "longitude":-1.59797194,
                        "latitude":47.21655958
                    },
                    {
                        "nom":"Ripossière",
                        "longitude":-1.53500550,
                        "latitude":47.19221273
                    },
                    {
                        "nom":"Trentemoult-Roquios",
                        "longitude":-1.58255320,
                        "latitude":47.19555860
                    },
                    {
                        "nom":"Rond-Point de Rennes",
                        "longitude":-1.56600754,
                        "latitude":47.23322804
                    },
                    {
                        "nom":"Romain Rolland",
                        "longitude":-1.60967109,
                        "latitude":47.20670222
                    },
                    {
                        "nom":"Robert Scott",
                        "longitude":-1.45892796,
                        "latitude":47.18699913
                    },
                    {
                        "nom":"Rousselière",
                        "longitude":-1.51835430,
                        "latitude":47.17565873
                    },
                    {
                        "nom":"Rosenberg",
                        "longitude":-1.65981468,
                        "latitude":47.23568862
                    },
                    {
                        "nom":"Roseraie",
                        "longitude":-1.52833053,
                        "latitude":47.26079610
                    },
                    {
                        "nom":"Recteur Schmitt",
                        "longitude":-1.55285022,
                        "latitude":47.25231631
                    },
                    {
                        "nom":"Ruette",
                        "longitude":-1.51127357,
                        "latitude":47.23335669
                    },
                    {
                        "nom":"Rond-Point de Vannes",
                        "longitude":-1.57301279,
                        "latitude":47.22956812
                    },
                    {
                        "nom":"Rivaudière",
                        "longitude":-1.65071629,
                        "latitude":47.22717663
                    },
                    {
                        "nom":"Roches Vertes",
                        "longitude":-1.52406118,
                        "latitude":47.18519466
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.55720037,
                        "latitude":47.25937477
                    },
                    {
                        "nom":"Santos Dumont",
                        "longitude":-1.56672196,
                        "latitude":47.25932313
                    },
                    {
                        "nom":"St-Aignan-de-Grand-Lieu",
                        "longitude":-1.63440654,
                        "latitude":47.12325584
                    },
                    {
                        "nom":"St-Aignan",
                        "longitude":-1.57844471,
                        "latitude":47.20803766
                    },
                    {
                        "nom":"Salentine",
                        "longitude":-1.61024106,
                        "latitude":47.24847494
                    },
                    {
                        "nom":"Sable d'Or",
                        "longitude":-1.45587347,
                        "latitude":47.26509240
                    },
                    {
                        "nom":"Saupin",
                        "longitude":-1.47634217,
                        "latitude":47.18480285
                    },
                    {
                        "nom":"Sautron",
                        "longitude":-1.68386822,
                        "latitude":47.26538705
                    },
                    {
                        "nom":"Saupin-Crédit Municipal",
                        "longitude":-1.53922912,
                        "latitude":47.21404683
                    },
                    {
                        "nom":"Savarières",
                        "longitude":-1.48876383,
                        "latitude":47.20888822
                    },
                    {
                        "nom":"St-Blaise",
                        "longitude":-1.46914664,
                        "latitude":47.16720837
                    },
                    {
                        "nom":"Sébilleau",
                        "longitude":-1.53169021,
                        "latitude":47.20484325
                    },
                    {
                        "nom":"St-Jean-Baptiste de la Salle",
                        "longitude":-1.56007173,
                        "latitude":47.23342883
                    },
                    {
                        "nom":"Schweitzer",
                        "longitude":-1.49518502,
                        "latitude":47.29486448
                    },
                    {
                        "nom":"St-Clément",
                        "longitude":-1.54669235,
                        "latitude":47.22217173
                    },
                    {
                        "nom":"SCREG",
                        "longitude":-1.63966591,
                        "latitude":47.19414178
                    },
                    {
                        "nom":"St-Donatien",
                        "longitude":-1.54034242,
                        "latitude":47.22941061
                    },
                    {
                        "nom":"Schoelcher",
                        "longitude":-1.62801333,
                        "latitude":47.22057396
                    },
                    {
                        "nom":"Seil",
                        "longitude":-1.57080276,
                        "latitude":47.19388619
                    },
                    {
                        "nom":"Sercel",
                        "longitude":-1.48959865,
                        "latitude":47.26677060
                    },
                    {
                        "nom":"Sèvres",
                        "longitude":-1.52813168,
                        "latitude":47.18667934
                    },
                    {
                        "nom":"St-Félix",
                        "longitude":-1.55648517,
                        "latitude":47.23138841
                    },
                    {
                        "nom":"San-Francisco",
                        "longitude":-1.51649196,
                        "latitude":47.21769135
                    },
                    {
                        "nom":"Sageran",
                        "longitude":-1.57122899,
                        "latitude":47.21963113
                    },
                    {
                        "nom":"Sillon de Bretagne",
                        "longitude":-1.60857095,
                        "latitude":47.24555982
                    },
                    {
                        "nom":"Sinière",
                        "longitude":-1.67784354,
                        "latitude":47.22569458
                    },
                    {
                        "nom":"St-Jacques",
                        "longitude":-1.53793433,
                        "latitude":47.19616724
                    },
                    {
                        "nom":"St-Jean-de-Boiseau",
                        "longitude":-1.72489753,
                        "latitude":47.19585207
                    },
                    {
                        "nom":"St-Jean",
                        "longitude":-1.52374341,
                        "latitude":47.19178010
                    },
                    {
                        "nom":"St-Joseph-de-Porterie",
                        "longitude":-1.51693176,
                        "latitude":47.26784216
                    },
                    {
                        "nom":"St-Laurent",
                        "longitude":-1.59822363,
                        "latitude":47.22357637
                    },
                    {
                        "nom":"Ste-Luce-sur-Loire",
                        "longitude":-1.48498474,
                        "latitude":47.25035223
                    },
                    {
                        "nom":"Solidarité",
                        "longitude":-1.58744262,
                        "latitude":47.21718879
                    },
                    {
                        "nom":"Salorges",
                        "longitude":-1.57503087,
                        "latitude":47.20473102
                    },
                    {
                        "nom":"St-Léger-les-Vignes",
                        "longitude":-1.73030412,
                        "latitude":47.13657004
                    },
                    {
                        "nom":"St-Martin",
                        "longitude":-1.69331638,
                        "latitude":47.21344493
                    },
                    {
                        "nom":"St-Mihiel",
                        "longitude":-1.55338437,
                        "latitude":47.22329706
                    },
                    {
                        "nom":"St-Michel",
                        "longitude":-1.64440886,
                        "latitude":47.22415208
                    },
                    {
                        "nom":"St-Nicolas",
                        "longitude":-1.55918473,
                        "latitude":47.21580574
                    },
                    {
                        "nom":"Sanitat",
                        "longitude":-1.56926084,
                        "latitude":47.21078117
                    },
                    {
                        "nom":"Stade SNUC",
                        "longitude":-1.58109165,
                        "latitude":47.22794281
                    },
                    {
                        "nom":"Souchais",
                        "longitude":-1.48961477,
                        "latitude":47.30765775
                    },
                    {
                        "nom":"Souchais-Ecole",
                        "longitude":-1.49833277,
                        "latitude":47.30367598
                    },
                    {
                        "nom":"Sorin",
                        "longitude":-1.56786290,
                        "latitude":47.19524682
                    },
                    {
                        "nom":"Souillarderie",
                        "longitude":-1.51612746,
                        "latitude":47.23832822
                    },
                    {
                        "nom":"Soweto",
                        "longitude":-1.65156746,
                        "latitude":47.23327207
                    },
                    {
                        "nom":"Saphirs",
                        "longitude":-1.65725626,
                        "latitude":47.20497251
                    },
                    {
                        "nom":"Sully Prudhomme",
                        "longitude":-1.59691320,
                        "latitude":47.23632078
                    },
                    {
                        "nom":"St-Paul-Salengro",
                        "longitude":-1.54606358,
                        "latitude":47.18436498
                    },
                    {
                        "nom":"Sarradin",
                        "longitude":-1.57022650,
                        "latitude":47.22038566
                    },
                    {
                        "nom":"Les Sorinières",
                        "longitude":-1.51809294,
                        "latitude":47.15918543
                    },
                    {
                        "nom":"Sartre",
                        "longitude":-1.64023862,
                        "latitude":47.21429811
                    },
                    {
                        "nom":"St-Sébastien-sur-Loire",
                        "longitude":-1.50333489,
                        "latitude":47.20750294
                    },
                    {
                        "nom":"St-Stanislas",
                        "longitude":-1.55660951,
                        "latitude":47.22219743
                    },
                    {
                        "nom":"Stade",
                        "longitude":-1.70790343,
                        "latitude":47.19131565
                    },
                    {
                        "nom":"Alexandre Vincent-Ste-Thérèse",
                        "longitude":-1.57747675,
                        "latitude":47.23274913
                    },
                    {
                        "nom":"St-Pierre",
                        "longitude":-1.55181863,
                        "latitude":47.21812604
                    },
                    {
                        "nom":"St-Exupéry",
                        "longitude":-1.59487984,
                        "latitude":47.15289572
                    },
                    {
                        "nom":"Surcouf",
                        "longitude":-1.44581947,
                        "latitude":47.26884475
                    },
                    {
                        "nom":"Savaudière",
                        "longitude":-1.51477024,
                        "latitude":47.28925918
                    },
                    {
                        "nom":"Solvardière",
                        "longitude":-1.64184443,
                        "latitude":47.21811576
                    },
                    {
                        "nom":"Stévin",
                        "longitude":-1.63491088,
                        "latitude":47.26005760
                    },
                    {
                        "nom":"Savonnières",
                        "longitude":-1.66277648,
                        "latitude":47.19748509
                    },
                    {
                        "nom":"Syrma",
                        "longitude":-1.47009418,
                        "latitude":47.27714239
                    },
                    {
                        "nom":"Saulzaie",
                        "longitude":-1.63183278,
                        "latitude":47.23107086
                    },
                    {
                        "nom":"Toutes Aides",
                        "longitude":-1.52347007,
                        "latitude":47.22646431
                    },
                    {
                        "nom":"Thébaudières",
                        "longitude":-1.36020747,
                        "latitude":47.31079318
                    },
                    {
                        "nom":"Trébuchet",
                        "longitude":-1.54694450,
                        "latitude":47.22018178
                    },
                    {
                        "nom":"Tristan Bernard",
                        "longitude":-1.46430888,
                        "latitude":47.20510430
                    },
                    {
                        "nom":"Tabarly",
                        "longitude":-1.54898736,
                        "latitude":47.19552476
                    },
                    {
                        "nom":"Trois Chênes",
                        "longitude":-1.47403605,
                        "latitude":47.25818959
                    },
                    {
                        "nom":"Tredégar",
                        "longitude":-1.60487941,
                        "latitude":47.25469292
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.74452027,
                        "latitude":47.19668985
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.47183229,
                        "latitude":47.21611364
                    },
                    {
                        "nom":"Tour Eiffel",
                        "longitude":-1.75425712,
                        "latitude":47.19958731
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.60813792,
                        "latitude":47.21459080
                    },
                    {
                        "nom":"Terray",
                        "longitude":-1.52673138,
                        "latitude":47.25148302
                    },
                    {
                        "nom":"Théatre",
                        "longitude":-1.72878494,
                        "latitude":47.21571204
                    },
                    {
                        "nom":"Centre de Thouaré-sur-Loire",
                        "longitude":-1.44131167,
                        "latitude":47.26702917
                    },
                    {
                        "nom":"Thébaudières",
                        "longitude":-1.60611664,
                        "latitude":47.24456294
                    },
                    {
                        "nom":"Tillay",
                        "longitude":-1.60627659,
                        "latitude":47.22879540
                    },
                    {
                        "nom":"Toutes Joies",
                        "longitude":-1.57292964,
                        "latitude":47.22119467
                    },
                    {
                        "nom":"Télindière",
                        "longitude":-1.73707782,
                        "latitude":47.19614258
                    },
                    {
                        "nom":"Talensac",
                        "longitude":-1.55603622,
                        "latitude":47.22158633
                    },
                    {
                        "nom":"Trois Métairies",
                        "longitude":-1.50163238,
                        "latitude":47.17639742
                    },
                    {
                        "nom":"Tourmaline",
                        "longitude":-1.63257689,
                        "latitude":47.22149773
                    },
                    {
                        "nom":"Trois Moulins",
                        "longitude":-1.54444757,
                        "latitude":47.17847503
                    },
                    {
                        "nom":"Trémissinière",
                        "longitude":-1.54378367,
                        "latitude":47.23857157
                    },
                    {
                        "nom":"Terre Neuvas",
                        "longitude":-1.59135787,
                        "latitude":47.18589150
                    },
                    {
                        "nom":"Tortière",
                        "longitude":-1.54962352,
                        "latitude":47.23522257
                    },
                    {
                        "nom":"Terpsichore",
                        "longitude":-1.50250131,
                        "latitude":47.28246303
                    },
                    {
                        "nom":"Tripode",
                        "longitude":-1.54080334,
                        "latitude":47.20840978
                    },
                    {
                        "nom":"Tréca",
                        "longitude":-1.50149834,
                        "latitude":47.25007399
                    },
                    {
                        "nom":"Trianon",
                        "longitude":-1.43038312,
                        "latitude":47.27322232
                    },
                    {
                        "nom":"Tribunes",
                        "longitude":-1.56965489,
                        "latitude":47.25057746
                    },
                    {
                        "nom":"Trocardière",
                        "longitude":-1.57002237,
                        "latitude":47.18319454
                    },
                    {
                        "nom":"Terres Quartières",
                        "longitude":-1.65923196,
                        "latitude":47.13479051
                    },
                    {
                        "nom":"Trentemoult",
                        "longitude":-1.58291617,
                        "latitude":47.19149315
                    },
                    {
                        "nom":"Touzelles",
                        "longitude":-1.46624635,
                        "latitude":47.20819254
                    },
                    {
                        "nom":"Usine Brûlée",
                        "longitude":-1.63206121,
                        "latitude":47.19494446
                    },
                    {
                        "nom":"Usine à gaz",
                        "longitude":-1.62054080,
                        "latitude":47.19462022
                    },
                    {
                        "nom":"Utrillo",
                        "longitude":-1.56346680,
                        "latitude":47.18206569
                    },
                    {
                        "nom":"Vaillant",
                        "longitude":-1.67113033,
                        "latitude":47.19962676
                    },
                    {
                        "nom":"Vallée",
                        "longitude":-1.57678668,
                        "latitude":47.25042573
                    },
                    {
                        "nom":"Vincent Auriol",
                        "longitude":-1.59227572,
                        "latitude":47.22179776
                    },
                    {
                        "nom":"Vaucanson",
                        "longitude":-1.58157525,
                        "latitude":47.21288501
                    },
                    {
                        "nom":"Val du Cens",
                        "longitude":-1.66515341,
                        "latitude":47.26108390
                    },
                    {
                        "nom":"Vieux Doulon",
                        "longitude":-1.50415391,
                        "latitude":47.23548543
                    },
                    {
                        "nom":"Ville-au-Denis",
                        "longitude":-1.62753008,
                        "latitude":47.16231434
                    },
                    {
                        "nom":"Verdure",
                        "longitude":-1.65340905,
                        "latitude":47.19591839
                    },
                    {
                        "nom":"Véga",
                        "longitude":-1.46445133,
                        "latitude":47.27966997
                    },
                    {
                        "nom":"Verger",
                        "longitude":-1.49704348,
                        "latitude":47.29309160
                    },
                    {
                        "nom":"Verrière",
                        "longitude":-1.57077393,
                        "latitude":47.27674882
                    },
                    {
                        "nom":"Vesprées",
                        "longitude":-1.50289758,
                        "latitude":47.24561434
                    },
                    {
                        "nom":"Verlaine",
                        "longitude":-1.59488151,
                        "latitude":47.23386817
                    },
                    {
                        "nom":"Village Expo",
                        "longitude":-1.61515286,
                        "latitude":47.21471073
                    },
                    {
                        "nom":"Vieux Four",
                        "longitude":-1.70087103,
                        "latitude":47.19093184
                    },
                    {
                        "nom":"Vincent Gâche",
                        "longitude":-1.54833304,
                        "latitude":47.20653496
                    },
                    {
                        "nom":"Viarme-Talensac",
                        "longitude":-1.56276201,
                        "latitude":47.22135898
                    },
                    {
                        "nom":"Violettes",
                        "longitude":-1.50887313,
                        "latitude":47.19813167
                    },
                    {
                        "nom":"Villeneuve",
                        "longitude":-1.51320907,
                        "latitude":47.19411407
                    },
                    {
                        "nom":"Vison",
                        "longitude":-1.50967453,
                        "latitude":47.24421766
                    },
                    {
                        "nom":"Viviers",
                        "longitude":-1.48488496,
                        "latitude":47.17308163
                    },
                    {
                        "nom":"Val d'Or",
                        "longitude":-1.58279107,
                        "latitude":47.24391709
                    },
                    {
                        "nom":"Vrière",
                        "longitude":-1.55607767,
                        "latitude":47.28210909
                    },
                    {
                        "nom":"Vincent Scotto",
                        "longitude":-1.56608492,
                        "latitude":47.25240959
                    },
                    {
                        "nom":"Vertonne",
                        "longitude":-1.49433357,
                        "latitude":47.18699783
                    },
                    {
                        "nom":"Vertou",
                        "longitude":-1.47128903,
                        "latitude":47.16947918
                    },
                    {
                        "nom":"Wattignies",
                        "longitude":-1.54643001,
                        "latitude":47.20398718
                    },
                    {
                        "nom":"Walt Disney",
                        "longitude":-1.48997084,
                        "latitude":47.19434794
                    },
                    {
                        "nom":"Croix Bonneau",
                        "longitude":-1.59547893,
                        "latitude":47.20961928
                    },
                    {
                        "nom":"Croix Jeannette",
                        "longitude":-1.62368918,
                        "latitude":47.17109344
                    },
                    {
                        "nom":"Croix de Rezé",
                        "longitude":-1.55891535,
                        "latitude":47.17942763
                    },
                    {
                        "nom":"Croisy",
                        "longitude":-1.62634800,
                        "latitude":47.25341677
                    },
                    {
                        "nom":"Croix Truin",
                        "longitude":-1.71173896,
                        "latitude":47.19136106
                    },
                    {
                        "nom":"Zac-des-Coteaux",
                        "longitude":-1.67882328,
                        "latitude":47.16477019
                    },
                    {
                        "nom":"Zénith",
                        "longitude":-1.62928013,
                        "latitude":47.23052822
                    },
                    {
                        "nom":"Zimmer",
                        "longitude":-1.65168552,
                        "latitude":47.21002940
                    },
                    {
                        "nom":"Zola",
                        "longitude":-1.58600550,
                        "latitude":47.21381511
                    },
                    {
                        "nom":"Abel Durand",
                        "longitude":-1.60339246,
                        "latitude":47.22015730
                    },
                    {
                        "nom":"Avenue Blanche",
                        "longitude":-1.58942059,
                        "latitude":47.22973100
                    },
                    {
                        "nom":"Avenue Blanche",
                        "longitude":-1.58914361,
                        "latitude":47.22956030
                    },
                    {
                        "nom":"Angle Chaillou",
                        "longitude":-1.57197754,
                        "latitude":47.26968290
                    },
                    {
                        "nom":"Angle Chaillou",
                        "longitude":-1.57211610,
                        "latitude":47.26976827
                    },
                    {
                        "nom":"Audubon",
                        "longitude":-1.72349280,
                        "latitude":47.21391693
                    },
                    {
                        "nom":"Audubon",
                        "longitude":-1.72309044,
                        "latitude":47.21384103
                    },
                    {
                        "nom":"Aimé Delrue",
                        "longitude":-1.55024277,
                        "latitude":47.20917256
                    },
                    {
                        "nom":"Aimé Delrue",
                        "longitude":-1.55023624,
                        "latitude":47.20908271
                    },
                    {
                        "nom":"Avenue des Pins",
                        "longitude":-1.59670445,
                        "latitude":47.25326073
                    },
                    {
                        "nom":"Avenue des Pins",
                        "longitude":-1.59766118,
                        "latitude":47.25367844
                    },
                    {
                        "nom":"Aéroclub",
                        "longitude":-1.59825918,
                        "latitude":47.15935541
                    },
                    {
                        "nom":"Aéroclub",
                        "longitude":-1.59799574,
                        "latitude":47.15936441
                    },
                    {
                        "nom":"Aéroport",
                        "longitude":-1.59741579,
                        "latitude":47.15686225
                    },
                    {
                        "nom":"Aéroport",
                        "longitude":-1.59719195,
                        "latitude":47.15741033
                    },
                    {
                        "nom":"Aérospatiale",
                        "longitude":-1.58793208,
                        "latitude":47.16078877
                    },
                    {
                        "nom":"Aérospatiale",
                        "longitude":-1.58704949,
                        "latitude":47.16135928
                    },
                    {
                        "nom":"Anatole France",
                        "longitude":-1.57126714,
                        "latitude":47.22377294
                    },
                    {
                        "nom":"Anatole France",
                        "longitude":-1.57153091,
                        "latitude":47.22376399
                    },
                    {
                        "nom":"Anatole France",
                        "longitude":-1.57193969,
                        "latitude":47.22393027
                    },
                    {
                        "nom":"L'Angebert",
                        "longitude":-1.45576884,
                        "latitude":47.14054089
                    },
                    {
                        "nom":"L'Angebert",
                        "longitude":-1.45551818,
                        "latitude":47.14072927
                    },
                    {
                        "nom":"Angevinière",
                        "longitude":-1.61093247,
                        "latitude":47.24529886
                    },
                    {
                        "nom":"Angevinière",
                        "longitude":-1.61093247,
                        "latitude":47.24529886
                    },
                    {
                        "nom":"Angevinière",
                        "longitude":-1.61236125,
                        "latitude":47.24512384
                    },
                    {
                        "nom":"Angevinière",
                        "longitude":-1.61156236,
                        "latitude":47.24505212
                    },
                    {
                        "nom":"Airbus",
                        "longitude":-1.59185083,
                        "latitude":47.15840336
                    },
                    {
                        "nom":"Airbus",
                        "longitude":-1.59044143,
                        "latitude":47.15899187
                    },
                    {
                        "nom":"Aiguillon",
                        "longitude":-1.49652294,
                        "latitude":47.25447241
                    },
                    {
                        "nom":"Aiguillon",
                        "longitude":-1.49691883,
                        "latitude":47.25445925
                    },
                    {
                        "nom":"Allende",
                        "longitude":-1.62387641,
                        "latitude":47.17541044
                    },
                    {
                        "nom":"Allende",
                        "longitude":-1.62320432,
                        "latitude":47.17525341
                    },
                    {
                        "nom":"Allende",
                        "longitude":-1.62462708,
                        "latitude":47.17484419
                    },
                    {
                        "nom":"Aulne",
                        "longitude":-1.63893833,
                        "latitude":47.27685201
                    },
                    {
                        "nom":"Aulne",
                        "longitude":-1.63893165,
                        "latitude":47.27676217
                    },
                    {
                        "nom":"Armoricaine",
                        "longitude":-1.64869359,
                        "latitude":47.25859140
                    },
                    {
                        "nom":"Armoricaine",
                        "longitude":-1.64747915,
                        "latitude":47.25827307
                    },
                    {
                        "nom":"Américains",
                        "longitude":-1.56820260,
                        "latitude":47.23612591
                    },
                    {
                        "nom":"Américains",
                        "longitude":-1.56791254,
                        "latitude":47.23577547
                    },
                    {
                        "nom":"Ar Mor",
                        "longitude":-1.62367286,
                        "latitude":47.22801889
                    },
                    {
                        "nom":"Ar Mor",
                        "longitude":-1.62409514,
                        "latitude":47.22836466
                    },
                    {
                        "nom":"Ampère",
                        "longitude":-1.55320077,
                        "latitude":47.26077064
                    },
                    {
                        "nom":"Ampère",
                        "longitude":-1.55346472,
                        "latitude":47.26076174
                    },
                    {
                        "nom":"Les Anges",
                        "longitude":-1.63640188,
                        "latitude":47.27477771
                    },
                    {
                        "nom":"Les Anges",
                        "longitude":-1.63653388,
                        "latitude":47.27477316
                    },
                    {
                        "nom":"Antons",
                        "longitude":-1.62256356,
                        "latitude":47.25768995
                    },
                    {
                        "nom":"Antons",
                        "longitude":-1.62323668,
                        "latitude":47.25784698
                    },
                    {
                        "nom":"Apave",
                        "longitude":-1.64769437,
                        "latitude":47.22565984
                    },
                    {
                        "nom":"Apave",
                        "longitude":-1.64754241,
                        "latitude":47.22539488
                    },
                    {
                        "nom":"Ampère",
                        "longitude":-1.50789738,
                        "latitude":47.30227711
                    },
                    {
                        "nom":"Ampère",
                        "longitude":-1.50788445,
                        "latitude":47.30209741
                    },
                    {
                        "nom":"Ambroise Paré",
                        "longitude":-1.60866390,
                        "latitude":47.22889385
                    },
                    {
                        "nom":"Ambroise Paré",
                        "longitude":-1.60854526,
                        "latitude":47.22907805
                    },
                    {
                        "nom":"Aquitaine",
                        "longitude":-1.61492638,
                        "latitude":47.20985471
                    },
                    {
                        "nom":"Aquitaine",
                        "longitude":-1.61478790,
                        "latitude":47.20976939
                    },
                    {
                        "nom":"Arago",
                        "longitude":-1.50480495,
                        "latitude":47.30346079
                    },
                    {
                        "nom":"Arago",
                        "longitude":-1.50494350,
                        "latitude":47.30354624
                    },
                    {
                        "nom":"Arbois",
                        "longitude":-1.58187869,
                        "latitude":47.25853899
                    },
                    {
                        "nom":"Arbois",
                        "longitude":-1.58213604,
                        "latitude":47.25844018
                    },
                    {
                        "nom":"Arcades",
                        "longitude":-1.65274239,
                        "latitude":47.21179426
                    },
                    {
                        "nom":"Arcades",
                        "longitude":-1.65355356,
                        "latitude":47.21203639
                    },
                    {
                        "nom":"Argonautes",
                        "longitude":-1.48182549,
                        "latitude":47.30242198
                    },
                    {
                        "nom":"Argonautes",
                        "longitude":-1.48182549,
                        "latitude":47.30242198
                    },
                    {
                        "nom":"Armorique",
                        "longitude":-1.44494779,
                        "latitude":47.27148504
                    },
                    {
                        "nom":"Armorique",
                        "longitude":-1.44534382,
                        "latitude":47.27147205
                    },
                    {
                        "nom":"Antarès",
                        "longitude":-1.45970027,
                        "latitude":47.28171763
                    },
                    {
                        "nom":"Antarès",
                        "longitude":-1.45970027,
                        "latitude":47.28171763
                    },
                    {
                        "nom":"La Chapelle Aulnay",
                        "longitude":-1.54314964,
                        "latitude":47.30821266
                    },
                    {
                        "nom":"La Chapelle Aulnay",
                        "longitude":-1.54342688,
                        "latitude":47.30838347
                    },
                    {
                        "nom":"La Chapelle Aulnay",
                        "longitude":-1.54248265,
                        "latitude":47.30814501
                    },
                    {
                        "nom":"L'Aufrère",
                        "longitude":-1.52507017,
                        "latitude":47.16804829
                    },
                    {
                        "nom":"Augé",
                        "longitude":-1.51736648,
                        "latitude":47.19496578
                    },
                    {
                        "nom":"Aveneaux",
                        "longitude":-1.62990901,
                        "latitude":47.23365905
                    },
                    {
                        "nom":"Aveneaux",
                        "longitude":-1.63051521,
                        "latitude":47.23291762
                    },
                    {
                        "nom":"Barbinière",
                        "longitude":-1.48180920,
                        "latitude":47.15958402
                    },
                    {
                        "nom":"Barbinière",
                        "longitude":-1.48169027,
                        "latitude":47.15976810
                    },
                    {
                        "nom":"Bagatelle",
                        "longitude":-1.62006132,
                        "latitude":47.24354485
                    },
                    {
                        "nom":"Bagatelle",
                        "longitude":-1.62005467,
                        "latitude":47.24345501
                    },
                    {
                        "nom":"Babinière",
                        "longitude":-1.54620749,
                        "latitude":47.25920497
                    },
                    {
                        "nom":"Bel Air",
                        "longitude":-1.55964729,
                        "latitude":47.22578752
                    },
                    {
                        "nom":"Bel Air",
                        "longitude":-1.55922545,
                        "latitude":47.22544151
                    },
                    {
                        "nom":"Balinière",
                        "longitude":-1.55385088,
                        "latitude":47.18608369
                    },
                    {
                        "nom":"Balinière",
                        "longitude":-1.55398268,
                        "latitude":47.18607923
                    },
                    {
                        "nom":"Balinière",
                        "longitude":-1.55440535,
                        "latitude":47.18462388
                    },
                    {
                        "nom":"Balinière",
                        "longitude":-1.55439882,
                        "latitude":47.18453403
                    },
                    {
                        "nom":"Bois des Anses",
                        "longitude":-1.49481369,
                        "latitude":47.24723416
                    },
                    {
                        "nom":"Bois des Anses",
                        "longitude":-1.49480082,
                        "latitude":47.24705446
                    },
                    {
                        "nom":"Bois des Anses",
                        "longitude":-1.49477506,
                        "latitude":47.24669507
                    },
                    {
                        "nom":"Bois des Anses",
                        "longitude":-1.49490701,
                        "latitude":47.24669068
                    },
                    {
                        "nom":"La Barre",
                        "longitude":-1.38295034,
                        "latitude":47.30844177
                    },
                    {
                        "nom":"La Barre",
                        "longitude":-1.38257905,
                        "latitude":47.30881395
                    },
                    {
                        "nom":"Bastille",
                        "longitude":-1.59722149,
                        "latitude":47.17938648
                    },
                    {
                        "nom":"Bastille",
                        "longitude":-1.59693154,
                        "latitude":47.17903610
                    },
                    {
                        "nom":"Bastille",
                        "longitude":-1.59693154,
                        "latitude":47.17903610
                    },
                    {
                        "nom":"Bastille",
                        "longitude":-1.59722149,
                        "latitude":47.17938648
                    },
                    {
                        "nom":"Batignolles",
                        "longitude":-1.52966934,
                        "latitude":47.25552751
                    },
                    {
                        "nom":"Batignolles",
                        "longitude":-1.52993977,
                        "latitude":47.25560851
                    },
                    {
                        "nom":"Baugerie",
                        "longitude":-1.51909416,
                        "latitude":47.20427473
                    },
                    {
                        "nom":"Baugerie",
                        "longitude":-1.51948320,
                        "latitude":47.20417165
                    },
                    {
                        "nom":"Basse Orévière",
                        "longitude":-1.65894412,
                        "latitude":47.22049657
                    },
                    {
                        "nom":"Basse Orévière",
                        "longitude":-1.65893741,
                        "latitude":47.22040673
                    },
                    {
                        "nom":"Bd de La Baule",
                        "longitude":-1.60725043,
                        "latitude":47.22227709
                    },
                    {
                        "nom":"Bd de La Baule",
                        "longitude":-1.60725043,
                        "latitude":47.22227709
                    },
                    {
                        "nom":"Bd de La Baule",
                        "longitude":-1.60734919,
                        "latitude":47.22182336
                    },
                    {
                        "nom":"Bd de La Baule",
                        "longitude":-1.60838431,
                        "latitude":47.22151772
                    },
                    {
                        "nom":"Boisbonne",
                        "longitude":-1.50837641,
                        "latitude":47.28686058
                    },
                    {
                        "nom":"Boisbonne",
                        "longitude":-1.50864049,
                        "latitude":47.28685178
                    },
                    {
                        "nom":"Bois Cesbron",
                        "longitude":-1.62453869,
                        "latitude":47.26113484
                    },
                    {
                        "nom":"Bois Cesbron",
                        "longitude":-1.62426144,
                        "latitude":47.26096422
                    },
                    {
                        "nom":"Basse Chênaie",
                        "longitude":-1.50919095,
                        "latitude":47.24117164
                    },
                    {
                        "nom":"Basse Chênaie",
                        "longitude":-1.50789102,
                        "latitude":47.24148518
                    },
                    {
                        "nom":"Basse Chênaie",
                        "longitude":-1.50865030,
                        "latitude":47.24100954
                    },
                    {
                        "nom":"Basse Chênaie",
                        "longitude":-1.50884038,
                        "latitude":47.24181377
                    },
                    {
                        "nom":"Branchoire",
                        "longitude":-1.60308856,
                        "latitude":47.21782589
                    },
                    {
                        "nom":"Branchoire",
                        "longitude":-1.60337214,
                        "latitude":47.21808640
                    },
                    {
                        "nom":"Branchoire",
                        "longitude":-1.60367558,
                        "latitude":47.21861645
                    },
                    {
                        "nom":"Branchoire",
                        "longitude":-1.60415010,
                        "latitude":47.21787967
                    },
                    {
                        "nom":"Blanchet",
                        "longitude":-1.54329386,
                        "latitude":47.17166881
                    },
                    {
                        "nom":"Blanchet",
                        "longitude":-1.54313606,
                        "latitude":47.17131385
                    },
                    {
                        "nom":"Bd des Anglais",
                        "longitude":-1.58270091,
                        "latitude":47.22644702
                    },
                    {
                        "nom":"Bd des Anglais",
                        "longitude":-1.58336035,
                        "latitude":47.22642460
                    },
                    {
                        "nom":"Bd des Anglais",
                        "longitude":-1.58298444,
                        "latitude":47.22670758
                    },
                    {
                        "nom":"Bd des Anglais",
                        "longitude":-1.58320871,
                        "latitude":47.22615955
                    },
                    {
                        "nom":"Baudelaire",
                        "longitude":-1.59314693,
                        "latitude":47.23365711
                    },
                    {
                        "nom":"Baudelaire",
                        "longitude":-1.59250720,
                        "latitude":47.23394912
                    },
                    {
                        "nom":"Bodinières",
                        "longitude":-1.64307564,
                        "latitude":47.20978665
                    },
                    {
                        "nom":"Bodinières",
                        "longitude":-1.64281196,
                        "latitude":47.20979576
                    },
                    {
                        "nom":"Bodinières",
                        "longitude":-1.64190242,
                        "latitude":47.21000731
                    },
                    {
                        "nom":"Bodinières",
                        "longitude":-1.64179062,
                        "latitude":47.21028138
                    },
                    {
                        "nom":"Bout des Landes",
                        "longitude":-1.58226680,
                        "latitude":47.26744242
                    },
                    {
                        "nom":"Bourdonnais",
                        "longitude":-1.57998111,
                        "latitude":47.20555361
                    },
                    {
                        "nom":"Bourdonnais",
                        "longitude":-1.57951951,
                        "latitude":47.20646998
                    },
                    {
                        "nom":"Bd de Doulon",
                        "longitude":-1.52001726,
                        "latitude":47.22441834
                    },
                    {
                        "nom":"Bd de Doulon",
                        "longitude":-1.52001726,
                        "latitude":47.22441834
                    },
                    {
                        "nom":"Bd de Doulon",
                        "longitude":-1.52028103,
                        "latitude":47.22440951
                    },
                    {
                        "nom":"Bd de Doulon",
                        "longitude":-1.51985298,
                        "latitude":47.22397351
                    },
                    {
                        "nom":"Bout des Pavés",
                        "longitude":-1.57629790,
                        "latitude":47.25278405
                    },
                    {
                        "nom":"Bout des Pavés",
                        "longitude":-1.57629132,
                        "latitude":47.25269421
                    },
                    {
                        "nom":"Bout des Pavés",
                        "longitude":-1.57620540,
                        "latitude":47.25332759
                    },
                    {
                        "nom":"Bourderies",
                        "longitude":-1.59592025,
                        "latitude":47.20663194
                    },
                    {
                        "nom":"Bourderies",
                        "longitude":-1.59564997,
                        "latitude":47.20655110
                    },
                    {
                        "nom":"Bd des Sports",
                        "longitude":-1.46812862,
                        "latitude":47.16589107
                    },
                    {
                        "nom":"Bd des Sports",
                        "longitude":-1.46876821,
                        "latitude":47.16559974
                    },
                    {
                        "nom":"Bel Ebat",
                        "longitude":-1.63623963,
                        "latitude":47.27793573
                    },
                    {
                        "nom":"Bel Endroit",
                        "longitude":-1.63557761,
                        "latitude":47.15329989
                    },
                    {
                        "nom":"Bel Endroit",
                        "longitude":-1.63396536,
                        "latitude":47.15470661
                    },
                    {
                        "nom":"Beauger",
                        "longitude":-1.55299443,
                        "latitude":47.23429829
                    },
                    {
                        "nom":"Beauger",
                        "longitude":-1.55298790,
                        "latitude":47.23420845
                    },
                    {
                        "nom":"Beillevaire",
                        "longitude":-1.60741126,
                        "latitude":47.24956252
                    },
                    {
                        "nom":"Beillevaire",
                        "longitude":-1.60727269,
                        "latitude":47.24947719
                    },
                    {
                        "nom":"Bêle",
                        "longitude":-1.51035666,
                        "latitude":47.26472929
                    },
                    {
                        "nom":"Bêle",
                        "longitude":-1.51021821,
                        "latitude":47.26464384
                    },
                    {
                        "nom":"Belges-Montbazon",
                        "longitude":-1.54098400,
                        "latitude":47.23461282
                    },
                    {
                        "nom":"Belges-Montbazon",
                        "longitude":-1.54017950,
                        "latitude":47.23445975
                    },
                    {
                        "nom":"Belges Montbazon",
                        "longitude":-1.54081137,
                        "latitude":47.23587954
                    },
                    {
                        "nom":"Belges-Montbazon",
                        "longitude":-1.54105566,
                        "latitude":47.23560113
                    },
                    {
                        "nom":"Beaulieu",
                        "longitude":-1.53704358,
                        "latitude":47.20394287
                    },
                    {
                        "nom":"Beaulieu",
                        "longitude":-1.53704358,
                        "latitude":47.20394287
                    },
                    {
                        "nom":"Beaulieu",
                        "longitude":-1.53476335,
                        "latitude":47.20347912
                    },
                    {
                        "nom":"Beaulieu",
                        "longitude":-1.53411717,
                        "latitude":47.20368096
                    },
                    {
                        "nom":"Belloc",
                        "longitude":-1.61460707,
                        "latitude":47.20374088
                    },
                    {
                        "nom":"Belloc",
                        "longitude":-1.61469247,
                        "latitude":47.20310746
                    },
                    {
                        "nom":"Bessonneau",
                        "longitude":-1.71377698,
                        "latitude":47.21479930
                    },
                    {
                        "nom":"Bessonneau",
                        "longitude":-1.71349968,
                        "latitude":47.21462890
                    },
                    {
                        "nom":"Belle Etoile",
                        "longitude":-1.45207869,
                        "latitude":47.28061724
                    },
                    {
                        "nom":"Belle Etoile",
                        "longitude":-1.45165075,
                        "latitude":47.28018099
                    },
                    {
                        "nom":"Beauvaiserie",
                        "longitude":-1.65444519,
                        "latitude":47.13627173
                    },
                    {
                        "nom":"Beauvaiserie",
                        "longitude":-1.65415018,
                        "latitude":47.13620990
                    },
                    {
                        "nom":"Bonne Garde",
                        "longitude":-1.53124407,
                        "latitude":47.19684238
                    },
                    {
                        "nom":"Bonne Garde",
                        "longitude":-1.53124407,
                        "latitude":47.19684238
                    },
                    {
                        "nom":"Beaugency",
                        "longitude":-1.48980943,
                        "latitude":47.20317952
                    },
                    {
                        "nom":"Beaugency",
                        "longitude":-1.49024992,
                        "latitude":47.20379533
                    },
                    {
                        "nom":"Basse-Goulaine",
                        "longitude":-1.46836878,
                        "latitude":47.20830260
                    },
                    {
                        "nom":"Basse-Goulaine",
                        "longitude":-1.46850063,
                        "latitude":47.20829825
                    },
                    {
                        "nom":"Bourgonnière",
                        "longitude":-1.65545309,
                        "latitude":47.20737690
                    },
                    {
                        "nom":"Bourgonnière",
                        "longitude":-1.65552459,
                        "latitude":47.20656376
                    },
                    {
                        "nom":"Bourgogne",
                        "longitude":-1.58920949,
                        "latitude":47.24667097
                    },
                    {
                        "nom":"Bourgogne",
                        "longitude":-1.58931505,
                        "latitude":47.24630711
                    },
                    {
                        "nom":"Bougrière",
                        "longitude":-1.47183238,
                        "latitude":47.25511025
                    },
                    {
                        "nom":"Bougrière",
                        "longitude":-1.47208992,
                        "latitude":47.25501168
                    },
                    {
                        "nom":"Bergerie",
                        "longitude":-1.62824352,
                        "latitude":47.23794964
                    },
                    {
                        "nom":"Bergerie",
                        "longitude":-1.62871787,
                        "latitude":47.23721277
                    },
                    {
                        "nom":"Bergeronnettes",
                        "longitude":-1.60124722,
                        "latitude":47.23950537
                    },
                    {
                        "nom":"Bergeronnettes",
                        "longitude":-1.60030394,
                        "latitude":47.23926738
                    },
                    {
                        "nom":"Bougainville",
                        "longitude":-1.58359414,
                        "latitude":47.19894579
                    },
                    {
                        "nom":"Bougainville",
                        "longitude":-1.58358756,
                        "latitude":47.19885595
                    },
                    {
                        "nom":"Bois Hardy",
                        "longitude":-1.60059220,
                        "latitude":47.19827606
                    },
                    {
                        "nom":"Bois Hardy",
                        "longitude":-1.60005832,
                        "latitude":47.19820423
                    },
                    {
                        "nom":"Aristide Briand",
                        "longitude":-1.49503045,
                        "latitude":47.29640065
                    },
                    {
                        "nom":"Aristide Briand",
                        "longitude":-1.49516251,
                        "latitude":47.29639626
                    },
                    {
                        "nom":"Belle-Ile",
                        "longitude":-1.73275085,
                        "latitude":47.22439959
                    },
                    {
                        "nom":"Belle-Ile",
                        "longitude":-1.73232107,
                        "latitude":47.22396438
                    },
                    {
                        "nom":"Bignons",
                        "longitude":-1.49298004,
                        "latitude":47.19947164
                    },
                    {
                        "nom":"Bignons",
                        "longitude":-1.49340125,
                        "latitude":47.19981789
                    },
                    {
                        "nom":"Bignons",
                        "longitude":-1.49260384,
                        "latitude":47.19975433
                    },
                    {
                        "nom":"Basse Ile",
                        "longitude":-1.56210239,
                        "latitude":47.19598233
                    },
                    {
                        "nom":"Basse Ile",
                        "longitude":-1.56313727,
                        "latitude":47.19567710
                    },
                    {
                        "nom":"Basse Indre",
                        "longitude":-1.67272277,
                        "latitude":47.19795002
                    },
                    {
                        "nom":"Basse Indre",
                        "longitude":-1.67203679,
                        "latitude":47.19761360
                    },
                    {
                        "nom":"Basse Indre",
                        "longitude":-1.67321635,
                        "latitude":47.19748247
                    },
                    {
                        "nom":"Bigeottière",
                        "longitude":-1.61953406,
                        "latitude":47.25608260
                    },
                    {
                        "nom":"Bigeottière",
                        "longitude":-1.61925685,
                        "latitude":47.25591197
                    },
                    {
                        "nom":"Bois Jaulin",
                        "longitude":-1.63698208,
                        "latitude":47.17757104
                    },
                    {
                        "nom":"Bois Jaulin",
                        "longitude":-1.63685031,
                        "latitude":47.17757559
                    },
                    {
                        "nom":"Beaujoire",
                        "longitude":-1.52593760,
                        "latitude":47.25880479
                    },
                    {
                        "nom":"Beaujoire",
                        "longitude":-1.52593760,
                        "latitude":47.25880479
                    },
                    {
                        "nom":"Beaujoire",
                        "longitude":-1.52783717,
                        "latitude":47.25946167
                    },
                    {
                        "nom":"Beaujoire",
                        "longitude":-1.52828062,
                        "latitude":47.25827598
                    },
                    {
                        "nom":"Balavoine",
                        "longitude":-1.55428586,
                        "latitude":47.30657698
                    },
                    {
                        "nom":"Balavoine",
                        "longitude":-1.55480112,
                        "latitude":47.30637948
                    },
                    {
                        "nom":"Blancho",
                        "longitude":-1.73493592,
                        "latitude":47.21315286
                    },
                    {
                        "nom":"Blancho",
                        "longitude":-1.73467907,
                        "latitude":47.21325202
                    },
                    {
                        "nom":"Boulodrome",
                        "longitude":-1.51540161,
                        "latitude":47.25762607
                    },
                    {
                        "nom":"Boulodrome",
                        "longitude":-1.51412721,
                        "latitude":47.25829906
                    },
                    {
                        "nom":"Belem",
                        "longitude":-1.45300144,
                        "latitude":47.26563709
                    },
                    {
                        "nom":"Belem",
                        "longitude":-1.45298870,
                        "latitude":47.26545739
                    },
                    {
                        "nom":"Berligout",
                        "longitude":-1.67842053,
                        "latitude":47.21576638
                    },
                    {
                        "nom":"Berligout",
                        "longitude":-1.67854564,
                        "latitude":47.21567195
                    },
                    {
                        "nom":"Bellestre",
                        "longitude":-1.69406522,
                        "latitude":47.14793376
                    },
                    {
                        "nom":"Bellestre",
                        "longitude":-1.69344058,
                        "latitude":47.14840602
                    },
                    {
                        "nom":"Bois de La Noé",
                        "longitude":-1.67169414,
                        "latitude":47.14186934
                    },
                    {
                        "nom":"Bois de La Noé",
                        "longitude":-1.67142407,
                        "latitude":47.14178867
                    },
                    {
                        "nom":"Blordière",
                        "longitude":-1.53398357,
                        "latitude":47.17990799
                    },
                    {
                        "nom":"Blordière",
                        "longitude":-1.53385829,
                        "latitude":47.18000227
                    },
                    {
                        "nom":"Bellevue",
                        "longitude":-1.47360365,
                        "latitude":47.23541838
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61153744,
                        "latitude":47.20870990
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61140560,
                        "latitude":47.20871441
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61228208,
                        "latitude":47.20805389
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61155733,
                        "latitude":47.20897943
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61127376,
                        "latitude":47.20871893
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61152418,
                        "latitude":47.20853021
                    },
                    {
                        "nom":"Mendès-France-Bellevue",
                        "longitude":-1.61228208,
                        "latitude":47.20805389
                    },
                    {
                        "nom":"Berlioz",
                        "longitude":-1.57163115,
                        "latitude":47.23961242
                    },
                    {
                        "nom":"Berlioz",
                        "longitude":-1.57149923,
                        "latitude":47.23961689
                    },
                    {
                        "nom":"Bernard",
                        "longitude":-1.52515008,
                        "latitude":47.16732508
                    },
                    {
                        "nom":"Blanchetière",
                        "longitude":-1.55584836,
                        "latitude":47.28986239
                    },
                    {
                        "nom":"Blanchetière",
                        "longitude":-1.55609936,
                        "latitude":47.28967380
                    },
                    {
                        "nom":"Bignonnet",
                        "longitude":-1.46511578,
                        "latitude":47.17553725
                    },
                    {
                        "nom":"Bignonnet",
                        "longitude":-1.46524755,
                        "latitude":47.17553290
                    },
                    {
                        "nom":"Bénélux",
                        "longitude":-1.49914288,
                        "latitude":47.25411511
                    },
                    {
                        "nom":"Bénélux",
                        "longitude":-1.49913644,
                        "latitude":47.25402527
                    },
                    {
                        "nom":"Bernanos",
                        "longitude":-1.61335590,
                        "latitude":47.20648589
                    },
                    {
                        "nom":"Bernanos",
                        "longitude":-1.61385671,
                        "latitude":47.20610844
                    },
                    {
                        "nom":"Bignon",
                        "longitude":-1.59787162,
                        "latitude":47.24934799
                    },
                    {
                        "nom":"Bignon",
                        "longitude":-1.59786501,
                        "latitude":47.24925815
                    },
                    {
                        "nom":"BN",
                        "longitude":-1.49662542,
                        "latitude":47.18394946
                    },
                    {
                        "nom":"BN",
                        "longitude":-1.49640687,
                        "latitude":47.18458718
                    },
                    {
                        "nom":"Bonneville",
                        "longitude":-1.53650309,
                        "latitude":47.24395007
                    },
                    {
                        "nom":"Bonneville",
                        "longitude":-1.53636465,
                        "latitude":47.24386466
                    },
                    {
                        "nom":"Centre de Bouguenais",
                        "longitude":-1.62324646,
                        "latitude":47.17939525
                    },
                    {
                        "nom":"Centre de Bouguenais",
                        "longitude":-1.62332507,
                        "latitude":47.17867198
                    },
                    {
                        "nom":"Bourdonnières",
                        "longitude":-1.52078997,
                        "latitude":47.18746583
                    },
                    {
                        "nom":"Bourdonnières",
                        "longitude":-1.52078997,
                        "latitude":47.18746583
                    },
                    {
                        "nom":"Bourdonnières",
                        "longitude":-1.52203023,
                        "latitude":47.18634350
                    },
                    {
                        "nom":"Bourdonnières",
                        "longitude":-1.52216850,
                        "latitude":47.18642894
                    },
                    {
                        "nom":"Bouffay",
                        "longitude":-1.55235513,
                        "latitude":47.21459535
                    },
                    {
                        "nom":"Bouffay",
                        "longitude":-1.55235513,
                        "latitude":47.21459535
                    },
                    {
                        "nom":"Bourgeonnière",
                        "longitude":-1.55979695,
                        "latitude":47.25325267
                    },
                    {
                        "nom":"Bourgeonnière",
                        "longitude":-1.55979040,
                        "latitude":47.25316283
                    },
                    {
                        "nom":"Bourgeonnière",
                        "longitude":-1.56097146,
                        "latitude":47.25303285
                    },
                    {
                        "nom":"Bourgeonnière",
                        "longitude":-1.56136733,
                        "latitude":47.25301947
                    },
                    {
                        "nom":"Bonde",
                        "longitude":-1.55202635,
                        "latitude":47.22280248
                    },
                    {
                        "nom":"Bonde",
                        "longitude":-1.55214517,
                        "latitude":47.22261834
                    },
                    {
                        "nom":"Bourrelière",
                        "longitude":-1.49898871,
                        "latitude":47.15793215
                    },
                    {
                        "nom":"Bourrelière",
                        "longitude":-1.49835581,
                        "latitude":47.15831350
                    },
                    {
                        "nom":"Boissière",
                        "longitude":-1.56588580,
                        "latitude":47.25511831
                    },
                    {
                        "nom":"Boissière",
                        "longitude":-1.56588580,
                        "latitude":47.25511831
                    },
                    {
                        "nom":"Boissière",
                        "longitude":-1.56575384,
                        "latitude":47.25512278
                    },
                    {
                        "nom":"Boissière",
                        "longitude":-1.56600465,
                        "latitude":47.25493416
                    },
                    {
                        "nom":"Boutière",
                        "longitude":-1.46809386,
                        "latitude":47.15796666
                    },
                    {
                        "nom":"Boutière",
                        "longitude":-1.46819367,
                        "latitude":47.15751304
                    },
                    {
                        "nom":"Bio Ouest Laënnec",
                        "longitude":-1.64187860,
                        "latitude":47.23279622
                    },
                    {
                        "nom":"Bio Ouest Laënnec",
                        "longitude":-1.64200381,
                        "latitude":47.23270183
                    },
                    {
                        "nom":"Bouvre",
                        "longitude":-1.61244717,
                        "latitude":47.17805474
                    },
                    {
                        "nom":"Bouvre",
                        "longitude":-1.61311926,
                        "latitude":47.17821183
                    },
                    {
                        "nom":"Boulay Paty",
                        "longitude":-1.57627077,
                        "latitude":47.22891717
                    },
                    {
                        "nom":"Boulay Paty",
                        "longitude":-1.57482650,
                        "latitude":47.22905625
                    },
                    {
                        "nom":"Bois Raguenet",
                        "longitude":-1.60046128,
                        "latitude":47.26655268
                    },
                    {
                        "nom":"Bois Raguenet",
                        "longitude":-1.60034915,
                        "latitude":47.26682671
                    },
                    {
                        "nom":"Bois Robillard",
                        "longitude":-1.51973294,
                        "latitude":47.23514550
                    },
                    {
                        "nom":"Bois Robillard",
                        "longitude":-1.51989725,
                        "latitude":47.23559032
                    },
                    {
                        "nom":"Bréchetière",
                        "longitude":-1.50345493,
                        "latitude":47.28468285
                    },
                    {
                        "nom":"Bréchetière",
                        "longitude":-1.50371901,
                        "latitude":47.28467406
                    },
                    {
                        "nom":"Briandière",
                        "longitude":-1.69370693,
                        "latitude":47.19055221
                    },
                    {
                        "nom":"Briandière",
                        "longitude":-1.69205782,
                        "latitude":47.18970915
                    },
                    {
                        "nom":"Bretonnière",
                        "longitude":-1.46289052,
                        "latitude":47.16651436
                    },
                    {
                        "nom":"Bretonnière",
                        "longitude":-1.46289052,
                        "latitude":47.16651436
                    },
                    {
                        "nom":"Bretonnière",
                        "longitude":-1.46274602,
                        "latitude":47.16633900
                    },
                    {
                        "nom":"Beauregard",
                        "longitude":-1.65649887,
                        "latitude":47.21076342
                    },
                    {
                        "nom":"Beauregard",
                        "longitude":-1.65711114,
                        "latitude":47.21011169
                    },
                    {
                        "nom":"Brains",
                        "longitude":-1.72303943,
                        "latitude":47.16961500
                    },
                    {
                        "nom":"Brouillard",
                        "longitude":-1.51446158,
                        "latitude":47.27395885
                    },
                    {
                        "nom":"Brouillard",
                        "longitude":-1.51458064,
                        "latitude":47.27377475
                    },
                    {
                        "nom":"Baronnière",
                        "longitude":-1.60378548,
                        "latitude":47.25959399
                    },
                    {
                        "nom":"Baronnière",
                        "longitude":-1.60391744,
                        "latitude":47.25958948
                    },
                    {
                        "nom":"Bretagne",
                        "longitude":-1.55836633,
                        "latitude":47.21727447
                    },
                    {
                        "nom":"Bretagne",
                        "longitude":-1.55835979,
                        "latitude":47.21718463
                    },
                    {
                        "nom":"Bruneau",
                        "longitude":-1.56181551,
                        "latitude":47.22832614
                    },
                    {
                        "nom":"Bruneau",
                        "longitude":-1.56138708,
                        "latitude":47.22789030
                    },
                    {
                        "nom":"Bobby Sands",
                        "longitude":-1.65277709,
                        "latitude":47.22998763
                    },
                    {
                        "nom":"Bobby Sands",
                        "longitude":-1.65290228,
                        "latitude":47.22989322
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58579949,
                        "latitude":47.23805046
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58592482,
                        "latitude":47.23795612
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58651849,
                        "latitude":47.23703524
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58699333,
                        "latitude":47.23809990
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58699333,
                        "latitude":47.23809990
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58685482,
                        "latitude":47.23801455
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58712525,
                        "latitude":47.23809541
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58740226,
                        "latitude":47.23826612
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58729012,
                        "latitude":47.23854014
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58688119,
                        "latitude":47.23837392
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58674268,
                        "latitude":47.23828857
                    },
                    {
                        "nom":"Beauséjour",
                        "longitude":-1.58646566,
                        "latitude":47.23811786
                    },
                    {
                        "nom":"Bossis",
                        "longitude":-1.71280909,
                        "latitude":47.21249135
                    },
                    {
                        "nom":"Bossis",
                        "longitude":-1.71228171,
                        "latitude":47.21250988
                    },
                    {
                        "nom":"Bois St-Louis",
                        "longitude":-1.58028406,
                        "latitude":47.24580363
                    },
                    {
                        "nom":"Bois St-Louis",
                        "longitude":-1.57926147,
                        "latitude":47.24628869
                    },
                    {
                        "nom":"Bussonnière",
                        "longitude":-1.44304738,
                        "latitude":47.26893560
                    },
                    {
                        "nom":"Bussonnière",
                        "longitude":-1.44376460,
                        "latitude":47.26972263
                    },
                    {
                        "nom":"Beau Soleil",
                        "longitude":-1.64545499,
                        "latitude":47.17781880
                    },
                    {
                        "nom":"Beau Soleil",
                        "longitude":-1.64478281,
                        "latitude":47.17766190
                    },
                    {
                        "nom":"Beau Soleil",
                        "longitude":-1.47502649,
                        "latitude":47.26094875
                    },
                    {
                        "nom":"Beau Soleil",
                        "longitude":-1.47572485,
                        "latitude":47.26146604
                    },
                    {
                        "nom":"Bastière",
                        "longitude":-1.45936421,
                        "latitude":47.14843797
                    },
                    {
                        "nom":"Bastière",
                        "longitude":-1.45941515,
                        "latitude":47.14915680
                    },
                    {
                        "nom":"Bois St-Lys",
                        "longitude":-1.48194193,
                        "latitude":47.30034674
                    },
                    {
                        "nom":"Bois St-Lys",
                        "longitude":-1.48194193,
                        "latitude":47.30034674
                    },
                    {
                        "nom":"Brétinais",
                        "longitude":-1.66591546,
                        "latitude":47.22178591
                    },
                    {
                        "nom":"Brétinais",
                        "longitude":-1.66577687,
                        "latitude":47.22170065
                    },
                    {
                        "nom":"Bottière",
                        "longitude":-1.51838071,
                        "latitude":47.24023431
                    },
                    {
                        "nom":"Bottière",
                        "longitude":-1.51811685,
                        "latitude":47.24024313
                    },
                    {
                        "nom":"Bouteillerie",
                        "longitude":-1.54360044,
                        "latitude":47.22146527
                    },
                    {
                        "nom":"Bouteillerie",
                        "longitude":-1.54282219,
                        "latitude":47.22167159
                    },
                    {
                        "nom":"Bertrand",
                        "longitude":-1.57568980,
                        "latitude":47.25712789
                    },
                    {
                        "nom":"Bertrand",
                        "longitude":-1.57568323,
                        "latitude":47.25703804
                    },
                    {
                        "nom":"Bugallière",
                        "longitude":-1.63768218,
                        "latitude":47.25996214
                    },
                    {
                        "nom":"Bugallière",
                        "longitude":-1.63794611,
                        "latitude":47.25995304
                    },
                    {
                        "nom":"Butte des Landes",
                        "longitude":-1.49427547,
                        "latitude":47.16034071
                    },
                    {
                        "nom":"Butte des Landes",
                        "longitude":-1.49378067,
                        "latitude":47.16080750
                    },
                    {
                        "nom":"Bougon",
                        "longitude":-1.61057452,
                        "latitude":47.17415582
                    },
                    {
                        "nom":"Bougon",
                        "longitude":-1.61081153,
                        "latitude":47.17378741
                    },
                    {
                        "nom":"Buisson",
                        "longitude":-1.48743916,
                        "latitude":47.26062797
                    },
                    {
                        "nom":"Buisson",
                        "longitude":-1.48751972,
                        "latitude":47.25990480
                    },
                    {
                        "nom":"Bouvardière",
                        "longitude":-1.59839758,
                        "latitude":47.23852184
                    },
                    {
                        "nom":"Bouvardière",
                        "longitude":-1.59838436,
                        "latitude":47.23834215
                    },
                    {
                        "nom":"Bouvernière",
                        "longitude":-1.61168790,
                        "latitude":47.23761711
                    },
                    {
                        "nom":"Bouvernière",
                        "longitude":-1.61145724,
                        "latitude":47.23807536
                    },
                    {
                        "nom":"Beauvoir",
                        "longitude":-1.63123773,
                        "latitude":47.17848969
                    },
                    {
                        "nom":"Beauvoir",
                        "longitude":-1.63057890,
                        "latitude":47.17851239
                    },
                    {
                        "nom":"Beauvoir",
                        "longitude":-1.62625055,
                        "latitude":47.17893163
                    },
                    {
                        "nom":"Beauvoir",
                        "longitude":-1.62638231,
                        "latitude":47.17892710
                    },
                    {
                        "nom":"Centre de Bouaye",
                        "longitude":-1.69178590,
                        "latitude":47.14396012
                    },
                    {
                        "nom":"Centre de Bouaye",
                        "longitude":-1.69164746,
                        "latitude":47.14387489
                    },
                    {
                        "nom":"Gare de Bouaye",
                        "longitude":-1.68094178,
                        "latitude":47.13825909
                    },
                    {
                        "nom":"Gare de Bouaye",
                        "longitude":-1.68094111,
                        "latitude":47.13825010
                    },
                    {
                        "nom":"Buzardières",
                        "longitude":-1.65967180,
                        "latitude":47.20723065
                    },
                    {
                        "nom":"Buzardières",
                        "longitude":-1.65990192,
                        "latitude":47.20677231
                    },
                    {
                        "nom":"Cabrol",
                        "longitude":-1.58866916,
                        "latitude":47.20327644
                    },
                    {
                        "nom":"Cabrol",
                        "longitude":-1.58833956,
                        "latitude":47.20418835
                    },
                    {
                        "nom":"Centre de Carquefou",
                        "longitude":-1.49005046,
                        "latitude":47.29710622
                    },
                    {
                        "nom":"Centre de Carquefou",
                        "longitude":-1.49137760,
                        "latitude":47.29715228
                    },
                    {
                        "nom":"Centre de Carquefou",
                        "longitude":-1.49093632,
                        "latitude":47.29653649
                    },
                    {
                        "nom":"Cadoire",
                        "longitude":-1.48808660,
                        "latitude":47.25673385
                    },
                    {
                        "nom":"Cadoire",
                        "longitude":-1.48808660,
                        "latitude":47.25673385
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.42885050,
                        "latitude":47.27588403
                    },
                    {
                        "nom":"Californie",
                        "longitude":-1.58426052,
                        "latitude":47.19180767
                    },
                    {
                        "nom":"Californie",
                        "longitude":-1.58448464,
                        "latitude":47.19125963
                    },
                    {
                        "nom":"Californie",
                        "longitude":-1.58524911,
                        "latitude":47.19087333
                    },
                    {
                        "nom":"Canclaux",
                        "longitude":-1.57477787,
                        "latitude":47.21392654
                    },
                    {
                        "nom":"Canclaux",
                        "longitude":-1.57490316,
                        "latitude":47.21383222
                    },
                    {
                        "nom":"Canclaux",
                        "longitude":-1.57471827,
                        "latitude":47.21491931
                    },
                    {
                        "nom":"Canclaux",
                        "longitude":-1.57488297,
                        "latitude":47.21536406
                    },
                    {
                        "nom":"Capellia",
                        "longitude":-1.55497680,
                        "latitude":47.27971448
                    },
                    {
                        "nom":"Capellia",
                        "longitude":-1.55497680,
                        "latitude":47.27971448
                    },
                    {
                        "nom":"Carquefou",
                        "longitude":-1.48536253,
                        "latitude":47.30374589
                    },
                    {
                        "nom":"Carquefou",
                        "longitude":-1.48548819,
                        "latitude":47.30365167
                    },
                    {
                        "nom":"Castorama",
                        "longitude":-1.50836563,
                        "latitude":47.25912172
                    },
                    {
                        "nom":"Castorama",
                        "longitude":-1.50690098,
                        "latitude":47.25899041
                    },
                    {
                        "nom":"Cartier",
                        "longitude":-1.45556222,
                        "latitude":47.26816466
                    },
                    {
                        "nom":"Cartier",
                        "longitude":-1.45564960,
                        "latitude":47.26753137
                    },
                    {
                        "nom":"Château D'Aux",
                        "longitude":-1.67388918,
                        "latitude":47.18529904
                    },
                    {
                        "nom":"Château D'Aux",
                        "longitude":-1.67427105,
                        "latitude":47.18510559
                    },
                    {
                        "nom":"Château D'Aux",
                        "longitude":-1.67455481,
                        "latitude":47.18536593
                    },
                    {
                        "nom":"Bac de Couëron",
                        "longitude":-1.75170597,
                        "latitude":47.20598337
                    },
                    {
                        "nom":"Charbonneau",
                        "longitude":-1.48858445,
                        "latitude":47.30066718
                    },
                    {
                        "nom":"Charbonneau",
                        "longitude":-1.49083624,
                        "latitude":47.30068260
                    },
                    {
                        "nom":"Charbonneau",
                        "longitude":-1.48833960,
                        "latitude":47.30094547
                    },
                    {
                        "nom":"Chapeau Berger",
                        "longitude":-1.46875604,
                        "latitude":47.17100393
                    },
                    {
                        "nom":"Chapeau Berger",
                        "longitude":-1.46875604,
                        "latitude":47.17100393
                    },
                    {
                        "nom":"Chapeau Berger",
                        "longitude":-1.46879436,
                        "latitude":47.17154304
                    },
                    {
                        "nom":"Chapeau Berger",
                        "longitude":-1.46710059,
                        "latitude":47.17186919
                    },
                    {
                        "nom":"Corbinerie",
                        "longitude":-1.54228836,
                        "latitude":47.16143502
                    },
                    {
                        "nom":"Corbinerie",
                        "longitude":-1.54203791,
                        "latitude":47.16162359
                    },
                    {
                        "nom":"Chemin Bleu",
                        "longitude":-1.52501183,
                        "latitude":47.17822772
                    },
                    {
                        "nom":"Chemin Bleu",
                        "longitude":-1.52515008,
                        "latitude":47.17831315
                    },
                    {
                        "nom":"Cambronne",
                        "longitude":-1.54761923,
                        "latitude":47.22583320
                    },
                    {
                        "nom":"Cambronne",
                        "longitude":-1.54850983,
                        "latitude":47.22535286
                    },
                    {
                        "nom":"Couëron-Bougon",
                        "longitude":-1.73386051,
                        "latitude":47.22508102
                    },
                    {
                        "nom":"Cambronne",
                        "longitude":-1.50682380,
                        "latitude":47.20639591
                    },
                    {
                        "nom":"Cambronne",
                        "longitude":-1.50631578,
                        "latitude":47.20668305
                    },
                    {
                        "nom":"Choblèterie",
                        "longitude":-1.46140176,
                        "latitude":47.13621221
                    },
                    {
                        "nom":"Choblèterie",
                        "longitude":-1.46153344,
                        "latitude":47.13620786
                    },
                    {
                        "nom":"Cochardières",
                        "longitude":-1.63320361,
                        "latitude":47.22994281
                    },
                    {
                        "nom":"Cochardières",
                        "longitude":-1.63446250,
                        "latitude":47.22908881
                    },
                    {
                        "nom":"Cochardières",
                        "longitude":-1.63412019,
                        "latitude":47.22982117
                    },
                    {
                        "nom":"Couchant",
                        "longitude":-1.51366167,
                        "latitude":47.27020292
                    },
                    {
                        "nom":"Couchant",
                        "longitude":-1.51378720,
                        "latitude":47.27010866
                    },
                    {
                        "nom":"Cimetière de la Classerie",
                        "longitude":-1.55882140,
                        "latitude":47.17087435
                    },
                    {
                        "nom":"Cimetière de la Classerie",
                        "longitude":-1.55806356,
                        "latitude":47.17135033
                    },
                    {
                        "nom":"Champ de Courses",
                        "longitude":-1.56114747,
                        "latitude":47.24456069
                    },
                    {
                        "nom":"Champ de Courses",
                        "longitude":-1.56197183,
                        "latitude":47.24498315
                    },
                    {
                        "nom":"Chêne Creux",
                        "longitude":-1.54874939,
                        "latitude":47.17040404
                    },
                    {
                        "nom":"Chêne Creux",
                        "longitude":-1.54845981,
                        "latitude":47.17005354
                    },
                    {
                        "nom":"Chêne des Anglais",
                        "longitude":-1.57322738,
                        "latitude":47.26144451
                    },
                    {
                        "nom":"Chêne des Anglais",
                        "longitude":-1.57322081,
                        "latitude":47.26135467
                    },
                    {
                        "nom":"Chêne des Anglais",
                        "longitude":-1.57310197,
                        "latitude":47.26153883
                    },
                    {
                        "nom":"Chêne des Anglais",
                        "longitude":-1.57362988,
                        "latitude":47.26152094
                    },
                    {
                        "nom":"Chêne des Anglais",
                        "longitude":-1.57454713,
                        "latitude":47.26139977
                    },
                    {
                        "nom":"Chêne des Anglais",
                        "longitude":-1.57481108,
                        "latitude":47.26139082
                    },
                    {
                        "nom":"Cité Internationale des Congrès",
                        "longitude":-1.54501797,
                        "latitude":47.21367185
                    },
                    {
                        "nom":"Cité Internationale des Congrès",
                        "longitude":-1.54514983,
                        "latitude":47.21366741
                    },
                    {
                        "nom":"Champ de Manoeuvre",
                        "longitude":-1.50074174,
                        "latitude":47.27270482
                    },
                    {
                        "nom":"Champ de Manoeuvre",
                        "longitude":-1.50005589,
                        "latitude":47.27236738
                    },
                    {
                        "nom":"Le Cellier",
                        "longitude":-1.34786995,
                        "latitude":47.32388492
                    },
                    {
                        "nom":"Le Cellier",
                        "longitude":-1.34771920,
                        "latitude":47.32361957
                    },
                    {
                        "nom":"Champ de Foire",
                        "longitude":-1.61890717,
                        "latitude":47.12424017
                    },
                    {
                        "nom":"Champ de Foire",
                        "longitude":-1.61853879,
                        "latitude":47.12461313
                    },
                    {
                        "nom":"Cimetière de la Gaudinière",
                        "longitude":-1.58355651,
                        "latitude":47.24172945
                    },
                    {
                        "nom":"Cimetière de la Gaudinière",
                        "longitude":-1.58272522,
                        "latitude":47.24301865
                    },
                    {
                        "nom":"Chêne Gala",
                        "longitude":-1.53923829,
                        "latitude":47.18315380
                    },
                    {
                        "nom":"Chêne Gala",
                        "longitude":-1.53968569,
                        "latitude":47.18385928
                    },
                    {
                        "nom":"Chabossière",
                        "longitude":-1.68236264,
                        "latitude":47.21544882
                    },
                    {
                        "nom":"Chabossière",
                        "longitude":-1.68222404,
                        "latitude":47.21536358
                    },
                    {
                        "nom":"Chambelles",
                        "longitude":-1.53058634,
                        "latitude":47.23162967
                    },
                    {
                        "nom":"Chambelles",
                        "longitude":-1.53141026,
                        "latitude":47.23205236
                    },
                    {
                        "nom":"Champ Fleuri",
                        "longitude":-1.52477013,
                        "latitude":47.14374039
                    },
                    {
                        "nom":"Champ Fleuri",
                        "longitude":-1.52558617,
                        "latitude":47.14407328
                    },
                    {
                        "nom":"Châtaigniers",
                        "longitude":-1.60190923,
                        "latitude":47.22155904
                    },
                    {
                        "nom":"Châtaigniers",
                        "longitude":-1.60192246,
                        "latitude":47.22173872
                    },
                    {
                        "nom":"Châtaigniers",
                        "longitude":-1.60287205,
                        "latitude":47.22206655
                    },
                    {
                        "nom":"Châtaigniers",
                        "longitude":-1.60287866,
                        "latitude":47.22215639
                    },
                    {
                        "nom":"Cholière",
                        "longitude":-1.60671900,
                        "latitude":47.25093723
                    },
                    {
                        "nom":"Cholière",
                        "longitude":-1.60672562,
                        "latitude":47.25102707
                    },
                    {
                        "nom":"Charmes",
                        "longitude":-1.46104727,
                        "latitude":47.19080195
                    },
                    {
                        "nom":"Charmes",
                        "longitude":-1.46120459,
                        "latitude":47.19115701
                    },
                    {
                        "nom":"Charbonnière",
                        "longitude":-1.64232461,
                        "latitude":47.26367510
                    },
                    {
                        "nom":"Charbonnière",
                        "longitude":-1.64206735,
                        "latitude":47.26377405
                    },
                    {
                        "nom":"Chocolaterie",
                        "longitude":-1.52769143,
                        "latitude":47.24280469
                    },
                    {
                        "nom":"Chocolaterie",
                        "longitude":-1.52794880,
                        "latitude":47.24270600
                    },
                    {
                        "nom":"Champonnière",
                        "longitude":-1.50603270,
                        "latitude":47.16409188
                    },
                    {
                        "nom":"Champonnière",
                        "longitude":-1.50588807,
                        "latitude":47.16391658
                    },
                    {
                        "nom":"Chalâtres",
                        "longitude":-1.53787365,
                        "latitude":47.23183531
                    },
                    {
                        "nom":"Chalâtres",
                        "longitude":-1.53861300,
                        "latitude":47.23108993
                    },
                    {
                        "nom":"Chaussée",
                        "longitude":-1.52106736,
                        "latitude":47.17664867
                    },
                    {
                        "nom":"Chaussée",
                        "longitude":-1.52203508,
                        "latitude":47.17724671
                    },
                    {
                        "nom":"Chatelets",
                        "longitude":-1.54051696,
                        "latitude":47.19716114
                    },
                    {
                        "nom":"Chanzy",
                        "longitude":-1.54497641,
                        "latitude":47.22403084
                    },
                    {
                        "nom":"Chanzy",
                        "longitude":-1.54547135,
                        "latitude":47.22356385
                    },
                    {
                        "nom":"CIFAM",
                        "longitude":-1.48814027,
                        "latitude":47.26672891
                    },
                    {
                        "nom":"CIFAM",
                        "longitude":-1.48588991,
                        "latitude":47.26671343
                    },
                    {
                        "nom":"Cimetière de St-Joseph-de-Porterie",
                        "longitude":-1.52068599,
                        "latitude":47.26852726
                    },
                    {
                        "nom":"Cimetière de St-Joseph-de-Porterie",
                        "longitude":-1.52158403,
                        "latitude":47.26813697
                    },
                    {
                        "nom":"Clos Ami",
                        "longitude":-1.65554467,
                        "latitude":47.21214757
                    },
                    {
                        "nom":"Clos Ami",
                        "longitude":-1.65714695,
                        "latitude":47.21236227
                    },
                    {
                        "nom":"Clos Ami",
                        "longitude":-1.65617709,
                        "latitude":47.21176537
                    },
                    {
                        "nom":"Clos Bonnet",
                        "longitude":-1.56358856,
                        "latitude":47.18917694
                    },
                    {
                        "nom":"Clos Bonnet",
                        "longitude":-1.56499823,
                        "latitude":47.19039015
                    },
                    {
                        "nom":"Centre René Gauducheau",
                        "longitude":-1.63820369,
                        "latitude":47.23850738
                    },
                    {
                        "nom":"Clétras",
                        "longitude":-1.51216575,
                        "latitude":47.25494213
                    },
                    {
                        "nom":"Clétras",
                        "longitude":-1.51257458,
                        "latitude":47.25510862
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.47233816,
                        "latitude":47.18979872
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.47255701,
                        "latitude":47.18916104
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.47229979,
                        "latitude":47.18925961
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.47246357,
                        "latitude":47.18970451
                    },
                    {
                        "nom":"Chalonges",
                        "longitude":-1.47273997,
                        "latitude":47.18987549
                    },
                    {
                        "nom":"Cimetière de La Montagne",
                        "longitude":-1.68660393,
                        "latitude":47.18746776
                    },
                    {
                        "nom":"Cimetière de La Montagne",
                        "longitude":-1.68645864,
                        "latitude":47.18729268
                    },
                    {
                        "nom":"Colonac",
                        "longitude":-1.60295852,
                        "latitude":47.22503587
                    },
                    {
                        "nom":"Colonac",
                        "longitude":-1.60316273,
                        "latitude":47.22421827
                    },
                    {
                        "nom":"Clotais",
                        "longitude":-1.71834643,
                        "latitude":47.19311033
                    },
                    {
                        "nom":"Clotais",
                        "longitude":-1.71820783,
                        "latitude":47.19302513
                    },
                    {
                        "nom":"Clos Roux",
                        "longitude":-1.75088644,
                        "latitude":47.19871610
                    },
                    {
                        "nom":"Clos Roux",
                        "longitude":-1.74978398,
                        "latitude":47.19812466
                    },
                    {
                        "nom":"Closeaux",
                        "longitude":-1.62806628,
                        "latitude":47.23912665
                    },
                    {
                        "nom":"Closeaux",
                        "longitude":-1.62759856,
                        "latitude":47.23995337
                    },
                    {
                        "nom":"Châlet",
                        "longitude":-1.54408774,
                        "latitude":47.23000511
                    },
                    {
                        "nom":"Châlet",
                        "longitude":-1.54444432,
                        "latitude":47.22945271
                    },
                    {
                        "nom":"Clouet",
                        "longitude":-1.51089539,
                        "latitude":47.16744204
                    },
                    {
                        "nom":"Clouet",
                        "longitude":-1.51034257,
                        "latitude":47.16710025
                    },
                    {
                        "nom":"Calvaire",
                        "longitude":-1.51918269,
                        "latitude":47.14680985
                    },
                    {
                        "nom":"Calvaire",
                        "longitude":-1.51899279,
                        "latitude":47.14600562
                    },
                    {
                        "nom":"Clouzeaux",
                        "longitude":-1.49259465,
                        "latitude":47.17931014
                    },
                    {
                        "nom":"Clouzeaux",
                        "longitude":-1.49260107,
                        "latitude":47.17939999
                    },
                    {
                        "nom":"Cirque-Marais",
                        "longitude":-1.55723249,
                        "latitude":47.21804233
                    },
                    {
                        "nom":"Cirque-Marais",
                        "longitude":-1.55711174,
                        "latitude":47.21819952
                    },
                    {
                        "nom":"Clémencière",
                        "longitude":-1.43568045,
                        "latitude":47.26953696
                    },
                    {
                        "nom":"Clémencière",
                        "longitude":-1.43542278,
                        "latitude":47.26963544
                    },
                    {
                        "nom":"Charmille",
                        "longitude":-1.60562586,
                        "latitude":47.25763967
                    },
                    {
                        "nom":"Charmille",
                        "longitude":-1.60611395,
                        "latitude":47.25708257
                    },
                    {
                        "nom":"Commune 1871",
                        "longitude":-1.50009235,
                        "latitude":47.19914494
                    },
                    {
                        "nom":"Commune 1871",
                        "longitude":-1.49970975,
                        "latitude":47.19933781
                    },
                    {
                        "nom":"Clos du Moulin",
                        "longitude":-1.52117338,
                        "latitude":47.14512195
                    },
                    {
                        "nom":"Clos du Moulin",
                        "longitude":-1.52131155,
                        "latitude":47.14520738
                    },
                    {
                        "nom":"Camus",
                        "longitude":-1.57264675,
                        "latitude":47.21913271
                    },
                    {
                        "nom":"Chantiers Navals",
                        "longitude":-1.56753441,
                        "latitude":47.20885819
                    },
                    {
                        "nom":"Chantiers Navals",
                        "longitude":-1.56753441,
                        "latitude":47.20885819
                    },
                    {
                        "nom":"Chantiers Navals",
                        "longitude":-1.56752785,
                        "latitude":47.20876834
                    },
                    {
                        "nom":"Chenonceau",
                        "longitude":-1.43483996,
                        "latitude":47.27262643
                    },
                    {
                        "nom":"Congo",
                        "longitude":-1.60913647,
                        "latitude":47.24067675
                    },
                    {
                        "nom":"Congo",
                        "longitude":-1.61011890,
                        "latitude":47.23965236
                    },
                    {
                        "nom":"Chinon",
                        "longitude":-1.42899425,
                        "latitude":47.27416824
                    },
                    {
                        "nom":"Chinon",
                        "longitude":-1.42912627,
                        "latitude":47.27416393
                    },
                    {
                        "nom":"Centaure",
                        "longitude":-1.62704148,
                        "latitude":47.16998730
                    },
                    {
                        "nom":"Centaure",
                        "longitude":-1.62717988,
                        "latitude":47.17007260
                    },
                    {
                        "nom":"Corlay",
                        "longitude":-1.64412854,
                        "latitude":47.20794884
                    },
                    {
                        "nom":"Corlay",
                        "longitude":-1.64426706,
                        "latitude":47.20803413
                    },
                    {
                        "nom":"Corbières",
                        "longitude":-1.48745260,
                        "latitude":47.20163663
                    },
                    {
                        "nom":"Corbières",
                        "longitude":-1.48747186,
                        "latitude":47.20190618
                    },
                    {
                        "nom":"Cochard",
                        "longitude":-1.54976344,
                        "latitude":47.22440993
                    },
                    {
                        "nom":"Cochard",
                        "longitude":-1.55052864,
                        "latitude":47.22402387
                    },
                    {
                        "nom":"Les Couëts",
                        "longitude":-1.58648200,
                        "latitude":47.18065352
                    },
                    {
                        "nom":"Les Couëts",
                        "longitude":-1.58648859,
                        "latitude":47.18074336
                    },
                    {
                        "nom":"Chohonnière",
                        "longitude":-1.48589688,
                        "latitude":47.26311075
                    },
                    {
                        "nom":"Chohonnière",
                        "longitude":-1.48604172,
                        "latitude":47.26328607
                    },
                    {
                        "nom":"Colinière",
                        "longitude":-1.50853300,
                        "latitude":47.23569971
                    },
                    {
                        "nom":"Colinière",
                        "longitude":-1.50853946,
                        "latitude":47.23578956
                    },
                    {
                        "nom":"Colinière",
                        "longitude":-1.50793154,
                        "latitude":47.23653034
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55676650,
                        "latitude":47.21345566
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55650932,
                        "latitude":47.21355442
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55600149,
                        "latitude":47.21384177
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55637092,
                        "latitude":47.21346902
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55637092,
                        "latitude":47.21346902
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55635131,
                        "latitude":47.21319949
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55623906,
                        "latitude":47.21347348
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55674689,
                        "latitude":47.21318612
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55648971,
                        "latitude":47.21328488
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55674689,
                        "latitude":47.21318612
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55592193,
                        "latitude":47.21456499
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55592193,
                        "latitude":47.21456499
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55569123,
                        "latitude":47.21448272
                    },
                    {
                        "nom":"Commerce",
                        "longitude":-1.55566474,
                        "latitude":47.21466375
                    },
                    {
                        "nom":"Corniche",
                        "longitude":-1.59440839,
                        "latitude":47.25261848
                    },
                    {
                        "nom":"Corniche",
                        "longitude":-1.59468551,
                        "latitude":47.25278917
                    },
                    {
                        "nom":"Coquelicots",
                        "longitude":-1.72917198,
                        "latitude":47.22254421
                    },
                    {
                        "nom":"Coquelicots",
                        "longitude":-1.72930385,
                        "latitude":47.22253956
                    },
                    {
                        "nom":"Conraie",
                        "longitude":-1.58773135,
                        "latitude":47.26077182
                    },
                    {
                        "nom":"Conraie",
                        "longitude":-1.58840441,
                        "latitude":47.26092905
                    },
                    {
                        "nom":"Cousteau",
                        "longitude":-1.51293285,
                        "latitude":47.23987594
                    },
                    {
                        "nom":"Cousteau",
                        "longitude":-1.51305831,
                        "latitude":47.23978169
                    },
                    {
                        "nom":"Coutancière",
                        "longitude":-1.56008462,
                        "latitude":47.30620126
                    },
                    {
                        "nom":"Coudray",
                        "longitude":-1.54243010,
                        "latitude":47.23267280
                    },
                    {
                        "nom":"Coudray",
                        "longitude":-1.54239752,
                        "latitude":47.23222357
                    },
                    {
                        "nom":"Chapelles",
                        "longitude":-1.53847942,
                        "latitude":47.16354476
                    },
                    {
                        "nom":"Chapelles",
                        "longitude":-1.53965688,
                        "latitude":47.16521640
                    },
                    {
                        "nom":"Chapelle Laënnec",
                        "longitude":-1.63783808,
                        "latitude":47.23536751
                    },
                    {
                        "nom":"Copernic",
                        "longitude":-1.56440687,
                        "latitude":47.21490855
                    },
                    {
                        "nom":"Copernic",
                        "longitude":-1.56480245,
                        "latitude":47.21489516
                    },
                    {
                        "nom":"Champagnère",
                        "longitude":-1.46136985,
                        "latitude":47.21024466
                    },
                    {
                        "nom":"Champagnère",
                        "longitude":-1.46124437,
                        "latitude":47.21033886
                    },
                    {
                        "nom":"Chaplin",
                        "longitude":-1.61637638,
                        "latitude":47.22232473
                    },
                    {
                        "nom":"Chaplin",
                        "longitude":-1.61625115,
                        "latitude":47.22241909
                    },
                    {
                        "nom":"Chaupières",
                        "longitude":-1.49505175,
                        "latitude":47.24317344
                    },
                    {
                        "nom":"Chaupières",
                        "longitude":-1.49499380,
                        "latitude":47.24236480
                    },
                    {
                        "nom":"Chat-Qui-Guette",
                        "longitude":-1.69751217,
                        "latitude":47.19195029
                    },
                    {
                        "nom":"Chat-Qui-Guette",
                        "longitude":-1.69711679,
                        "latitude":47.19196413
                    },
                    {
                        "nom":"Coqueluchon",
                        "longitude":-1.47032832,
                        "latitude":47.18383096
                    },
                    {
                        "nom":"Coqueluchon",
                        "longitude":-1.47034749,
                        "latitude":47.18410051
                    },
                    {
                        "nom":"Clairais",
                        "longitude":-1.43848811,
                        "latitude":47.27556910
                    },
                    {
                        "nom":"Corberie",
                        "longitude":-1.52404890,
                        "latitude":47.15205072
                    },
                    {
                        "nom":"Corberie",
                        "longitude":-1.52394309,
                        "latitude":47.15241454
                    },
                    {
                        "nom":"Courocerie",
                        "longitude":-1.49914592,
                        "latitude":47.25231376
                    },
                    {
                        "nom":"Courocerie",
                        "longitude":-1.49940339,
                        "latitude":47.25221514
                    },
                    {
                        "nom":"Château de Rezé",
                        "longitude":-1.55956467,
                        "latitude":47.18471967
                    },
                    {
                        "nom":"Château de Rezé",
                        "longitude":-1.55943289,
                        "latitude":47.18472412
                    },
                    {
                        "nom":"Crémetterie",
                        "longitude":-1.60774358,
                        "latitude":47.21820708
                    },
                    {
                        "nom":"Crémetterie",
                        "longitude":-1.60827767,
                        "latitude":47.21827887
                    },
                    {
                        "nom":"Chemin Rouge",
                        "longitude":-1.51761350,
                        "latitude":47.25160798
                    },
                    {
                        "nom":"Chemin Rouge",
                        "longitude":-1.51763293,
                        "latitude":47.25187752
                    },
                    {
                        "nom":"Criport",
                        "longitude":-1.50361202,
                        "latitude":47.15795815
                    },
                    {
                        "nom":"Criport",
                        "longitude":-1.50350606,
                        "latitude":47.15832194
                    },
                    {
                        "nom":"Place du Cirque",
                        "longitude":-1.55702150,
                        "latitude":47.21695965
                    },
                    {
                        "nom":"Place du Cirque",
                        "longitude":-1.55715337,
                        "latitude":47.21695519
                    },
                    {
                        "nom":"Place du Cirque",
                        "longitude":-1.55687655,
                        "latitude":47.21678441
                    },
                    {
                        "nom":"Place du Cirque",
                        "longitude":-1.55675776,
                        "latitude":47.21696856
                    },
                    {
                        "nom":"Croissant",
                        "longitude":-1.53043801,
                        "latitude":47.24055106
                    },
                    {
                        "nom":"Croissant",
                        "longitude":-1.52994279,
                        "latitude":47.24101799
                    },
                    {
                        "nom":"Crétinières",
                        "longitude":-1.55346690,
                        "latitude":47.27715351
                    },
                    {
                        "nom":"Crétinières",
                        "longitude":-1.55315709,
                        "latitude":47.27653350
                    },
                    {
                        "nom":"Cravate",
                        "longitude":-1.58843772,
                        "latitude":47.24336474
                    },
                    {
                        "nom":"Cravate",
                        "longitude":-1.58829920,
                        "latitude":47.24327939
                    },
                    {
                        "nom":"Chassay",
                        "longitude":-1.48421005,
                        "latitude":47.24506425
                    },
                    {
                        "nom":"Chassay",
                        "longitude":-1.48409095,
                        "latitude":47.24524832
                    },
                    {
                        "nom":"Chassay",
                        "longitude":-1.48462514,
                        "latitude":47.24532068
                    },
                    {
                        "nom":"Cimetière St-Clair",
                        "longitude":-1.59242049,
                        "latitude":47.21656884
                    },
                    {
                        "nom":"Cimetière St-Clair",
                        "longitude":-1.59376552,
                        "latitude":47.21688326
                    },
                    {
                        "nom":"Chapelle-sur-Erdre",
                        "longitude":-1.55005779,
                        "latitude":47.29942423
                    },
                    {
                        "nom":"Chapelle-sur-Erdre",
                        "longitude":-1.55005779,
                        "latitude":47.29942423
                    },
                    {
                        "nom":"Cassegrain",
                        "longitude":-1.48751211,
                        "latitude":47.18767482
                    },
                    {
                        "nom":"Cassegrain",
                        "longitude":-1.48950839,
                        "latitude":47.18787872
                    },
                    {
                        "nom":"Carrières",
                        "longitude":-1.63819582,
                        "latitude":47.20815365
                    },
                    {
                        "nom":"Carrières",
                        "longitude":-1.63807066,
                        "latitude":47.20824804
                    },
                    {
                        "nom":"Clos Siban",
                        "longitude":-1.64588445,
                        "latitude":47.21023004
                    },
                    {
                        "nom":"Clos Siban",
                        "longitude":-1.64641182,
                        "latitude":47.21021181
                    },
                    {
                        "nom":"Casimir Périer",
                        "longitude":-1.56892329,
                        "latitude":47.23150809
                    },
                    {
                        "nom":"Casimir Périer",
                        "longitude":-1.56863981,
                        "latitude":47.23124749
                    },
                    {
                        "nom":"Casimir Périer",
                        "longitude":-1.56829658,
                        "latitude":47.23197965
                    },
                    {
                        "nom":"Classerie",
                        "longitude":-1.55874400,
                        "latitude":47.16799479
                    },
                    {
                        "nom":"Classerie",
                        "longitude":-1.55875707,
                        "latitude":47.16817448
                    },
                    {
                        "nom":"Casterneau",
                        "longitude":-1.53264215,
                        "latitude":47.22714751
                    },
                    {
                        "nom":"Casterneau",
                        "longitude":-1.53223347,
                        "latitude":47.22698109
                    },
                    {
                        "nom":"Conservatoire",
                        "longitude":-1.53277550,
                        "latitude":47.20705845
                    },
                    {
                        "nom":"Conservatoire",
                        "longitude":-1.53303919,
                        "latitude":47.20704960
                    },
                    {
                        "nom":"Coteaux",
                        "longitude":-1.67013882,
                        "latitude":47.18461890
                    },
                    {
                        "nom":"Coteaux",
                        "longitude":-1.66932799,
                        "latitude":47.18437689
                    },
                    {
                        "nom":"Caboteurs",
                        "longitude":-1.58729878,
                        "latitude":47.18639013
                    },
                    {
                        "nom":"Caboteurs",
                        "longitude":-1.58743716,
                        "latitude":47.18647549
                    },
                    {
                        "nom":"Châtelier",
                        "longitude":-1.59312375,
                        "latitude":47.18475042
                    },
                    {
                        "nom":"Châtelier",
                        "longitude":-1.59326213,
                        "latitude":47.18483577
                    },
                    {
                        "nom":"Courteline",
                        "longitude":-1.58773214,
                        "latitude":47.23555285
                    },
                    {
                        "nom":"Courteline",
                        "longitude":-1.58773874,
                        "latitude":47.23564270
                    },
                    {
                        "nom":"Carterons",
                        "longitude":-1.73904497,
                        "latitude":47.21850232
                    },
                    {
                        "nom":"Carterons",
                        "longitude":-1.73865625,
                        "latitude":47.21860614
                    },
                    {
                        "nom":"Clos Toreau",
                        "longitude":-1.52907208,
                        "latitude":47.19421331
                    },
                    {
                        "nom":"Clos Toreau",
                        "longitude":-1.52907208,
                        "latitude":47.19421331
                    },
                    {
                        "nom":"Chantrerie Grandes Ecoles",
                        "longitude":-1.51701187,
                        "latitude":47.28179922
                    },
                    {
                        "nom":"Chantrerie Grandes Ecoles",
                        "longitude":-1.51700539,
                        "latitude":47.28170938
                    },
                    {
                        "nom":"Chantrerie Grandes Ecoles",
                        "longitude":-1.51810700,
                        "latitude":47.28230302
                    },
                    {
                        "nom":"Chantrerie Grandes Ecoles",
                        "longitude":-1.51810051,
                        "latitude":47.28221317
                    },
                    {
                        "nom":"Coty",
                        "longitude":-1.48806168,
                        "latitude":47.24898935
                    },
                    {
                        "nom":"Coty",
                        "longitude":-1.48820006,
                        "latitude":47.24907482
                    },
                    {
                        "nom":"Gare de Couëron",
                        "longitude":-1.72409285,
                        "latitude":47.22182251
                    },
                    {
                        "nom":"Gare de Couëron",
                        "longitude":-1.72395416,
                        "latitude":47.22173732
                    },
                    {
                        "nom":"Chausserie",
                        "longitude":-1.70421559,
                        "latitude":47.14946944
                    },
                    {
                        "nom":"Chausserie",
                        "longitude":-1.70405679,
                        "latitude":47.14911471
                    },
                    {
                        "nom":"Curie",
                        "longitude":-1.48692026,
                        "latitude":47.19048644
                    },
                    {
                        "nom":"Curie",
                        "longitude":-1.48745391,
                        "latitude":47.19055879
                    },
                    {
                        "nom":"Cuvier",
                        "longitude":-1.57414588,
                        "latitude":47.21070555
                    },
                    {
                        "nom":"Cuvier",
                        "longitude":-1.57333508,
                        "latitude":47.21046286
                    },
                    {
                        "nom":"Cheveux Blancs",
                        "longitude":-1.63504746,
                        "latitude":47.26545709
                    },
                    {
                        "nom":"Cheveux Blancs",
                        "longitude":-1.63517944,
                        "latitude":47.26545255
                    },
                    {
                        "nom":"Cimetière de Vieux Doulon",
                        "longitude":-1.50040266,
                        "latitude":47.23849235
                    },
                    {
                        "nom":"Cimetière de Vieux Doulon",
                        "longitude":-1.50066651,
                        "latitude":47.23848357
                    },
                    {
                        "nom":"Chapeau Verni",
                        "longitude":-1.51140383,
                        "latitude":47.18922082
                    },
                    {
                        "nom":"Chapeau Verni",
                        "longitude":-1.51073836,
                        "latitude":47.18915299
                    },
                    {
                        "nom":"Chapeau Verni",
                        "longitude":-1.50928208,
                        "latitude":47.18911155
                    },
                    {
                        "nom":"Chapeau Verni",
                        "longitude":-1.50888667,
                        "latitude":47.18912476
                    },
                    {
                        "nom":"Chevalerie",
                        "longitude":-1.54517072,
                        "latitude":47.25221480
                    },
                    {
                        "nom":"Chevalerie",
                        "longitude":-1.54530920,
                        "latitude":47.25230020
                    },
                    {
                        "nom":"Chauvinière",
                        "longitude":-1.57100721,
                        "latitude":47.25098198
                    },
                    {
                        "nom":"Chauvinière",
                        "longitude":-1.57210879,
                        "latitude":47.25157511
                    },
                    {
                        "nom":"Cheverny",
                        "longitude":-1.60487161,
                        "latitude":47.24597455
                    },
                    {
                        "nom":"Cheverny",
                        "longitude":-1.60541702,
                        "latitude":47.24547854
                    },
                    {
                        "nom":"Convention",
                        "longitude":-1.58670450,
                        "latitude":47.21072899
                    },
                    {
                        "nom":"Convention",
                        "longitude":-1.58650009,
                        "latitude":47.21154656
                    },
                    {
                        "nom":"Chézine",
                        "longitude":-1.59897057,
                        "latitude":47.23372865
                    },
                    {
                        "nom":"Chézine",
                        "longitude":-1.59936628,
                        "latitude":47.23371514
                    },
                    {
                        "nom":"Danemark",
                        "longitude":-1.49857702,
                        "latitude":47.26097867
                    },
                    {
                        "nom":"Danemark",
                        "longitude":-1.49652975,
                        "latitude":47.26194734
                    },
                    {
                        "nom":"Danemark",
                        "longitude":-1.49744075,
                        "latitude":47.26173694
                    },
                    {
                        "nom":"Dalby",
                        "longitude":-1.52971650,
                        "latitude":47.22508411
                    },
                    {
                        "nom":"Dalby",
                        "longitude":-1.52922141,
                        "latitude":47.22555105
                    },
                    {
                        "nom":"Dalby",
                        "longitude":-1.52969702,
                        "latitude":47.22481457
                    },
                    {
                        "nom":"Danube",
                        "longitude":-1.50061050,
                        "latitude":47.30405060
                    },
                    {
                        "nom":"Danube",
                        "longitude":-1.50047841,
                        "latitude":47.30405499
                    },
                    {
                        "nom":"Davum",
                        "longitude":-1.49608243,
                        "latitude":47.26493425
                    },
                    {
                        "nom":"Davum",
                        "longitude":-1.49620153,
                        "latitude":47.26475017
                    },
                    {
                        "nom":"Dayat",
                        "longitude":-1.67426307,
                        "latitude":47.20086884
                    },
                    {
                        "nom":"Dayat",
                        "longitude":-1.67562166,
                        "latitude":47.20136198
                    },
                    {
                        "nom":"Duchesse Anne-Chateau",
                        "longitude":-1.54719152,
                        "latitude":47.21630061
                    },
                    {
                        "nom":"Duchesse Anne-Chateau",
                        "longitude":-1.54719152,
                        "latitude":47.21630061
                    },
                    {
                        "nom":"Duchesse Anne-Chateau",
                        "longitude":-1.54679592,
                        "latitude":47.21631394
                    },
                    {
                        "nom":"Duchesse Anne-Chateau",
                        "longitude":-1.54690170,
                        "latitude":47.21595011
                    },
                    {
                        "nom":"Du Chaffault",
                        "longitude":-1.58184631,
                        "latitude":47.20756176
                    },
                    {
                        "nom":"Du Chaffault",
                        "longitude":-1.58184631,
                        "latitude":47.20756176
                    },
                    {
                        "nom":"Desmoulins",
                        "longitude":-1.52217027,
                        "latitude":47.23227195
                    },
                    {
                        "nom":"Desmoulins",
                        "longitude":-1.52133117,
                        "latitude":47.23347087
                    },
                    {
                        "nom":"Dervallières",
                        "longitude":-1.59808602,
                        "latitude":47.22889513
                    },
                    {
                        "nom":"Dervallières",
                        "longitude":-1.59821130,
                        "latitude":47.22880079
                    },
                    {
                        "nom":"Diane",
                        "longitude":-1.57811502,
                        "latitude":47.23966265
                    },
                    {
                        "nom":"Diane",
                        "longitude":-1.57798310,
                        "latitude":47.23966713
                    },
                    {
                        "nom":"Espace Diderot",
                        "longitude":-1.56258269,
                        "latitude":47.18443740
                    },
                    {
                        "nom":"Espace Diderot",
                        "longitude":-1.56258269,
                        "latitude":47.18443740
                    },
                    {
                        "nom":"Espace Diderot",
                        "longitude":-1.56297151,
                        "latitude":47.18433416
                    },
                    {
                        "nom":"Espace Diderot",
                        "longitude":-1.56292570,
                        "latitude":47.18370524
                    },
                    {
                        "nom":"Espace Diderot",
                        "longitude":-1.56351175,
                        "latitude":47.18449601
                    },
                    {
                        "nom":"Delacroix",
                        "longitude":-1.58924255,
                        "latitude":47.22370249
                    },
                    {
                        "nom":"Delacroix",
                        "longitude":-1.58936784,
                        "latitude":47.22360815
                    },
                    {
                        "nom":"Drillet",
                        "longitude":-1.66623091,
                        "latitude":47.23483550
                    },
                    {
                        "nom":"Drillet",
                        "longitude":-1.66569657,
                        "latitude":47.23476398
                    },
                    {
                        "nom":"Delorme",
                        "longitude":-1.56472955,
                        "latitude":47.21570823
                    },
                    {
                        "nom":"Delorme",
                        "longitude":-1.56416279,
                        "latitude":47.21518701
                    },
                    {
                        "nom":"Domaine",
                        "longitude":-1.47086745,
                        "latitude":47.17840937
                    },
                    {
                        "nom":"Domaine",
                        "longitude":-1.47072928,
                        "latitude":47.17832387
                    },
                    {
                        "nom":"Dumont d'Urville",
                        "longitude":-1.63633918,
                        "latitude":47.22587166
                    },
                    {
                        "nom":"Dumont d'Urville",
                        "longitude":-1.63633250,
                        "latitude":47.22578182
                    },
                    {
                        "nom":"Danais",
                        "longitude":-1.58969749,
                        "latitude":47.21368938
                    },
                    {
                        "nom":"Danais",
                        "longitude":-1.58982935,
                        "latitude":47.21368489
                    },
                    {
                        "nom":"Doucet",
                        "longitude":-1.62960572,
                        "latitude":47.27771363
                    },
                    {
                        "nom":"Dolto",
                        "longitude":-1.63617274,
                        "latitude":47.21650999
                    },
                    {
                        "nom":"Dolto",
                        "longitude":-1.63642978,
                        "latitude":47.21641106
                    },
                    {
                        "nom":"Doumer",
                        "longitude":-1.57842860,
                        "latitude":47.22226896
                    },
                    {
                        "nom":"Doumer",
                        "longitude":-1.57852760,
                        "latitude":47.22181526
                    },
                    {
                        "nom":"Douettée",
                        "longitude":-1.49833108,
                        "latitude":47.20775964
                    },
                    {
                        "nom":"Douettée",
                        "longitude":-1.49899677,
                        "latitude":47.20782755
                    },
                    {
                        "nom":"Déportés",
                        "longitude":-1.49183227,
                        "latitude":47.19635756
                    },
                    {
                        "nom":"Déportés",
                        "longitude":-1.49228559,
                        "latitude":47.19715306
                    },
                    {
                        "nom":"Désirade",
                        "longitude":-1.48446172,
                        "latitude":47.27819851
                    },
                    {
                        "nom":"Désirade",
                        "longitude":-1.48415913,
                        "latitude":47.27766816
                    },
                    {
                        "nom":"Druides",
                        "longitude":-1.50742048,
                        "latitude":47.23861884
                    },
                    {
                        "nom":"Druides",
                        "longitude":-1.50742048,
                        "latitude":47.23861884
                    },
                    {
                        "nom":"Droitière",
                        "longitude":-1.37313692,
                        "latitude":47.30821718
                    },
                    {
                        "nom":"Droitière",
                        "longitude":-1.37313692,
                        "latitude":47.30821718
                    },
                    {
                        "nom":"Desaix",
                        "longitude":-1.54205870,
                        "latitude":47.22755156
                    },
                    {
                        "nom":"Desaix",
                        "longitude":-1.54328641,
                        "latitude":47.22624932
                    },
                    {
                        "nom":"Dukas",
                        "longitude":-1.60251920,
                        "latitude":47.23522868
                    },
                    {
                        "nom":"Dukas",
                        "longitude":-1.60209701,
                        "latitude":47.23488283
                    },
                    {
                        "nom":"Dunant",
                        "longitude":-1.52142343,
                        "latitude":47.24391522
                    },
                    {
                        "nom":"Dunant",
                        "longitude":-1.52129149,
                        "latitude":47.24391964
                    },
                    {
                        "nom":"Embarcadère",
                        "longitude":-1.64653306,
                        "latitude":47.19408468
                    },
                    {
                        "nom":"Embarcadère",
                        "longitude":-1.64687497,
                        "latitude":47.19335228
                    },
                    {
                        "nom":"Ecobuts",
                        "longitude":-1.50777884,
                        "latitude":47.19393516
                    },
                    {
                        "nom":"Ecobuts",
                        "longitude":-1.50764703,
                        "latitude":47.19393956
                    },
                    {
                        "nom":"Echoppes",
                        "longitude":-1.68558360,
                        "latitude":47.14399692
                    },
                    {
                        "nom":"Echoppes",
                        "longitude":-1.68531350,
                        "latitude":47.14391628
                    },
                    {
                        "nom":"Echoppes",
                        "longitude":-1.68645373,
                        "latitude":47.14310177
                    },
                    {
                        "nom":"Echoppes",
                        "longitude":-1.68649696,
                        "latitude":47.14297415
                    },
                    {
                        "nom":"Echalier",
                        "longitude":-1.46128039,
                        "latitude":47.21457056
                    },
                    {
                        "nom":"Echalier",
                        "longitude":-1.46184614,
                        "latitude":47.21509229
                    },
                    {
                        "nom":"Ecosse",
                        "longitude":-1.52635200,
                        "latitude":47.21510965
                    },
                    {
                        "nom":"Ecosse",
                        "longitude":-1.52622663,
                        "latitude":47.21520392
                    },
                    {
                        "nom":"Ecole Centrale-Audencia",
                        "longitude":-1.55126280,
                        "latitude":47.24867716
                    },
                    {
                        "nom":"Ecole Centrale-Audencia",
                        "longitude":-1.55126280,
                        "latitude":47.24867716
                    },
                    {
                        "nom":"Ecole Centrale-Audencia",
                        "longitude":-1.55152016,
                        "latitude":47.24857841
                    },
                    {
                        "nom":"Ecole Centrale-Audencia",
                        "longitude":-1.55115045,
                        "latitude":47.24895114
                    },
                    {
                        "nom":"Ecole Centrale-Audencia",
                        "longitude":-1.55161943,
                        "latitude":47.24812474
                    },
                    {
                        "nom":"Ecole Centrale-Audencia",
                        "longitude":-1.55185065,
                        "latitude":47.24766661
                    },
                    {
                        "nom":"Enchanterie",
                        "longitude":-1.52375537,
                        "latitude":47.23041759
                    },
                    {
                        "nom":"Enchanterie",
                        "longitude":-1.52337912,
                        "latitude":47.23070039
                    },
                    {
                        "nom":"Edit de Nantes",
                        "longitude":-1.56886391,
                        "latitude":47.21439732
                    },
                    {
                        "nom":"Edit de Nantes",
                        "longitude":-1.56847489,
                        "latitude":47.21450057
                    },
                    {
                        "nom":"Erdreau",
                        "longitude":-1.50127235,
                        "latitude":47.29114983
                    },
                    {
                        "nom":"Erdreau",
                        "longitude":-1.50153001,
                        "latitude":47.29105120
                    },
                    {
                        "nom":"Egalité",
                        "longitude":-1.58858337,
                        "latitude":47.20931397
                    },
                    {
                        "nom":"Egalité",
                        "longitude":-1.58858996,
                        "latitude":47.20940382
                    },
                    {
                        "nom":"Egalité",
                        "longitude":-1.58713307,
                        "latitude":47.20936337
                    },
                    {
                        "nom":"Egalité",
                        "longitude":-1.58719244,
                        "latitude":47.20837059
                    },
                    {
                        "nom":"Einstein",
                        "longitude":-1.57089717,
                        "latitude":47.26395527
                    },
                    {
                        "nom":"Einstein",
                        "longitude":-1.57104228,
                        "latitude":47.26413048
                    },
                    {
                        "nom":"Embellie",
                        "longitude":-1.51869135,
                        "latitude":47.26472122
                    },
                    {
                        "nom":"Embellie",
                        "longitude":-1.51835371,
                        "latitude":47.26554307
                    },
                    {
                        "nom":"Ennerie Corbon",
                        "longitude":-1.70470652,
                        "latitude":47.14197580
                    },
                    {
                        "nom":"Ennerie Corbon",
                        "longitude":-1.70509475,
                        "latitude":47.14187209
                    },
                    {
                        "nom":"Ecoles",
                        "longitude":-1.75940563,
                        "latitude":47.19778294
                    },
                    {
                        "nom":"Ecoles",
                        "longitude":-1.75912140,
                        "latitude":47.19752281
                    },
                    {
                        "nom":"Epinais",
                        "longitude":-1.61055959,
                        "latitude":47.13632633
                    },
                    {
                        "nom":"Epinais",
                        "longitude":-1.61054635,
                        "latitude":47.13614664
                    },
                    {
                        "nom":"Epinay",
                        "longitude":-1.48692971,
                        "latitude":47.29234637
                    },
                    {
                        "nom":"Epinay",
                        "longitude":-1.48692328,
                        "latitude":47.29225652
                    },
                    {
                        "nom":"Equipement",
                        "longitude":-1.57550052,
                        "latitude":47.22380946
                    },
                    {
                        "nom":"Equipement",
                        "longitude":-1.57577086,
                        "latitude":47.22389035
                    },
                    {
                        "nom":"Eraudière",
                        "longitude":-1.53511401,
                        "latitude":47.24120472
                    },
                    {
                        "nom":"Eraudière",
                        "longitude":-1.53501460,
                        "latitude":47.24165838
                    },
                    {
                        "nom":"Eraudière",
                        "longitude":-1.53521809,
                        "latitude":47.24264226
                    },
                    {
                        "nom":"Eraudière",
                        "longitude":-1.53484181,
                        "latitude":47.24292509
                    },
                    {
                        "nom":"Escall",
                        "longitude":-1.50301835,
                        "latitude":47.19940772
                    },
                    {
                        "nom":"Escall",
                        "longitude":-1.50328845,
                        "latitude":47.19948878
                    },
                    {
                        "nom":"Ecoles",
                        "longitude":-1.52753353,
                        "latitude":47.14544898
                    },
                    {
                        "nom":"Ecoles",
                        "longitude":-1.52894123,
                        "latitude":47.14666265
                    },
                    {
                        "nom":"Esso",
                        "longitude":-1.62715773,
                        "latitude":47.19475301
                    },
                    {
                        "nom":"Esso",
                        "longitude":-1.62715108,
                        "latitude":47.19466317
                    },
                    {
                        "nom":"Eugène Sue",
                        "longitude":-1.59603562,
                        "latitude":47.23337846
                    },
                    {
                        "nom":"Eugène Sue",
                        "longitude":-1.59602241,
                        "latitude":47.23319878
                    },
                    {
                        "nom":"Europe",
                        "longitude":-1.50407499,
                        "latitude":47.26016529
                    },
                    {
                        "nom":"Europe",
                        "longitude":-1.50282619,
                        "latitude":47.26119756
                    },
                    {
                        "nom":"Europe",
                        "longitude":-1.47629459,
                        "latitude":47.17300614
                    },
                    {
                        "nom":"Europe",
                        "longitude":-1.47656452,
                        "latitude":47.17308726
                    },
                    {
                        "nom":"Ecole Vétérinaire",
                        "longitude":-1.52263248,
                        "latitude":47.28998722
                    },
                    {
                        "nom":"Ecole Vétérinaire",
                        "longitude":-1.52349619,
                        "latitude":47.29094903
                    },
                    {
                        "nom":"Facultés",
                        "longitude":-1.55515038,
                        "latitude":47.24575398
                    },
                    {
                        "nom":"Facultés",
                        "longitude":-1.55515038,
                        "latitude":47.24575398
                    },
                    {
                        "nom":"Facultés",
                        "longitude":-1.55552578,
                        "latitude":47.24673203
                    },
                    {
                        "nom":"Fardière",
                        "longitude":-1.60682070,
                        "latitude":47.19851337
                    },
                    {
                        "nom":"Fardière",
                        "longitude":-1.60520580,
                        "latitude":47.19811829
                    },
                    {
                        "nom":"Fallières",
                        "longitude":-1.57927872,
                        "latitude":47.22485204
                    },
                    {
                        "nom":"Fallières",
                        "longitude":-1.57942376,
                        "latitude":47.22502725
                    },
                    {
                        "nom":"Faneurs",
                        "longitude":-1.70259309,
                        "latitude":47.21375066
                    },
                    {
                        "nom":"Faneurs",
                        "longitude":-1.70205892,
                        "latitude":47.21367931
                    },
                    {
                        "nom":"Fantaisie",
                        "longitude":-1.57555877,
                        "latitude":47.25352966
                    },
                    {
                        "nom":"Fantaisie",
                        "longitude":-1.57450312,
                        "latitude":47.25356547
                    },
                    {
                        "nom":"Fauvelière",
                        "longitude":-1.50531922,
                        "latitude":47.28299968
                    },
                    {
                        "nom":"Fauvelière",
                        "longitude":-1.50491020,
                        "latitude":47.28283317
                    },
                    {
                        "nom":"Fresche Blanc",
                        "longitude":-1.55820038,
                        "latitude":47.25312647
                    },
                    {
                        "nom":"Fresche Blanc",
                        "longitude":-1.55685465,
                        "latitude":47.25281165
                    },
                    {
                        "nom":"Faraday",
                        "longitude":-1.61643132,
                        "latitude":47.22664620
                    },
                    {
                        "nom":"Faraday",
                        "longitude":-1.61656984,
                        "latitude":47.22673152
                    },
                    {
                        "nom":"Fac de Droit",
                        "longitude":-1.55210398,
                        "latitude":47.24387531
                    },
                    {
                        "nom":"Fac de Droit",
                        "longitude":-1.55196551,
                        "latitude":47.24378992
                    },
                    {
                        "nom":"Fac de Lettres",
                        "longitude":-1.55183232,
                        "latitude":47.24559572
                    },
                    {
                        "nom":"Fac de Lettres",
                        "longitude":-1.55181925,
                        "latitude":47.24541603
                    },
                    {
                        "nom":"Ferrière",
                        "longitude":-1.59160402,
                        "latitude":47.24325691
                    },
                    {
                        "nom":"Ferrière",
                        "longitude":-1.59160402,
                        "latitude":47.24325691
                    },
                    {
                        "nom":"Ferrière",
                        "longitude":-1.59102357,
                        "latitude":47.24435750
                    },
                    {
                        "nom":"Ferrière",
                        "longitude":-1.59076630,
                        "latitude":47.24445633
                    },
                    {
                        "nom":"Félix Faure",
                        "longitude":-1.56953165,
                        "latitude":47.22716425
                    },
                    {
                        "nom":"Félix Faure",
                        "longitude":-1.56952508,
                        "latitude":47.22707441
                    },
                    {
                        "nom":"Félix Faure",
                        "longitude":-1.57019110,
                        "latitude":47.22714190
                    },
                    {
                        "nom":"Félix Faure",
                        "longitude":-1.56997325,
                        "latitude":47.22777976
                    },
                    {
                        "nom":"Fief Guérin",
                        "longitude":-1.66076091,
                        "latitude":47.14396105
                    },
                    {
                        "nom":"Fief Guérin",
                        "longitude":-1.66049085,
                        "latitude":47.14388036
                    },
                    {
                        "nom":"Fleuriaye",
                        "longitude":-1.49941791,
                        "latitude":47.29850645
                    },
                    {
                        "nom":"Fleuriaye",
                        "longitude":-1.49954353,
                        "latitude":47.29841221
                    },
                    {
                        "nom":"Fleuriaye",
                        "longitude":-1.50025552,
                        "latitude":47.29910904
                    },
                    {
                        "nom":"Fleuriaye",
                        "longitude":-1.50011054,
                        "latitude":47.29893373
                    },
                    {
                        "nom":"Floride",
                        "longitude":-1.60369399,
                        "latitude":47.21348186
                    },
                    {
                        "nom":"Floride",
                        "longitude":-1.60382584,
                        "latitude":47.21347735
                    },
                    {
                        "nom":"Fleurs",
                        "longitude":-1.71768908,
                        "latitude":47.21583272
                    },
                    {
                        "nom":"Fleurs",
                        "longitude":-1.71809144,
                        "latitude":47.21590864
                    },
                    {
                        "nom":"François Mitterrand",
                        "longitude":-1.63816890,
                        "latitude":47.22202559
                    },
                    {
                        "nom":"François Mitterrand",
                        "longitude":-1.63817557,
                        "latitude":47.22211543
                    },
                    {
                        "nom":"François Mitterrand",
                        "longitude":-1.63803035,
                        "latitude":47.22194029
                    },
                    {
                        "nom":"François Mitterrand",
                        "longitude":-1.63857119,
                        "latitude":47.22210178
                    },
                    {
                        "nom":"François Mitterrand",
                        "longitude":-1.63884161,
                        "latitude":47.22218253
                    },
                    {
                        "nom":"Fresnel",
                        "longitude":-1.52333113,
                        "latitude":47.24286067
                    },
                    {
                        "nom":"Fresnel",
                        "longitude":-1.52334410,
                        "latitude":47.24304037
                    },
                    {
                        "nom":"Foch-Cathédrale",
                        "longitude":-1.55032104,
                        "latitude":47.21934742
                    },
                    {
                        "nom":"Foch-Cathédrale",
                        "longitude":-1.55031451,
                        "latitude":47.21925758
                    },
                    {
                        "nom":"Foch-Cathédrale",
                        "longitude":-1.55044639,
                        "latitude":47.21925313
                    },
                    {
                        "nom":"Foch-Cathédrale",
                        "longitude":-1.55054562,
                        "latitude":47.21879945
                    },
                    {
                        "nom":"Foch-Cathédrale",
                        "longitude":-1.55048556,
                        "latitude":47.21979221
                    },
                    {
                        "nom":"Fonderies",
                        "longitude":-1.54396268,
                        "latitude":47.20641203
                    },
                    {
                        "nom":"Fonderies",
                        "longitude":-1.54369900,
                        "latitude":47.20642091
                    },
                    {
                        "nom":"Forum d'Orvault",
                        "longitude":-1.61517856,
                        "latitude":47.25443054
                    },
                    {
                        "nom":"Forum d'Orvault",
                        "longitude":-1.61515199,
                        "latitude":47.25407117
                    },
                    {
                        "nom":"Fontaine",
                        "longitude":-1.48943624,
                        "latitude":47.20535343
                    },
                    {
                        "nom":"Fontaine",
                        "longitude":-1.48918540,
                        "latitude":47.20554189
                    },
                    {
                        "nom":"Foulquier",
                        "longitude":-1.54911861,
                        "latitude":47.31017344
                    },
                    {
                        "nom":"Foulquier",
                        "longitude":-1.54911861,
                        "latitude":47.31017344
                    },
                    {
                        "nom":"Frachon",
                        "longitude":-1.62049565,
                        "latitude":47.21903091
                    },
                    {
                        "nom":"Frachon",
                        "longitude":-1.62050229,
                        "latitude":47.21912076
                    },
                    {
                        "nom":"Frébaudière",
                        "longitude":-1.62655239,
                        "latitude":47.27223426
                    },
                    {
                        "nom":"Frébaudière",
                        "longitude":-1.62708704,
                        "latitude":47.27230596
                    },
                    {
                        "nom":"Fraîches",
                        "longitude":-1.48948053,
                        "latitude":47.17455019
                    },
                    {
                        "nom":"Fraîches",
                        "longitude":-1.49056037,
                        "latitude":47.17487456
                    },
                    {
                        "nom":"Forêt",
                        "longitude":-1.57541602,
                        "latitude":47.24434768
                    },
                    {
                        "nom":"Forêt",
                        "longitude":-1.57554138,
                        "latitude":47.24425336
                    },
                    {
                        "nom":"Frégate",
                        "longitude":-1.47570796,
                        "latitude":47.23525876
                    },
                    {
                        "nom":"Frégate",
                        "longitude":-1.47610372,
                        "latitude":47.23524567
                    },
                    {
                        "nom":"François II",
                        "longitude":-1.68324467,
                        "latitude":47.21839050
                    },
                    {
                        "nom":"François II",
                        "longitude":-1.68244676,
                        "latitude":47.21832825
                    },
                    {
                        "nom":"Franklin",
                        "longitude":-1.62038789,
                        "latitude":47.22651044
                    },
                    {
                        "nom":"Franklin",
                        "longitude":-1.62067824,
                        "latitude":47.22686075
                    },
                    {
                        "nom":"Frémoire",
                        "longitude":-1.47496169,
                        "latitude":47.15242571
                    },
                    {
                        "nom":"Frémoire",
                        "longitude":-1.47634919,
                        "latitude":47.15337046
                    },
                    {
                        "nom":"Frênes",
                        "longitude":-1.48770396,
                        "latitude":47.25323412
                    },
                    {
                        "nom":"Frênes",
                        "longitude":-1.48757843,
                        "latitude":47.25332835
                    },
                    {
                        "nom":"Frêne Rond",
                        "longitude":-1.49642323,
                        "latitude":47.19035074
                    },
                    {
                        "nom":"Frêne Rond",
                        "longitude":-1.49641680,
                        "latitude":47.19026089
                    },
                    {
                        "nom":"Félix Tableau",
                        "longitude":-1.54695065,
                        "latitude":47.18748742
                    },
                    {
                        "nom":"Félix Tableau",
                        "longitude":-1.54681233,
                        "latitude":47.18740202
                    },
                    {
                        "nom":"Félix Tableau",
                        "longitude":-1.54738515,
                        "latitude":47.18801317
                    },
                    {
                        "nom":"Félix Tableau",
                        "longitude":-1.54648212,
                        "latitude":47.18831382
                    },
                    {
                        "nom":"Fructidor",
                        "longitude":-1.55963273,
                        "latitude":47.24010857
                    },
                    {
                        "nom":"Fructidor",
                        "longitude":-1.56104460,
                        "latitude":47.23952046
                    },
                    {
                        "nom":"Gagnerie",
                        "longitude":-1.50722581,
                        "latitude":47.23223083
                    },
                    {
                        "nom":"Gagnerie",
                        "longitude":-1.50748317,
                        "latitude":47.23213218
                    },
                    {
                        "nom":"Garillère",
                        "longitude":-1.52075715,
                        "latitude":47.19251060
                    },
                    {
                        "nom":"Garillère",
                        "longitude":-1.52114610,
                        "latitude":47.19240751
                    },
                    {
                        "nom":"Galheur",
                        "longitude":-1.58118031,
                        "latitude":47.16597257
                    },
                    {
                        "nom":"Galheur",
                        "longitude":-1.57991548,
                        "latitude":47.16673615
                    },
                    {
                        "nom":"Gambetta",
                        "longitude":-1.53846192,
                        "latitude":47.22352952
                    },
                    {
                        "nom":"Gambetta",
                        "longitude":-1.53871918,
                        "latitude":47.22343080
                    },
                    {
                        "nom":"Garotterie",
                        "longitude":-1.62710292,
                        "latitude":47.12620991
                    },
                    {
                        "nom":"Garotterie",
                        "longitude":-1.62710292,
                        "latitude":47.12620991
                    },
                    {
                        "nom":"Gassendi",
                        "longitude":-1.58009416,
                        "latitude":47.19987545
                    },
                    {
                        "nom":"Gassendi",
                        "longitude":-1.57944161,
                        "latitude":47.19998770
                    },
                    {
                        "nom":"Gâtine",
                        "longitude":-1.66066224,
                        "latitude":47.21341132
                    },
                    {
                        "nom":"Gâtine",
                        "longitude":-1.66012141,
                        "latitude":47.21324994
                    },
                    {
                        "nom":"Gautellerie",
                        "longitude":-1.60196482,
                        "latitude":47.17742295
                    },
                    {
                        "nom":"Gautellerie",
                        "longitude":-1.60209659,
                        "latitude":47.17741844
                    },
                    {
                        "nom":"Grand Blottereau",
                        "longitude":-1.51114046,
                        "latitude":47.22966852
                    },
                    {
                        "nom":"Grand Blottereau",
                        "longitude":-1.51164868,
                        "latitude":47.22938136
                    },
                    {
                        "nom":"Guiblinière",
                        "longitude":-1.52028568,
                        "latitude":47.29231724
                    },
                    {
                        "nom":"Guiblinière",
                        "longitude":-1.52042422,
                        "latitude":47.29240267
                    },
                    {
                        "nom":"Grand Bois",
                        "longitude":-1.58921614,
                        "latitude":47.23415131
                    },
                    {
                        "nom":"Grand Bois",
                        "longitude":-1.59060114,
                        "latitude":47.23320345
                    },
                    {
                        "nom":"Grande-Bretagne",
                        "longitude":-1.50236925,
                        "latitude":47.26220345
                    },
                    {
                        "nom":"Grande-Bretagne",
                        "longitude":-1.50224372,
                        "latitude":47.26229769
                    },
                    {
                        "nom":"Grand Carcouët",
                        "longitude":-1.59272469,
                        "latitude":47.23150986
                    },
                    {
                        "nom":"Grand Carcouët",
                        "longitude":-1.59286320,
                        "latitude":47.23159521
                    },
                    {
                        "nom":"Grand Carcouët",
                        "longitude":-1.59176840,
                        "latitude":47.23109211
                    },
                    {
                        "nom":"Grand Carcouët",
                        "longitude":-1.59218390,
                        "latitude":47.23134815
                    },
                    {
                        "nom":"Grande Censive",
                        "longitude":-1.54836375,
                        "latitude":47.25246754
                    },
                    {
                        "nom":"Grande Censive",
                        "longitude":-1.54992762,
                        "latitude":47.25214465
                    },
                    {
                        "nom":"Grande Censive",
                        "longitude":-1.54960493,
                        "latitude":47.25134494
                    },
                    {
                        "nom":"Gachet 1",
                        "longitude":-1.52392909,
                        "latitude":47.29327618
                    },
                    {
                        "nom":"Gachet 1",
                        "longitude":-1.52393558,
                        "latitude":47.29336602
                    },
                    {
                        "nom":"Gachet 2",
                        "longitude":-1.52401570,
                        "latitude":47.29264284
                    },
                    {
                        "nom":"Gauchoux",
                        "longitude":-1.60044963,
                        "latitude":47.14604009
                    },
                    {
                        "nom":"Gauchoux",
                        "longitude":-1.59992947,
                        "latitude":47.14614795
                    },
                    {
                        "nom":"Glacisol",
                        "longitude":-1.50465591,
                        "latitude":47.25168005
                    },
                    {
                        "nom":"Glacisol",
                        "longitude":-1.50438554,
                        "latitude":47.25159900
                    },
                    {
                        "nom":"Gare de Chantenay",
                        "longitude":-1.59394851,
                        "latitude":47.19778236
                    },
                    {
                        "nom":"Gare de Chantenay",
                        "longitude":-1.59355966,
                        "latitude":47.19788570
                    },
                    {
                        "nom":"Gare de Chantenay",
                        "longitude":-1.59355966,
                        "latitude":47.19788570
                    },
                    {
                        "nom":"Gare de Chantenay",
                        "longitude":-1.59500965,
                        "latitude":47.19783622
                    },
                    {
                        "nom":"Goldens",
                        "longitude":-1.50045104,
                        "latitude":47.16175622
                    },
                    {
                        "nom":"Goldens",
                        "longitude":-1.49967350,
                        "latitude":47.16196226
                    },
                    {
                        "nom":"Gendronnerie",
                        "longitude":-1.63906358,
                        "latitude":47.12732846
                    },
                    {
                        "nom":"Gendronnerie",
                        "longitude":-1.64009670,
                        "latitude":47.12702253
                    },
                    {
                        "nom":"Grande Ouche",
                        "longitude":-1.57979430,
                        "latitude":47.18133148
                    },
                    {
                        "nom":"Grande Ouche",
                        "longitude":-1.57979430,
                        "latitude":47.18133148
                    },
                    {
                        "nom":"Grande Pièce",
                        "longitude":-1.51091509,
                        "latitude":47.19896414
                    },
                    {
                        "nom":"Grande Pièce",
                        "longitude":-1.51039424,
                        "latitude":47.19907160
                    },
                    {
                        "nom":"Guindré",
                        "longitude":-1.53762890,
                        "latitude":47.23940900
                    },
                    {
                        "nom":"Guindré",
                        "longitude":-1.53801165,
                        "latitude":47.23921601
                    },
                    {
                        "nom":"Grande Noëlle",
                        "longitude":-1.50221513,
                        "latitude":47.16241793
                    },
                    {
                        "nom":"Grande Noëlle",
                        "longitude":-1.50220869,
                        "latitude":47.16232808
                    },
                    {
                        "nom":"Géraudière",
                        "longitude":-1.56716305,
                        "latitude":47.26173999
                    },
                    {
                        "nom":"Géraudière",
                        "longitude":-1.56716305,
                        "latitude":47.26173999
                    },
                    {
                        "nom":"Géraudière",
                        "longitude":-1.56697783,
                        "latitude":47.26282705
                    },
                    {
                        "nom":"Géraudière",
                        "longitude":-1.56764428,
                        "latitude":47.26289456
                    },
                    {
                        "nom":"Géraudière",
                        "longitude":-1.56639743,
                        "latitude":47.26212616
                    },
                    {
                        "nom":"Gesvrine",
                        "longitude":-1.55189430,
                        "latitude":47.27009140
                    },
                    {
                        "nom":"Gesvrine",
                        "longitude":-1.55213869,
                        "latitude":47.26981296
                    },
                    {
                        "nom":"Gesvres",
                        "longitude":-1.54853315,
                        "latitude":47.26390010
                    },
                    {
                        "nom":"Gesvres",
                        "longitude":-1.54864554,
                        "latitude":47.26362612
                    },
                    {
                        "nom":"Grignon",
                        "longitude":-1.46630003,
                        "latitude":47.21638639
                    },
                    {
                        "nom":"Grignon",
                        "longitude":-1.46643829,
                        "latitude":47.21647189
                    },
                    {
                        "nom":"Gauguin",
                        "longitude":-1.55468713,
                        "latitude":47.16669088
                    },
                    {
                        "nom":"Gauguin",
                        "longitude":-1.55562893,
                        "latitude":47.16692925
                    },
                    {
                        "nom":"Gilarderie",
                        "longitude":-1.68373525,
                        "latitude":47.18621680
                    },
                    {
                        "nom":"Gilarderie",
                        "longitude":-1.68252224,
                        "latitude":47.18589884
                    },
                    {
                        "nom":"Gibraye",
                        "longitude":-1.51505887,
                        "latitude":47.20513022
                    },
                    {
                        "nom":"Gibraye",
                        "longitude":-1.51637728,
                        "latitude":47.20508614
                    },
                    {
                        "nom":"Gare d'Indre",
                        "longitude":-1.66227247,
                        "latitude":47.20488859
                    },
                    {
                        "nom":"Gare d'Indre",
                        "longitude":-1.66175187,
                        "latitude":47.20499673
                    },
                    {
                        "nom":"Gicquelière",
                        "longitude":-1.45894604,
                        "latitude":47.25805673
                    },
                    {
                        "nom":"Gicquelière",
                        "longitude":-1.45908439,
                        "latitude":47.25814223
                    },
                    {
                        "nom":"Grillaud",
                        "longitude":-1.58181162,
                        "latitude":47.21972216
                    },
                    {
                        "nom":"Grillaud",
                        "longitude":-1.58171923,
                        "latitude":47.22026571
                    },
                    {
                        "nom":"Grande Lande",
                        "longitude":-1.49776396,
                        "latitude":47.19246766
                    },
                    {
                        "nom":"Grande Lande",
                        "longitude":-1.49761927,
                        "latitude":47.19229235
                    },
                    {
                        "nom":"Gilière",
                        "longitude":-1.55257368,
                        "latitude":47.29942955
                    },
                    {
                        "nom":"Gilière",
                        "longitude":-1.55335302,
                        "latitude":47.29922316
                    },
                    {
                        "nom":"Galarne",
                        "longitude":-1.52994633,
                        "latitude":47.20814414
                    },
                    {
                        "nom":"Galarne",
                        "longitude":-1.52981448,
                        "latitude":47.20814857
                    },
                    {
                        "nom":"Grange au Loup",
                        "longitude":-1.51604758,
                        "latitude":47.26291822
                    },
                    {
                        "nom":"Grange au Loup",
                        "longitude":-1.51594150,
                        "latitude":47.26328202
                    },
                    {
                        "nom":"Gare Maritime",
                        "longitude":-1.57333616,
                        "latitude":47.20686011
                    },
                    {
                        "nom":"Gare Maritime",
                        "longitude":-1.57308561,
                        "latitude":47.20704874
                    },
                    {
                        "nom":"Gare Maritime",
                        "longitude":-1.57308561,
                        "latitude":47.20704874
                    },
                    {
                        "nom":"Gare Maritime",
                        "longitude":-1.57280880,
                        "latitude":47.20687800
                    },
                    {
                        "nom":"Gare Maritime",
                        "longitude":-1.57334983,
                        "latitude":47.20523842
                    },
                    {
                        "nom":"Guy Mollet",
                        "longitude":-1.55367050,
                        "latitude":47.24904628
                    },
                    {
                        "nom":"Guy Mollet",
                        "longitude":-1.55394747,
                        "latitude":47.24921707
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53356109,
                        "latitude":47.19964668
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53356109,
                        "latitude":47.19964668
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53407540,
                        "latitude":47.19944927
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53379875,
                        "latitude":47.19927843
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53301429,
                        "latitude":47.19939484
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53287597,
                        "latitude":47.19930942
                    },
                    {
                        "nom":"Gréneraie",
                        "longitude":-1.53352211,
                        "latitude":47.19910759
                    },
                    {
                        "nom":"Gobinière",
                        "longitude":-1.58682125,
                        "latitude":47.25017483
                    },
                    {
                        "nom":"Gobinière",
                        "longitude":-1.58716434,
                        "latitude":47.24944261
                    },
                    {
                        "nom":"Grande Ouche",
                        "longitude":-1.46659534,
                        "latitude":47.21124312
                    },
                    {
                        "nom":"Grande Ouche",
                        "longitude":-1.46672720,
                        "latitude":47.21123877
                    },
                    {
                        "nom":"Golf",
                        "longitude":-1.60486096,
                        "latitude":47.23649968
                    },
                    {
                        "nom":"Golf",
                        "longitude":-1.60431345,
                        "latitude":47.23624819
                    },
                    {
                        "nom":"Gournière",
                        "longitude":-1.54939421,
                        "latitude":47.26846441
                    },
                    {
                        "nom":"Gournière",
                        "longitude":-1.54938114,
                        "latitude":47.26828472
                    },
                    {
                        "nom":"Grand Portail",
                        "longitude":-1.52291324,
                        "latitude":47.20225550
                    },
                    {
                        "nom":"Grand Portail",
                        "longitude":-1.52304507,
                        "latitude":47.20225108
                    },
                    {
                        "nom":"Gare de Pont-Rousseau",
                        "longitude":-1.54906834,
                        "latitude":47.19300016
                    },
                    {
                        "nom":"Gare de Pont-Rousseau",
                        "longitude":-1.54906834,
                        "latitude":47.19300016
                    },
                    {
                        "nom":"Grammoire",
                        "longitude":-1.47268780,
                        "latitude":47.18357282
                    },
                    {
                        "nom":"Grammoire",
                        "longitude":-1.47308957,
                        "latitude":47.18364959
                    },
                    {
                        "nom":"Gare de l'Etat",
                        "longitude":-1.56017356,
                        "latitude":47.20397352
                    },
                    {
                        "nom":"Gare de l'Etat",
                        "longitude":-1.55996222,
                        "latitude":47.20470120
                    },
                    {
                        "nom":"Grimm",
                        "longitude":-1.64658287,
                        "latitude":47.21606058
                    },
                    {
                        "nom":"Grimm",
                        "longitude":-1.64543628,
                        "latitude":47.21664063
                    },
                    {
                        "nom":"Grimm",
                        "longitude":-1.64671473,
                        "latitude":47.21605602
                    },
                    {
                        "nom":"Grimm",
                        "longitude":-1.64552801,
                        "latitude":47.21609703
                    },
                    {
                        "nom":"Grimm",
                        "longitude":-1.64637935,
                        "latitude":47.21687826
                    },
                    {
                        "nom":"Graslin",
                        "longitude":-1.56345851,
                        "latitude":47.21277904
                    },
                    {
                        "nom":"Graslin",
                        "longitude":-1.56332010,
                        "latitude":47.21269365
                    },
                    {
                        "nom":"Grimau",
                        "longitude":-1.65446267,
                        "latitude":47.22776755
                    },
                    {
                        "nom":"Grimau",
                        "longitude":-1.65459456,
                        "latitude":47.22776299
                    },
                    {
                        "nom":"Gripots",
                        "longitude":-1.49181313,
                        "latitude":47.19239539
                    },
                    {
                        "nom":"Gripots",
                        "longitude":-1.49181955,
                        "latitude":47.19248524
                    },
                    {
                        "nom":"Garotterie",
                        "longitude":-1.60678975,
                        "latitude":47.22499493
                    },
                    {
                        "nom":"Garotterie",
                        "longitude":-1.60687526,
                        "latitude":47.22436152
                    },
                    {
                        "nom":"George Sand",
                        "longitude":-1.54497861,
                        "latitude":47.16575765
                    },
                    {
                        "nom":"George Sand",
                        "longitude":-1.54577558,
                        "latitude":47.16582084
                    },
                    {
                        "nom":"Grassinière",
                        "longitude":-1.50800609,
                        "latitude":47.16582731
                    },
                    {
                        "nom":"Grassinière",
                        "longitude":-1.50826958,
                        "latitude":47.16581851
                    },
                    {
                        "nom":"Gare SNCF Nord",
                        "longitude":-1.54241017,
                        "latitude":47.21781263
                    },
                    {
                        "nom":"Gare SNCF Nord",
                        "longitude":-1.54241668,
                        "latitude":47.21790248
                    },
                    {
                        "nom":"Gare SNCF Nord",
                        "longitude":-1.54266739,
                        "latitude":47.21771391
                    },
                    {
                        "nom":"Gare SNCF Nord",
                        "longitude":-1.54204061,
                        "latitude":47.21818533
                    },
                    {
                        "nom":"Gare SNCF Sud",
                        "longitude":-1.54223429,
                        "latitude":47.21538677
                    },
                    {
                        "nom":"Gare SNCF Sud",
                        "longitude":-1.54319643,
                        "latitude":47.21589478
                    },
                    {
                        "nom":"Gare SNCF Sud",
                        "longitude":-1.54305805,
                        "latitude":47.21580937
                    },
                    {
                        "nom":"Gare SNCF Sud",
                        "longitude":-1.54222126,
                        "latitude":47.21520708
                    },
                    {
                        "nom":"Gare SNCF Sud",
                        "longitude":-1.54223429,
                        "latitude":47.21538677
                    },
                    {
                        "nom":"Garettes-Landreau",
                        "longitude":-1.63072434,
                        "latitude":47.27317170
                    },
                    {
                        "nom":"Garettes-Landreau",
                        "longitude":-1.63045367,
                        "latitude":47.27309094
                    },
                    {
                        "nom":"Gaston Veil",
                        "longitude":-1.55572043,
                        "latitude":47.20997839
                    },
                    {
                        "nom":"Gaston Veil",
                        "longitude":-1.55560165,
                        "latitude":47.21016254
                    },
                    {
                        "nom":"Guilloterie",
                        "longitude":-1.55720661,
                        "latitude":47.17227999
                    },
                    {
                        "nom":"Guilloterie",
                        "longitude":-1.55735797,
                        "latitude":47.17254507
                    },
                    {
                        "nom":"Gustave Roch",
                        "longitude":-1.55198669,
                        "latitude":47.20406998
                    },
                    {
                        "nom":"Gustave Roch",
                        "longitude":-1.55195405,
                        "latitude":47.20362075
                    },
                    {
                        "nom":"Gare de Vertou Nord",
                        "longitude":-1.47834737,
                        "latitude":47.18698803
                    },
                    {
                        "nom":"Gare de Vertou Nord",
                        "longitude":-1.47848557,
                        "latitude":47.18707351
                    },
                    {
                        "nom":"Gare de Vertou Sud",
                        "longitude":-1.47666976,
                        "latitude":47.18569263
                    },
                    {
                        "nom":"Gare de Vertou Sud",
                        "longitude":-1.47668256,
                        "latitude":47.18587233
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52325089,
                        "latitude":47.24907778
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52205037,
                        "latitude":47.24893783
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52232724,
                        "latitude":47.24910869
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52288098,
                        "latitude":47.24945041
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52203092,
                        "latitude":47.24866828
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52203092,
                        "latitude":47.24866828
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52365971,
                        "latitude":47.24924422
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52338932,
                        "latitude":47.24916321
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52297402,
                        "latitude":47.24890692
                    },
                    {
                        "nom":"Haluchère-Batignolles",
                        "longitude":-1.52226888,
                        "latitude":47.24830007
                    },
                    {
                        "nom":"Harouys",
                        "longitude":-1.56747172,
                        "latitude":47.21705645
                    },
                    {
                        "nom":"Harouys",
                        "longitude":-1.56704989,
                        "latitude":47.21671047
                    },
                    {
                        "nom":"Haubans",
                        "longitude":-1.53001825,
                        "latitude":47.21462644
                    },
                    {
                        "nom":"Haubans",
                        "longitude":-1.52987340,
                        "latitude":47.21445117
                    },
                    {
                        "nom":"Halvêque",
                        "longitude":-1.52041001,
                        "latitude":47.25736862
                    },
                    {
                        "nom":"Halvêque",
                        "longitude":-1.52041001,
                        "latitude":47.25736862
                    },
                    {
                        "nom":"Halvêque",
                        "longitude":-1.51883695,
                        "latitude":47.25940261
                    },
                    {
                        "nom":"Halvêque",
                        "longitude":-1.51946442,
                        "latitude":47.25893132
                    },
                    {
                        "nom":"Hangar à Bananes",
                        "longitude":-1.57312719,
                        "latitude":47.20038230
                    },
                    {
                        "nom":"Halbarderie",
                        "longitude":-1.43810608,
                        "latitude":47.26828686
                    },
                    {
                        "nom":"Halbarderie",
                        "longitude":-1.43746510,
                        "latitude":47.26857801
                    },
                    {
                        "nom":"Hôpital Bellier",
                        "longitude":-1.52454473,
                        "latitude":47.22120456
                    },
                    {
                        "nom":"Hôpital Bellier",
                        "longitude":-1.52454473,
                        "latitude":47.22120456
                    },
                    {
                        "nom":"Herbray",
                        "longitude":-1.47110759,
                        "latitude":47.15021161
                    },
                    {
                        "nom":"Herbray",
                        "longitude":-1.47099504,
                        "latitude":47.15048552
                    },
                    {
                        "nom":"Hauts de Couëron",
                        "longitude":-1.66970210,
                        "latitude":47.24056965
                    },
                    {
                        "nom":"Haudrière",
                        "longitude":-1.46759427,
                        "latitude":47.15465082
                    },
                    {
                        "nom":"Haudrière",
                        "longitude":-1.46758151,
                        "latitude":47.15447112
                    },
                    {
                        "nom":"Hermeland",
                        "longitude":-1.61724443,
                        "latitude":47.23049132
                    },
                    {
                        "nom":"Hermeland",
                        "longitude":-1.61712581,
                        "latitude":47.23067553
                    },
                    {
                        "nom":"Hermeland",
                        "longitude":-1.61679561,
                        "latitude":47.22978615
                    },
                    {
                        "nom":"Hermeland",
                        "longitude":-1.61680890,
                        "latitude":47.22996584
                    },
                    {
                        "nom":"Hameau Fleuri",
                        "longitude":-1.59006054,
                        "latitude":47.25105532
                    },
                    {
                        "nom":"Hameau Fleuri",
                        "longitude":-1.58938761,
                        "latitude":47.25089809
                    },
                    {
                        "nom":"Haute Forêt",
                        "longitude":-1.55544842,
                        "latitude":47.23349494
                    },
                    {
                        "nom":"Haute Forêt",
                        "longitude":-1.55542880,
                        "latitude":47.23322540
                    },
                    {
                        "nom":"Haute Forêt",
                        "longitude":-1.46565950,
                        "latitude":47.16831427
                    },
                    {
                        "nom":"Haute Forêt",
                        "longitude":-1.46565950,
                        "latitude":47.16831427
                    },
                    {
                        "nom":"Haute Forêt",
                        "longitude":-1.46537686,
                        "latitude":47.16805341
                    },
                    {
                        "nom":"Haute Ile",
                        "longitude":-1.55534827,
                        "latitude":47.19395903
                    },
                    {
                        "nom":"Haute Ile",
                        "longitude":-1.55632975,
                        "latitude":47.19473647
                    },
                    {
                        "nom":"Haute Indre",
                        "longitude":-1.64993520,
                        "latitude":47.19540815
                    },
                    {
                        "nom":"Haute Indre",
                        "longitude":-1.64986159,
                        "latitude":47.19441990
                    },
                    {
                        "nom":"Hippodrome",
                        "longitude":-1.56728130,
                        "latitude":47.24705518
                    },
                    {
                        "nom":"Hippodrome",
                        "longitude":-1.56771648,
                        "latitude":47.24758084
                    },
                    {
                        "nom":"Haute Lande",
                        "longitude":-1.52345288,
                        "latitude":47.15477271
                    },
                    {
                        "nom":"Haute Lande",
                        "longitude":-1.52365804,
                        "latitude":47.15395523
                    },
                    {
                        "nom":"Houmaille",
                        "longitude":-1.58696260,
                        "latitude":47.18901359
                    },
                    {
                        "nom":"Houmaille",
                        "longitude":-1.58696919,
                        "latitude":47.18910344
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.47244142,
                        "latitude":47.17268334
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.47244781,
                        "latitude":47.17277319
                    },
                    {
                        "nom":"Hôtel Dieu",
                        "longitude":-1.55321406,
                        "latitude":47.21186436
                    },
                    {
                        "nom":"Hôtel Dieu",
                        "longitude":-1.55334592,
                        "latitude":47.21185991
                    },
                    {
                        "nom":"Hôtel Dieu",
                        "longitude":-1.55287075,
                        "latitude":47.21259648
                    },
                    {
                        "nom":"Hôtel Dieu",
                        "longitude":-1.55298954,
                        "latitude":47.21241234
                    },
                    {
                        "nom":"Hongrie",
                        "longitude":-1.52403029,
                        "latitude":47.21590797
                    },
                    {
                        "nom":"Hongrie",
                        "longitude":-1.52351579,
                        "latitude":47.21610533
                    },
                    {
                        "nom":"Housseau",
                        "longitude":-1.49842423,
                        "latitude":47.28097750
                    },
                    {
                        "nom":"Housseau",
                        "longitude":-1.49854981,
                        "latitude":47.28088326
                    },
                    {
                        "nom":"Hôpital Laënnec",
                        "longitude":-1.64086027,
                        "latitude":47.23688456
                    },
                    {
                        "nom":"Hôtel de Région",
                        "longitude":-1.52614007,
                        "latitude":47.21034330
                    },
                    {
                        "nom":"Hôtel de Région",
                        "longitude":-1.52600821,
                        "latitude":47.21034772
                    },
                    {
                        "nom":"Hérelle",
                        "longitude":-1.58667861,
                        "latitude":47.20136272
                    },
                    {
                        "nom":"Hérelle",
                        "longitude":-1.58669837,
                        "latitude":47.20163226
                    },
                    {
                        "nom":"Mairie de Carquefou",
                        "longitude":-1.49031126,
                        "latitude":47.29889879
                    },
                    {
                        "nom":"Mairie de Carquefou",
                        "longitude":-1.49163200,
                        "latitude":47.29885499
                    },
                    {
                        "nom":"Mairie de Carquefou",
                        "longitude":-1.49044333,
                        "latitude":47.29889441
                    },
                    {
                        "nom":"Hôtel de Ville",
                        "longitude":-1.55444296,
                        "latitude":47.21785734
                    },
                    {
                        "nom":"Hôtel de Ville",
                        "longitude":-1.55430456,
                        "latitude":47.21777194
                    },
                    {
                        "nom":"Helvêtie",
                        "longitude":-1.49798070,
                        "latitude":47.25082145
                    },
                    {
                        "nom":"Helvêtie",
                        "longitude":-1.49797426,
                        "latitude":47.25073160
                    },
                    {
                        "nom":"ICAM",
                        "longitude":-1.50186234,
                        "latitude":47.27356816
                    },
                    {
                        "nom":"ICAM",
                        "longitude":-1.50187525,
                        "latitude":47.27374785
                    },
                    {
                        "nom":"Ile de France",
                        "longitude":-1.46798232,
                        "latitude":47.16940836
                    },
                    {
                        "nom":"Ile de Nantes",
                        "longitude":-1.53863042,
                        "latitude":47.20578090
                    },
                    {
                        "nom":"Ile de Nantes",
                        "longitude":-1.53878178,
                        "latitude":47.20604601
                    },
                    {
                        "nom":"Imprimeurs",
                        "longitude":-1.67008841,
                        "latitude":47.23866470
                    },
                    {
                        "nom":"Imprimeurs",
                        "longitude":-1.67022032,
                        "latitude":47.23866011
                    },
                    {
                        "nom":"Internat Laënnec",
                        "longitude":-1.64058805,
                        "latitude":47.23500246
                    },
                    {
                        "nom":"Internat Laënnec",
                        "longitude":-1.64099045,
                        "latitude":47.23507865
                    },
                    {
                        "nom":"Indre",
                        "longitude":-1.66661760,
                        "latitude":47.20113476
                    },
                    {
                        "nom":"Indre",
                        "longitude":-1.66833797,
                        "latitude":47.20116504
                    },
                    {
                        "nom":"Irlandais",
                        "longitude":-1.60705292,
                        "latitude":47.22318454
                    },
                    {
                        "nom":"Irlandais",
                        "longitude":-1.60678254,
                        "latitude":47.22310372
                    },
                    {
                        "nom":"Iroise",
                        "longitude":-1.53642616,
                        "latitude":47.24836583
                    },
                    {
                        "nom":"Iroise",
                        "longitude":-1.53641965,
                        "latitude":47.24827599
                    },
                    {
                        "nom":"IUT",
                        "longitude":-1.50506472,
                        "latitude":47.29786834
                    },
                    {
                        "nom":"IUT",
                        "longitude":-1.50440437,
                        "latitude":47.29789031
                    },
                    {
                        "nom":"Jalière",
                        "longitude":-1.59698972,
                        "latitude":47.26432935
                    },
                    {
                        "nom":"Jalière",
                        "longitude":-1.59698972,
                        "latitude":47.26432935
                    },
                    {
                        "nom":"Jalière",
                        "longitude":-1.59697649,
                        "latitude":47.26414967
                    },
                    {
                        "nom":"Jamet",
                        "longitude":-1.60304042,
                        "latitude":47.20999149
                    },
                    {
                        "nom":"Jamet",
                        "longitude":-1.60304703,
                        "latitude":47.21008134
                    },
                    {
                        "nom":"Jamet",
                        "longitude":-1.60277672,
                        "latitude":47.21000051
                    },
                    {
                        "nom":"Jaunais",
                        "longitude":-1.53370427,
                        "latitude":47.17604452
                    },
                    {
                        "nom":"Jaunais",
                        "longitude":-1.53382955,
                        "latitude":47.17595024
                    },
                    {
                        "nom":"Japy",
                        "longitude":-1.51156536,
                        "latitude":47.25946532
                    },
                    {
                        "nom":"Japy",
                        "longitude":-1.51040344,
                        "latitude":47.25986433
                    },
                    {
                        "nom":"Jarriais",
                        "longitude":-1.71826229,
                        "latitude":47.21293011
                    },
                    {
                        "nom":"Jarriais",
                        "longitude":-1.71905335,
                        "latitude":47.21290228
                    },
                    {
                        "nom":"Jean Bart",
                        "longitude":-1.67272277,
                        "latitude":47.19795002
                    },
                    {
                        "nom":"Jean Bart",
                        "longitude":-1.67627783,
                        "latitude":47.19953764
                    },
                    {
                        "nom":"Jeanne Bernard",
                        "longitude":-1.59128052,
                        "latitude":47.23525187
                    },
                    {
                        "nom":"Jeanne Bernard",
                        "longitude":-1.59066715,
                        "latitude":47.23590325
                    },
                    {
                        "nom":"Jacques Cartier",
                        "longitude":-1.63619237,
                        "latitude":47.22389516
                    },
                    {
                        "nom":"Jacques Cartier",
                        "longitude":-1.63646280,
                        "latitude":47.22397590
                    },
                    {
                        "nom":"Jacques Demy",
                        "longitude":-1.68282901,
                        "latitude":47.15813597
                    },
                    {
                        "nom":"Jacques Demy",
                        "longitude":-1.68320819,
                        "latitude":47.15878928
                    },
                    {
                        "nom":"Joncours",
                        "longitude":-1.58672381,
                        "latitude":47.22000538
                    },
                    {
                        "nom":"Joncours",
                        "longitude":-1.58624903,
                        "latitude":47.22074209
                    },
                    {
                        "nom":"Jean V",
                        "longitude":-1.56591135,
                        "latitude":47.21197546
                    },
                    {
                        "nom":"Jean V",
                        "longitude":-1.56444785,
                        "latitude":47.21184488
                    },
                    {
                        "nom":"Jules Ferry",
                        "longitude":-1.48752402,
                        "latitude":47.25071836
                    },
                    {
                        "nom":"Jules Ferry",
                        "longitude":-1.48740492,
                        "latitude":47.25090243
                    },
                    {
                        "nom":"Jean Fraix",
                        "longitude":-1.54196353,
                        "latitude":47.18612433
                    },
                    {
                        "nom":"Jean Fraix",
                        "longitude":-1.54225315,
                        "latitude":47.18647484
                    },
                    {
                        "nom":"Jaguère",
                        "longitude":-1.56722515,
                        "latitude":47.17563358
                    },
                    {
                        "nom":"Jaguère",
                        "longitude":-1.56655978,
                        "latitude":47.17556606
                    },
                    {
                        "nom":"Jahardières",
                        "longitude":-1.64274770,
                        "latitude":47.12539960
                    },
                    {
                        "nom":"Jahardières",
                        "longitude":-1.64249110,
                        "latitude":47.12549855
                    },
                    {
                        "nom":"Jean Jaurès",
                        "longitude":-1.54979885,
                        "latitude":47.30132429
                    },
                    {
                        "nom":"Jean Jaurès",
                        "longitude":-1.54979231,
                        "latitude":47.30123444
                    },
                    {
                        "nom":"Jean Jaurès",
                        "longitude":-1.56031719,
                        "latitude":47.21864959
                    },
                    {
                        "nom":"Jean Jaurès",
                        "longitude":-1.56044252,
                        "latitude":47.21855529
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51938315,
                        "latitude":47.18994470
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51724841,
                        "latitude":47.18965588
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51724194,
                        "latitude":47.18956604
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51750555,
                        "latitude":47.18955722
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51658939,
                        "latitude":47.18967793
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51938315,
                        "latitude":47.18994470
                    },
                    {
                        "nom":"Joliverie",
                        "longitude":-1.51937668,
                        "latitude":47.18985485
                    },
                    {
                        "nom":"Jean Macé",
                        "longitude":-1.58905149,
                        "latitude":47.19948051
                    },
                    {
                        "nom":"Jean Macé",
                        "longitude":-1.58926240,
                        "latitude":47.20055415
                    },
                    {
                        "nom":"Jean Moulin",
                        "longitude":-1.60017212,
                        "latitude":47.20693709
                    },
                    {
                        "nom":"Jean Moulin",
                        "longitude":-1.60030396,
                        "latitude":47.20693259
                    },
                    {
                        "nom":"Jean Moulin",
                        "longitude":-1.59953276,
                        "latitude":47.20722914
                    },
                    {
                        "nom":"Jean Moulin",
                        "longitude":-1.59993488,
                        "latitude":47.20730548
                    },
                    {
                        "nom":"Jean Monnet",
                        "longitude":-1.54978652,
                        "latitude":47.16649613
                    },
                    {
                        "nom":"Jean Monnet",
                        "longitude":-1.54992478,
                        "latitude":47.16658153
                    },
                    {
                        "nom":"Jean Monnet",
                        "longitude":-1.46524928,
                        "latitude":47.16625637
                    },
                    {
                        "nom":"Jean Monnet",
                        "longitude":-1.46524928,
                        "latitude":47.16625637
                    },
                    {
                        "nom":"Jaunaie",
                        "longitude":-1.72719837,
                        "latitude":47.21919091
                    },
                    {
                        "nom":"Jaunaie",
                        "longitude":-1.72723249,
                        "latitude":47.21964009
                    },
                    {
                        "nom":"Jaunaie",
                        "longitude":-1.72797586,
                        "latitude":47.21898334
                    },
                    {
                        "nom":"Jaunaie",
                        "longitude":-1.72783718,
                        "latitude":47.21889815
                    },
                    {
                        "nom":"Jonelière",
                        "longitude":-1.54076407,
                        "latitude":47.25164252
                    },
                    {
                        "nom":"Jouetterie",
                        "longitude":-1.66249597,
                        "latitude":47.13147945
                    },
                    {
                        "nom":"Jouetterie",
                        "longitude":-1.66293090,
                        "latitude":47.13182463
                    },
                    {
                        "nom":"Jean Perrin",
                        "longitude":-1.56089755,
                        "latitude":47.18125199
                    },
                    {
                        "nom":"Jean Perrin",
                        "longitude":-1.56076577,
                        "latitude":47.18125645
                    },
                    {
                        "nom":"Jean Rostand",
                        "longitude":-1.59505437,
                        "latitude":47.24701224
                    },
                    {
                        "nom":"Jean Rostand",
                        "longitude":-1.59517970,
                        "latitude":47.24691790
                    },
                    {
                        "nom":"Justice",
                        "longitude":-1.63102461,
                        "latitude":47.20948167
                    },
                    {
                        "nom":"Justice",
                        "longitude":-1.63207268,
                        "latitude":47.20935550
                    },
                    {
                        "nom":"Juiverie",
                        "longitude":-1.62464411,
                        "latitude":47.21078204
                    },
                    {
                        "nom":"Juiverie",
                        "longitude":-1.62477596,
                        "latitude":47.21077751
                    },
                    {
                        "nom":"Jumelière",
                        "longitude":-1.48773150,
                        "latitude":47.29430114
                    },
                    {
                        "nom":"Jumelière",
                        "longitude":-1.48748666,
                        "latitude":47.29457943
                    },
                    {
                        "nom":"Jules Vallès",
                        "longitude":-1.57114591,
                        "latitude":47.17324888
                    },
                    {
                        "nom":"Jules Vallès",
                        "longitude":-1.57076374,
                        "latitude":47.17344198
                    },
                    {
                        "nom":"Jules Verne",
                        "longitude":-1.51714235,
                        "latitude":47.24874169
                    },
                    {
                        "nom":"Jules Verne",
                        "longitude":-1.51728077,
                        "latitude":47.24882713
                    },
                    {
                        "nom":"Janvraie",
                        "longitude":-1.61234453,
                        "latitude":47.19994542
                    },
                    {
                        "nom":"Janvraie",
                        "longitude":-1.61138865,
                        "latitude":47.19952783
                    },
                    {
                        "nom":"Keren",
                        "longitude":-1.53338216,
                        "latitude":47.24468532
                    },
                    {
                        "nom":"Keren",
                        "longitude":-1.53362652,
                        "latitude":47.24440692
                    },
                    {
                        "nom":"Koenig",
                        "longitude":-1.61563916,
                        "latitude":47.19803104
                    },
                    {
                        "nom":"Koenig",
                        "longitude":-1.61550734,
                        "latitude":47.19803556
                    },
                    {
                        "nom":"Koufra",
                        "longitude":-1.52980732,
                        "latitude":47.25011904
                    },
                    {
                        "nom":"Koufra",
                        "longitude":-1.53001925,
                        "latitude":47.24939142
                    },
                    {
                        "nom":"Koufra",
                        "longitude":-1.53048008,
                        "latitude":47.25027661
                    },
                    {
                        "nom":"Koufra",
                        "longitude":-1.53048008,
                        "latitude":47.25027661
                    },
                    {
                        "nom":"Lac de Beaulieu",
                        "longitude":-1.69310375,
                        "latitude":47.21939729
                    },
                    {
                        "nom":"Lac de Beaulieu",
                        "longitude":-1.69243768,
                        "latitude":47.21933051
                    },
                    {
                        "nom":"L'Arche",
                        "longitude":-1.70861048,
                        "latitude":47.21290905
                    },
                    {
                        "nom":"L'Arche",
                        "longitude":-1.70834000,
                        "latitude":47.21282847
                    },
                    {
                        "nom":"Ladoumègue",
                        "longitude":-1.48653946,
                        "latitude":47.29614184
                    },
                    {
                        "nom":"Ladoumègue",
                        "longitude":-1.48665224,
                        "latitude":47.29586793
                    },
                    {
                        "nom":"Largeau",
                        "longitude":-1.51014238,
                        "latitude":47.19556749
                    },
                    {
                        "nom":"Largeau",
                        "longitude":-1.51076272,
                        "latitude":47.19500639
                    },
                    {
                        "nom":"Largeau",
                        "longitude":-1.51079501,
                        "latitude":47.19545563
                    },
                    {
                        "nom":"Largeau",
                        "longitude":-1.50997182,
                        "latitude":47.19503280
                    },
                    {
                        "nom":"Les Ailes",
                        "longitude":-1.57567912,
                        "latitude":47.17021274
                    },
                    {
                        "nom":"Les Ailes",
                        "longitude":-1.57417029,
                        "latitude":47.17125475
                    },
                    {
                        "nom":"Lallié",
                        "longitude":-1.54474874,
                        "latitude":47.22818154
                    },
                    {
                        "nom":"Lallié",
                        "longitude":-1.54635903,
                        "latitude":47.22668625
                    },
                    {
                        "nom":"Lamartine",
                        "longitude":-1.57866714,
                        "latitude":47.21649651
                    },
                    {
                        "nom":"Lamartine",
                        "longitude":-1.57893087,
                        "latitude":47.21648755
                    },
                    {
                        "nom":"Launay",
                        "longitude":-1.63213722,
                        "latitude":47.26546719
                    },
                    {
                        "nom":"Launay",
                        "longitude":-1.63226920,
                        "latitude":47.26546265
                    },
                    {
                        "nom":"Lauriers",
                        "longitude":-1.60502977,
                        "latitude":47.20469941
                    },
                    {
                        "nom":"Lauriers",
                        "longitude":-1.60490455,
                        "latitude":47.20479377
                    },
                    {
                        "nom":"La Croix",
                        "longitude":-1.48670499,
                        "latitude":47.16527571
                    },
                    {
                        "nom":"La Croix",
                        "longitude":-1.48683673,
                        "latitude":47.16527134
                    },
                    {
                        "nom":"Les Bauches",
                        "longitude":-1.67736468,
                        "latitude":47.26336174
                    },
                    {
                        "nom":"Les Bauches",
                        "longitude":-1.67723271,
                        "latitude":47.26336633
                    },
                    {
                        "nom":"Lindbergh",
                        "longitude":-1.59751423,
                        "latitude":47.15460712
                    },
                    {
                        "nom":"Lindbergh",
                        "longitude":-1.59765914,
                        "latitude":47.15478231
                    },
                    {
                        "nom":"L'Ebaupin",
                        "longitude":-1.51000653,
                        "latitude":47.17161474
                    },
                    {
                        "nom":"L'Ebaupin",
                        "longitude":-1.50987477,
                        "latitude":47.17161914
                    },
                    {
                        "nom":"Liberté",
                        "longitude":-1.58909763,
                        "latitude":47.19830804
                    },
                    {
                        "nom":"Liberté",
                        "longitude":-1.58895923,
                        "latitude":47.19822269
                    },
                    {
                        "nom":"Le Cardo",
                        "longitude":-1.58423398,
                        "latitude":47.26179143
                    },
                    {
                        "nom":"Le Cardo",
                        "longitude":-1.58423398,
                        "latitude":47.26179143
                    },
                    {
                        "nom":"Le Cardo",
                        "longitude":-1.58461673,
                        "latitude":47.26159828
                    },
                    {
                        "nom":"Le Cardo",
                        "longitude":-1.58422739,
                        "latitude":47.26170158
                    },
                    {
                        "nom":"Le Cardo",
                        "longitude":-1.58399639,
                        "latitude":47.26215977
                    },
                    {
                        "nom":"Le Cardo",
                        "longitude":-1.58411519,
                        "latitude":47.26197560
                    },
                    {
                        "nom":"Le Corbusier",
                        "longitude":-1.56854374,
                        "latitude":47.19008987
                    },
                    {
                        "nom":"Le Corbusier",
                        "longitude":-1.56868209,
                        "latitude":47.19017524
                    },
                    {
                        "nom":"La Carrée",
                        "longitude":-1.54385650,
                        "latitude":47.16849749
                    },
                    {
                        "nom":"La Carrée",
                        "longitude":-1.54318474,
                        "latitude":47.16834000
                    },
                    {
                        "nom":"La Carrée",
                        "longitude":-1.54282855,
                        "latitude":47.16889240
                    },
                    {
                        "nom":"La Carrée",
                        "longitude":-1.54307903,
                        "latitude":47.16870383
                    },
                    {
                        "nom":"Lechat",
                        "longitude":-1.54389596,
                        "latitude":47.17633201
                    },
                    {
                        "nom":"Lechat",
                        "longitude":-1.54372512,
                        "latitude":47.17579736
                    },
                    {
                        "nom":"La Coulée",
                        "longitude":-1.57471905,
                        "latitude":47.26013301
                    },
                    {
                        "nom":"La Coulée",
                        "longitude":-1.57493697,
                        "latitude":47.25949515
                    },
                    {
                        "nom":"Le Chêne",
                        "longitude":-1.48445890,
                        "latitude":47.16526021
                    },
                    {
                        "nom":"Le Chêne",
                        "longitude":-1.48419540,
                        "latitude":47.16526896
                    },
                    {
                        "nom":"La Cogne",
                        "longitude":-1.55674105,
                        "latitude":47.28758066
                    },
                    {
                        "nom":"La Cogne",
                        "longitude":-1.55734888,
                        "latitude":47.28683964
                    },
                    {
                        "nom":"La Close",
                        "longitude":-1.57277848,
                        "latitude":47.24083446
                    },
                    {
                        "nom":"La Close",
                        "longitude":-1.57363572,
                        "latitude":47.24170606
                    },
                    {
                        "nom":"La Côte",
                        "longitude":-1.56044768,
                        "latitude":47.28214172
                    },
                    {
                        "nom":"La Côte",
                        "longitude":-1.56057971,
                        "latitude":47.28213726
                    },
                    {
                        "nom":"Landas",
                        "longitude":-1.51076745,
                        "latitude":47.16933769
                    },
                    {
                        "nom":"Landas",
                        "longitude":-1.51152569,
                        "latitude":47.16886202
                    },
                    {
                        "nom":"Landreau",
                        "longitude":-1.51608955,
                        "latitude":47.23229520
                    },
                    {
                        "nom":"Landreau",
                        "longitude":-1.51608955,
                        "latitude":47.23229520
                    },
                    {
                        "nom":"Landreau",
                        "longitude":-1.51589939,
                        "latitude":47.23149098
                    },
                    {
                        "nom":"Landreau",
                        "longitude":-1.51590587,
                        "latitude":47.23158083
                    },
                    {
                        "nom":"Landas",
                        "longitude":-1.55960992,
                        "latitude":47.17445040
                    },
                    {
                        "nom":"Landas",
                        "longitude":-1.55976130,
                        "latitude":47.17471548
                    },
                    {
                        "nom":"Lechat",
                        "longitude":-1.58093056,
                        "latitude":47.20408023
                    },
                    {
                        "nom":"Lechat",
                        "longitude":-1.58115476,
                        "latitude":47.20353220
                    },
                    {
                        "nom":"Leygues",
                        "longitude":-1.54149346,
                        "latitude":47.20334285
                    },
                    {
                        "nom":"Leygues",
                        "longitude":-1.54188245,
                        "latitude":47.20323969
                    },
                    {
                        "nom":"Levoyer",
                        "longitude":-1.58101777,
                        "latitude":47.19443993
                    },
                    {
                        "nom":"Levoyer",
                        "longitude":-1.58176916,
                        "latitude":47.19387396
                    },
                    {
                        "nom":"Les Faulx",
                        "longitude":-1.52258688,
                        "latitude":47.15741367
                    },
                    {
                        "nom":"Les Faulx",
                        "longitude":-1.52270565,
                        "latitude":47.15722956
                    },
                    {
                        "nom":"La ferme",
                        "longitude":-1.67955048,
                        "latitude":47.26427645
                    },
                    {
                        "nom":"La ferme",
                        "longitude":-1.68034232,
                        "latitude":47.26424888
                    },
                    {
                        "nom":"Laurier Fleuri",
                        "longitude":-1.48432323,
                        "latitude":47.18742039
                    },
                    {
                        "nom":"Laurier Fleuri",
                        "longitude":-1.48486326,
                        "latitude":47.18758260
                    },
                    {
                        "nom":"Laurier Fleuri",
                        "longitude":-1.48329448,
                        "latitude":47.18781477
                    },
                    {
                        "nom":"Laurier Fleuri",
                        "longitude":-1.48372835,
                        "latitude":47.18834076
                    },
                    {
                        "nom":"La Forêt",
                        "longitude":-1.63839344,
                        "latitude":47.15032034
                    },
                    {
                        "nom":"La Forêt",
                        "longitude":-1.63938699,
                        "latitude":47.14947537
                    },
                    {
                        "nom":"Lefeuvre",
                        "longitude":-1.49514244,
                        "latitude":47.26658659
                    },
                    {
                        "nom":"Lefeuvre",
                        "longitude":-1.49502977,
                        "latitude":47.26686052
                    },
                    {
                        "nom":"La Garde",
                        "longitude":-1.50849013,
                        "latitude":47.25533492
                    },
                    {
                        "nom":"La Garde",
                        "longitude":-1.50918227,
                        "latitude":47.25576216
                    },
                    {
                        "nom":"La Garde",
                        "longitude":-1.50881222,
                        "latitude":47.25613475
                    },
                    {
                        "nom":"La Garde",
                        "longitude":-1.50785986,
                        "latitude":47.25760751
                    },
                    {
                        "nom":"Le Goffic",
                        "longitude":-1.56312702,
                        "latitude":47.22999305
                    },
                    {
                        "nom":"Le Goffic",
                        "longitude":-1.56385203,
                        "latitude":47.23086918
                    },
                    {
                        "nom":"La Grille",
                        "longitude":-1.48119010,
                        "latitude":47.25092822
                    },
                    {
                        "nom":"La Grille",
                        "longitude":-1.48198826,
                        "latitude":47.25099185
                    },
                    {
                        "nom":"Lagathu",
                        "longitude":-1.53652635,
                        "latitude":47.18036290
                    },
                    {
                        "nom":"Lagathu",
                        "longitude":-1.53710547,
                        "latitude":47.18106396
                    },
                    {
                        "nom":"La Hache",
                        "longitude":-1.49893768,
                        "latitude":47.26231761
                    },
                    {
                        "nom":"La Hache",
                        "longitude":-1.49905677,
                        "latitude":47.26213352
                    },
                    {
                        "nom":"La Herdrie",
                        "longitude":-1.45269297,
                        "latitude":47.19783168
                    },
                    {
                        "nom":"Le Halleray",
                        "longitude":-1.45660738,
                        "latitude":47.26984144
                    },
                    {
                        "nom":"La Houssais",
                        "longitude":-1.55378624,
                        "latitude":47.17428702
                    },
                    {
                        "nom":"La Houssais",
                        "longitude":-1.55294998,
                        "latitude":47.17368480
                    },
                    {
                        "nom":"L'Hopitau",
                        "longitude":-1.55163557,
                        "latitude":47.27199148
                    },
                    {
                        "nom":"L'Hopitau",
                        "longitude":-1.55165518,
                        "latitude":47.27226101
                    },
                    {
                        "nom":"Libération",
                        "longitude":-1.67920590,
                        "latitude":47.17691697
                    },
                    {
                        "nom":"Libération",
                        "longitude":-1.67908088,
                        "latitude":47.17701141
                    },
                    {
                        "nom":"Lilas",
                        "longitude":-1.46890324,
                        "latitude":47.13776565
                    },
                    {
                        "nom":"Lilas",
                        "longitude":-1.46823208,
                        "latitude":47.13760772
                    },
                    {
                        "nom":"Lionnière",
                        "longitude":-1.69631901,
                        "latitude":47.21820393
                    },
                    {
                        "nom":"Lionnière",
                        "longitude":-1.69595730,
                        "latitude":47.21866696
                    },
                    {
                        "nom":"Lippmann",
                        "longitude":-1.52128541,
                        "latitude":47.23833587
                    },
                    {
                        "nom":"Lippmann",
                        "longitude":-1.52040731,
                        "latitude":47.23899570
                    },
                    {
                        "nom":"Littré",
                        "longitude":-1.58245805,
                        "latitude":47.21771868
                    },
                    {
                        "nom":"Littré",
                        "longitude":-1.58246463,
                        "latitude":47.21780852
                    },
                    {
                        "nom":"Léon jost",
                        "longitude":-1.56332179,
                        "latitude":47.23629112
                    },
                    {
                        "nom":"Léon jost",
                        "longitude":-1.56342095,
                        "latitude":47.23583744
                    },
                    {
                        "nom":"Léo Lagrange",
                        "longitude":-1.55480695,
                        "latitude":47.29737286
                    },
                    {
                        "nom":"Léo Lagrange",
                        "longitude":-1.55530903,
                        "latitude":47.29699568
                    },
                    {
                        "nom":"Les Lions",
                        "longitude":-1.61389637,
                        "latitude":47.24961074
                    },
                    {
                        "nom":"Les Lions",
                        "longitude":-1.61549961,
                        "latitude":47.24982601
                    },
                    {
                        "nom":"Le Clémois",
                        "longitude":-1.72257729,
                        "latitude":47.13738336
                    },
                    {
                        "nom":"Le Clémois",
                        "longitude":-1.72243883,
                        "latitude":47.13729817
                    },
                    {
                        "nom":"La Lorie",
                        "longitude":-1.65700442,
                        "latitude":47.23524557
                    },
                    {
                        "nom":"La Lorie",
                        "longitude":-1.65658859,
                        "latitude":47.23498976
                    },
                    {
                        "nom":"La Marche",
                        "longitude":-1.65970344,
                        "latitude":47.18110831
                    },
                    {
                        "nom":"La Marche",
                        "longitude":-1.65914283,
                        "latitude":47.18067739
                    },
                    {
                        "nom":"Lambert",
                        "longitude":-1.53967137,
                        "latitude":47.17089014
                    },
                    {
                        "nom":"Lambert",
                        "longitude":-1.53904511,
                        "latitude":47.17136155
                    },
                    {
                        "nom":"Louise Michel",
                        "longitude":-1.61528732,
                        "latitude":47.22011033
                    },
                    {
                        "nom":"Louise Michel",
                        "longitude":-1.61659274,
                        "latitude":47.21988542
                    },
                    {
                        "nom":"Lemoine",
                        "longitude":-1.54740057,
                        "latitude":47.23556768
                    },
                    {
                        "nom":"Lemoine",
                        "longitude":-1.54625901,
                        "latitude":47.23623659
                    },
                    {
                        "nom":"Lemoine",
                        "longitude":-1.54645761,
                        "latitude":47.23532924
                    },
                    {
                        "nom":"Lamoricière",
                        "longitude":-1.57243725,
                        "latitude":47.21445630
                    },
                    {
                        "nom":"Lamoricière",
                        "longitude":-1.57257567,
                        "latitude":47.21454167
                    },
                    {
                        "nom":"La Montagne",
                        "longitude":-1.68987486,
                        "latitude":47.18879466
                    },
                    {
                        "nom":"La Montagne",
                        "longitude":-1.68918890,
                        "latitude":47.18845833
                    },
                    {
                        "nom":"Le Nain",
                        "longitude":-1.60096082,
                        "latitude":47.22663531
                    },
                    {
                        "nom":"Le Nain",
                        "longitude":-1.60046633,
                        "latitude":47.22710255
                    },
                    {
                        "nom":"La Noue",
                        "longitude":-1.60769065,
                        "latitude":47.14038786
                    },
                    {
                        "nom":"La Noue",
                        "longitude":-1.60745376,
                        "latitude":47.14075627
                    },
                    {
                        "nom":"Linot",
                        "longitude":-1.52589009,
                        "latitude":47.26547109
                    },
                    {
                        "nom":"Linot",
                        "longitude":-1.52614758,
                        "latitude":47.26537241
                    },
                    {
                        "nom":"Loire",
                        "longitude":-1.48655882,
                        "latitude":47.24831869
                    },
                    {
                        "nom":"Loire",
                        "longitude":-1.48657166,
                        "latitude":47.24849839
                    },
                    {
                        "nom":"Longchamp",
                        "longitude":-1.58240304,
                        "latitude":47.23501359
                    },
                    {
                        "nom":"Longchamp",
                        "longitude":-1.58240304,
                        "latitude":47.23501359
                    },
                    {
                        "nom":"Bac du Pellerin",
                        "longitude":-1.75490668,
                        "latitude":47.20289715
                    },
                    {
                        "nom":"Le Pellerin",
                        "longitude":-1.75914657,
                        "latitude":47.20130524
                    },
                    {
                        "nom":"Le Pellerin",
                        "longitude":-1.75914657,
                        "latitude":47.20130524
                    },
                    {
                        "nom":"La Plée",
                        "longitude":-1.46341777,
                        "latitude":47.19441632
                    },
                    {
                        "nom":"La Plée",
                        "longitude":-1.46219951,
                        "latitude":47.19400619
                    },
                    {
                        "nom":"La Plée",
                        "longitude":-1.46411936,
                        "latitude":47.19313230
                    },
                    {
                        "nom":"La Plée",
                        "longitude":-1.46266938,
                        "latitude":47.19318013
                    },
                    {
                        "nom":"La Plume",
                        "longitude":-1.50547068,
                        "latitude":47.19491281
                    },
                    {
                        "nom":"La Plume",
                        "longitude":-1.50520704,
                        "latitude":47.19492160
                    },
                    {
                        "nom":"La Roche",
                        "longitude":-1.51931543,
                        "latitude":47.21651614
                    },
                    {
                        "nom":"La Roche",
                        "longitude":-1.51801616,
                        "latitude":47.21682980
                    },
                    {
                        "nom":"Les Roches",
                        "longitude":-1.57933738,
                        "latitude":47.26357897
                    },
                    {
                        "nom":"Les Roches",
                        "longitude":-1.58036688,
                        "latitude":47.26318375
                    },
                    {
                        "nom":"Les Roches",
                        "longitude":-1.58001044,
                        "latitude":47.26373625
                    },
                    {
                        "nom":"Lourneau",
                        "longitude":-1.48841095,
                        "latitude":47.20025387
                    },
                    {
                        "nom":"Lourneau",
                        "longitude":-1.48803472,
                        "latitude":47.20053655
                    },
                    {
                        "nom":"Lourneau",
                        "longitude":-1.48887405,
                        "latitude":47.19933786
                    },
                    {
                        "nom":"Lourneau",
                        "longitude":-1.48887405,
                        "latitude":47.19933786
                    },
                    {
                        "nom":"La Salle",
                        "longitude":-1.68915135,
                        "latitude":47.21431111
                    },
                    {
                        "nom":"La Salle",
                        "longitude":-1.68849886,
                        "latitude":47.21442398
                    },
                    {
                        "nom":"Les Salles",
                        "longitude":-1.53603503,
                        "latitude":47.25027033
                    },
                    {
                        "nom":"Les Salles",
                        "longitude":-1.53604805,
                        "latitude":47.25045002
                    },
                    {
                        "nom":"La Source",
                        "longitude":-1.55170423,
                        "latitude":47.30567326
                    },
                    {
                        "nom":"La Source",
                        "longitude":-1.55169769,
                        "latitude":47.30558341
                    },
                    {
                        "nom":"Léon Say",
                        "longitude":-1.56931311,
                        "latitude":47.22960347
                    },
                    {
                        "nom":"Léon Say",
                        "longitude":-1.56953754,
                        "latitude":47.22905546
                    },
                    {
                        "nom":"Le Tour",
                        "longitude":-1.68945617,
                        "latitude":47.15160805
                    },
                    {
                        "nom":"Le Tour",
                        "longitude":-1.68894964,
                        "latitude":47.15189600
                    },
                    {
                        "nom":"Lieu Unique",
                        "longitude":-1.54582722,
                        "latitude":47.21571612
                    },
                    {
                        "nom":"Lieu Unique",
                        "longitude":-1.54570187,
                        "latitude":47.21581040
                    },
                    {
                        "nom":"Lycée de Bouaye",
                        "longitude":-1.68144912,
                        "latitude":47.15576111
                    },
                    {
                        "nom":"Lycée de Bouaye",
                        "longitude":-1.68144912,
                        "latitude":47.15576111
                    },
                    {
                        "nom":"Lycée Appert",
                        "longitude":-1.61140346,
                        "latitude":47.25167763
                    },
                    {
                        "nom":"Lycée Appert",
                        "longitude":-1.61082924,
                        "latitude":47.25106681
                    },
                    {
                        "nom":"Lycée Rieffel",
                        "longitude":-1.64276506,
                        "latitude":47.25537342
                    },
                    {
                        "nom":"Lycée Rieffel",
                        "longitude":-1.64248078,
                        "latitude":47.25546429
                    },
                    {
                        "nom":"Lusançay",
                        "longitude":-1.58241386,
                        "latitude":47.20267876
                    },
                    {
                        "nom":"Lusançay",
                        "longitude":-1.58255227,
                        "latitude":47.20276412
                    },
                    {
                        "nom":"Maillardière",
                        "longitude":-1.63436175,
                        "latitude":47.27403732
                    },
                    {
                        "nom":"Maillardière",
                        "longitude":-1.63395239,
                        "latitude":47.27387127
                    },
                    {
                        "nom":"MIN",
                        "longitude":-1.55378787,
                        "latitude":47.20157737
                    },
                    {
                        "nom":"MIN",
                        "longitude":-1.55379440,
                        "latitude":47.20166722
                    },
                    {
                        "nom":"Mandon",
                        "longitude":-1.47106457,
                        "latitude":47.16075047
                    },
                    {
                        "nom":"Mandon",
                        "longitude":-1.47064382,
                        "latitude":47.16040413
                    },
                    {
                        "nom":"Gare de Mauves",
                        "longitude":-1.38969773,
                        "latitude":47.29318500
                    },
                    {
                        "nom":"Gare de Mauves",
                        "longitude":-1.38969773,
                        "latitude":47.29318500
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.55144998,
                        "latitude":47.18940718
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.55144998,
                        "latitude":47.18940718
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.55154262,
                        "latitude":47.18886365
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.55168747,
                        "latitude":47.18903889
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.55102195,
                        "latitude":47.18897129
                    },
                    {
                        "nom":"8 Mai",
                        "longitude":-1.55104153,
                        "latitude":47.18924083
                    },
                    {
                        "nom":"Mail",
                        "longitude":-1.61162839,
                        "latitude":47.26905318
                    },
                    {
                        "nom":"Mail",
                        "longitude":-1.61136441,
                        "latitude":47.26906222
                    },
                    {
                        "nom":"Maison d'Arrêt",
                        "longitude":-1.50333251,
                        "latitude":47.26640432
                    },
                    {
                        "nom":"Maison d'Arrêt",
                        "longitude":-1.50297526,
                        "latitude":47.26695659
                    },
                    {
                        "nom":"Matière",
                        "longitude":-1.44900684,
                        "latitude":47.27648529
                    },
                    {
                        "nom":"Matière",
                        "longitude":-1.44900684,
                        "latitude":47.27648529
                    },
                    {
                        "nom":"Centre de Mauves",
                        "longitude":-1.39409930,
                        "latitude":47.29745553
                    },
                    {
                        "nom":"Centre de Mauves",
                        "longitude":-1.39286040,
                        "latitude":47.29677512
                    },
                    {
                        "nom":"Mazères",
                        "longitude":-1.35047871,
                        "latitude":47.31371579
                    },
                    {
                        "nom":"Mazères",
                        "longitude":-1.35060463,
                        "latitude":47.31362172
                    },
                    {
                        "nom":"Maison Blanche",
                        "longitude":-1.60479723,
                        "latitude":47.22128017
                    },
                    {
                        "nom":"Maison Blanche",
                        "longitude":-1.60469183,
                        "latitude":47.22164405
                    },
                    {
                        "nom":"Mont-Blanc",
                        "longitude":-1.55581380,
                        "latitude":47.17674040
                    },
                    {
                        "nom":"Mont-Blanc",
                        "longitude":-1.55521489,
                        "latitude":47.17576990
                    },
                    {
                        "nom":"Mairie de Bouguenais",
                        "longitude":-1.61925355,
                        "latitude":47.17899206
                    },
                    {
                        "nom":"Mairie de Bouguenais",
                        "longitude":-1.61872647,
                        "latitude":47.17901016
                    },
                    {
                        "nom":"Moulin Brûlé",
                        "longitude":-1.65526034,
                        "latitude":47.25971537
                    },
                    {
                        "nom":"Moulin Brûlé",
                        "longitude":-1.65458039,
                        "latitude":47.25946869
                    },
                    {
                        "nom":"Moulin Boisseau",
                        "longitude":-1.48413143,
                        "latitude":47.28838631
                    },
                    {
                        "nom":"Moulin Boisseau",
                        "longitude":-1.48419923,
                        "latitude":47.28748346
                    },
                    {
                        "nom":"Moulin Cassé",
                        "longitude":-1.46913780,
                        "latitude":47.25627992
                    },
                    {
                        "nom":"Moulin Cassé",
                        "longitude":-1.46849712,
                        "latitude":47.25657124
                    },
                    {
                        "nom":"Marché Commun",
                        "longitude":-1.49993852,
                        "latitude":47.25967252
                    },
                    {
                        "nom":"Marché Commun",
                        "longitude":-1.49972615,
                        "latitude":47.26040008
                    },
                    {
                        "nom":"Mairie de Chantenay",
                        "longitude":-1.58783853,
                        "latitude":47.20636707
                    },
                    {
                        "nom":"Mairie de Chantenay",
                        "longitude":-1.58750231,
                        "latitude":47.20718914
                    },
                    {
                        "nom":"Mairie de Chantenay",
                        "longitude":-1.58814175,
                        "latitude":47.20689716
                    },
                    {
                        "nom":"Mairie de Couëron",
                        "longitude":-1.72720030,
                        "latitude":47.21225494
                    },
                    {
                        "nom":"Mairie de Couëron",
                        "longitude":-1.72705481,
                        "latitude":47.21207991
                    },
                    {
                        "nom":"Madame Curie",
                        "longitude":-1.55433651,
                        "latitude":47.18912957
                    },
                    {
                        "nom":"Madame Curie",
                        "longitude":-1.55552923,
                        "latitude":47.18917934
                    },
                    {
                        "nom":"Maison des Arts",
                        "longitude":-1.61753102,
                        "latitude":47.21111642
                    },
                    {
                        "nom":"Maison des Arts",
                        "longitude":-1.61745893,
                        "latitude":47.21192952
                    },
                    {
                        "nom":"Maison des Arts",
                        "longitude":-1.61819026,
                        "latitude":47.21109379
                    },
                    {
                        "nom":"Mandel",
                        "longitude":-1.53707786,
                        "latitude":47.20259073
                    },
                    {
                        "nom":"Mandel",
                        "longitude":-1.53720319,
                        "latitude":47.20249645
                    },
                    {
                        "nom":"Moulin des Landes",
                        "longitude":-1.48145788,
                        "latitude":47.26208697
                    },
                    {
                        "nom":"Moulin des Landes",
                        "longitude":-1.48145788,
                        "latitude":47.26208697
                    },
                    {
                        "nom":"Madeleine",
                        "longitude":-1.48349616,
                        "latitude":47.27579884
                    },
                    {
                        "nom":"Madeleine",
                        "longitude":-1.48361533,
                        "latitude":47.27561477
                    },
                    {
                        "nom":"Mairie de Doulon",
                        "longitude":-1.51860295,
                        "latitude":47.22680732
                    },
                    {
                        "nom":"Mairie de Doulon",
                        "longitude":-1.51860295,
                        "latitude":47.22680732
                    },
                    {
                        "nom":"Mairie de Doulon",
                        "longitude":-1.51874780,
                        "latitude":47.22698261
                    },
                    {
                        "nom":"Mairie de Doulon",
                        "longitude":-1.51964515,
                        "latitude":47.22659233
                    },
                    {
                        "nom":"Moulin de Porterie",
                        "longitude":-1.51234680,
                        "latitude":47.27583068
                    },
                    {
                        "nom":"Moulin de Porterie",
                        "longitude":-1.51221479,
                        "latitude":47.27583508
                    },
                    {
                        "nom":"Moulin de Porterie",
                        "longitude":-1.51338998,
                        "latitude":47.27561575
                    },
                    {
                        "nom":"Moulin de Porterie",
                        "longitude":-1.51338351,
                        "latitude":47.27552590
                    },
                    {
                        "nom":"Madoire",
                        "longitude":-1.63898680,
                        "latitude":47.26505118
                    },
                    {
                        "nom":"Madoire",
                        "longitude":-1.63806295,
                        "latitude":47.26508301
                    },
                    {
                        "nom":"Mondésir",
                        "longitude":-1.56884285,
                        "latitude":47.21773053
                    },
                    {
                        "nom":"Mondésir",
                        "longitude":-1.56910659,
                        "latitude":47.21772159
                    },
                    {
                        "nom":"Médiathèque",
                        "longitude":-1.56082889,
                        "latitude":47.21115673
                    },
                    {
                        "nom":"Médiathèque",
                        "longitude":-1.56082889,
                        "latitude":47.21115673
                    },
                    {
                        "nom":"Méditerranée",
                        "longitude":-1.48501031,
                        "latitude":47.27106552
                    },
                    {
                        "nom":"Méditerranée",
                        "longitude":-1.48575094,
                        "latitude":47.27032049
                    },
                    {
                        "nom":"Méliès",
                        "longitude":-1.55630487,
                        "latitude":47.18167753
                    },
                    {
                        "nom":"Méliès",
                        "longitude":-1.55616656,
                        "latitude":47.18159213
                    },
                    {
                        "nom":"Métairie",
                        "longitude":-1.69930369,
                        "latitude":47.21395599
                    },
                    {
                        "nom":"Métairie",
                        "longitude":-1.69836721,
                        "latitude":47.21380863
                    },
                    {
                        "nom":"Marie Galante",
                        "longitude":-1.45104526,
                        "latitude":47.26417032
                    },
                    {
                        "nom":"Marie Galante",
                        "longitude":-1.45077491,
                        "latitude":47.26408914
                    },
                    {
                        "nom":"Monge-Chauvinière",
                        "longitude":-1.56809047,
                        "latitude":47.25270197
                    },
                    {
                        "nom":"Monge-Chauvinière",
                        "longitude":-1.56942314,
                        "latitude":47.25283697
                    },
                    {
                        "nom":"Mongendrière",
                        "longitude":-1.55433459,
                        "latitude":47.29270544
                    },
                    {
                        "nom":"Mongendrière",
                        "longitude":-1.55457252,
                        "latitude":47.29233716
                    },
                    {
                        "nom":"Mangin",
                        "longitude":-1.54507404,
                        "latitude":47.20169115
                    },
                    {
                        "nom":"Mangin",
                        "longitude":-1.54507404,
                        "latitude":47.20169115
                    },
                    {
                        "nom":"Mangin",
                        "longitude":-1.54361087,
                        "latitude":47.20156030
                    },
                    {
                        "nom":"Mangin",
                        "longitude":-1.54362390,
                        "latitude":47.20173999
                    },
                    {
                        "nom":"Mangin",
                        "longitude":-1.54595122,
                        "latitude":47.20103112
                    },
                    {
                        "nom":"Mangin",
                        "longitude":-1.54553618,
                        "latitude":47.20077491
                    },
                    {
                        "nom":"Magnoliers",
                        "longitude":-1.60568888,
                        "latitude":47.22620349
                    },
                    {
                        "nom":"Magnoliers",
                        "longitude":-1.60506255,
                        "latitude":47.22667526
                    },
                    {
                        "nom":"Mignonnerie",
                        "longitude":-1.48305182,
                        "latitude":47.24366163
                    },
                    {
                        "nom":"Mignonnerie",
                        "longitude":-1.48178738,
                        "latitude":47.24262277
                    },
                    {
                        "nom":"Mainguais",
                        "longitude":-1.48557518,
                        "latitude":47.28266468
                    },
                    {
                        "nom":"Mainguais",
                        "longitude":-1.48537888,
                        "latitude":47.28177057
                    },
                    {
                        "nom":"Marcel Hatet",
                        "longitude":-1.53070863,
                        "latitude":47.22234888
                    },
                    {
                        "nom":"Marcel Hatet",
                        "longitude":-1.53107180,
                        "latitude":47.22188637
                    },
                    {
                        "nom":"Mahaudières",
                        "longitude":-1.56039923,
                        "latitude":47.18892461
                    },
                    {
                        "nom":"Mahaudières",
                        "longitude":-1.56092642,
                        "latitude":47.18890678
                    },
                    {
                        "nom":"Moulin à l'Huile",
                        "longitude":-1.56288910,
                        "latitude":47.17776199
                    },
                    {
                        "nom":"Moulin à l'Huile",
                        "longitude":-1.56269844,
                        "latitude":47.17695783
                    },
                    {
                        "nom":"Moulin à l'Huile",
                        "longitude":-1.56219100,
                        "latitude":47.17724522
                    },
                    {
                        "nom":"Moulin à l'Huile",
                        "longitude":-1.56214520,
                        "latitude":47.17661629
                    },
                    {
                        "nom":"Minais",
                        "longitude":-1.46445709,
                        "latitude":47.25742494
                    },
                    {
                        "nom":"Minais",
                        "longitude":-1.46641753,
                        "latitude":47.25709013
                    },
                    {
                        "nom":"Michelet",
                        "longitude":-1.55649603,
                        "latitude":47.23517083
                    },
                    {
                        "nom":"Michelet",
                        "longitude":-1.55649603,
                        "latitude":47.23517083
                    },
                    {
                        "nom":"Michelet",
                        "longitude":-1.55662794,
                        "latitude":47.23516637
                    },
                    {
                        "nom":"Michelet",
                        "longitude":-1.55724171,
                        "latitude":47.23451518
                    },
                    {
                        "nom":"Michelet",
                        "longitude":-1.55698443,
                        "latitude":47.23461394
                    },
                    {
                        "nom":"Mimosas",
                        "longitude":-1.58748790,
                        "latitude":47.24123544
                    },
                    {
                        "nom":"Mimosas",
                        "longitude":-1.58762642,
                        "latitude":47.24132080
                    },
                    {
                        "nom":"Mairie d'Indre",
                        "longitude":-1.66830435,
                        "latitude":47.20071585
                    },
                    {
                        "nom":"Mairie d'Indre",
                        "longitude":-1.66907509,
                        "latitude":47.20041883
                    },
                    {
                        "nom":"Mairie d'Indre",
                        "longitude":-1.66864601,
                        "latitude":47.19998338
                    },
                    {
                        "nom":"Mitrie",
                        "longitude":-1.52588742,
                        "latitude":47.22881508
                    },
                    {
                        "nom":"Mitrie",
                        "longitude":-1.52650798,
                        "latitude":47.22825389
                    },
                    {
                        "nom":"Mitrie",
                        "longitude":-1.52541388,
                        "latitude":47.22775017
                    },
                    {
                        "nom":"Mitrie",
                        "longitude":-1.52681720,
                        "latitude":47.22887398
                    },
                    {
                        "nom":"Malakoff",
                        "longitude":-1.52113569,
                        "latitude":47.21609496
                    },
                    {
                        "nom":"Malakoff",
                        "longitude":-1.52126108,
                        "latitude":47.21600070
                    },
                    {
                        "nom":"Magellan",
                        "longitude":-1.46442750,
                        "latitude":47.18816871
                    },
                    {
                        "nom":"Magellan",
                        "longitude":-1.46428932,
                        "latitude":47.18808321
                    },
                    {
                        "nom":"Morlachère",
                        "longitude":-1.48262804,
                        "latitude":47.16180846
                    },
                    {
                        "nom":"Morlachère",
                        "longitude":-1.48370116,
                        "latitude":47.16204305
                    },
                    {
                        "nom":"Maladrie",
                        "longitude":-1.49427894,
                        "latitude":47.18808041
                    },
                    {
                        "nom":"Maladrie",
                        "longitude":-1.49627525,
                        "latitude":47.18828419
                    },
                    {
                        "nom":"Moulin Hérel",
                        "longitude":-1.63757971,
                        "latitude":47.21943384
                    },
                    {
                        "nom":"Moulin Hérel",
                        "longitude":-1.63759306,
                        "latitude":47.21961352
                    },
                    {
                        "nom":"Moulin",
                        "longitude":-1.50370123,
                        "latitude":47.18866730
                    },
                    {
                        "nom":"Moulin",
                        "longitude":-1.50517039,
                        "latitude":47.18888850
                    },
                    {
                        "nom":"Mellinet",
                        "longitude":-1.57667075,
                        "latitude":47.21089003
                    },
                    {
                        "nom":"Mellinet",
                        "longitude":-1.57607719,
                        "latitude":47.21181087
                    },
                    {
                        "nom":"Mellinet",
                        "longitude":-1.57779128,
                        "latitude":47.21175266
                    },
                    {
                        "nom":"Mellinet",
                        "longitude":-1.57563565,
                        "latitude":47.21119539
                    },
                    {
                        "nom":"Moulinets",
                        "longitude":-1.62919879,
                        "latitude":47.20981473
                    },
                    {
                        "nom":"Moulinets",
                        "longitude":-1.62921210,
                        "latitude":47.20999442
                    },
                    {
                        "nom":"Mulonnière",
                        "longitude":-1.55000977,
                        "latitude":47.26601192
                    },
                    {
                        "nom":"Mulonnière",
                        "longitude":-1.54997711,
                        "latitude":47.26556269
                    },
                    {
                        "nom":"Malraux",
                        "longitude":-1.51741841,
                        "latitude":47.25440647
                    },
                    {
                        "nom":"Malraux",
                        "longitude":-1.51767585,
                        "latitude":47.25430780
                    },
                    {
                        "nom":"Mermoz",
                        "longitude":-1.58459890,
                        "latitude":47.23971251
                    },
                    {
                        "nom":"Mermoz",
                        "longitude":-1.58496831,
                        "latitude":47.23933968
                    },
                    {
                        "nom":"Mermoz",
                        "longitude":-1.51552255,
                        "latitude":47.19872011
                    },
                    {
                        "nom":"Mermoz",
                        "longitude":-1.51552901,
                        "latitude":47.19880996
                    },
                    {
                        "nom":"Moulin Neuf",
                        "longitude":-1.51633641,
                        "latitude":47.23021548
                    },
                    {
                        "nom":"Moulin Neuf",
                        "longitude":-1.51644242,
                        "latitude":47.22985168
                    },
                    {
                        "nom":"Manufacture",
                        "longitude":-1.53731925,
                        "latitude":47.21870439
                    },
                    {
                        "nom":"Manufacture",
                        "longitude":-1.53732576,
                        "latitude":47.21879424
                    },
                    {
                        "nom":"Manufacture",
                        "longitude":-1.53654753,
                        "latitude":47.21900053
                    },
                    {
                        "nom":"Manufacture",
                        "longitude":-1.53759601,
                        "latitude":47.21887522
                    },
                    {
                        "nom":"Moulinier",
                        "longitude":-1.66556362,
                        "latitude":47.13526403
                    },
                    {
                        "nom":"Moulinier",
                        "longitude":-1.66580556,
                        "latitude":47.13549882
                    },
                    {
                        "nom":"Minoterie",
                        "longitude":-1.72160815,
                        "latitude":47.21695584
                    },
                    {
                        "nom":"Minoterie",
                        "longitude":-1.72160133,
                        "latitude":47.21686600
                    },
                    {
                        "nom":"Malnoue",
                        "longitude":-1.50514128,
                        "latitude":47.20320971
                    },
                    {
                        "nom":"Malnoue",
                        "longitude":-1.50549809,
                        "latitude":47.20265743
                    },
                    {
                        "nom":"Ménétrier",
                        "longitude":-1.52387404,
                        "latitude":47.24122134
                    },
                    {
                        "nom":"Ménétrier",
                        "longitude":-1.52374860,
                        "latitude":47.24131561
                    },
                    {
                        "nom":"Maison Neuve",
                        "longitude":-1.46727465,
                        "latitude":47.27471369
                    },
                    {
                        "nom":"Maison Neuve",
                        "longitude":-1.46727465,
                        "latitude":47.27471369
                    },
                    {
                        "nom":"Mouettes",
                        "longitude":-1.74291146,
                        "latitude":47.21025853
                    },
                    {
                        "nom":"Mouettes",
                        "longitude":-1.74227284,
                        "latitude":47.21055137
                    },
                    {
                        "nom":"Moinard",
                        "longitude":-1.72928032,
                        "latitude":47.21353271
                    },
                    {
                        "nom":"Moinard",
                        "longitude":-1.72939851,
                        "latitude":47.21334839
                    },
                    {
                        "nom":"Moinet",
                        "longitude":-1.49257017,
                        "latitude":47.26577139
                    },
                    {
                        "nom":"Moinet",
                        "longitude":-1.49202289,
                        "latitude":47.26551937
                    },
                    {
                        "nom":"Moinet",
                        "longitude":-1.49236094,
                        "latitude":47.26469759
                    },
                    {
                        "nom":"Moinet",
                        "longitude":-1.49211627,
                        "latitude":47.26497590
                    },
                    {
                        "nom":"Morrhonnière-Petit Port",
                        "longitude":-1.55643273,
                        "latitude":47.24156766
                    },
                    {
                        "nom":"Morrhonnière-Petit Port",
                        "longitude":-1.55643273,
                        "latitude":47.24156766
                    },
                    {
                        "nom":"Morrhonnière-Petit Port",
                        "longitude":-1.55630080,
                        "latitude":47.24157211
                    },
                    {
                        "nom":"Morrhonnière-Petit Port",
                        "longitude":-1.55731697,
                        "latitude":47.24099740
                    },
                    {
                        "nom":"Motte rouge",
                        "longitude":-1.55410676,
                        "latitude":47.22777599
                    },
                    {
                        "nom":"Motte rouge",
                        "longitude":-1.55411329,
                        "latitude":47.22786584
                    },
                    {
                        "nom":"Moutonnerie",
                        "longitude":-1.53315129,
                        "latitude":47.21956494
                    },
                    {
                        "nom":"Moutonnerie",
                        "longitude":-1.53315129,
                        "latitude":47.21956494
                    },
                    {
                        "nom":"Moutonnerie",
                        "longitude":-1.53354692,
                        "latitude":47.21955165
                    },
                    {
                        "nom":"Moutonnerie",
                        "longitude":-1.53355342,
                        "latitude":47.21964150
                    },
                    {
                        "nom":"Mairie d'Orvault",
                        "longitude":-1.62292189,
                        "latitude":47.27145824
                    },
                    {
                        "nom":"Mairie d'Orvault",
                        "longitude":-1.62369389,
                        "latitude":47.27116153
                    },
                    {
                        "nom":"Mairie d'Orvault",
                        "longitude":-1.62326462,
                        "latitude":47.27072592
                    },
                    {
                        "nom":"Monzie",
                        "longitude":-1.53991144,
                        "latitude":47.20339609
                    },
                    {
                        "nom":"Monzie",
                        "longitude":-1.53963475,
                        "latitude":47.20322527
                    },
                    {
                        "nom":"Marcel Paul",
                        "longitude":-1.61626953,
                        "latitude":47.24592664
                    },
                    {
                        "nom":"Marcel Paul",
                        "longitude":-1.61626953,
                        "latitude":47.24592664
                    },
                    {
                        "nom":"Marcel Paul",
                        "longitude":-1.61571523,
                        "latitude":47.24558537
                    },
                    {
                        "nom":"Marcel Paul",
                        "longitude":-1.61716648,
                        "latitude":47.24678758
                    },
                    {
                        "nom":"Marcel Paul",
                        "longitude":-1.61572851,
                        "latitude":47.24576505
                    },
                    {
                        "nom":"Millepertuis",
                        "longitude":-1.67942003,
                        "latitude":47.22203672
                    },
                    {
                        "nom":"Millepertuis",
                        "longitude":-1.67967702,
                        "latitude":47.22193769
                    },
                    {
                        "nom":"Melpomène",
                        "longitude":-1.50003396,
                        "latitude":47.27759170
                    },
                    {
                        "nom":"Melpomène",
                        "longitude":-1.50039132,
                        "latitude":47.27703944
                    },
                    {
                        "nom":"Maison Poitard",
                        "longitude":-1.64974194,
                        "latitude":47.14668519
                    },
                    {
                        "nom":"Maison Poitard",
                        "longitude":-1.64831344,
                        "latitude":47.14700488
                    },
                    {
                        "nom":"Marquis de Dion",
                        "longitude":-1.48779883,
                        "latitude":47.29709079
                    },
                    {
                        "nom":"Marquis de Dion",
                        "longitude":-1.48793090,
                        "latitude":47.29708641
                    },
                    {
                        "nom":"Mocquelière",
                        "longitude":-1.61902651,
                        "latitude":47.23313228
                    },
                    {
                        "nom":"Mocquelière",
                        "longitude":-1.61864409,
                        "latitude":47.23332554
                    },
                    {
                        "nom":"Maraîchers",
                        "longitude":-1.51069199,
                        "latitude":47.18483142
                    },
                    {
                        "nom":"Maraîchers",
                        "longitude":-1.51082379,
                        "latitude":47.18482702
                    },
                    {
                        "nom":"Maison Radieuse",
                        "longitude":-1.56694643,
                        "latitude":47.18829761
                    },
                    {
                        "nom":"Maison Radieuse",
                        "longitude":-1.56641220,
                        "latitude":47.18785637
                    },
                    {
                        "nom":"Mairie de Rezé",
                        "longitude":-1.57022370,
                        "latitude":47.19138392
                    },
                    {
                        "nom":"Mairie de Rezé",
                        "longitude":-1.56982174,
                        "latitude":47.19130749
                    },
                    {
                        "nom":"Morges",
                        "longitude":-1.47215531,
                        "latitude":47.16494737
                    },
                    {
                        "nom":"Morges",
                        "longitude":-1.47202356,
                        "latitude":47.16495172
                    },
                    {
                        "nom":"Maurienne",
                        "longitude":-1.44246145,
                        "latitude":47.27561915
                    },
                    {
                        "nom":"Maurienne",
                        "longitude":-1.44245510,
                        "latitude":47.27552930
                    },
                    {
                        "nom":"Moulin Rothard",
                        "longitude":-1.74035914,
                        "latitude":47.19584634
                    },
                    {
                        "nom":"Moulin Rothard",
                        "longitude":-1.74115678,
                        "latitude":47.19590819
                    },
                    {
                        "nom":"Marrière",
                        "longitude":-1.52466349,
                        "latitude":47.23750227
                    },
                    {
                        "nom":"Marrière",
                        "longitude":-1.52426773,
                        "latitude":47.23751552
                    },
                    {
                        "nom":"Marterie",
                        "longitude":-1.53136943,
                        "latitude":47.17846469
                    },
                    {
                        "nom":"Marterie",
                        "longitude":-1.53182319,
                        "latitude":47.17926005
                    },
                    {
                        "nom":"Marterie",
                        "longitude":-1.53057879,
                        "latitude":47.17849125
                    },
                    {
                        "nom":"Mairie de St-Herblain",
                        "longitude":-1.64823952,
                        "latitude":47.21167984
                    },
                    {
                        "nom":"Mairie de St-Herblain",
                        "longitude":-1.64871337,
                        "latitude":47.21094288
                    },
                    {
                        "nom":"Mairie de St-Herblain",
                        "longitude":-1.64885191,
                        "latitude":47.21102816
                    },
                    {
                        "nom":"Mairie de St-Herblain",
                        "longitude":-1.64902391,
                        "latitude":47.21156264
                    },
                    {
                        "nom":"Mairie de St-Herblain",
                        "longitude":-1.64876022,
                        "latitude":47.21157177
                    },
                    {
                        "nom":"Mairie de St-Herblain",
                        "longitude":-1.64823952,
                        "latitude":47.21167984
                    },
                    {
                        "nom":"Marseillaise",
                        "longitude":-1.59094994,
                        "latitude":47.20734188
                    },
                    {
                        "nom":"Marseillaise",
                        "longitude":-1.59188602,
                        "latitude":47.20749011
                    },
                    {
                        "nom":"Mairie des Sorinières",
                        "longitude":-1.53229012,
                        "latitude":47.14916207
                    },
                    {
                        "nom":"Mairie des Sorinières",
                        "longitude":-1.53271767,
                        "latitude":47.14959804
                    },
                    {
                        "nom":"Mississipi",
                        "longitude":-1.60914177,
                        "latitude":47.23716387
                    },
                    {
                        "nom":"Mississipi",
                        "longitude":-1.60926705,
                        "latitude":47.23706951
                    },
                    {
                        "nom":"Monteil",
                        "longitude":-1.54953382,
                        "latitude":47.21396999
                    },
                    {
                        "nom":"Monteil",
                        "longitude":-1.54863690,
                        "latitude":47.21436050
                    },
                    {
                        "nom":"Métairie",
                        "longitude":-1.51028673,
                        "latitude":47.20492938
                    },
                    {
                        "nom":"Métairie",
                        "longitude":-1.51149914,
                        "latitude":47.20524916
                    },
                    {
                        "nom":"Martellière",
                        "longitude":-1.52492749,
                        "latitude":47.19903572
                    },
                    {
                        "nom":"Martellière",
                        "longitude":-1.52639054,
                        "latitude":47.19916680
                    },
                    {
                        "nom":"Mortrais",
                        "longitude":-1.61667784,
                        "latitude":47.12800978
                    },
                    {
                        "nom":"Mortrais",
                        "longitude":-1.61644107,
                        "latitude":47.12837820
                    },
                    {
                        "nom":"Monty",
                        "longitude":-1.43296748,
                        "latitude":47.27421861
                    },
                    {
                        "nom":"Mortier Vannerie",
                        "longitude":-1.49939264,
                        "latitude":47.18016469
                    },
                    {
                        "nom":"Mortier Vannerie",
                        "longitude":-1.49937977,
                        "latitude":47.17998499
                    },
                    {
                        "nom":"Mortier Vannerie",
                        "longitude":-1.49937977,
                        "latitude":47.17998499
                    },
                    {
                        "nom":"Mortier Vannerie",
                        "longitude":-1.49939264,
                        "latitude":47.18016469
                    },
                    {
                        "nom":"Monteverdi",
                        "longitude":-1.44632051,
                        "latitude":47.26657684
                    },
                    {
                        "nom":"Monteverdi",
                        "longitude":-1.44632051,
                        "latitude":47.26657684
                    },
                    {
                        "nom":"Mauvoisins",
                        "longitude":-1.52546555,
                        "latitude":47.19001113
                    },
                    {
                        "nom":"Mauvoisins",
                        "longitude":-1.52559735,
                        "latitude":47.19000671
                    },
                    {
                        "nom":"Nantes-Atlantique",
                        "longitude":-1.60091305,
                        "latitude":47.15773346
                    },
                    {
                        "nom":"Nouveau Bêle",
                        "longitude":-1.50190990,
                        "latitude":47.26870322
                    },
                    {
                        "nom":"Nouveau Bêle",
                        "longitude":-1.50192926,
                        "latitude":47.26897277
                    },
                    {
                        "nom":"Noé Blanche",
                        "longitude":-1.50612485,
                        "latitude":47.25739514
                    },
                    {
                        "nom":"Noé Blanche",
                        "longitude":-1.50435472,
                        "latitude":47.25853485
                    },
                    {
                        "nom":"Noé Cottée",
                        "longitude":-1.47858966,
                        "latitude":47.19409498
                    },
                    {
                        "nom":"Noé Cottée",
                        "longitude":-1.47927437,
                        "latitude":47.19443256
                    },
                    {
                        "nom":"Naudières",
                        "longitude":-1.53666522,
                        "latitude":47.17315290
                    },
                    {
                        "nom":"Naudières",
                        "longitude":-1.53603890,
                        "latitude":47.17362430
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59217451,
                        "latitude":47.17721695
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59270817,
                        "latitude":47.17728881
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59270817,
                        "latitude":47.17728881
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59269498,
                        "latitude":47.17710912
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59256322,
                        "latitude":47.17711362
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59256322,
                        "latitude":47.17711362
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59230628,
                        "latitude":47.17721246
                    },
                    {
                        "nom":"Neustrie",
                        "longitude":-1.59217451,
                        "latitude":47.17721695
                    },
                    {
                        "nom":"Niagara",
                        "longitude":-1.49333602,
                        "latitude":47.30600333
                    },
                    {
                        "nom":"Niagara",
                        "longitude":-1.49477615,
                        "latitude":47.30577542
                    },
                    {
                        "nom":"Nil",
                        "longitude":-1.60794317,
                        "latitude":47.24242889
                    },
                    {
                        "nom":"Nil",
                        "longitude":-1.60748775,
                        "latitude":47.24163385
                    },
                    {
                        "nom":"Nil",
                        "longitude":-1.60856303,
                        "latitude":47.24186727
                    },
                    {
                        "nom":"Noé Lambert",
                        "longitude":-1.52849088,
                        "latitude":47.23007878
                    },
                    {
                        "nom":"Noé Lambert",
                        "longitude":-1.52820760,
                        "latitude":47.22981809
                    },
                    {
                        "nom":"Noé Allais",
                        "longitude":-1.68802196,
                        "latitude":47.22038554
                    },
                    {
                        "nom":"Noé Allais",
                        "longitude":-1.68868129,
                        "latitude":47.22036251
                    },
                    {
                        "nom":"Noë St-Jean",
                        "longitude":-1.73513717,
                        "latitude":47.21927100
                    },
                    {
                        "nom":"Noë St-Jean",
                        "longitude":-1.73499847,
                        "latitude":47.21918582
                    },
                    {
                        "nom":"North'House",
                        "longitude":-1.57118445,
                        "latitude":47.19549447
                    },
                    {
                        "nom":"North'House",
                        "longitude":-1.57187633,
                        "latitude":47.19592134
                    },
                    {
                        "nom":"Noé Nozou 1",
                        "longitude":-1.63722119,
                        "latitude":47.13024744
                    },
                    {
                        "nom":"Noé Nozou 1",
                        "longitude":-1.63703288,
                        "latitude":47.13019990
                    },
                    {
                        "nom":"Noëlles",
                        "longitude":-1.60434756,
                        "latitude":47.24030012
                    },
                    {
                        "nom":"Noëlles",
                        "longitude":-1.60474332,
                        "latitude":47.24028659
                    },
                    {
                        "nom":"Noé Nozou 2",
                        "longitude":-1.64163529,
                        "latitude":47.13210355
                    },
                    {
                        "nom":"Noé Nozou 2",
                        "longitude":-1.64226624,
                        "latitude":47.13242401
                    },
                    {
                        "nom":"Noé Rocard",
                        "longitude":-1.49028471,
                        "latitude":47.16362569
                    },
                    {
                        "nom":"Noé Rocard",
                        "longitude":-1.49066067,
                        "latitude":47.16334300
                    },
                    {
                        "nom":"Néruda",
                        "longitude":-1.61365099,
                        "latitude":47.21764447
                    },
                    {
                        "nom":"Néruda",
                        "longitude":-1.61365099,
                        "latitude":47.21764447
                    },
                    {
                        "nom":"Néruda",
                        "longitude":-1.61297841,
                        "latitude":47.21748738
                    },
                    {
                        "nom":"Néruda",
                        "longitude":-1.61296514,
                        "latitude":47.21730769
                    },
                    {
                        "nom":"Néruda",
                        "longitude":-1.61140267,
                        "latitude":47.21763144
                    },
                    {
                        "nom":"Néruda",
                        "longitude":-1.61102034,
                        "latitude":47.21782468
                    },
                    {
                        "nom":"Ouche Blanche",
                        "longitude":-1.56407418,
                        "latitude":47.17952322
                    },
                    {
                        "nom":"Ouche Blanche",
                        "longitude":-1.56437046,
                        "latitude":47.17996352
                    },
                    {
                        "nom":"Océane",
                        "longitude":-1.63593842,
                        "latitude":47.22759682
                    },
                    {
                        "nom":"Océane",
                        "longitude":-1.63632074,
                        "latitude":47.22740350
                    },
                    {
                        "nom":"Couëron-Océan",
                        "longitude":-1.73919594,
                        "latitude":47.21354273
                    },
                    {
                        "nom":"Couëron-Océan",
                        "longitude":-1.73815488,
                        "latitude":47.21375970
                    },
                    {
                        "nom":"Couëron-Océan",
                        "longitude":-1.73802303,
                        "latitude":47.21376436
                    },
                    {
                        "nom":"Couëron-Océan",
                        "longitude":-1.73918225,
                        "latitude":47.21336306
                    },
                    {
                        "nom":"Ouche Cormier",
                        "longitude":-1.62400648,
                        "latitude":47.26466582
                    },
                    {
                        "nom":"Ouche Cormier",
                        "longitude":-1.62361055,
                        "latitude":47.26467941
                    },
                    {
                        "nom":"Ouche des landes",
                        "longitude":-1.48289116,
                        "latitude":47.19512326
                    },
                    {
                        "nom":"Ouche des landes",
                        "longitude":-1.48181737,
                        "latitude":47.19488867
                    },
                    {
                        "nom":"Ordronneau",
                        "longitude":-1.57437473,
                        "latitude":47.19394511
                    },
                    {
                        "nom":"Ordronneau",
                        "longitude":-1.57489540,
                        "latitude":47.19383736
                    },
                    {
                        "nom":"Orvault-Grand Val",
                        "longitude":-1.59436976,
                        "latitude":47.26288752
                    },
                    {
                        "nom":"Orvault-Grand Val",
                        "longitude":-1.59217888,
                        "latitude":47.26188135
                    },
                    {
                        "nom":"Orvault-Grand Val",
                        "longitude":-1.59217227,
                        "latitude":47.26179151
                    },
                    {
                        "nom":"Orvault-Grand Val",
                        "longitude":-1.59229764,
                        "latitude":47.26169717
                    },
                    {
                        "nom":"Orvault-Grand Val",
                        "longitude":-1.59285196,
                        "latitude":47.26203856
                    },
                    {
                        "nom":"Orvault-Grand Val",
                        "longitude":-1.59436976,
                        "latitude":47.26288752
                    },
                    {
                        "nom":"Olivier",
                        "longitude":-1.72495903,
                        "latitude":47.21755840
                    },
                    {
                        "nom":"Olivier",
                        "longitude":-1.72468851,
                        "latitude":47.21747786
                    },
                    {
                        "nom":"Oliveraie",
                        "longitude":-1.53047604,
                        "latitude":47.18804171
                    },
                    {
                        "nom":"Oliveraie",
                        "longitude":-1.53117399,
                        "latitude":47.18855867
                    },
                    {
                        "nom":"Olympe de Gouge",
                        "longitude":-1.54450975,
                        "latitude":47.30870732
                    },
                    {
                        "nom":"Olympe de Gouge",
                        "longitude":-1.54466143,
                        "latitude":47.30897242
                    },
                    {
                        "nom":"Orvault-Morlière",
                        "longitude":-1.60325434,
                        "latitude":47.24700256
                    },
                    {
                        "nom":"Orvault-Morlière",
                        "longitude":-1.60325434,
                        "latitude":47.24700256
                    },
                    {
                        "nom":"Orvault-Morlière",
                        "longitude":-1.60312240,
                        "latitude":47.24700707
                    },
                    {
                        "nom":"Orvault-Morlière",
                        "longitude":-1.60352484,
                        "latitude":47.24708339
                    },
                    {
                        "nom":"Orvault-Morlière",
                        "longitude":-1.60389416,
                        "latitude":47.24671050
                    },
                    {
                        "nom":"Onchère",
                        "longitude":-1.46704982,
                        "latitude":47.20276230
                    },
                    {
                        "nom":"Onchère",
                        "longitude":-1.46704982,
                        "latitude":47.20276230
                    },
                    {
                        "nom":"Ouche Quinet",
                        "longitude":-1.50449351,
                        "latitude":47.19602617
                    },
                    {
                        "nom":"Ouche Quinet",
                        "longitude":-1.50462533,
                        "latitude":47.19602177
                    },
                    {
                        "nom":"Orvault",
                        "longitude":-1.64041888,
                        "latitude":47.27896266
                    },
                    {
                        "nom":"Ouessant",
                        "longitude":-1.44671961,
                        "latitude":47.27034624
                    },
                    {
                        "nom":"Ouessant",
                        "longitude":-1.44670053,
                        "latitude":47.27007669
                    },
                    {
                        "nom":"50 Otages",
                        "longitude":-1.55565480,
                        "latitude":47.21997801
                    },
                    {
                        "nom":"50 Otages",
                        "longitude":-1.55566134,
                        "latitude":47.22006786
                    },
                    {
                        "nom":"50 Otages",
                        "longitude":-1.55563519,
                        "latitude":47.21970848
                    },
                    {
                        "nom":"50 Otages",
                        "longitude":-1.55512076,
                        "latitude":47.21990598
                    },
                    {
                        "nom":"Paix",
                        "longitude":-1.59233294,
                        "latitude":47.18297601
                    },
                    {
                        "nom":"Paix",
                        "longitude":-1.59242516,
                        "latitude":47.18243245
                    },
                    {
                        "nom":"Parc",
                        "longitude":-1.48417618,
                        "latitude":47.27235400
                    },
                    {
                        "nom":"Parc",
                        "longitude":-1.48393143,
                        "latitude":47.27263229
                    },
                    {
                        "nom":"Pablée",
                        "longitude":-1.74029683,
                        "latitude":47.21584580
                    },
                    {
                        "nom":"Pablée",
                        "longitude":-1.74015814,
                        "latitude":47.21576063
                    },
                    {
                        "nom":"Parc",
                        "longitude":-1.47883161,
                        "latitude":47.21417095
                    },
                    {
                        "nom":"Parc",
                        "longitude":-1.47936549,
                        "latitude":47.21424333
                    },
                    {
                        "nom":"Petit Anjou",
                        "longitude":-1.50453728,
                        "latitude":47.20584171
                    },
                    {
                        "nom":"Petit Anjou",
                        "longitude":-1.50466267,
                        "latitude":47.20574746
                    },
                    {
                        "nom":"Pont Allard",
                        "longitude":-1.66627488,
                        "latitude":47.19655292
                    },
                    {
                        "nom":"Pont Allard",
                        "longitude":-1.66642012,
                        "latitude":47.19672802
                    },
                    {
                        "nom":"Palmiers",
                        "longitude":-1.53300429,
                        "latitude":47.19011839
                    },
                    {
                        "nom":"Palmiers",
                        "longitude":-1.53300429,
                        "latitude":47.19011839
                    },
                    {
                        "nom":"Pâtis Rondin",
                        "longitude":-1.51642433,
                        "latitude":47.24795511
                    },
                    {
                        "nom":"Pâtis Rondin",
                        "longitude":-1.51693917,
                        "latitude":47.24775778
                    },
                    {
                        "nom":"Parc de la Bouvre",
                        "longitude":-1.61309195,
                        "latitude":47.17605106
                    },
                    {
                        "nom":"Parc de la Bouvre",
                        "longitude":-1.61388252,
                        "latitude":47.17602394
                    },
                    {
                        "nom":"Parc de la Bouvre",
                        "longitude":-1.61248538,
                        "latitude":47.17499101
                    },
                    {
                        "nom":"Parc Bel Air",
                        "longitude":-1.47485280,
                        "latitude":47.17872785
                    },
                    {
                        "nom":"Parc Bel Air",
                        "longitude":-1.47470823,
                        "latitude":47.17855251
                    },
                    {
                        "nom":"Paul Bert",
                        "longitude":-1.59799118,
                        "latitude":47.21322637
                    },
                    {
                        "nom":"Paul Bert",
                        "longitude":-1.59785272,
                        "latitude":47.21314103
                    },
                    {
                        "nom":"Paimboeuf",
                        "longitude":-1.62064218,
                        "latitude":47.17633227
                    },
                    {
                        "nom":"Paimboeuf",
                        "longitude":-1.61907431,
                        "latitude":47.17656629
                    },
                    {
                        "nom":"Pâtis Brûlé",
                        "longitude":-1.50736222,
                        "latitude":47.19917280
                    },
                    {
                        "nom":"Pâtis Brûlé",
                        "longitude":-1.50732719,
                        "latitude":47.20052494
                    },
                    {
                        "nom":"Pâtis Brûlé",
                        "longitude":-1.50755858,
                        "latitude":47.20006690
                    },
                    {
                        "nom":"Pâtis Brûlé",
                        "longitude":-1.50723684,
                        "latitude":47.19926705
                    },
                    {
                        "nom":"Poulbot",
                        "longitude":-1.64835997,
                        "latitude":47.21329698
                    },
                    {
                        "nom":"Poulbot",
                        "longitude":-1.64801795,
                        "latitude":47.21402938
                    },
                    {
                        "nom":"Poulbot",
                        "longitude":-1.64801795,
                        "latitude":47.21402938
                    },
                    {
                        "nom":"Poulbot",
                        "longitude":-1.64835328,
                        "latitude":47.21320714
                    },
                    {
                        "nom":"Petit Breton",
                        "longitude":-1.60011407,
                        "latitude":47.21333400
                    },
                    {
                        "nom":"Petit Breton",
                        "longitude":-1.60023932,
                        "latitude":47.21323965
                    },
                    {
                        "nom":"Port Boyer",
                        "longitude":-1.53867607,
                        "latitude":47.24108505
                    },
                    {
                        "nom":"Port Boyer",
                        "longitude":-1.53828029,
                        "latitude":47.24109835
                    },
                    {
                        "nom":"Petit Carcouët",
                        "longitude":-1.58679594,
                        "latitude":47.22819914
                    },
                    {
                        "nom":"Petit Carcouët",
                        "longitude":-1.58677617,
                        "latitude":47.22792961
                    },
                    {
                        "nom":"Picabia",
                        "longitude":-1.57669991,
                        "latitude":47.25466181
                    },
                    {
                        "nom":"Picabia",
                        "longitude":-1.57682529,
                        "latitude":47.25456749
                    },
                    {
                        "nom":"Picaudière",
                        "longitude":-1.48541452,
                        "latitude":47.28041848
                    },
                    {
                        "nom":"Picaudière",
                        "longitude":-1.48507335,
                        "latitude":47.27934905
                    },
                    {
                        "nom":"Place Centrale",
                        "longitude":-1.67306746,
                        "latitude":47.26252041
                    },
                    {
                        "nom":"Place Centrale",
                        "longitude":-1.67198475,
                        "latitude":47.26219776
                    },
                    {
                        "nom":"Portechaise",
                        "longitude":-1.52570982,
                        "latitude":47.20072074
                    },
                    {
                        "nom":"Portechaise",
                        "longitude":-1.52596699,
                        "latitude":47.20062205
                    },
                    {
                        "nom":"Planchonnais",
                        "longitude":-1.47476516,
                        "latitude":47.25357239
                    },
                    {
                        "nom":"Planchonnais",
                        "longitude":-1.47528020,
                        "latitude":47.25337524
                    },
                    {
                        "nom":"Poincaré",
                        "longitude":-1.58464004,
                        "latitude":47.22223794
                    },
                    {
                        "nom":"Poincaré",
                        "longitude":-1.58451474,
                        "latitude":47.22233227
                    },
                    {
                        "nom":"Poincaré",
                        "longitude":-1.58520707,
                        "latitude":47.22275905
                    },
                    {
                        "nom":"Poincaré",
                        "longitude":-1.58507519,
                        "latitude":47.22276354
                    },
                    {
                        "nom":"Petite Censive",
                        "longitude":-1.56111925,
                        "latitude":47.26050332
                    },
                    {
                        "nom":"Petite Censive",
                        "longitude":-1.56422022,
                        "latitude":47.26129915
                    },
                    {
                        "nom":"Petit Chantilly",
                        "longitude":-1.60063033,
                        "latitude":47.25447779
                    },
                    {
                        "nom":"Petit Chantilly",
                        "longitude":-1.60076891,
                        "latitude":47.25456313
                    },
                    {
                        "nom":"Pontecorvo",
                        "longitude":-1.53093836,
                        "latitude":47.24746924
                    },
                    {
                        "nom":"Pontecorvo",
                        "longitude":-1.53256261,
                        "latitude":47.24615385
                    },
                    {
                        "nom":"Pont de Chézine",
                        "longitude":-1.61437263,
                        "latitude":47.23815563
                    },
                    {
                        "nom":"Pont de Chézine",
                        "longitude":-1.61436599,
                        "latitude":47.23806578
                    },
                    {
                        "nom":"Pas d'Ane",
                        "longitude":-1.47041300,
                        "latitude":47.18130640
                    },
                    {
                        "nom":"Pas d'Ane",
                        "longitude":-1.46993696,
                        "latitude":47.18204264
                    },
                    {
                        "nom":"Pont du Cens",
                        "longitude":-1.57731488,
                        "latitude":47.24860646
                    },
                    {
                        "nom":"Pont du Cens",
                        "longitude":-1.57707072,
                        "latitude":47.24888495
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.58146854,
                        "latitude":47.26557814
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.58145537,
                        "latitude":47.26539845
                    },
                    {
                        "nom":"Porte Douillard",
                        "longitude":-1.51930528,
                        "latitude":47.28973819
                    },
                    {
                        "nom":"Porte Douillard",
                        "longitude":-1.51863207,
                        "latitude":47.28958055
                    },
                    {
                        "nom":"Prairie au duc",
                        "longitude":-1.56508419,
                        "latitude":47.20425767
                    },
                    {
                        "nom":"Prairie au duc",
                        "longitude":-1.56442501,
                        "latitude":47.20427999
                    },
                    {
                        "nom":"Perdriaux",
                        "longitude":-1.48590223,
                        "latitude":47.16142961
                    },
                    {
                        "nom":"Perdriaux",
                        "longitude":-1.48562593,
                        "latitude":47.16125865
                    },
                    {
                        "nom":"Pégers",
                        "longitude":-1.47153044,
                        "latitude":47.13758868
                    },
                    {
                        "nom":"Pelousière",
                        "longitude":-1.64702227,
                        "latitude":47.20775876
                    },
                    {
                        "nom":"Pelousière",
                        "longitude":-1.64702227,
                        "latitude":47.20775876
                    },
                    {
                        "nom":"Perron",
                        "longitude":-1.60175834,
                        "latitude":47.22489675
                    },
                    {
                        "nom":"Perron",
                        "longitude":-1.60092736,
                        "latitude":47.22438473
                    },
                    {
                        "nom":"Perray",
                        "longitude":-1.51414241,
                        "latitude":47.24749096
                    },
                    {
                        "nom":"Perray",
                        "longitude":-1.51295490,
                        "latitude":47.24753061
                    },
                    {
                        "nom":"Pétrels",
                        "longitude":-1.50737626,
                        "latitude":47.25456149
                    },
                    {
                        "nom":"Pétrels",
                        "longitude":-1.50696099,
                        "latitude":47.25430514
                    },
                    {
                        "nom":"Purfina",
                        "longitude":-1.63494095,
                        "latitude":47.19457500
                    },
                    {
                        "nom":"Purfina",
                        "longitude":-1.63454553,
                        "latitude":47.19458863
                    },
                    {
                        "nom":"Pagerie",
                        "longitude":-1.65066066,
                        "latitude":47.17853941
                    },
                    {
                        "nom":"Pagerie",
                        "longitude":-1.65318427,
                        "latitude":47.17872222
                    },
                    {
                        "nom":"Plinguetière",
                        "longitude":-1.61374439,
                        "latitude":47.13297444
                    },
                    {
                        "nom":"Plinguetière",
                        "longitude":-1.61301406,
                        "latitude":47.13381016
                    },
                    {
                        "nom":"Péguy",
                        "longitude":-1.61393627,
                        "latitude":47.22150769
                    },
                    {
                        "nom":"Péguy",
                        "longitude":-1.61367916,
                        "latitude":47.22160658
                    },
                    {
                        "nom":"Parc Chantrerie",
                        "longitude":-1.52107843,
                        "latitude":47.28679692
                    },
                    {
                        "nom":"Parc Chantrerie",
                        "longitude":-1.52090098,
                        "latitude":47.28617242
                    },
                    {
                        "nom":"Pré Hervé",
                        "longitude":-1.51786761,
                        "latitude":47.24412420
                    },
                    {
                        "nom":"Pré Hervé",
                        "longitude":-1.51757136,
                        "latitude":47.24368379
                    },
                    {
                        "nom":"Piano'cktail",
                        "longitude":-1.60652973,
                        "latitude":47.17483482
                    },
                    {
                        "nom":"Piano'cktail",
                        "longitude":-1.60666149,
                        "latitude":47.17483031
                    },
                    {
                        "nom":"Pierre Blanche",
                        "longitude":-1.62733285,
                        "latitude":47.17213899
                    },
                    {
                        "nom":"Pierre Blanche",
                        "longitude":-1.62719445,
                        "latitude":47.17205368
                    },
                    {
                        "nom":"Picasso",
                        "longitude":-1.53692066,
                        "latitude":47.21502510
                    },
                    {
                        "nom":"Picasso",
                        "longitude":-1.53589176,
                        "latitude":47.21541994
                    },
                    {
                        "nom":"Pierre Abélard",
                        "longitude":-1.68458074,
                        "latitude":47.15508427
                    },
                    {
                        "nom":"Pierre Abélard",
                        "longitude":-1.68479352,
                        "latitude":47.15475257
                    },
                    {
                        "nom":"Pipay",
                        "longitude":-1.53690356,
                        "latitude":47.22574347
                    },
                    {
                        "nom":"Pipay",
                        "longitude":-1.53728621,
                        "latitude":47.22555048
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54206624,
                        "latitude":47.19665866
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54282459,
                        "latitude":47.19618279
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54269277,
                        "latitude":47.19618723
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54255444,
                        "latitude":47.19610182
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54293687,
                        "latitude":47.19590881
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54207275,
                        "latitude":47.19674851
                    },
                    {
                        "nom":"Pirmil",
                        "longitude":-1.54299548,
                        "latitude":47.19671744
                    },
                    {
                        "nom":"Pin Sec",
                        "longitude":-1.51809262,
                        "latitude":47.24357630
                    },
                    {
                        "nom":"Pin Sec",
                        "longitude":-1.51809262,
                        "latitude":47.24357630
                    },
                    {
                        "nom":"Pin Sec",
                        "longitude":-1.51886481,
                        "latitude":47.24328029
                    },
                    {
                        "nom":"Pin Sec",
                        "longitude":-1.51860094,
                        "latitude":47.24328911
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.44355980,
                        "latitude":47.26495622
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.44355980,
                        "latitude":47.26495622
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.44407507,
                        "latitude":47.26475921
                    },
                    {
                        "nom":"Point du jour",
                        "longitude":-1.44434541,
                        "latitude":47.26484041
                    },
                    {
                        "nom":"Placis",
                        "longitude":-1.51994400,
                        "latitude":47.19956291
                    },
                    {
                        "nom":"Placis",
                        "longitude":-1.52008230,
                        "latitude":47.19964834
                    },
                    {
                        "nom":"Port la Blanche",
                        "longitude":-1.51667583,
                        "latitude":47.27163335
                    },
                    {
                        "nom":"Port la Blanche",
                        "longitude":-1.51643125,
                        "latitude":47.27191171
                    },
                    {
                        "nom":"Polyclinique",
                        "longitude":-1.61163773,
                        "latitude":47.22798140
                    },
                    {
                        "nom":"Polyclinique",
                        "longitude":-1.61163773,
                        "latitude":47.22798140
                    },
                    {
                        "nom":"Polyclinique",
                        "longitude":-1.61260661,
                        "latitude":47.22677731
                    },
                    {
                        "nom":"Polyclinique",
                        "longitude":-1.61274513,
                        "latitude":47.22686263
                    },
                    {
                        "nom":"Plessis",
                        "longitude":-1.55228866,
                        "latitude":47.30097024
                    },
                    {
                        "nom":"Plessis",
                        "longitude":-1.55242073,
                        "latitude":47.30096579
                    },
                    {
                        "nom":"Pouliguen",
                        "longitude":-1.54305732,
                        "latitude":47.23949666
                    },
                    {
                        "nom":"Pouliguen",
                        "longitude":-1.54232443,
                        "latitude":47.24033191
                    },
                    {
                        "nom":"Port launay",
                        "longitude":-1.74683222,
                        "latitude":47.20966925
                    },
                    {
                        "nom":"Port launay",
                        "longitude":-1.74630488,
                        "latitude":47.20968794
                    },
                    {
                        "nom":"Ploreau",
                        "longitude":-1.55178525,
                        "latitude":47.30314874
                    },
                    {
                        "nom":"Ploreau",
                        "longitude":-1.55191079,
                        "latitude":47.30305444
                    },
                    {
                        "nom":"Place des Pêcheurs",
                        "longitude":-1.66871410,
                        "latitude":47.13644248
                    },
                    {
                        "nom":"Place des Pêcheurs",
                        "longitude":-1.66901893,
                        "latitude":47.13645889
                    },
                    {
                        "nom":"Plessis",
                        "longitude":-1.66021017,
                        "latitude":47.26044466
                    },
                    {
                        "nom":"Plessis",
                        "longitude":-1.66007149,
                        "latitude":47.26035939
                    },
                    {
                        "nom":"Platière",
                        "longitude":-1.55883419,
                        "latitude":47.19465181
                    },
                    {
                        "nom":"Platière",
                        "longitude":-1.56107499,
                        "latitude":47.19457601
                    },
                    {
                        "nom":"Pont Marchand",
                        "longitude":-1.63138111,
                        "latitude":47.26062944
                    },
                    {
                        "nom":"Pont Marchand",
                        "longitude":-1.63124914,
                        "latitude":47.26063398
                    },
                    {
                        "nom":"Port aux Meules",
                        "longitude":-1.49305446,
                        "latitude":47.16542495
                    },
                    {
                        "nom":"Port aux Meules",
                        "longitude":-1.49373890,
                        "latitude":47.16576244
                    },
                    {
                        "nom":"Pont de la Métairie",
                        "longitude":-1.44602801,
                        "latitude":47.26244371
                    },
                    {
                        "nom":"Pont de la Métairie",
                        "longitude":-1.44628563,
                        "latitude":47.26234520
                    },
                    {
                        "nom":"Pont de la Métairie",
                        "longitude":-1.44722227,
                        "latitude":47.26249460
                    },
                    {
                        "nom":"Pont de la Métairie",
                        "longitude":-1.45021813,
                        "latitude":47.25996468
                    },
                    {
                        "nom":"Prairie de Mauves",
                        "longitude":-1.51666021,
                        "latitude":47.22002740
                    },
                    {
                        "nom":"Prairie de Mauves",
                        "longitude":-1.51641587,
                        "latitude":47.22030576
                    },
                    {
                        "nom":"Prière",
                        "longitude":-1.59613974,
                        "latitude":47.22220639
                    },
                    {
                        "nom":"Prière",
                        "longitude":-1.59681234,
                        "latitude":47.22236358
                    },
                    {
                        "nom":"Pinier",
                        "longitude":-1.54533445,
                        "latitude":47.18159748
                    },
                    {
                        "nom":"Pinier",
                        "longitude":-1.54555093,
                        "latitude":47.18276106
                    },
                    {
                        "nom":"Plantiveau",
                        "longitude":-1.51155784,
                        "latitude":47.24649647
                    },
                    {
                        "nom":"Plantiveau",
                        "longitude":-1.51140650,
                        "latitude":47.24623134
                    },
                    {
                        "nom":"Poitou",
                        "longitude":-1.56535206,
                        "latitude":47.22424353
                    },
                    {
                        "nom":"Poitou",
                        "longitude":-1.56548394,
                        "latitude":47.22423907
                    },
                    {
                        "nom":"Poulenc",
                        "longitude":-1.58125232,
                        "latitude":47.25540792
                    },
                    {
                        "nom":"Poulenc",
                        "longitude":-1.58111378,
                        "latitude":47.25532256
                    },
                    {
                        "nom":"Pommeraie",
                        "longitude":-1.69578708,
                        "latitude":47.14625206
                    },
                    {
                        "nom":"Pommeraie",
                        "longitude":-1.69633410,
                        "latitude":47.14650313
                    },
                    {
                        "nom":"Pontereau",
                        "longitude":-1.37870501,
                        "latitude":47.30641716
                    },
                    {
                        "nom":"Pontereau",
                        "longitude":-1.37883711,
                        "latitude":47.30641291
                    },
                    {
                        "nom":"Potiron",
                        "longitude":-1.51871013,
                        "latitude":47.27048463
                    },
                    {
                        "nom":"Potiron",
                        "longitude":-1.51896766,
                        "latitude":47.27038596
                    },
                    {
                        "nom":"Poste",
                        "longitude":-1.55412164,
                        "latitude":47.29523442
                    },
                    {
                        "nom":"Poste",
                        "longitude":-1.55412164,
                        "latitude":47.29523442
                    },
                    {
                        "nom":"Pottier",
                        "longitude":-1.62750170,
                        "latitude":47.17798785
                    },
                    {
                        "nom":"Pottier",
                        "longitude":-1.62734333,
                        "latitude":47.17763302
                    },
                    {
                        "nom":"Poyaux",
                        "longitude":-1.53052577,
                        "latitude":47.16678439
                    },
                    {
                        "nom":"Poyaux",
                        "longitude":-1.53103977,
                        "latitude":47.16658699
                    },
                    {
                        "nom":"Pompidou",
                        "longitude":-1.52830369,
                        "latitude":47.20918997
                    },
                    {
                        "nom":"Pompidou",
                        "longitude":-1.52752557,
                        "latitude":47.20939620
                    },
                    {
                        "nom":"Petit Port",
                        "longitude":-1.55653739,
                        "latitude":47.24300518
                    },
                    {
                        "nom":"Petit Port",
                        "longitude":-1.55576542,
                        "latitude":47.24330144
                    },
                    {
                        "nom":"Petit Port",
                        "longitude":-1.55711745,
                        "latitude":47.24370612
                    },
                    {
                        "nom":"Peupliers",
                        "longitude":-1.49733040,
                        "latitude":47.19932695
                    },
                    {
                        "nom":"Peupliers",
                        "longitude":-1.49563592,
                        "latitude":47.19965352
                    },
                    {
                        "nom":"Pré Poulain",
                        "longitude":-1.44462168,
                        "latitude":47.27248637
                    },
                    {
                        "nom":"Pré Poulain",
                        "longitude":-1.44458990,
                        "latitude":47.27203712
                    },
                    {
                        "nom":"Petit Plessis",
                        "longitude":-1.38607257,
                        "latitude":47.30194725
                    },
                    {
                        "nom":"Petit Plessis",
                        "longitude":-1.38619840,
                        "latitude":47.30185314
                    },
                    {
                        "nom":"Papotière",
                        "longitude":-1.49681824,
                        "latitude":47.24014265
                    },
                    {
                        "nom":"Papotière",
                        "longitude":-1.49733308,
                        "latitude":47.23994541
                    },
                    {
                        "nom":"Procé",
                        "longitude":-1.58175188,
                        "latitude":47.22251630
                    },
                    {
                        "nom":"Procé",
                        "longitude":-1.58175846,
                        "latitude":47.22260614
                    },
                    {
                        "nom":"Praudière",
                        "longitude":-1.58387156,
                        "latitude":47.25685006
                    },
                    {
                        "nom":"Praudière",
                        "longitude":-1.58377913,
                        "latitude":47.25739360
                    },
                    {
                        "nom":"Preux",
                        "longitude":-1.61814957,
                        "latitude":47.22127313
                    },
                    {
                        "nom":"Preux",
                        "longitude":-1.61827480,
                        "latitude":47.22117876
                    },
                    {
                        "nom":"Profondine",
                        "longitude":-1.48878110,
                        "latitude":47.19618872
                    },
                    {
                        "nom":"Profondine",
                        "longitude":-1.48851746,
                        "latitude":47.19619748
                    },
                    {
                        "nom":"Portail Rouge",
                        "longitude":-1.50439053,
                        "latitude":47.24430373
                    },
                    {
                        "nom":"Portail Rouge",
                        "longitude":-1.50489247,
                        "latitude":47.24392676
                    },
                    {
                        "nom":"Petit Rocher",
                        "longitude":-1.47912671,
                        "latitude":47.24054937
                    },
                    {
                        "nom":"Petit Rocher",
                        "longitude":-1.47899478,
                        "latitude":47.24055373
                    },
                    {
                        "nom":"Primevères",
                        "longitude":-1.67302856,
                        "latitude":47.22496145
                    },
                    {
                        "nom":"Primevères",
                        "longitude":-1.67288995,
                        "latitude":47.22487620
                    },
                    {
                        "nom":"Pyramide",
                        "longitude":-1.49266797,
                        "latitude":47.20803804
                    },
                    {
                        "nom":"Pyramide",
                        "longitude":-1.49279982,
                        "latitude":47.20803366
                    },
                    {
                        "nom":"Perrines",
                        "longitude":-1.51548277,
                        "latitude":47.22754208
                    },
                    {
                        "nom":"Perrines",
                        "longitude":-1.51548924,
                        "latitude":47.22763193
                    },
                    {
                        "nom":"Pont-Rousseau-Martyrs",
                        "longitude":-1.54819272,
                        "latitude":47.19185882
                    },
                    {
                        "nom":"Pont-Rousseau-Martyrs",
                        "longitude":-1.54832452,
                        "latitude":47.19185437
                    },
                    {
                        "nom":"Pont-Rousseau-Martyrs",
                        "longitude":-1.54767201,
                        "latitude":47.19196644
                    },
                    {
                        "nom":"Pont-Rousseau-Martyrs",
                        "longitude":-1.54757281,
                        "latitude":47.19242012
                    },
                    {
                        "nom":"Perrières",
                        "longitude":-1.55197896,
                        "latitude":47.30944669
                    },
                    {
                        "nom":"Perrières",
                        "longitude":-1.55197896,
                        "latitude":47.30944669
                    },
                    {
                        "nom":"Pressoir",
                        "longitude":-1.63892195,
                        "latitude":47.12364032
                    },
                    {
                        "nom":"Pressoir",
                        "longitude":-1.63865202,
                        "latitude":47.12355958
                    },
                    {
                        "nom":"Portereau",
                        "longitude":-1.50171153,
                        "latitude":47.16828894
                    },
                    {
                        "nom":"Portereau",
                        "longitude":-1.50077639,
                        "latitude":47.16813998
                    },
                    {
                        "nom":"Portereau",
                        "longitude":-1.50150894,
                        "latitude":47.16730498
                    },
                    {
                        "nom":"Porte de Rezé",
                        "longitude":-1.54079053,
                        "latitude":47.15716224
                    },
                    {
                        "nom":"Porte de Rezé",
                        "longitude":-1.54092876,
                        "latitude":47.15724765
                    },
                    {
                        "nom":"Porte de Sautron",
                        "longitude":-1.61983480,
                        "latitude":47.25120854
                    },
                    {
                        "nom":"Porte de Sautron",
                        "longitude":-1.61874600,
                        "latitude":47.25079554
                    },
                    {
                        "nom":"Plaisance",
                        "longitude":-1.59124771,
                        "latitude":47.24020674
                    },
                    {
                        "nom":"Plaisance",
                        "longitude":-1.59124771,
                        "latitude":47.24020674
                    },
                    {
                        "nom":"Parc de la Sèvre",
                        "longitude":-1.47925036,
                        "latitude":47.16444225
                    },
                    {
                        "nom":"Parc de la Sèvre",
                        "longitude":-1.48046168,
                        "latitude":47.16476235
                    },
                    {
                        "nom":"Pasteur",
                        "longitude":-1.67863326,
                        "latitude":47.20098683
                    },
                    {
                        "nom":"Pasteur",
                        "longitude":-1.67889689,
                        "latitude":47.20097765
                    },
                    {
                        "nom":"Place St Méen",
                        "longitude":-1.34741542,
                        "latitude":47.31921670
                    },
                    {
                        "nom":"Place St Méen",
                        "longitude":-1.34727709,
                        "latitude":47.31913107
                    },
                    {
                        "nom":"Prières",
                        "longitude":-1.52615191,
                        "latitude":47.15008877
                    },
                    {
                        "nom":"Prières",
                        "longitude":-1.52643476,
                        "latitude":47.15034948
                    },
                    {
                        "nom":"Parc des Sports",
                        "longitude":-1.44906847,
                        "latitude":47.26054278
                    },
                    {
                        "nom":"Parc des Sports",
                        "longitude":-1.45021176,
                        "latitude":47.25987483
                    },
                    {
                        "nom":"Parc des sports",
                        "longitude":-1.45055211,
                        "latitude":47.26094437
                    },
                    {
                        "nom":"Pôle sud",
                        "longitude":-1.46989534,
                        "latitude":47.18888878
                    },
                    {
                        "nom":"Pôle sud",
                        "longitude":-1.46895354,
                        "latitude":47.18864971
                    },
                    {
                        "nom":"Petit Bois",
                        "longitude":-1.67735585,
                        "latitude":47.18571871
                    },
                    {
                        "nom":"Petit Bois",
                        "longitude":-1.67838988,
                        "latitude":47.18541245
                    },
                    {
                        "nom":"Patache",
                        "longitude":-1.61917944,
                        "latitude":47.27095614
                    },
                    {
                        "nom":"Patache",
                        "longitude":-1.61959538,
                        "latitude":47.27121209
                    },
                    {
                        "nom":"Polytech'",
                        "longitude":-1.51363341,
                        "latitude":47.28083125
                    },
                    {
                        "nom":"Polytech'",
                        "longitude":-1.51375897,
                        "latitude":47.28073700
                    },
                    {
                        "nom":"Poitou",
                        "longitude":-1.46975761,
                        "latitude":47.20924741
                    },
                    {
                        "nom":"Poitou",
                        "longitude":-1.46926854,
                        "latitude":47.20980394
                    },
                    {
                        "nom":"Plessis Tison",
                        "longitude":-1.53346310,
                        "latitude":47.23666684
                    },
                    {
                        "nom":"Plessis Tison",
                        "longitude":-1.53358852,
                        "latitude":47.23657256
                    },
                    {
                        "nom":"Platanes",
                        "longitude":-1.52471966,
                        "latitude":47.24560617
                    },
                    {
                        "nom":"Platanes",
                        "longitude":-1.52522146,
                        "latitude":47.24522911
                    },
                    {
                        "nom":"Portillon",
                        "longitude":-1.46856042,
                        "latitude":47.14966540
                    },
                    {
                        "nom":"Portillon",
                        "longitude":-1.46906172,
                        "latitude":47.14928857
                    },
                    {
                        "nom":"Portillon",
                        "longitude":-1.46800167,
                        "latitude":47.14923355
                    },
                    {
                        "nom":"Portillon",
                        "longitude":-1.46875363,
                        "latitude":47.14866831
                    },
                    {
                        "nom":"Printemps",
                        "longitude":-1.50038685,
                        "latitude":47.18850756
                    },
                    {
                        "nom":"Printemps",
                        "longitude":-1.50039329,
                        "latitude":47.18859741
                    },
                    {
                        "nom":"Portricq",
                        "longitude":-1.52177247,
                        "latitude":47.26524864
                    },
                    {
                        "nom":"Portricq",
                        "longitude":-1.52123154,
                        "latitude":47.26508661
                    },
                    {
                        "nom":"Portereau",
                        "longitude":-1.47593793,
                        "latitude":47.19211133
                    },
                    {
                        "nom":"Portereau",
                        "longitude":-1.47733928,
                        "latitude":47.19323576
                    },
                    {
                        "nom":"Pâtures",
                        "longitude":-1.55638729,
                        "latitude":47.30452472
                    },
                    {
                        "nom":"Pâtures",
                        "longitude":-1.55638074,
                        "latitude":47.30443488
                    },
                    {
                        "nom":"Pâtis Viaud",
                        "longitude":-1.45761689,
                        "latitude":47.13867871
                    },
                    {
                        "nom":"Pâtis Viaud",
                        "longitude":-1.45747884,
                        "latitude":47.13859320
                    },
                    {
                        "nom":"Planty",
                        "longitude":-1.49494731,
                        "latitude":47.17526907
                    },
                    {
                        "nom":"Planty",
                        "longitude":-1.49534905,
                        "latitude":47.17534576
                    },
                    {
                        "nom":"Pluchets",
                        "longitude":-1.65443833,
                        "latitude":47.21858103
                    },
                    {
                        "nom":"Pluchets",
                        "longitude":-1.65445173,
                        "latitude":47.21876071
                    },
                    {
                        "nom":"Petit Village",
                        "longitude":-1.65048707,
                        "latitude":47.21700643
                    },
                    {
                        "nom":"Petit Village",
                        "longitude":-1.65141678,
                        "latitude":47.21706433
                    },
                    {
                        "nom":"Pavillon",
                        "longitude":-1.67948603,
                        "latitude":47.14366941
                    },
                    {
                        "nom":"Pavillon",
                        "longitude":-1.67934761,
                        "latitude":47.14358417
                    },
                    {
                        "nom":"Porte de Vertou",
                        "longitude":-1.50567017,
                        "latitude":47.18112627
                    },
                    {
                        "nom":"Porte de Vertou",
                        "longitude":-1.50457078,
                        "latitude":47.18053249
                    },
                    {
                        "nom":"Porte de Vertou",
                        "longitude":-1.50484723,
                        "latitude":47.18070340
                    },
                    {
                        "nom":"Porte de Vertou",
                        "longitude":-1.50484723,
                        "latitude":47.18070340
                    },
                    {
                        "nom":"Porte de Vertou",
                        "longitude":-1.50457722,
                        "latitude":47.18062234
                    },
                    {
                        "nom":"Porte de Vertou",
                        "longitude":-1.50444544,
                        "latitude":47.18062673
                    },
                    {
                        "nom":"Quai des Antilles",
                        "longitude":-1.56954034,
                        "latitude":47.20374641
                    },
                    {
                        "nom":"Quai des Antilles",
                        "longitude":-1.56954034,
                        "latitude":47.20374641
                    },
                    {
                        "nom":"Quai Brunais",
                        "longitude":-1.66957679,
                        "latitude":47.19652821
                    },
                    {
                        "nom":"Quai Brunais",
                        "longitude":-1.66889085,
                        "latitude":47.19619177
                    },
                    {
                        "nom":"Quai Surcouf",
                        "longitude":-1.57788061,
                        "latitude":47.19490685
                    },
                    {
                        "nom":"Quai Surcouf",
                        "longitude":-1.57788061,
                        "latitude":47.19490685
                    },
                    {
                        "nom":"Quintaine",
                        "longitude":-1.46420283,
                        "latitude":47.21663570
                    },
                    {
                        "nom":"Rabine",
                        "longitude":-1.64356226,
                        "latitude":47.26075015
                    },
                    {
                        "nom":"Rabine",
                        "longitude":-1.64329832,
                        "latitude":47.26075925
                    },
                    {
                        "nom":"Ragon",
                        "longitude":-1.54255518,
                        "latitude":47.16511879
                    },
                    {
                        "nom":"Ragon",
                        "longitude":-1.54244296,
                        "latitude":47.16539277
                    },
                    {
                        "nom":"Ravardière",
                        "longitude":-1.66466204,
                        "latitude":47.18381849
                    },
                    {
                        "nom":"Ravardière",
                        "longitude":-1.66532092,
                        "latitude":47.18379560
                    },
                    {
                        "nom":"Ranzay",
                        "longitude":-1.53043062,
                        "latitude":47.25325039
                    },
                    {
                        "nom":"Ranzay",
                        "longitude":-1.53043062,
                        "latitude":47.25325039
                    },
                    {
                        "nom":"Ranzay",
                        "longitude":-1.52982481,
                        "latitude":47.25218993
                    },
                    {
                        "nom":"Ranzay",
                        "longitude":-1.52994827,
                        "latitude":47.25389701
                    },
                    {
                        "nom":"Ranzay",
                        "longitude":-1.52892710,
                        "latitude":47.25258028
                    },
                    {
                        "nom":"Roche Ballue",
                        "longitude":-1.65616350,
                        "latitude":47.17969984
                    },
                    {
                        "nom":"Roche Ballue",
                        "longitude":-1.65575479,
                        "latitude":47.17953387
                    },
                    {
                        "nom":"René Bouhier",
                        "longitude":-1.57131140,
                        "latitude":47.20990103
                    },
                    {
                        "nom":"René Bouhier",
                        "longitude":-1.56985450,
                        "latitude":47.20986036
                    },
                    {
                        "nom":"Robinière 1",
                        "longitude":-1.53558119,
                        "latitude":47.16364228
                    },
                    {
                        "nom":"Robinière 2",
                        "longitude":-1.53606087,
                        "latitude":47.16479701
                    },
                    {
                        "nom":"Robinière 3",
                        "longitude":-1.53545595,
                        "latitude":47.16373655
                    },
                    {
                        "nom":"René Bernier",
                        "longitude":-1.49919154,
                        "latitude":47.19395122
                    },
                    {
                        "nom":"René Bernier",
                        "longitude":-1.49945517,
                        "latitude":47.19394244
                    },
                    {
                        "nom":"René Cassin",
                        "longitude":-1.57620931,
                        "latitude":47.26422552
                    },
                    {
                        "nom":"René Cassin",
                        "longitude":-1.57620273,
                        "latitude":47.26413568
                    },
                    {
                        "nom":"René Cassin",
                        "longitude":-1.57374108,
                        "latitude":47.26484962
                    },
                    {
                        "nom":"René Cassin",
                        "longitude":-1.57400505,
                        "latitude":47.26484067
                    },
                    {
                        "nom":"René Cassin",
                        "longitude":-1.57700120,
                        "latitude":47.26419866
                    },
                    {
                        "nom":"René Cassin",
                        "longitude":-1.57633471,
                        "latitude":47.26413120
                    },
                    {
                        "nom":"Rocher",
                        "longitude":-1.63949256,
                        "latitude":47.21315293
                    },
                    {
                        "nom":"Rocher",
                        "longitude":-1.63961773,
                        "latitude":47.21305853
                    },
                    {
                        "nom":"Rocher",
                        "longitude":-1.50522732,
                        "latitude":47.16943260
                    },
                    {
                        "nom":"Rocher",
                        "longitude":-1.50536552,
                        "latitude":47.16951806
                    },
                    {
                        "nom":"Route de la Chapelle",
                        "longitude":-1.57618523,
                        "latitude":47.25485941
                    },
                    {
                        "nom":"Route de la Chapelle",
                        "longitude":-1.57629088,
                        "latitude":47.25449556
                    },
                    {
                        "nom":"Rochu",
                        "longitude":-1.66647964,
                        "latitude":47.21519099
                    },
                    {
                        "nom":"Rochu",
                        "longitude":-1.66647291,
                        "latitude":47.21510115
                    },
                    {
                        "nom":"Raoul Dufy",
                        "longitude":-1.59960820,
                        "latitude":47.22262841
                    },
                    {
                        "nom":"Raoul Dufy",
                        "longitude":-1.59961481,
                        "latitude":47.22271825
                    },
                    {
                        "nom":"Radigois",
                        "longitude":-1.51802149,
                        "latitude":47.19305249
                    },
                    {
                        "nom":"Radigois",
                        "longitude":-1.51750718,
                        "latitude":47.19324983
                    },
                    {
                        "nom":"Rue de Mauves",
                        "longitude":-1.43264535,
                        "latitude":47.27152739
                    },
                    {
                        "nom":"Rue de Mauves",
                        "longitude":-1.43277102,
                        "latitude":47.27143322
                    },
                    {
                        "nom":"Rue de Nantes",
                        "longitude":-1.53427678,
                        "latitude":47.15107676
                    },
                    {
                        "nom":"Rue de Nantes",
                        "longitude":-1.53429626,
                        "latitude":47.15134630
                    },
                    {
                        "nom":"Redras",
                        "longitude":-1.50251433,
                        "latitude":47.16474964
                    },
                    {
                        "nom":"Redras",
                        "longitude":-1.50197447,
                        "latitude":47.16458751
                    },
                    {
                        "nom":"Réfractaires",
                        "longitude":-1.54687868,
                        "latitude":47.30304372
                    },
                    {
                        "nom":"Réfractaires",
                        "longitude":-1.54751948,
                        "latitude":47.30275197
                    },
                    {
                        "nom":"Retas",
                        "longitude":-1.53705860,
                        "latitude":47.15485606
                    },
                    {
                        "nom":"Retas",
                        "longitude":-1.53693338,
                        "latitude":47.15495034
                    },
                    {
                        "nom":"Reynière",
                        "longitude":-1.57865918,
                        "latitude":47.25612645
                    },
                    {
                        "nom":"Reynière",
                        "longitude":-1.57916728,
                        "latitude":47.25583900
                    },
                    {
                        "nom":"Reigniers",
                        "longitude":-1.46582359,
                        "latitude":47.13714694
                    },
                    {
                        "nom":"Reigniers",
                        "longitude":-1.46514607,
                        "latitude":47.13689914
                    },
                    {
                        "nom":"Rue d'Indre",
                        "longitude":-1.66418944,
                        "latitude":47.20401138
                    },
                    {
                        "nom":"Rue d'Indre",
                        "longitude":-1.66416929,
                        "latitude":47.20374186
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.47598176,
                        "latitude":47.21498576
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.47585629,
                        "latitude":47.21507998
                    },
                    {
                        "nom":"Rouleaux",
                        "longitude":-1.46621185,
                        "latitude":47.20026822
                    },
                    {
                        "nom":"Rouleaux",
                        "longitude":-1.46605448,
                        "latitude":47.19991317
                    },
                    {
                        "nom":"Rennes-Longchamp",
                        "longitude":-1.57063549,
                        "latitude":47.23865543
                    },
                    {
                        "nom":"Rennes-Longchamp",
                        "longitude":-1.56992338,
                        "latitude":47.23795902
                    },
                    {
                        "nom":"Roche Maurice",
                        "longitude":-1.61828778,
                        "latitude":47.19631886
                    },
                    {
                        "nom":"Roche Maurice",
                        "longitude":-1.61828114,
                        "latitude":47.19622902
                    },
                    {
                        "nom":"Romanet",
                        "longitude":-1.60898075,
                        "latitude":47.21167971
                    },
                    {
                        "nom":"Romanet",
                        "longitude":-1.60898075,
                        "latitude":47.21167971
                    },
                    {
                        "nom":"Romanet",
                        "longitude":-1.60849310,
                        "latitude":47.21223682
                    },
                    {
                        "nom":"Romanet",
                        "longitude":-1.60875018,
                        "latitude":47.21213795
                    },
                    {
                        "nom":"Romanet",
                        "longitude":-1.60851233,
                        "latitude":47.21070498
                    },
                    {
                        "nom":"Renan",
                        "longitude":-1.61302404,
                        "latitude":47.21631491
                    },
                    {
                        "nom":"Renan",
                        "longitude":-1.61355149,
                        "latitude":47.21629683
                    },
                    {
                        "nom":"Rinière",
                        "longitude":-1.63589246,
                        "latitude":47.12905932
                    },
                    {
                        "nom":"Rinière",
                        "longitude":-1.63626075,
                        "latitude":47.12868631
                    },
                    {
                        "nom":"Renault",
                        "longitude":-1.49543208,
                        "latitude":47.26324468
                    },
                    {
                        "nom":"Renault",
                        "longitude":-1.49570894,
                        "latitude":47.26341561
                    },
                    {
                        "nom":"Renaudières",
                        "longitude":-1.50796107,
                        "latitude":47.29948308
                    },
                    {
                        "nom":"Renaudières",
                        "longitude":-1.50808668,
                        "latitude":47.29938883
                    },
                    {
                        "nom":"Reinetière",
                        "longitude":-1.49153714,
                        "latitude":47.25688956
                    },
                    {
                        "nom":"Reinetière",
                        "longitude":-1.49127320,
                        "latitude":47.25689832
                    },
                    {
                        "nom":"Rocade",
                        "longitude":-1.47441804,
                        "latitude":47.16334141
                    },
                    {
                        "nom":"Rocade",
                        "longitude":-1.47442443,
                        "latitude":47.16343126
                    },
                    {
                        "nom":"Rouet",
                        "longitude":-1.52139985,
                        "latitude":47.14826668
                    },
                    {
                        "nom":"Rouet",
                        "longitude":-1.52221594,
                        "latitude":47.14859959
                    },
                    {
                        "nom":"Rond-Point de Paris",
                        "longitude":-1.53541116,
                        "latitude":47.23434980
                    },
                    {
                        "nom":"Rond-Point de Paris",
                        "longitude":-1.53640139,
                        "latitude":47.23341589
                    },
                    {
                        "nom":"Rond-Point de Paris",
                        "longitude":-1.53657884,
                        "latitude":47.23404038
                    },
                    {
                        "nom":"Rond-Point de Paris",
                        "longitude":-1.53483148,
                        "latitude":47.23364875
                    },
                    {
                        "nom":"République",
                        "longitude":-1.55474101,
                        "latitude":47.20559819
                    },
                    {
                        "nom":"République",
                        "longitude":-1.55448386,
                        "latitude":47.20569695
                    },
                    {
                        "nom":"République",
                        "longitude":-1.55531525,
                        "latitude":47.20440793
                    },
                    {
                        "nom":"République",
                        "longitude":-1.55537406,
                        "latitude":47.20521654
                    },
                    {
                        "nom":"Repos de Chasse",
                        "longitude":-1.59797194,
                        "latitude":47.21655958
                    },
                    {
                        "nom":"Repos de Chasse",
                        "longitude":-1.59956751,
                        "latitude":47.21668523
                    },
                    {
                        "nom":"Ripossière",
                        "longitude":-1.53456459,
                        "latitude":47.19159708
                    },
                    {
                        "nom":"Ripossière",
                        "longitude":-1.53500550,
                        "latitude":47.19221273
                    },
                    {
                        "nom":"Trentemoult-Roquios",
                        "longitude":-1.58255320,
                        "latitude":47.19555860
                    },
                    {
                        "nom":"Rond-Point de Rennes",
                        "longitude":-1.56549303,
                        "latitude":47.23342559
                    },
                    {
                        "nom":"Rond-Point de Rennes",
                        "longitude":-1.56529557,
                        "latitude":47.23253160
                    },
                    {
                        "nom":"Rond-Point de Rennes",
                        "longitude":-1.56530213,
                        "latitude":47.23262145
                    },
                    {
                        "nom":"Rond-Point de Rennes",
                        "longitude":-1.56625824,
                        "latitude":47.23303941
                    },
                    {
                        "nom":"Rond-Point de Rennes",
                        "longitude":-1.56600754,
                        "latitude":47.23322804
                    },
                    {
                        "nom":"Romain Rolland",
                        "longitude":-1.60979630,
                        "latitude":47.20660786
                    },
                    {
                        "nom":"Romain Rolland",
                        "longitude":-1.60967109,
                        "latitude":47.20670222
                    },
                    {
                        "nom":"Robert Scott",
                        "longitude":-1.45892796,
                        "latitude":47.18699913
                    },
                    {
                        "nom":"Robert Scott",
                        "longitude":-1.45892796,
                        "latitude":47.18699913
                    },
                    {
                        "nom":"Rousselière",
                        "longitude":-1.51835430,
                        "latitude":47.17565873
                    },
                    {
                        "nom":"Rousselière",
                        "longitude":-1.51755075,
                        "latitude":47.17550550
                    },
                    {
                        "nom":"Rosenberg",
                        "longitude":-1.65981468,
                        "latitude":47.23568862
                    },
                    {
                        "nom":"Rosenberg",
                        "longitude":-1.65980796,
                        "latitude":47.23559878
                    },
                    {
                        "nom":"Roseraie",
                        "longitude":-1.52833053,
                        "latitude":47.26079610
                    },
                    {
                        "nom":"Roseraie",
                        "longitude":-1.52832404,
                        "latitude":47.26070625
                    },
                    {
                        "nom":"Recteur Schmitt",
                        "longitude":-1.55283061,
                        "latitude":47.25204678
                    },
                    {
                        "nom":"Recteur Schmitt",
                        "longitude":-1.55283061,
                        "latitude":47.25204678
                    },
                    {
                        "nom":"Recteur Schmitt",
                        "longitude":-1.55271173,
                        "latitude":47.25223092
                    },
                    {
                        "nom":"Recteur Schmitt",
                        "longitude":-1.55285022,
                        "latitude":47.25231631
                    },
                    {
                        "nom":"Recteur Schmitt",
                        "longitude":-1.55301486,
                        "latitude":47.25276109
                    },
                    {
                        "nom":"Recteur Schmitt",
                        "longitude":-1.55327223,
                        "latitude":47.25266234
                    },
                    {
                        "nom":"Ruette",
                        "longitude":-1.51075238,
                        "latitude":47.23346414
                    },
                    {
                        "nom":"Ruette",
                        "longitude":-1.51127357,
                        "latitude":47.23335669
                    },
                    {
                        "nom":"Rond-Point de Vannes",
                        "longitude":-1.57278183,
                        "latitude":47.23002629
                    },
                    {
                        "nom":"Rond-Point de Vannes",
                        "longitude":-1.57290717,
                        "latitude":47.22993197
                    },
                    {
                        "nom":"Rond-Point de Vannes",
                        "longitude":-1.57301279,
                        "latitude":47.22956812
                    },
                    {
                        "nom":"Rond-Point de Vannes",
                        "longitude":-1.57197075,
                        "latitude":47.22978359
                    },
                    {
                        "nom":"Rivaudière",
                        "longitude":-1.65071629,
                        "latitude":47.22717663
                    },
                    {
                        "nom":"Rivaudière",
                        "longitude":-1.65031394,
                        "latitude":47.22710048
                    },
                    {
                        "nom":"Roches Vertes",
                        "longitude":-1.52406118,
                        "latitude":47.18519466
                    },
                    {
                        "nom":"Roches Vertes",
                        "longitude":-1.52456891,
                        "latitude":47.18490744
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.55815690,
                        "latitude":47.25979280
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.55816345,
                        "latitude":47.25988265
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.55720037,
                        "latitude":47.25937477
                    },
                    {
                        "nom":"Rivière",
                        "longitude":-1.55745777,
                        "latitude":47.25927601
                    },
                    {
                        "nom":"Santos Dumont",
                        "longitude":-1.56805479,
                        "latitude":47.25945815
                    },
                    {
                        "nom":"Santos Dumont",
                        "longitude":-1.56805479,
                        "latitude":47.25945815
                    },
                    {
                        "nom":"Santos Dumont",
                        "longitude":-1.56673508,
                        "latitude":47.25950282
                    },
                    {
                        "nom":"Santos Dumont",
                        "longitude":-1.56672196,
                        "latitude":47.25932313
                    },
                    {
                        "nom":"St-Aignan-de-Grand-Lieu",
                        "longitude":-1.63440654,
                        "latitude":47.12325584
                    },
                    {
                        "nom":"St-Aignan",
                        "longitude":-1.57844471,
                        "latitude":47.20803766
                    },
                    {
                        "nom":"St-Aignan",
                        "longitude":-1.57831944,
                        "latitude":47.20813199
                    },
                    {
                        "nom":"Salentine",
                        "longitude":-1.61024106,
                        "latitude":47.24847494
                    },
                    {
                        "nom":"Salentine",
                        "longitude":-1.61038626,
                        "latitude":47.24865011
                    },
                    {
                        "nom":"Sables d'Or",
                        "longitude":-1.45587347,
                        "latitude":47.26509240
                    },
                    {
                        "nom":"Saupin",
                        "longitude":-1.47634217,
                        "latitude":47.18480285
                    },
                    {
                        "nom":"Saupin",
                        "longitude":-1.47575740,
                        "latitude":47.18401164
                    },
                    {
                        "nom":"Sautron",
                        "longitude":-1.68386822,
                        "latitude":47.26538705
                    },
                    {
                        "nom":"Saupin-Crédit Municipal",
                        "longitude":-1.53922912,
                        "latitude":47.21404683
                    },
                    {
                        "nom":"Saupin-Crédit Municipal",
                        "longitude":-1.54013913,
                        "latitude":47.21383608
                    },
                    {
                        "nom":"Savarières",
                        "longitude":-1.48824284,
                        "latitude":47.20899558
                    },
                    {
                        "nom":"Savarières",
                        "longitude":-1.48876383,
                        "latitude":47.20888822
                    },
                    {
                        "nom":"Savarières",
                        "longitude":-1.48937812,
                        "latitude":47.20823738
                    },
                    {
                        "nom":"Savarières",
                        "longitude":-1.48923342,
                        "latitude":47.20806206
                    },
                    {
                        "nom":"Savarières",
                        "longitude":-1.49034916,
                        "latitude":47.21072691
                    },
                    {
                        "nom":"Savarières",
                        "longitude":-1.49018517,
                        "latitude":47.21028204
                    },
                    {
                        "nom":"St-Blaise",
                        "longitude":-1.46914664,
                        "latitude":47.16720837
                    },
                    {
                        "nom":"St-Blaise",
                        "longitude":-1.46901489,
                        "latitude":47.16721273
                    },
                    {
                        "nom":"St-Blaise",
                        "longitude":-1.46902127,
                        "latitude":47.16730258
                    },
                    {
                        "nom":"Sébilleau",
                        "longitude":-1.53184154,
                        "latitude":47.20510837
                    },
                    {
                        "nom":"Sébilleau",
                        "longitude":-1.53169021,
                        "latitude":47.20484325
                    },
                    {
                        "nom":"St-Jean-Baptiste de la Salle",
                        "longitude":-1.56007173,
                        "latitude":47.23342883
                    },
                    {
                        "nom":"St-Jean-Baptiste de la Salle",
                        "longitude":-1.56034209,
                        "latitude":47.23350975
                    },
                    {
                        "nom":"Schweitzer",
                        "longitude":-1.49518502,
                        "latitude":47.29486448
                    },
                    {
                        "nom":"Schweitzer",
                        "longitude":-1.49490801,
                        "latitude":47.29469355
                    },
                    {
                        "nom":"St-Clément",
                        "longitude":-1.54669235,
                        "latitude":47.22217173
                    },
                    {
                        "nom":"St-Clément",
                        "longitude":-1.54720031,
                        "latitude":47.22188441
                    },
                    {
                        "nom":"SCREG",
                        "longitude":-1.63966591,
                        "latitude":47.19414178
                    },
                    {
                        "nom":"SCREG",
                        "longitude":-1.63940230,
                        "latitude":47.19415088
                    },
                    {
                        "nom":"St-Donatien",
                        "longitude":-1.54034242,
                        "latitude":47.22941061
                    },
                    {
                        "nom":"St-Donatien",
                        "longitude":-1.54083745,
                        "latitude":47.22894363
                    },
                    {
                        "nom":"Schoelcher",
                        "longitude":-1.62800667,
                        "latitude":47.22048412
                    },
                    {
                        "nom":"Schoelcher",
                        "longitude":-1.62801333,
                        "latitude":47.22057396
                    },
                    {
                        "nom":"Seil",
                        "longitude":-1.57080276,
                        "latitude":47.19388619
                    },
                    {
                        "nom":"Seil",
                        "longitude":-1.57066439,
                        "latitude":47.19380081
                    },
                    {
                        "nom":"Seil",
                        "longitude":-1.56940531,
                        "latitude":47.19465413
                    },
                    {
                        "nom":"Seil",
                        "longitude":-1.57177135,
                        "latitude":47.19448381
                    },
                    {
                        "nom":"Sercel",
                        "longitude":-1.48959865,
                        "latitude":47.26677060
                    },
                    {
                        "nom":"Sercel",
                        "longitude":-1.48847498,
                        "latitude":47.26770848
                    },
                    {
                        "nom":"Sèvres",
                        "longitude":-1.52730200,
                        "latitude":47.18616678
                    },
                    {
                        "nom":"Sèvres",
                        "longitude":-1.52813168,
                        "latitude":47.18667934
                    },
                    {
                        "nom":"St-Félix",
                        "longitude":-1.55648517,
                        "latitude":47.23138841
                    },
                    {
                        "nom":"St-Félix",
                        "longitude":-1.55661707,
                        "latitude":47.23138396
                    },
                    {
                        "nom":"San-Francisco",
                        "longitude":-1.51649196,
                        "latitude":47.21769135
                    },
                    {
                        "nom":"San-Francisco",
                        "longitude":-1.51572013,
                        "latitude":47.21798734
                    },
                    {
                        "nom":"Sageran",
                        "longitude":-1.57122899,
                        "latitude":47.21963113
                    },
                    {
                        "nom":"Sillon de Bretagne",
                        "longitude":-1.60870951,
                        "latitude":47.24564515
                    },
                    {
                        "nom":"Sillon de Bretagne",
                        "longitude":-1.60857095,
                        "latitude":47.24555982
                    },
                    {
                        "nom":"Sillon de Bretagne",
                        "longitude":-1.60884807,
                        "latitude":47.24573048
                    },
                    {
                        "nom":"Sillon de Bretagne",
                        "longitude":-1.60843901,
                        "latitude":47.24556434
                    },
                    {
                        "nom":"Sinière",
                        "longitude":-1.67848945,
                        "latitude":47.22549193
                    },
                    {
                        "nom":"Sinière",
                        "longitude":-1.67784354,
                        "latitude":47.22569458
                    },
                    {
                        "nom":"St-Jacques",
                        "longitude":-1.53793433,
                        "latitude":47.19616724
                    },
                    {
                        "nom":"St-Jacques",
                        "longitude":-1.53764642,
                        "latitude":47.19401533
                    },
                    {
                        "nom":"St-Jean-de-Boiseau",
                        "longitude":-1.72476572,
                        "latitude":47.19585671
                    },
                    {
                        "nom":"St-Jean-de-Boiseau",
                        "longitude":-1.72489753,
                        "latitude":47.19585207
                    },
                    {
                        "nom":"St-Jean",
                        "longitude":-1.52374341,
                        "latitude":47.19178010
                    },
                    {
                        "nom":"St-Jean",
                        "longitude":-1.52344092,
                        "latitude":47.19124985
                    },
                    {
                        "nom":"St-Joseph-de-Porterie",
                        "longitude":-1.51680624,
                        "latitude":47.26793641
                    },
                    {
                        "nom":"St-Joseph-de-Porterie",
                        "longitude":-1.51693176,
                        "latitude":47.26784216
                    },
                    {
                        "nom":"St-Laurent",
                        "longitude":-1.59822363,
                        "latitude":47.22357637
                    },
                    {
                        "nom":"St-Laurent",
                        "longitude":-1.59739271,
                        "latitude":47.22306432
                    },
                    {
                        "nom":"Ste-Luce-sur-Loire",
                        "longitude":-1.48498474,
                        "latitude":47.25035223
                    },
                    {
                        "nom":"Ste-Luce-sur-Loire",
                        "longitude":-1.48511670,
                        "latitude":47.25034785
                    },
                    {
                        "nom":"Solidarité",
                        "longitude":-1.58744262,
                        "latitude":47.21718879
                    },
                    {
                        "nom":"Solidarité",
                        "longitude":-1.58744921,
                        "latitude":47.21727863
                    },
                    {
                        "nom":"Salorges",
                        "longitude":-1.57503087,
                        "latitude":47.20473102
                    },
                    {
                        "nom":"Salorges",
                        "longitude":-1.57441766,
                        "latitude":47.20538231
                    },
                    {
                        "nom":"St-Léger-les-Vignes",
                        "longitude":-1.73030412,
                        "latitude":47.13657004
                    },
                    {
                        "nom":"St-Léger-les-Vignes",
                        "longitude":-1.72988870,
                        "latitude":47.13631449
                    },
                    {
                        "nom":"St-Martin",
                        "longitude":-1.69331638,
                        "latitude":47.21344493
                    },
                    {
                        "nom":"St-Martin",
                        "longitude":-1.69305269,
                        "latitude":47.21345415
                    },
                    {
                        "nom":"St-Mihiel",
                        "longitude":-1.55338437,
                        "latitude":47.22329706
                    },
                    {
                        "nom":"St-Mihiel",
                        "longitude":-1.55338437,
                        "latitude":47.22329706
                    },
                    {
                        "nom":"St-Michel",
                        "longitude":-1.64440886,
                        "latitude":47.22415208
                    },
                    {
                        "nom":"St-Michel",
                        "longitude":-1.64454074,
                        "latitude":47.22414752
                    },
                    {
                        "nom":"St-Nicolas",
                        "longitude":-1.55918473,
                        "latitude":47.21580574
                    },
                    {
                        "nom":"St-Nicolas",
                        "longitude":-1.56049028,
                        "latitude":47.21558147
                    },
                    {
                        "nom":"Sanitat",
                        "longitude":-1.56926084,
                        "latitude":47.21078117
                    },
                    {
                        "nom":"Sanitat",
                        "longitude":-1.56790300,
                        "latitude":47.21028678
                    },
                    {
                        "nom":"Stade SNUC",
                        "longitude":-1.58109165,
                        "latitude":47.22794281
                    },
                    {
                        "nom":"Stade SNUC",
                        "longitude":-1.58004309,
                        "latitude":47.22806851
                    },
                    {
                        "nom":"Souchais",
                        "longitude":-1.48961477,
                        "latitude":47.30765775
                    },
                    {
                        "nom":"Souchais-Ecole",
                        "longitude":-1.49833277,
                        "latitude":47.30367598
                    },
                    {
                        "nom":"Souchais-Ecole",
                        "longitude":-1.49872258,
                        "latitude":47.30357297
                    },
                    {
                        "nom":"Sorin",
                        "longitude":-1.56785635,
                        "latitude":47.19515698
                    },
                    {
                        "nom":"Sorin",
                        "longitude":-1.56786290,
                        "latitude":47.19524682
                    },
                    {
                        "nom":"Souillarderie",
                        "longitude":-1.51599553,
                        "latitude":47.23833263
                    },
                    {
                        "nom":"Souillarderie",
                        "longitude":-1.51612746,
                        "latitude":47.23832822
                    },
                    {
                        "nom":"Souillarderie",
                        "longitude":-1.51582478,
                        "latitude":47.23779795
                    },
                    {
                        "nom":"Souillarderie",
                        "longitude":-1.51625291,
                        "latitude":47.23823396
                    },
                    {
                        "nom":"Soweto",
                        "longitude":-1.65156746,
                        "latitude":47.23327207
                    },
                    {
                        "nom":"Soweto",
                        "longitude":-1.65156746,
                        "latitude":47.23327207
                    },
                    {
                        "nom":"Saphirs",
                        "longitude":-1.65725626,
                        "latitude":47.20497251
                    },
                    {
                        "nom":"Saphirs",
                        "longitude":-1.65804723,
                        "latitude":47.20494509
                    },
                    {
                        "nom":"Sully Prudhomme",
                        "longitude":-1.59691320,
                        "latitude":47.23632078
                    },
                    {
                        "nom":"Sully Prudhomme",
                        "longitude":-1.59664277,
                        "latitude":47.23623994
                    },
                    {
                        "nom":"Sully Prudhomme",
                        "longitude":-1.59663616,
                        "latitude":47.23615010
                    },
                    {
                        "nom":"St-Paul-Salengro",
                        "longitude":-1.54606358,
                        "latitude":47.18436498
                    },
                    {
                        "nom":"St-Paul-Salengro",
                        "longitude":-1.54626705,
                        "latitude":47.18534886
                    },
                    {
                        "nom":"Sarradin",
                        "longitude":-1.57022650,
                        "latitude":47.22038566
                    },
                    {
                        "nom":"Les Sorinières",
                        "longitude":-1.51809294,
                        "latitude":47.15918543
                    },
                    {
                        "nom":"Sartre",
                        "longitude":-1.64023862,
                        "latitude":47.21429811
                    },
                    {
                        "nom":"Sartre",
                        "longitude":-1.64091793,
                        "latitude":47.21454488
                    },
                    {
                        "nom":"St-Sébastien-sur-Loire",
                        "longitude":-1.50281394,
                        "latitude":47.20761036
                    },
                    {
                        "nom":"St-Sébastien-sur-Loire",
                        "longitude":-1.50333489,
                        "latitude":47.20750294
                    },
                    {
                        "nom":"St-Stanislas",
                        "longitude":-1.55660951,
                        "latitude":47.22219743
                    },
                    {
                        "nom":"St-Stanislas",
                        "longitude":-1.55705093,
                        "latitude":47.22281299
                    },
                    {
                        "nom":"Stade",
                        "longitude":-1.70723769,
                        "latitude":47.19124895
                    },
                    {
                        "nom":"Stade",
                        "longitude":-1.70790343,
                        "latitude":47.19131565
                    },
                    {
                        "nom":"Alexandre Vincent-Ste-Thérèse",
                        "longitude":-1.57748332,
                        "latitude":47.23283897
                    },
                    {
                        "nom":"Alexandre Vincent-Ste-Thérèse",
                        "longitude":-1.57747675,
                        "latitude":47.23274913
                    },
                    {
                        "nom":"St-Pierre",
                        "longitude":-1.55195050,
                        "latitude":47.21812159
                    },
                    {
                        "nom":"St-Pierre",
                        "longitude":-1.55181863,
                        "latitude":47.21812604
                    },
                    {
                        "nom":"St-Exupéry",
                        "longitude":-1.59487984,
                        "latitude":47.15289572
                    },
                    {
                        "nom":"St-Exupéry",
                        "longitude":-1.59487984,
                        "latitude":47.15289572
                    },
                    {
                        "nom":"Surcouf",
                        "longitude":-1.44512766,
                        "latitude":47.26841713
                    },
                    {
                        "nom":"Surcouf",
                        "longitude":-1.44581947,
                        "latitude":47.26884475
                    },
                    {
                        "nom":"Savaudière",
                        "longitude":-1.51477024,
                        "latitude":47.28925918
                    },
                    {
                        "nom":"Savaudière",
                        "longitude":-1.51409057,
                        "latitude":47.28901168
                    },
                    {
                        "nom":"Solvardière",
                        "longitude":-1.64184443,
                        "latitude":47.21811576
                    },
                    {
                        "nom":"Solvardière",
                        "longitude":-1.64224002,
                        "latitude":47.21810211
                    },
                    {
                        "nom":"Stévin",
                        "longitude":-1.63491088,
                        "latitude":47.26005760
                    },
                    {
                        "nom":"Stévin",
                        "longitude":-1.63438301,
                        "latitude":47.26007577
                    },
                    {
                        "nom":"Savonnières",
                        "longitude":-1.66277648,
                        "latitude":47.19748509
                    },
                    {
                        "nom":"Savonnières",
                        "longitude":-1.66251286,
                        "latitude":47.19749424
                    },
                    {
                        "nom":"Syrma",
                        "longitude":-1.47086712,
                        "latitude":47.27684670
                    },
                    {
                        "nom":"Syrma",
                        "longitude":-1.47009418,
                        "latitude":47.27714239
                    },
                    {
                        "nom":"Saulzaie",
                        "longitude":-1.63183278,
                        "latitude":47.23107086
                    },
                    {
                        "nom":"Saulzaie",
                        "longitude":-1.63209657,
                        "latitude":47.23106178
                    },
                    {
                        "nom":"Toutes Aides",
                        "longitude":-1.52347007,
                        "latitude":47.22646431
                    },
                    {
                        "nom":"Toutes Aides",
                        "longitude":-1.52304846,
                        "latitude":47.22611817
                    },
                    {
                        "nom":"Toutes Aides",
                        "longitude":-1.52186142,
                        "latitude":47.22615791
                    },
                    {
                        "nom":"Thébaudières",
                        "longitude":-1.36020747,
                        "latitude":47.31079318
                    },
                    {
                        "nom":"Thébaudières",
                        "longitude":-1.35886767,
                        "latitude":47.31056591
                    },
                    {
                        "nom":"Trébuchet",
                        "longitude":-1.54559314,
                        "latitude":47.21977697
                    },
                    {
                        "nom":"Trébuchet",
                        "longitude":-1.54694450,
                        "latitude":47.22018178
                    },
                    {
                        "nom":"Tristan Bernard",
                        "longitude":-1.46430888,
                        "latitude":47.20510430
                    },
                    {
                        "nom":"Tristan Bernard",
                        "longitude":-1.46488733,
                        "latitude":47.20580571
                    },
                    {
                        "nom":"Tabarly",
                        "longitude":-1.54871720,
                        "latitude":47.19544380
                    },
                    {
                        "nom":"Tabarly",
                        "longitude":-1.54898736,
                        "latitude":47.19552476
                    },
                    {
                        "nom":"Trois Chênes",
                        "longitude":-1.47403605,
                        "latitude":47.25818959
                    },
                    {
                        "nom":"Trois Chênes",
                        "longitude":-1.47453446,
                        "latitude":47.25961410
                    },
                    {
                        "nom":"Tredégar",
                        "longitude":-1.60487941,
                        "latitude":47.25469292
                    },
                    {
                        "nom":"Tredégar",
                        "longitude":-1.60369180,
                        "latitude":47.25473351
                    },
                    {
                        "nom":"Tredégar",
                        "longitude":-1.60456251,
                        "latitude":47.25398321
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.74466578,
                        "latitude":47.19686485
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.74452027,
                        "latitude":47.19668985
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.47195777,
                        "latitude":47.21601944
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.47183229,
                        "latitude":47.21611364
                    },
                    {
                        "nom":"Tour Eiffel",
                        "longitude":-1.75479809,
                        "latitude":47.19974826
                    },
                    {
                        "nom":"Tour Eiffel",
                        "longitude":-1.75425712,
                        "latitude":47.19958731
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.60813792,
                        "latitude":47.21459080
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.60813792,
                        "latitude":47.21459080
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.60557305,
                        "latitude":47.21386794
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.60663452,
                        "latitude":47.21392169
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.60668087,
                        "latitude":47.21455059
                    },
                    {
                        "nom":"Tertre",
                        "longitude":-1.60641716,
                        "latitude":47.21455962
                    },
                    {
                        "nom":"Terray",
                        "longitude":-1.52728517,
                        "latitude":47.25182472
                    },
                    {
                        "nom":"Terray",
                        "longitude":-1.52673138,
                        "latitude":47.25148302
                    },
                    {
                        "nom":"Théatre",
                        "longitude":-1.72878494,
                        "latitude":47.21571204
                    },
                    {
                        "nom":"Théatre",
                        "longitude":-1.72870770,
                        "latitude":47.21643537
                    },
                    {
                        "nom":"Centre de Thouaré-sur-Loire",
                        "longitude":-1.44142385,
                        "latitude":47.26655719
                    },
                    {
                        "nom":"Centre de Thouaré-sur-Loire",
                        "longitude":-1.44142385,
                        "latitude":47.26655719
                    },
                    {
                        "nom":"Centre de Thouaré-sur-Loire",
                        "longitude":-1.44118525,
                        "latitude":47.26692524
                    },
                    {
                        "nom":"Thébaudières",
                        "longitude":-1.60611664,
                        "latitude":47.24456294
                    },
                    {
                        "nom":"Thébaudières",
                        "longitude":-1.60686185,
                        "latitude":47.24390698
                    },
                    {
                        "nom":"Tillay",
                        "longitude":-1.60654038,
                        "latitude":47.22878637
                    },
                    {
                        "nom":"Tillay",
                        "longitude":-1.60627659,
                        "latitude":47.22879540
                    },
                    {
                        "nom":"Toutes Joies",
                        "longitude":-1.57292964,
                        "latitude":47.22119467
                    },
                    {
                        "nom":"Télindière",
                        "longitude":-1.73721646,
                        "latitude":47.19622776
                    },
                    {
                        "nom":"Télindière",
                        "longitude":-1.73707782,
                        "latitude":47.19614258
                    },
                    {
                        "nom":"Talensac",
                        "longitude":-1.55603622,
                        "latitude":47.22158633
                    },
                    {
                        "nom":"Talensac",
                        "longitude":-1.55576593,
                        "latitude":47.22150539
                    },
                    {
                        "nom":"Trois Métairies",
                        "longitude":-1.50163238,
                        "latitude":47.17639742
                    },
                    {
                        "nom":"Trois Métairies",
                        "longitude":-1.50204702,
                        "latitude":47.17665379
                    },
                    {
                        "nom":"Tourmaline",
                        "longitude":-1.63270209,
                        "latitude":47.22140335
                    },
                    {
                        "nom":"Tourmaline",
                        "longitude":-1.63257689,
                        "latitude":47.22149773
                    },
                    {
                        "nom":"Trois Moulins",
                        "longitude":-1.54444757,
                        "latitude":47.17847503
                    },
                    {
                        "nom":"Trois Moulins",
                        "longitude":-1.54415798,
                        "latitude":47.17812452
                    },
                    {
                        "nom":"Trémissinière",
                        "longitude":-1.54428528,
                        "latitude":47.23819442
                    },
                    {
                        "nom":"Trémissinière",
                        "longitude":-1.54378367,
                        "latitude":47.23857157
                    },
                    {
                        "nom":"Terre Neuvas",
                        "longitude":-1.59031016,
                        "latitude":47.18601729
                    },
                    {
                        "nom":"Terre Neuvas",
                        "longitude":-1.59135787,
                        "latitude":47.18589150
                    },
                    {
                        "nom":"Tortière",
                        "longitude":-1.54962352,
                        "latitude":47.23522257
                    },
                    {
                        "nom":"Tortière",
                        "longitude":-1.54974890,
                        "latitude":47.23512827
                    },
                    {
                        "nom":"Terpsichore",
                        "longitude":-1.50250131,
                        "latitude":47.28246303
                    },
                    {
                        "nom":"Terpsichore",
                        "longitude":-1.50155772,
                        "latitude":47.28222424
                    },
                    {
                        "nom":"Tripode",
                        "longitude":-1.54067149,
                        "latitude":47.20841422
                    },
                    {
                        "nom":"Tripode",
                        "longitude":-1.54080334,
                        "latitude":47.20840978
                    },
                    {
                        "nom":"Tréca",
                        "longitude":-1.50149834,
                        "latitude":47.25007399
                    },
                    {
                        "nom":"Tréca",
                        "longitude":-1.50137929,
                        "latitude":47.25025808
                    },
                    {
                        "nom":"Trianon",
                        "longitude":-1.43038312,
                        "latitude":47.27322232
                    },
                    {
                        "nom":"Tribunes",
                        "longitude":-1.56849431,
                        "latitude":47.24917570
                    },
                    {
                        "nom":"Tribunes",
                        "longitude":-1.56965489,
                        "latitude":47.25057746
                    },
                    {
                        "nom":"Tribunes",
                        "longitude":-1.56834853,
                        "latitude":47.25080184
                    },
                    {
                        "nom":"Trocardière",
                        "longitude":-1.57002237,
                        "latitude":47.18319454
                    },
                    {
                        "nom":"Trocardière",
                        "longitude":-1.57002237,
                        "latitude":47.18319454
                    },
                    {
                        "nom":"Terres Quartières",
                        "longitude":-1.65923196,
                        "latitude":47.13479051
                    },
                    {
                        "nom":"Terres Quartières",
                        "longitude":-1.65950308,
                        "latitude":47.13470903
                    },
                    {
                        "nom":"Trentemoult",
                        "longitude":-1.58291617,
                        "latitude":47.19149315
                    },
                    {
                        "nom":"Trentemoult",
                        "longitude":-1.58149265,
                        "latitude":47.19190185
                    },
                    {
                        "nom":"Trentemoult",
                        "longitude":-1.58277779,
                        "latitude":47.19140779
                    },
                    {
                        "nom":"Trentemoult",
                        "longitude":-1.58162446,
                        "latitude":47.19189737
                    },
                    {
                        "nom":"Touzelles",
                        "longitude":-1.46624635,
                        "latitude":47.20819254
                    },
                    {
                        "nom":"Touzelles",
                        "longitude":-1.46653560,
                        "latitude":47.20854324
                    },
                    {
                        "nom":"Usine Brûlée",
                        "longitude":-1.63206121,
                        "latitude":47.19494446
                    },
                    {
                        "nom":"Usine Brûlée",
                        "longitude":-1.63205455,
                        "latitude":47.19485462
                    },
                    {
                        "nom":"Usine à gaz",
                        "longitude":-1.62054080,
                        "latitude":47.19462022
                    },
                    {
                        "nom":"Usine à gaz",
                        "longitude":-1.61898568,
                        "latitude":47.19503391
                    },
                    {
                        "nom":"Utrillo",
                        "longitude":-1.56336120,
                        "latitude":47.18242954
                    },
                    {
                        "nom":"Utrillo",
                        "longitude":-1.56346680,
                        "latitude":47.18206569
                    },
                    {
                        "nom":"Vaillant",
                        "longitude":-1.67113033,
                        "latitude":47.19962676
                    },
                    {
                        "nom":"Vaillant",
                        "longitude":-1.67113033,
                        "latitude":47.19962676
                    },
                    {
                        "nom":"Vallée",
                        "longitude":-1.57668761,
                        "latitude":47.25087942
                    },
                    {
                        "nom":"Vallée",
                        "longitude":-1.57678668,
                        "latitude":47.25042573
                    },
                    {
                        "nom":"Vincent Auriol",
                        "longitude":-1.59214384,
                        "latitude":47.22180226
                    },
                    {
                        "nom":"Vincent Auriol",
                        "longitude":-1.59227572,
                        "latitude":47.22179776
                    },
                    {
                        "nom":"Vaucanson",
                        "longitude":-1.58157525,
                        "latitude":47.21288501
                    },
                    {
                        "nom":"Vaucanson",
                        "longitude":-1.58089624,
                        "latitude":47.21263789
                    },
                    {
                        "nom":"Val du Cens",
                        "longitude":-1.66716656,
                        "latitude":47.26146439
                    },
                    {
                        "nom":"Val du Cens",
                        "longitude":-1.66515341,
                        "latitude":47.26108390
                    },
                    {
                        "nom":"Vieux Doulon",
                        "longitude":-1.50402844,
                        "latitude":47.23557967
                    },
                    {
                        "nom":"Vieux Doulon",
                        "longitude":-1.50415391,
                        "latitude":47.23548543
                    },
                    {
                        "nom":"Ville-au-Denis",
                        "longitude":-1.62753008,
                        "latitude":47.16231434
                    },
                    {
                        "nom":"Ville-au-Denis",
                        "longitude":-1.62702977,
                        "latitude":47.16269186
                    },
                    {
                        "nom":"Verdure",
                        "longitude":-1.65340905,
                        "latitude":47.19591839
                    },
                    {
                        "nom":"Verdure",
                        "longitude":-1.65324375,
                        "latitude":47.19547375
                    },
                    {
                        "nom":"Véga",
                        "longitude":-1.46355268,
                        "latitude":47.28005981
                    },
                    {
                        "nom":"Véga",
                        "longitude":-1.46445133,
                        "latitude":47.27966997
                    },
                    {
                        "nom":"Verger",
                        "longitude":-1.49704348,
                        "latitude":47.29309160
                    },
                    {
                        "nom":"Verger",
                        "longitude":-1.49717554,
                        "latitude":47.29308722
                    },
                    {
                        "nom":"Verrière",
                        "longitude":-1.57052304,
                        "latitude":47.27693745
                    },
                    {
                        "nom":"Verrière",
                        "longitude":-1.57077393,
                        "latitude":47.27674882
                    },
                    {
                        "nom":"Vesprées",
                        "longitude":-1.50289758,
                        "latitude":47.24561434
                    },
                    {
                        "nom":"Vesprées",
                        "longitude":-1.50264660,
                        "latitude":47.24580282
                    },
                    {
                        "nom":"Verlaine",
                        "longitude":-1.59488151,
                        "latitude":47.23386817
                    },
                    {
                        "nom":"Verlaine",
                        "longitude":-1.59457807,
                        "latitude":47.23333811
                    },
                    {
                        "nom":"Village Expo",
                        "longitude":-1.61477056,
                        "latitude":47.21490398
                    },
                    {
                        "nom":"Village Expo",
                        "longitude":-1.61515286,
                        "latitude":47.21471073
                    },
                    {
                        "nom":"Vieux Four",
                        "longitude":-1.70138463,
                        "latitude":47.19073369
                    },
                    {
                        "nom":"Vieux Four",
                        "longitude":-1.70087103,
                        "latitude":47.19093184
                    },
                    {
                        "nom":"Vincent Gâche",
                        "longitude":-1.54833304,
                        "latitude":47.20653496
                    },
                    {
                        "nom":"Vincent Gâche",
                        "longitude":-1.54833304,
                        "latitude":47.20653496
                    },
                    {
                        "nom":"Vincent Gâche",
                        "longitude":-1.54737753,
                        "latitude":47.20611684
                    },
                    {
                        "nom":"Vincent Gâche",
                        "longitude":-1.54815553,
                        "latitude":47.20591048
                    },
                    {
                        "nom":"Viarme-Talensac",
                        "longitude":-1.56263013,
                        "latitude":47.22136344
                    },
                    {
                        "nom":"Viarme-Talensac",
                        "longitude":-1.56276201,
                        "latitude":47.22135898
                    },
                    {
                        "nom":"Violettes",
                        "longitude":-1.50887313,
                        "latitude":47.19813167
                    },
                    {
                        "nom":"Violettes",
                        "longitude":-1.50922341,
                        "latitude":47.19748953
                    },
                    {
                        "nom":"Villeneuve",
                        "longitude":-1.51320907,
                        "latitude":47.19411407
                    },
                    {
                        "nom":"Villeneuve",
                        "longitude":-1.51385522,
                        "latitude":47.19391235
                    },
                    {
                        "nom":"Vison",
                        "longitude":-1.50967453,
                        "latitude":47.24421766
                    },
                    {
                        "nom":"Vison",
                        "longitude":-1.50982586,
                        "latitude":47.24448280
                    },
                    {
                        "nom":"Viviers",
                        "longitude":-1.48488496,
                        "latitude":47.17308163
                    },
                    {
                        "nom":"Viviers",
                        "longitude":-1.48516132,
                        "latitude":47.17325259
                    },
                    {
                        "nom":"Val d'Or",
                        "longitude":-1.58279107,
                        "latitude":47.24391709
                    },
                    {
                        "nom":"Val d'Or",
                        "longitude":-1.58192022,
                        "latitude":47.24466723
                    },
                    {
                        "nom":"Vrière",
                        "longitude":-1.55629479,
                        "latitude":47.28327261
                    },
                    {
                        "nom":"Vrière",
                        "longitude":-1.55607767,
                        "latitude":47.28210909
                    },
                    {
                        "nom":"Vincent Scotto",
                        "longitude":-1.56603245,
                        "latitude":47.25169084
                    },
                    {
                        "nom":"Vincent Scotto",
                        "longitude":-1.56603245,
                        "latitude":47.25169084
                    },
                    {
                        "nom":"Vincent Scotto",
                        "longitude":-1.56608492,
                        "latitude":47.25240959
                    },
                    {
                        "nom":"Vertonne",
                        "longitude":-1.49433357,
                        "latitude":47.18699783
                    },
                    {
                        "nom":"Vertonne",
                        "longitude":-1.49390603,
                        "latitude":47.18656173
                    },
                    {
                        "nom":"Vertou",
                        "longitude":-1.47128903,
                        "latitude":47.16947918
                    },
                    {
                        "nom":"Vertou",
                        "longitude":-1.47111255,
                        "latitude":47.16885458
                    },
                    {
                        "nom":"Wattignies",
                        "longitude":-1.54643001,
                        "latitude":47.20398718
                    },
                    {
                        "nom":"Wattignies",
                        "longitude":-1.54643001,
                        "latitude":47.20398718
                    },
                    {
                        "nom":"Walt Disney",
                        "longitude":-1.48997084,
                        "latitude":47.19434794
                    },
                    {
                        "nom":"Walt Disney",
                        "longitude":-1.49000938,
                        "latitude":47.19488704
                    },
                    {
                        "nom":"Croix Bonneau",
                        "longitude":-1.59547893,
                        "latitude":47.20961928
                    },
                    {
                        "nom":"Croix Bonneau",
                        "longitude":-1.59547893,
                        "latitude":47.20961928
                    },
                    {
                        "nom":"Croix Bonneau",
                        "longitude":-1.59632944,
                        "latitude":47.21040088
                    },
                    {
                        "nom":"Croix Bonneau",
                        "longitude":-1.59633605,
                        "latitude":47.21049072
                    },
                    {
                        "nom":"Croix Bonneau",
                        "longitude":-1.59732477,
                        "latitude":47.20955628
                    },
                    {
                        "nom":"Croix Jeannette",
                        "longitude":-1.62368918,
                        "latitude":47.17109344
                    },
                    {
                        "nom":"Croix de Rezé",
                        "longitude":-1.55915929,
                        "latitude":47.17914917
                    },
                    {
                        "nom":"Croix de Rezé",
                        "longitude":-1.55891535,
                        "latitude":47.17942763
                    },
                    {
                        "nom":"Croix de Rezé",
                        "longitude":-1.55824994,
                        "latitude":47.17936007
                    },
                    {
                        "nom":"Croix de Rezé",
                        "longitude":-1.55896111,
                        "latitude":47.18005656
                    },
                    {
                        "nom":"Croisy",
                        "longitude":-1.62677049,
                        "latitude":47.25376253
                    },
                    {
                        "nom":"Croisy",
                        "longitude":-1.62634800,
                        "latitude":47.25341677
                    },
                    {
                        "nom":"Croix Truin",
                        "longitude":-1.71121179,
                        "latitude":47.19137959
                    },
                    {
                        "nom":"Croix Truin",
                        "longitude":-1.71173896,
                        "latitude":47.19136106
                    },
                    {
                        "nom":"Zac-des-Coteaux",
                        "longitude":-1.67882328,
                        "latitude":47.16477019
                    },
                    {
                        "nom":"Zénith",
                        "longitude":-1.62928013,
                        "latitude":47.23052822
                    },
                    {
                        "nom":"Zénith",
                        "longitude":-1.62941868,
                        "latitude":47.23061352
                    },
                    {
                        "nom":"Zimmer",
                        "longitude":-1.65168552,
                        "latitude":47.21002940
                    },
                    {
                        "nom":"Zimmer",
                        "longitude":-1.65169222,
                        "latitude":47.21011924
                    },
                    {
                        "nom":"Zola",
                        "longitude":-1.58600550,
                        "latitude":47.21381511
                    },
                    {
                        "nom":"Zola",
                        "longitude":-1.58468035,
                        "latitude":47.21377015
                    },
                    {
                        "nom":"Zola",
                        "longitude":-1.58694827,
                        "latitude":47.21405322
                    },
                    {
                        "nom":"Zola",
                        "longitude":-1.58622958,
                        "latitude":47.21506845
                    }
                ],
                commerces:[
                    {
                        "nom": "MC DONALDS",
                        "adresse": "CENTRE CIAL LECLERC PARC ACTIVITE LA FON CALIN",
                        "cp": "44190",
                        "ville": "CLISSON",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.192865,
                            "lon": -1.458315
                        }
                    },
                    {
                        "nom": "MC DONALDS",
                        "adresse": "CENTRE CIAL LECLERC",
                        "cp": "44150",
                        "ville": "ST GEREON",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.156457,
                            "lon": -1.524920
                        }
                    },
                    {
                        "nom": "SESAME",
                        "adresse": "86 RUE MARECHAL JOFFRE",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.220305,
                            "lon": -1.548862
                        }
                    },
                    {
                        "nom": "SUSHI SHOP",
                        "adresse": "3 RUE RACINE",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.213417,
                            "lon": -1.562318
                        }
                    },
                    {
                        "nom": "MC DONALDS",
                        "adresse": "3 RUE DE GEORGES",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.213815,
                            "lon": -1.557871
                        }
                    },
                    {
                        "nom": "MC DONALDS",
                        "adresse": "CENTRE CIAL LECLERC",
                        "cp": "44115",
                        "ville": "BASSE GOULAINE",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.160275,
                            "lon": -1.509043
                        }
                    },
                    {
                        "nom": "PIZZA SPRINT",
                        "adresse": "3 RUE DE LA GARE",
                        "cp": "44110",
                        "ville": "CHATEAUBRIANT",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.214104,
                            "lon": -1.581452
                        }
                    },
                    {
                        "nom": "SUBWAY",
                        "adresse": "BOULEVARD DE LA PRAIRIE",
                        "cp": "44150",
                        "ville": "ST GEREON",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.204760,
                            "lon": -1.557318
                        }
                    },
                    {
                        "nom": "MC DONALDS",
                        "adresse": "ROUTE DE SAINT NAZAIRE",
                        "cp": "44110",
                        "ville": "CHATEAUBRIANT",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.210716,
                            "lon": -1.615910
                        }
                    },
                    {
                        "nom": "LA TAVERNE DE MAITRE KANTER",
                        "adresse": "1 PLACE ROYALE",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 1,
                        "geoloc": {
                            "lat": 47.214640,
                            "lon": -1.557474
                        }
                    },
                    {
                        "nom": "QUICK",
                        "adresse": "PLACE GRASLIN",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.213435,
                            "lon": -1.561490
                        }
                    },
                    {
                        "nom": "DOMINO'S PIZZA",
                        "adresse": "31 PLACE VIARME",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.220008,
                            "lon": -1.561722
                        }
                    },
                    {
                        "nom": "SUBWAY",
                        "adresse": "23 RUE DE LA FOSSE",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.213123,
                            "lon": -1.558347
                        }
                    },
                    {
                        "nom": "FEEL JUICE",
                        "adresse": "11 RUE DES HALLES",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.215636,
                            "lon": -1.555390
                        }
                    },
                    {
                        "nom": "FRANCESCA",
                        "adresse": "3 RUE DE L ECHELLE",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.215312,
                            "lon": -1.558295
                        }
                    },
                    {
                        "nom": "L ATELIER DES CHEFS",
                        "adresse": "11 RUE DE LA CLAVURERIE",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.215703,
                            "lon": -1.556289
                        }
                    },
                    {
                        "nom": "SUBWAY",
                        "adresse": "44 RUE DU MARECHAL JOFFRE",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.221313,
                            "lon": -1.547509
                        }
                    },
                    {
                        "nom": "PIZZA SPRINT",
                        "adresse": "1 PLACE EMILE ZOLA",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.214113,
                            "lon": -1.585088
                        }
                    },
                    {
                        "nom": "SPEED BURGER",
                        "adresse": "182 RUE PAUL BELLAMY",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.231196,
                            "lon": -1.563716
                        }
                    },
                    {
                        "nom": "BASILIC CO",
                        "adresse": "16 RUE DE STRASBOURG",
                        "cp": "44000",
                        "ville": "NANTES",
                        "codeNap": "56.10C",
                        "client": 0,
                        "geoloc": {
                            "lat": 47.216388,
                            "lon": -1.551222
                        }
                    }
                ],
                flux:[
                    {
                        "iris":"0105",
                        "potentielDepense": {
                            "produits frais": 57605560,
                            "boissons et liquides": 16451478,
                            "pain et patisserie": 8644822,
                            "surgelés": 5799272,
                            "beauté": 16869438,
                            "enfants": 7274479,
                            "mode et vetements": 31397894,
                            "bijoux et accessoires" : 8187046,
                            "culture et loisirs" : 45523247,
                            "automobiles et véhicules": 83172019,
                            "restaurants": 34199529,
                            "cafés et discothèques": 6786222,
                            "santé optique": 31919152
                        },
                        "depenseMoyenne":  {
                            "produits frais": 705,
                            "boissons et liquides": 201,
                            "pain et patisserie": 105,
                            "surgelés": 71,
                            "beauté": 206,
                            "enfants": 89,
                            "mode et vetements": 384,
                            "bijoux et accessoires" : 100,
                            "culture et loisirs" : 557,
                            "automobiles et véhicules": 1018,
                            "restaurants": 419,
                            "cafés et discothèques": 83,
                            "santé optique": 390
                        }
                    },
                    {
                        "iris":"0106",
                        "potentielDepense": {
                            "produits frais": 96009265,
                            "boissons et liquides": 27419130,
                            "pain et patisserie": 14408038,
                            "surgelés": 9665454,
                            "beauté": 28115731,
                            "enfants": 12124132,
                            "mode et vetements": 52329824,
                            "bijoux et accessoires" : 13645077,
                            "culture et loisirs" : 75872079,
                            "automobiles et véhicules": 138620032,
                            "restaurants": 56999216,
                            "cafés et discothèques": 11310370,
                            "santé optique": 53198587
                        },
                        "depenseMoyenne":  {
                            "produits frais": 1175,
                            "boissons et liquides": 335,
                            "pain et patisserie": 176,
                            "surgelés": 118,
                            "beauté": 344,
                            "enfants": 148,
                            "mode et vetements": 640,
                            "bijoux et accessoires" : 167,
                            "culture et loisirs" : 929,
                            "automobiles et véhicules": 1697,
                            "restaurants": 698,
                            "cafés et discothèques": 138,
                            "santé optique": 651
                        }
                    },
                    {
                        "iris":"0404",
                        "potentielDepense": {
                            "produits frais": 38403706,
                            "boissons et liquides": 10967652,
                            "pain et patisserie": 5763215,
                            "surgelés": 3866181,
                            "beauté": 11246292,
                            "enfants": 4849653,
                            "mode et vetements": 20931929,
                            "bijoux et accessoires" : 5458030,
                            "culture et loisirs" : 30348831,
                            "automobiles et véhicules": 55448012,
                            "restaurants": 22799686,
                            "cafés et discothèques": 4524148,
                            "santé optique": 21279435
                        },
                        "depenseMoyenne":  {
                            "produits frais": 470,
                            "boissons et liquides": 134,
                            "pain et patisserie": 70,
                            "surgelés": 47,
                            "beauté": 137,
                            "enfants": 59,
                            "mode et vetements": 256,
                            "bijoux et accessoires" : 67,
                            "culture et loisirs" : 371,
                            "automobiles et véhicules": 678,
                            "restaurants": 279,
                            "cafés et discothèques": 55,
                            "santé optique": 260
                        }
                    }
                ],
                imageSource : 'assets/images/todo-avant.png'

                /*iris:[],
                flux:[],
                commerces:[],
                clients:[],
                population:[],
                transports:[]*/
            };


            return _this._data;
        }
    ]);


}());

