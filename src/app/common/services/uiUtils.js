﻿(function () {
    var mod = angular.module('jsTemplateApp.common.services.uiUtils', []);

    mod.factory('uiUtils', function ($window, $compile, $location, $log, utils, constants) {

        function navigateExtUrl(url) {
            if (url) {
                // on cordova you have a file document url, so no http ...
                var isCordova = $window.document.URL.indexOf('http://') === -1 && $window.document.URL.indexOf('https://') === -1;

                try {
                    if (isCordova) {
                        $window.open(url, '_system');
                    }
                    else {
                        $window.open(url, '_blank');
                    }
                }
                catch (error) {
                    $log.error('Open ext try failed', error);
                }
            }

            return false;
        }
        function goBack() {
            if (constants.logOptions.logNavigation) {
                $log.info("Navigating back from.", $location.url());
            }
            $window.history.back();
        }
        // central hub for $location path navigation
        function navigateToPath(path) {
            if (constants.logOptions.logNavigation) {
                $log.info("Navigating from " + $location.url() + " to " + path);
            }

            $location.url(path);
        }
        // central hub for $location path navigation
        function navigateHome() {
            if (constants.logOptions.logNavigation) {
                $log.info("Navigating from " + $location.url() + " to HOME");
            }

            $location.url(constants.navigationUrl.home);
        }

        return {
            goBack: goBack,
            navigateHome: navigateHome,
            navigateToPath: navigateToPath,
            navigateExtUrl: navigateExtUrl
        };

    });// end of factory

}());

