﻿
angular.module('jsTemplateApp.common.services', [
				'jsTemplateApp.common.services.constants',
				'jsTemplateApp.common.services.utils',
                'jsTemplateApp.common.services.uiUtils',
				'jsTemplateApp.common.services.globalContext'
]);
