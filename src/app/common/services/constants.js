﻿
    /**
     * Created by sebastien on 02/11/15.
     */
    (function () {
        var mod = angular.module('jsTemplateApp.common.services.constants', []);

        function get() {
            return 'test';
        }


        mod.constant('constants', {

           navigationUrl:{
               home: '/home',
               login: '/login',
               todo: '/todo'
           }

        });

    }());