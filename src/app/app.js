﻿//"use strict";

(function () {


    var app = angular.module('jsTemplateApp', [
		'ui.router',
		'ui.mask',
		'ui.bootstrap',
		'ui.validate',
		'ngMessages', //
		'webStorageModule', // module to store stuff in session and or local storage
        'cgBusy', // directive for the busy indication

		//'apiMock',
		//'angularMoment',
		
		'templates-app',
		'jsTemplateApp.common.services',
		'jsTemplateApp.controller',
		'jsTemplateApp.home.controller',
        'jsTemplateApp.login.controller',
        'jsTemplateApp.todo.controller',
        'jsTemplateApp.admin1.controller',
        'jsTemplateApp.admin2.controller',
		'bpatl.directives.numbers',
        'bpatl.directives.keybinding'
    ]);

    app.config(function myAppConfig($stateProvider, $urlRouterProvider, $compileProvider, $provide, constants) {

        $urlRouterProvider.otherwise('/login');

    });

    // run application
    app.run(function ($log, $httpBackend, constants) {
    });

}());

