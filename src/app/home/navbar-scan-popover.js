
/**
 * Created by sebastien on 15/12/15.
 */
(function () {
    var mod = angular.module('jsTemplateApp.directives.navbarScanPopover', []);
    /*
     This directive shows the popover of all the missing docs to scan
     ugly bug work around: https://github.com/angular/angular.js/issues/2151
     popTemplate must be directly injected
     */
    mod.directive("navbarScanPopover", function ($modal, $compile, $templateCache, $log, globalcontext, uiUtils, utils) {

        var popRepeatTemplate = '<ul class="nav nav-pills nav-stacked"><li ng-repeat="item in items"><a href="" data-ng-click="scanDoc(item)" onclick="alert(\'hi\')">{{item.LIEVAG}}</a></li></ul>';
        /**
         *  open a modal window to rescan the document
         *  @param pScannedDoc: IScannedDocument
         */
        function openScanModal(pScannedDoc) {

            var modal = $modal.open({
                templateUrl: 'home/modif-scan.tpl.html',
                controller: 'ModifScanCtrl',
                // we pass the alert of missing document to the modal
                resolve: {
                    scannedDoc: function () {
                        return pScannedDoc;
                    }
                }

            });

            // when the modal is closed
            modal.result.then(function () {

            });

        }

        return {
            restrict: 'AE',
            //templateUrl: 'clientInfo/directives/navbar-scan-popover.tpl.html',
            template:'<span class="popover-link bplibicon bplibicon-medium bplibicon-scan" data-toggle="popover" tabindex="0" data-trigger="focus"><span class="sr-only">Scan</span><span class="popover-options"></span></span>',

            link: function (scope, elt, attr, ctrl) {
                // inject repeater element into directives pop element
                var $popOptions = elt.find('.popover-options');
                if ($popOptions) {
                    // create popover
                    var $popContainer = $(elt).find('.popover-link');
                    if ($popOptions && $popContainer) {

                        $popContainer.click(function (e) {
                            this.focus();
                            //e.preventDefault();
                        });

                        $popContainer.popover({
                            placement: 'bottom',
                            //container: 'body',
                            // add popovers to view not to body
                            // for some mysterious reason it does not find the body element, so we use the class selector on the body.
                            container: '.navbar-top',
                            content: function () {
                                //var h = $popOptions.html();
                                var h = '<ul class="nav nav-pills nav-stacked"><li data-ng-repeat="item in items "><a href="" data-ng-click="scanDoc(item)"> {{ item.LIEVAG }} </a></li></ul>';
                                var lnFunc = $compile(h);
                                var t = lnFunc(scope);
                                scope.$apply();
                                return t;
                            },
                            //content: popOverContent,
                            html: true,
                            //template: template,
                            delay: { "show": 100, "hide": 200 }
                        });
                    }

                }
            },

            controller: ['$scope', '$log', 'utils', function ($scope, $log, utils) {
                // click on scan document link
                /**
                 * @param alertDoc: <IAlertDoc>
                 */
                $scope.scanDoc = function (alertDoc) {
                    $log.info("enter navbarScanPopover scanDoc. ");

                    if ($scope.scanneddocs) {

                        var sd = utils.getFirstItem($scope.scanneddocs, function (sd) {
                            return sd.alertCode === alertDoc.CTEVAD;
                        });
                        // if the missing document is already in the scanned documents list, we open the modif-scan modal
                        if (sd) {
                            openScanModal(sd);
                        }
                        // otherwise we try to capture a photo directly
                        else {
                            /*sd = clientHelperService.capturePhotoB64(alertDoc.CTEVAD, alertDoc.LIEVAG, $scope.scanneddocs);
                            if (sd) {
                                openScanModal(sd);
                            }*/
                        }
                    }
                };

            }],
            scope: {
                items: '=',
                scanneddocs: '='
            }
        };
    });

}());