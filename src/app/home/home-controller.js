﻿(function () {
    var mod = angular.module('jsTemplateApp.home.controller',
        [ ]);

    mod.config(function config($stateProvider, constants) {

        $stateProvider.state('home', {
            url: constants.navigationUrl.home,
            views: {
                "main": {
                    controller: 'HomeController',
                    templateUrl: 'home/home.tpl.html'
                }
            },
            data: { pageTitle: 'Accueil jsTemplateApp Administration' }
        });
    });

    mod.controller('HomeController', function ($scope, $location, $log, $modal, $q, $timeout, uiUtils, constants, globalContext) {

        $scope.global = globalContext;
        $scope.selectedIris = undefined;
        $scope.filters= {
            commerces: false,
            transports: false,
            population: false,
            flux: false
        };

        $scope.showCommerce = false;

        $scope.showNav = false;

        $scope.imgSrcNav = 'assets/images/piaf-pictos_23.png';

        $scope.showNavFunc = function(){
            $scope.showNav = ! $scope.showNav;
            if($scope.showNav === true){
                $scope.imgSrcNav = 'assets/images/piaf-pictos_26.png';
            }
            else{
                $scope.imgSrcNav = 'assets/images/piaf-pictos_23.png';
            }
        };

        $scope.navToTodo = function(){
            $location.path('/todo');
        };

        $scope.navLogin = function(){
            $location.path('/login');
        };

        $scope.clientClass='fa-arrow-circle-o-down';
        $scope.projetClass='fa-arrow-circle-o-down';
        $scope.cartoClass='fa-arrow-circle-o-up';
        $scope.pieceJointeClass='fa-arrow-circle-o-down';

        $scope.showPhoto = false;

        $scope.clientCollapse = 'collapse';

        $scope.clickPJ = function(){
            if ($scope.pieceJointeClass==='fa-arrow-circle-o-down'){
                $scope.pieceJointeClass='fa-arrow-circle-o-up';
            }
            else{
                $scope.pieceJointeClass='fa-arrow-circle-o-down';
            }
        };

        $scope.clickCarto = function(){
            if ($scope.cartoClass ==='fa-arrow-circle-o-down'){
                $scope.cartoClass ='fa-arrow-circle-o-up';
            }
            else{
                $scope.cartoClass ='fa-arrow-circle-o-down';
            }
        };

        $scope.clickProjet = function(){
            if ($scope.projetClass ==='fa-arrow-circle-o-down'){
                $scope.projetClass ='fa-arrow-circle-o-up';
            }
            else{
                $scope.projetClass ='fa-arrow-circle-o-down';
            }
        };

        $scope.addProspect = function(){
            $("#dataClient").show();
            $scope.clientClass='fa-arrow-circle-o-up';
            $("#raisonSociale").val('');
            $("#adresse").val('');
            $("#dateER").val('');
            $("#telephone").val('');
            $("#mail").val('');
            $("#noteMcDo").val('');
            $("#activite").val('');
            $scope.clientCollapse = '';
        };

        $(document).ready(function () {
            //called when key is pressed in textbox
            $("#matricule").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
        });

        /*****************
        * switch buttons
        ******************/

        $(document).ready(function () {

            var position = 30;

            $('.ui-slider-control')
                .UISlider({
                    min: 1,
                    max: 100,
                    value: position,
                    smooth: false
                })
                .on('change', function (event, value) {

                    $('#value').text(value);

                });

            $('#value').text(position);

        });



        $(".switch").checkable();
        $("[data-name=chalandise]").checkable("check", true|false);
        $("[data-name=population]").checkable("check", true|false);
        $("[data-name=commerces]").checkable("check", true|false);
        $("[data-name=transports]").checkable("check", true|false);
        $("[data-name=client]").checkable("check", true|false);

        $scope.checkSelectedFilters = function(){
            var cntChecked = 0;
            if (document.getElementById("chalandise").className.indexOf("checked") > -1){
                $scope.activeFilter('flux');
            }
            if (document.getElementById("population").className.indexOf("checked") > -1){
                $scope.activeFilter('population');
            }
            if (document.getElementById("commerces").className.indexOf("checked") > -1){
                $scope.activeFilter('commerces');
            }
            if (document.getElementById("transports").className.indexOf("checked") > -1){
                $scope.activeFilter('transports');
            }
        };

        /*****************
        * Initialization MAP
        ******************/

        var LayerIris = new L.LayerGroup();

        var LayerBien = new L.LayerGroup();
        var LayerCommerces = new L.LayerGroup();
        var LayerActual = new L.LayerGroup();

        var map = L.map('map', { zoomControl: true, minZoom: 14, maxZoom: 20 }).setView([47.218371047, -1.5536210], 14);

        map.touchZoom.disable();
        map.doubleClickZoom.disable();
        map.scrollWheelZoom.disable();
        map.boxZoom.disable();
        map.keyboard.disable();
        $(".leaflet-control-zoom").css("visibility", "visible");

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);


        /*
         get the color from the property and the number of filters selected
         */
        function getColor(d) {
            return d > (0.9) ? '#BD0026' :
                    d > (0.8) ? '#BD0026' :
                    d > (0.7) ? '#BD0026' :
                    d > (0.6) ? '#BD0026' :
                    d > (0.5) ? '#BD0026' :
                    d > (0.4) ? '#E31A1C' :
                    d > (0.3) ? '#FC4E2A' :
                    d > (0.2) ? '#FD8D3C' :
                    d > (0.1) ? '#FEB24C' :
                    d > (0) ? '#FED976' :
                '#FFEDA0';
        }

        function style(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.7,
                fillColor: getColor(feature.properties.rank)
            };
        }

        function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
                weight: 5,
                color: '#666',
                dashArray: '',
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera) {
                layer.bringToFront();
            }

            //info.update(layer.feature.properties);
        }

        var geojson;

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
            //info.update();
        }

        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
            $scope.$apply(function () {
                //$scope.showDetail = true;
                $scope.selectedIris = e.target.feature;
            });
        }

        function onEachFeature(feature, layer) {
            //layer.mainScope = $scope;
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            });
        }

        function getMainLayer(){
            if(typeof geojson != 'undefined'){
                LayerIris.removeLayer(geojson);
                geojson = null;
            }

            geojson = L.geoJson($scope.global.iris, {
                style: style,
                onEachFeature: onEachFeature
            }).addTo(map);
            LayerIris.addLayer(geojson);
            map.addLayer(LayerIris);
        }
        getMainLayer();


        /*det current location*/
        function onLocationFound(e) {
            var radius = e.accuracy / 2;

            /*var marker = L.marker(e.latlng).addTo(map)
                .bindPopup("Votre position actuelle");
            LayerActual.addLayer(marker);
            globalContext.showActualLocation = true;
            map.addLayer(LayerActual);
            L.circle(e.latlng, radius).addTo(map);*/

            var homeMarker = L.ExtraMarkers.icon({
                icon: 'fa-home',//See glyphicons or font-awesome (must include prefix)
                markerColor: 'blue',//'red', 'orange-dark', 'orange', 'yellow', 'blue-dark', 'cyan', 'purple', 'violet', 'pink', 'green-dark', 'green', 'green-light', 'black', 'white'
                shape: 'circle',//'circle', 'square', 'star', 'penta'
                prefix: 'fa',//'fa' for font-awesome or 'glyphicon' for bootstrap 3
                iconColor: 'white'//'white', 'black' or css code (hex, rgba etc)
            });
            //show the marker
            /*var marker = L.marker(e.latlng, homeMarker).addTo(map)
                .bindPopup("Votre position actuelle");*/
            var marker = L.marker([47.221652, -1.5575258], {icon: homeMarker}).addTo(map)
                .bindPopup("Votre position actuelle");

            globalContext.showHome = true;
            LayerActual.addLayer(marker);
            map.addLayer(LayerActual);
        }

        map.on('locationfound', onLocationFound);

        function onLocationError(e) {
            alert(e.message);
        }

        map.on('locationerror', onLocationError);

        /*
         * Add button to localise me Now
         * */
        L.easyButton('<i class="fa fa-map-marker" style="font-size: 2em;color:blue;" title="Votre localisation actuelle"></i>', function () {
            if (typeof globalContext.showActualLocation === 'undefined') {
                //map.locate({setView: false, maxZoom: 12});
                var homeMarker = L.ExtraMarkers.icon({
                    icon: 'fa-home',//See glyphicons or font-awesome (must include prefix)
                    markerColor: 'blue',//'red', 'orange-dark', 'orange', 'yellow', 'blue-dark', 'cyan', 'purple', 'violet', 'pink', 'green-dark', 'green', 'green-light', 'black', 'white'
                    shape: 'circle',//'circle', 'square', 'star', 'penta'
                    prefix: 'fa',//'fa' for font-awesome or 'glyphicon' for bootstrap 3
                    iconColor: 'white'//'white', 'black' or css code (hex, rgba etc)
                });
                //show the marker
                /*var marker = L.marker(e.latlng, homeMarker).addTo(map)
                 .bindPopup("Votre position actuelle");*/
                var marker = L.marker([47.221652, -1.5575258], {icon: homeMarker}).addTo(map)
                    .bindPopup("Votre position actuelle");

                globalContext.showHome = true;
                LayerActual.addLayer(marker);
                map.addLayer(LayerActual);
            }
            else if (globalContext.showActualLocation === false){
                globalContext.showActualLocation = true;
                //showMarker
                map.addLayer(LayerActual);
            }
            else{
                globalContext.showActualLocation = false;
                //hideMarker
                map.removeLayer(LayerActual);
            }
        }).addTo(map);


        /*****************
        * scope FUNCTIONS Declaration
        ******************/

        function initScope() {



            /*if (!$scope.rights.canAccessPage) {
                uiUtils.navigateToPath(constants.navigationUrl.accessDenied);
            }*/
        }

        geocoder = new google.maps.Geocoder();
        $scope.location = {};

        $scope.geocodeAddress = function(){

            var adresse = document.getElementById('adresseProjet').value;
            globalContext.homeAdress = adresse;

            geocoder.geocode({'address': adresse}, function(results, status){
                if (status === google.maps.GeocoderStatus.OK){
                    var lat = results[0].geometry.location.lat();
                    var lon = results[0].geometry.location.lng();
                    var location = {lat: lat, lon: lon};
                    //save in globalcontext the user home

                    var homeMarker = L.ExtraMarkers.icon({
                        icon: 'fa-eur',//See glyphicons or font-awesome (must include prefix)
                        markerColor: 'orange',//'red', 'orange-dark', 'orange', 'yellow', 'blue-dark', 'cyan', 'purple', 'violet', 'pink', 'green-dark', 'green', 'green-light', 'black', 'white'
                        shape: 'circle',//'circle', 'square', 'star', 'penta'
                        prefix: 'fa',//'fa' for font-awesome or 'glyphicon' for bootstrap 3
                        iconColor: 'white'//'white', 'black' or css code (hex, rgba etc)
                    });
                    //show the marker
                    var marker = L.marker(location, {icon: homeMarker}).addTo(map)
                        .bindPopup("Bien à financer");
                    globalContext.showHome = true;
                    LayerBien.addLayer(marker);
                    map.addLayer(LayerBien);
                }
            });
        };

        function inside(point, vs) {
            var x = point[0], y = point[1];
            var inside2 = false;
            for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                var xi = vs[i][0], yi = vs[i][1];
                var xj = vs[j][0], yj = vs[j][1];

                var intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) {inside2 = !inside2;}
            }
            return inside2;
        }

        var attachMarkersCommerce = function(quartier, cnt, data){
            data.forEach(function(elt){
                if (inside([ elt.geoloc.lon, elt.geoloc.lat ], quartier.geometry.coordinates[0]) === true){
                    $scope.context.arrayNotesCommerce[cnt]++;
                }
            });
        };

        var attachMarkersPopulation = function(quartier, cnt, data){
            data.forEach(function(elt){
                if (elt.IRIS.toString().substring(elt.IRIS.toString().length - 4) === quartier.properties.IRIS){
                    $scope.context.arrayNotesPopulation[cnt] += elt.P12_POP;
                }
            });
        };

        var attachMarkersTransport = function(quartier, cnt, data){
            data.forEach(function(elt){
                if (inside([ elt.longitude, elt.latitude ], quartier.geometry.coordinates[0]) === true){
                    $scope.context.arrayNotesTransport[cnt]++;
                }
            });
        };

        var attachMarkersFlux = function(quartier, cnt, data){
            data.forEach(function(elt){
                if (elt.iris === quartier.properties.IRIS){
                    $scope.context.arrayNotesFlux[cnt] += elt.potentielDepense.restaurants;
                }
            });
        };

        /*calcul rank for each iris*/
        $scope.engine=function(){
            var myArray = ["0", "1", "2"];
            var cpt = 0;

            $scope.context.arrayNotesCommerce = [0,0,0];
            $scope.context.arrayNotesPopulation = [0,0,0];
            $scope.context.arrayNotesTransport = [0,0,0];
            $scope.context.arrayNotesFlux = [0,0,0];

            globalContext.iris.features.forEach(function(theIris){
                myArray[cpt] = theIris.properties.IRIS;
                cpt++;
            });

            var cnt = 0;

            globalContext.iris.features.forEach(function(theIris){
                if ($scope.filters.commerces) {
                    attachMarkersCommerce(theIris, cnt, globalContext.commerces);
                }
                if ($scope.filters.population) {
                    attachMarkersPopulation(theIris, cnt, globalContext.population);
                }
                if ($scope.filters.transports) {
                    attachMarkersTransport(theIris, cnt, globalContext.transports);
                }
                if ($scope.filters.flux) {
                    attachMarkersFlux(theIris, cnt, globalContext.flux);
                }
                cnt++;
            });

            var totalCommerce = 0;
            $scope.context.arrayNotesCommerce.forEach(function(theNote){
                totalCommerce += theNote;
            });
            var i = 0;
            for ( i = 0; i < $scope.context.arrayNotesCommerce.length; i++){
                if(totalCommerce === 0){
                    totalCommerce = 1;
                }
                $scope.context.arrayNotesCommerce[i] = $scope.context.arrayNotesCommerce[i] / totalCommerce;
            }

            var totalPopulation = 0;
            $scope.context.arrayNotesPopulation.forEach(function(theNote){
                totalPopulation += theNote;
            });
            for ( i = 0; i < $scope.context.arrayNotesPopulation.length; i++){
                if(totalPopulation === 0){
                    totalPopulation = 1;
                }
                $scope.context.arrayNotesPopulation[i] = $scope.context.arrayNotesPopulation[i] / totalPopulation;
            }

            var totalTransport = 0;
            $scope.context.arrayNotesTransport.forEach(function(theNote){
                totalTransport += theNote;
            });
            for ( i = 0; i < $scope.context.arrayNotesTransport.length; i++){
                if(totalTransport === 0){
                    totalTransport = 1;
                }
                $scope.context.arrayNotesTransport[i] = $scope.context.arrayNotesTransport[i] / totalTransport;
            }

            var totalFlux = 0;
            $scope.context.arrayNotesFlux.forEach(function(theNote){
                totalFlux += theNote;
            });
            for ( i = 0; i < $scope.context.arrayNotesFlux.length; i++){
                if(totalFlux === 0){
                    totalFlux = 1;
                }
                $scope.context.arrayNotesFlux[i] = $scope.context.arrayNotesFlux[i] / totalFlux;
            }

            var arrayNotesFinales = [0,0,0];
            var totalFinal = 0;
            for ( i = 0; i < myArray.length; i++) {
                arrayNotesFinales[i] += $scope.context.arrayNotesCommerce[i];
                arrayNotesFinales[i] += $scope.context.arrayNotesPopulation[i];
                arrayNotesFinales[i] += $scope.context.arrayNotesTransport[i];
                arrayNotesFinales[i] += $scope.context.arrayNotesFlux[i];
                totalFinal += arrayNotesFinales[i];
            }
            for ( i = 0; i < myArray.length; i++) {
                if(totalFinal === 0){
                    totalFinal = 1;
                }
                arrayNotesFinales[i] = arrayNotesFinales[i] / totalFinal;
            }
            var compt = 0;
            globalContext.iris.features.forEach(function(iris){
               iris.properties.rank =  arrayNotesFinales[compt];
               compt++;
            });
            getMainLayer();
        };

        $scope.rechClient = function() {
             //alert('longueur : ');
             if ($("#matricule").val().length == 7 ) {
                    $("#raisonSociale").val('SARL LABRIOCHE');
                    $("#adresse").val('12 rue de la farine 75010 PARIS');
                    $("#dateER").val('12/01/2012');
                    $("#telephone").val('0140344573');
                    $("#mail").val('sarllabrioche@gmail.com');
                    $("#noteMcDo").val('1');
                    $("#activite").val('HOTELLERIE/RESTAURATION');
                   
                    $("#dataClient").show();
                 $scope.clientCollapse = '';
                 $scope.clientClass='fa-arrow-circle-o-up';
             } else {
                    $("#dataClient").hide();
                 $scope.clientCollapse = 'collapse';
                 $scope.clientClass='fa-arrow-circle-o-down';
                    }
       };

        $scope.showCommerces = function(){
            var officeMarker = L.ExtraMarkers.icon({
                icon: 'fa-cutlery',//See glyphicons or font-awesome (must include prefix)
                markerColor: '#bd362f',//'red', 'orange-dark', 'orange', 'yellow', 'blue-dark', 'cyan', 'purple', 'violet', 'pink', 'green-dark', 'green', 'green-light', 'black', 'white'
                shape: 'circle',//'circle', 'square', 'star', 'penta'
                prefix: 'fa',//'fa' for font-awesome or 'glyphicon' for bootstrap 3
                iconColor: 'white'//'white', 'black' or css code (hex, rgba etc)
            });
            var officeMarkerBank = L.ExtraMarkers.icon({
                icon: 'fa-cutlery',//See glyphicons or font-awesome (must include prefix)
                markerColor: 'cyan',//'red', 'orange-dark', 'orange', 'yellow', 'blue-dark', 'cyan', 'purple', 'violet', 'pink', 'green-dark', 'green', 'green-light', 'black', 'white'
                shape: 'circle',//'circle', 'square', 'star', 'penta'
                prefix: 'fa',//'fa' for font-awesome or 'glyphicon' for bootstrap 3
                iconColor: 'white'//'white', 'black' or css code (hex, rgba etc)
            });
            if($scope.showCommerce === false){

                LayerCommerces = new L.LayerGroup();
                globalContext.commerces.forEach(function(commerce){
                    var lat = commerce.geoloc.lat;
                    var lon = commerce.geoloc.lon;
                    if(commerce.client === 1){
                        var marker1 = L.marker({lat, lon}, {icon: officeMarkerBank}).addTo(map)
                            .bindPopup(commerce.nom + "</br>Notation Risque : "/* + commerce.note-mcdono*/);
                        LayerCommerces.addLayer(marker1);
                    }else{
                        var marker = L.marker({lat, lon}, {icon: officeMarker}).addTo(map)
                            .bindPopup(commerce.nom);
                        LayerCommerces.addLayer(marker);
                    }
                });
                $scope.showCommerce = true;
                map.addLayer(LayerCommerces);
            }
            else{
                $scope.showCommerce = false;
                map.removeLayer(LayerCommerces);
                LayerCommerces = null;
            }
        };

        $scope.activeFilter =function(filterName){
            if (filterName === 'commerces'){
                $scope.filters.commerces = !$scope.filters.commerces;
            }
            if (filterName === 'transports'){
                $scope.filters.transports = !$scope.filters.transports;
            }
            if (filterName === 'flux'){
                $scope.filters.flux = !$scope.filters.flux;
            }
            if (filterName === 'population'){
                $scope.filters.population = !$scope.filters.population;
            }
            $scope.engine();
        };

        /*****************
        * scope EVENTS, WATCHES
        ******************/

        $q.all($scope.bootPromiseHash).then(function (resultHash) {
            $log.log("bootPromiseHash.then");

            initScope();

        }, function (reason) {
            $log.error("reason");
        });


        /******
         * CAMERA
         */
        var img1 = "http://20.1.0.129:8000/assets/img/test_photo1.jpg";
        var img2 = "http://localhost:59614/build/assets/img/pictures/Auto_Tempo_Decouvert.png";
        $scope.imgSource = img1;

        //window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
        //window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        //function fail() {
        //    console.log("failed to get filesystem");
        //}

        //function gotFS(fileSystem) {
        //    console.log("got filesystem");

        //    // save the file system for later access
        //    console.log(fileSystem.root.fullPath);
        //    window.rootFS = fileSystem.root;
        //}

        var mail = {
            to: 'atlhe00@dom801.ibp',
            from: 'test@dom801.ibp',
            cc: null,
            bcc: null,
            subject: "Envoi d'un test",
            body: 'test',
            filename: 'capture.png',
            attachments: null
        };

        if (navigator.camera) {
            //$scope._pictureSource = navigator.camera.PictureSourceType;
            //$scope._destinationType = navigator.camera.DestinationType;

            $scope.capturePhotoB64 = function () {
                //Camera.PictureSourceType.CAMERA
                navigator.camera.getPicture(onSuccess, onFail, {
                    quality: 50,
                    destinationType: Camera.DestinationType.DATA_URL
                });

                function onSuccess(imageData) {
                    console.log('Success Base64! ');
                    mail.attachments = imageData;
                    var smallImage = document.getElementById('myImage');
                    smallImage.src = "data:image/png;base64," + imageData;

                    var imageURI = '';
                    var PERSISTENT;
                    if (typeof LocalFileSystem === 'undefined') {
                        PERSISTENT = window.PERSISTENT;
                        navigator.webkitPersistentStorage.requestQuota(1024 * 1024 * 280,
                            function (grantedBytes) {
                                window.webkitRequestFileSystem(PERSISTENT, 0, gotFS, fail);
                            }, function (e) {
                                console.log('Error', e);
                            });
                    } else {
                        PERSISTENT = LocalFileSystem.PERSISTENT;
                        window.requestFileSystem(PERSISTENT, 0, gotFS, fail);
                    }
                    function gotFS(fileSystem) {
                        fileSystem.root.getFile("screenshot.png", { create: true, exclusive: false }, gotFileEntry, fail);
                    }

                    function gotFileEntry(fileEntry) {
                        imageURI = fileEntry.toURL();
                        fileEntry.createWriter(gotFileWriter, fail);
                        console.log('URL : ' + imageURI);
                    }

                    function gotFileWriter(writer) {
                        console.log("open and write");
                        writer.seek(0);
                        writer.write(imageData);
                        console.log("close and save");

                        //file mail
                        //$http.post(constants.serviceUrl.mailer2, mail);

                        //File transfert
                        /*var options = new FileUploadOptions();
                        options.fileKey = "file";
                        options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                        options.mimeType = "image/png";

                        var ft = new FileTransfer();*/
                        //ft.upload(imageURI, encodeURI("http://some.server.com/upload.php"), win, fail, options);
                    }

                    function fail(error) {
                        console.log('Fail on file transfert : ' + error.code);
                    }
                }

                function onFail(message) {
                    console.log('Failed because: ' + message);
                }
            };
            //Take a photo and retrieve the image's file location:
            // this is better for memory
            $scope.capturePhotoFileLoc = function () {
                $log.log("enter TestCameraCtrl.capturePhotoFileLoc");
                try {
                    navigator.camera.getPicture(onSuccess, onFail, {
                        sourceType: Camera.PictureSourceType.CAMERA,
                        quality: 50, //Quality of the saved image, expressed as a range of 0-100
                        destinationType: Camera.DestinationType.FILE_URI, // for iOS this goes to application temp folder and is deleted when the app closes !!
                        encodingType: Camera.EncodingType.PNG // defaults to JPEG
                    });
                }
                catch (err) {
                    $log.err("error on getPicture:", err);
                }

            };
        }
        else {
            $log.log("the camera feature is not available, it only works on devices.");
            //alert('not available');

            $scope.capturePhotoFileLoc = function () {
                $log.warn("The method TestCameraCtrl.capturePhotoFileLoc is part of the camera feature, which is not available, it only works on devices.");
                //alert('cam1');
            };
            $scope.capturePhotoB64 = function () {
                $log.warn("The method TestCameraCtrl.capturePhotoB64 is part of the camera feature, which is not available, it only works on devices.");
                //alert('cam2');
            };

        }

        //var sendMail = function (str) {
        //    return $http.get(constants.serviceUrl.mailer2 + str, null);
        //};

        function onSuccess(imageURI) {
            $log.log('scope.capturePhotoFileLoc: onSuccess');
            $log.log('got an image : ' + imageURI);

            $scope.$apply(function () {
                $scope.imgSource = imageURI;
                $scope.showPhoto = true;
            });
        }

        //function movePic(file) {
        //    window.resolveLocalFileSystemURL(file, resolveOnSuccess, resOnError);
        //}

        ////Callback function when the file system uri has been resolved
        //function resolveOnSuccess(entry) {
        //    var d = new Date();
        //    var n = d.getTime();
        //    //new file name
        //    var newFileName = n + ".PNG";
        //    var myFolderApp = "Camera";
        //    console.log('fullPath : ' +entry.fullPath);
        //    console.log('URL : ' + entry.toURL());

        //    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
        //        //The folder is created if doesn't exist
        //        fileSys.root.getDirectory(myFolderApp,
        //                        { create: true, exclusive: false },
        //                        function (directory) {
        //                            entry.moveTo(window.rootFS, newFileName, successMove, resOnError);
        //                        },
        //                        resOnError);

        //    },
        //    resOnError);
        //}
        ////Callback function when the file has been moved successfully - inserting the complete path
        //function successMove(entry) {
        //    //Store imagepath in session for future use
        //    // like to store it in database
        //    //alert('success moved : ' + entry.fullPath);
        //    console.log('success moved : ' + entry.fullPath);
        //    $timeout(function () {
        //        $scope.$apply(function () {
        //            //$scope.imgSource = 'http://20.1.0.129:8085' + entry.fullPath;
        //            $("#myImage").show();
        //            $("#myImage").attr("src", currentFilePath);

        //        });
        //    }, 100);
        //}

        //function resOnError(error) {
        //    //alert('error moved : ' + error.code);
        //    console.log('error moved : ' + error.code);
        //}

        function onFail(message) {
            $timeout(function () {
                //alert(message + "onSuccess called");
                console.log(message + "onSuccess called");
            }, 0);

            $log.error('Failed because: ' + message);
        }

        //File transfert result
        function win(r) {
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
        }

        function fail(error) {
            alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
        }

        $scope.deletePhoto = function(){
          $scope.showPhoto = false;
        };

    });

}());
