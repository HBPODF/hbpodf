# JSTemplateApp

This is loosly based on

[Josh Miller ngBoilerplate](http://joshdmiller.github.com/ng-boilerplate)  
[Morris fork ngBuilerplate](https://github.com/MorrisLLC/ng-boilerplate)

To use this template, follow the procedure in doc/prerequisite.md and doc/installation.md.


## Theme and plugins

theme and plugins are based on: [SB Admin 2](http://startbootstrap.com/template-overviews/sb-admin-2/)

We did not include all plugin of the theme
In order to get other plugins do a git clone:  
   
```git clone https://github.com/IronSummitMedia/startbootstrap-sb-admin-2.git```



